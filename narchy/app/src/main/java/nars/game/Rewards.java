package nars.game;

import jcog.Is;
import jcog.data.list.FastCoWList;
import nars.NAL;
import nars.Term;
import nars.focus.PriAmp;
import nars.game.reward.Reward;

import java.util.DoubleSummaryStatistics;
import java.util.Iterator;
import java.util.function.ToDoubleFunction;
import java.util.stream.Stream;

@Is({"Reward_system", "Bellman_equation","Felicific_calculus"})
public class Rewards implements Iterable<Reward> {


    public final FastCoWList<Reward> rewards = new FastCoWList<>(Reward[]::new);

    public PriAmp pri;

    public final DoubleSummaryStatistics rewardStat = new DoubleSummaryStatistics();

    /** whether happiness is computed linearly (mean) or geometric (root-mean-square) */
    private boolean happinessGeometric = NAL.HAPPINESS_GEOMETRIC;

    /**
     * avg reward satisfaction, current measurement
     * happiness metric scalar aggregation of all reward components
     */
    @Is({"Multi-objective_optimization","Pareto_efficiency"})
    public final double happiness(long start, long end, float dur) {
        ToDoubleFunction<Reward> component = r -> r.happy(start, end, dur);
        if (happinessGeometric)
            return rewards.rmsBy(component);
        else
            return rewards.meanBy(component);
//        return rewards.
//            meanBy
//            //rmsBy
//            //meanWeightedBy
//            //rmsWeightedBy
//                (component/*, Reward::strength*/);
    }

    public void add(Reward r) {
        Term rt = r.term();
        var rte = rt.equals();
        if (rewards.OR(z -> rte.test(z.term())))
            throw new RuntimeException("reward exists with the ID: " + rt);

        rewards.add(r);
    }

    public final int size() {
        return rewards.size();
    }

    @Override
    public final Iterator<Reward> iterator() {
        return rewards.iterator();
    }

    public final void update(Game game) {
        rewards.forEach(game::update);
        rewardStat.accept(rewards.meanBy(Reward::rewardOrZero));
    }

    public final Stream<Reward> stream() {
        return rewards.stream();
    }

    public final double rewardMean() {
        return rewardStat.getAverage();
    }

    /** relative time offset, in game durs, where reward beliefs occurr.
     *  retroactive, at the same time as the actions which produced it */
    public static final int REWARD_OFFSET_DURS = -1;

}