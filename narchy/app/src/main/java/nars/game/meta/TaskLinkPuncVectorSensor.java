package nars.game.meta;

import jcog.Util;
import jcog.data.iterator.ArrayIterator;
import jcog.pri.bag.Bag;
import nars.$;
import nars.Op;
import nars.focus.BagFocus;
import nars.game.Game;
import nars.game.sensor.SubSignalComponent;
import nars.game.sensor.VectorSensor;
import nars.link.TaskLink;
import nars.premise.Premise;
import nars.task.NALTask;

import java.util.Arrays;
import java.util.Iterator;

import static java.lang.Float.NaN;
import static nars.Op.INH;

class TaskLinkPuncVectorSensor extends VectorSensor {

    private final BagFocus w;
    final double[] punc;
    final float[] buf;
    final float[] pct;

    public double mean;

    final SubSignalComponent[] c;

    TaskLinkPuncVectorSensor(BagFocus w) {
        super(INH.the(w.id, MetaGame.focus));
        this.w = w;
        punc = new double[4];
        buf = new float[4];
        pct = new float[4];
        c = new SubSignalComponent[4];
        for (int i = 0; i < 4; i++) {
            int I = i;
            c[i] = component(
                Op.punc($.p(w.id, MetaGame.pri), NALTask.p(i)),
                () -> pct[I], w.nar);
        }
    }

    @Override
    public void accept(Game g) {
        Arrays.fill(punc, 0);
        Bag<Premise, Premise> b = w.bag;
        int n = b.size(); //estimate
        b.forEach(x -> {
            ((TaskLink) x).priGet(buf);
            for (int i = 0; i < 4; i++)
                punc[i] += buf[i];
        });
        double sum;
        if (n > 0 && (sum = Util.sum(punc)) > Float.MIN_NORMAL) {
            //normalize
            for (int i = 0; i < 4; i++)
                pct[i] = (float) (punc[i] / sum);
            mean = sum / n;
        } else {
            Arrays.fill(pct, NaN);
            mean = NaN;
        }

        super.accept(g);
    }

    @Override
    public Iterator<SubSignalComponent> iterator() {
        return ArrayIterator.iterate(c);
    }

    @Override
    public int size() {
        return c.length;
    }
}