package nars.game.action;

import nars.Term;
import nars.game.Game;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;


public class GoalAction extends AbstractGoalAction {

    private final MotorFunction motor;
    private float motorFeedback = Float.NaN;

    public GoalAction(Term term, MotorFunction motor) {
        super(term);

        this.motor = motor;
    }

    @Override
    protected void update(@Nullable Truth belief, @Nullable Truth goalTruth, Game g) {
        motorFeedback = motor.apply(belief, goalTruth);
    }

    @Override
    protected Truth feedback(Game g) {
        return g.truther()/*concept.truther(game)*/.truth(feedback(motorFeedback), resolution());
    }

    protected float feedback(float m) {
        return m;
    }
}