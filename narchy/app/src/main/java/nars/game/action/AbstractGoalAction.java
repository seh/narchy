package nars.game.action;

import jcog.random.RandomBits;
import jcog.signal.FloatRange;
import jcog.signal.anomaly.ewma.Ewma;
import nars.$;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.game.Game;
import nars.game.sensor.ScalarSensor;
import nars.game.sensor.ScalarSignalConcept;
import nars.table.BeliefTables;
import nars.table.dynamic.SensorBeliefTables;
import nars.table.eternal.EternalDefaultTable;
import nars.table.temporal.NavigableMapBeliefTable;
import nars.truth.MutableTruth;
import nars.truth.Truth;
import nars.truth.proj.MutableTruthProjection;
import nars.truth.proj.TruthProjection;
import nars.truth.util.Revision;
import org.jetbrains.annotations.Nullable;

import static nars.Op.GOAL;
import static nars.truth.func.TruthFunctions.e2c;


/**
 * ActionConcept which is driven by Goals that are interpreted into feedback Beliefs
 * <p>
 * Goal/Belief Feedback Loop:
 * <patham9_> when a goal task is processed, the following happens: In order to decide on whether it is relevant for the current situation, at first it is projected to the current time, then it is revised with previous "desires", then it is checked to what extent this projected revised desire is already fullfilled (which revises its budget) , if its below satisfaction threshold then it is pursued, if its an operation it is additionally checked if
 * <patham9_> executed
 * <patham9_> the consequences of this, to give examples, are a lot:
 * <patham9_> 1 the system wont execute something if it has a strong objection against it. (example: it wont jump down again 5 meters if it previously observed that this damages it, no matter if it thinks about that situation again or not)
 * <patham9_> 2. the system wont lose time with thoughts about already satisfied goals (due to budget shrinking proportional to satisfaction)
 * <patham9_> 3. the system wont execute and pursue what is already satisfied
 * <patham9_> 4. the system wont try to execute and pursue things in the current moment which are "sheduled" to be in the future.
 * <patham9_> 5. the system wont pursue a goal it already pursued for the same reason (due to revision, it is related to 1)
 * <p>
 *  TODO goal truth bias parameter
 */
public abstract class AbstractGoalAction extends ScalarSensor implements AbstractAction {

    /** >=0; pickup duration. smaller is more temporally exact (less bleed-through influence from past or future) */
    private static final float TRUTH_DURS =
        //1;
        1/2f;
        //1/3f;
        //1/10f;

    /**
     * current belief state
     */
    private final MutableTruth beliefTruth = new MutableTruth();

    /**
     * current action state, including curiosity
     */
    final MutableTruth actionTruth = new MutableTruth();

    /**
     * current action state excluding curiosity
     */
    public final MutableTruth actionTruthOrganic = new MutableTruth();

    /**
     * determines the temporal horizon of the action evaluator
     * which determines the effective goal action, in game frame durs.
     * <p>
     * inversely proportional to 'impulsiveness'.  higher values
     * decrease the maximum effective control frequency
     * (which is determined by the game's frame duration).
     * <p>
     * =0: includes only the present, within the game's present frame time
     * >0: includes projected truth from present, but also some amount of past and future via projection
     * <p>
     * values >=0 are valid
     */
    public final FloatRange truthDurs = new FloatRange(0.1f, 0, 2);


    private static final boolean DEX_SMOOTH = true;

    @Deprecated private final Ewma dexMean = new Ewma().period(2).with(0);
    public boolean trace = false;

    /** number of 'extrema' in the 'numeric semantics' of the signal */
    public int poles = 2;

    /**
     * set if tracing is enabled
     */
    @Nullable
    private transient TruthProjection actionReason;
    private double actionPri = Double.NaN;


    public AbstractGoalAction(Term term) {
        super(term);
    }

//    private static Truth truth(Lst<NALTask> t, float dur, Game g) {
//        if (t.isEmpty())
//            return null;
//
//        Moment a = g.actions.goalMatch;
//        IntegralTruthProjection p = new IntegralTruthProjection(a.start(), a.end(), t.size());
//        p.dur(dur);
//        p.addAll((Collection<? extends NALTask>) t);
//        return truth(p);
//    }

    private static @Nullable Truth truth(@Nullable TruthProjection t) {
        return t != null ? t.truth() : null;
    }

    @Override
    public void accept(Game g) {

    }

    @Override
    public final void actAfter(Game g) {
        float dur = g.time.dur;
        float truthDur = truthDurs.floatValue() * dur;
        update(
                computeBeliefTruth() ?
                        beliefTruth(g, truthDur, truthDur).ifIs() : null,
                goalTruth(g, Math.max(1, truthDur * TRUTH_DURS)
                        //truthDur
                        //1
                        //0, 1
                        //0, 1, truthDur/2
                        //0, truthDur
                        //0, truthDur/2
                ).ifIs(), g);

        dexMean.accept(_dexterity());
        feedback(feedback(g), g);
    }

    protected abstract Truth feedback(Game g);
//    public EternalDefaultTable beliefDefault(Truth t, NAR n) {
//        return EternalDefaultTable.add(this, t, BELIEF, n);
//    }

    @Override
    protected ScalarSignalConcept conceptNew(Game g) {
        NAR n = g.nar;
        var c = new ScalarSignalConcept(id,
                new SensorBeliefTables(id, true,
                        n.conceptBuilder.temporalTable(id, true)),
                new BeliefTables(), n);

        ((BeliefTables) c.goals()).add(

                //n.conceptBuilder.temporalTable(term, false)

                new NavigableMapBeliefTable()
            /*{
                @Override
                public void remember(Remember r) {
                    if (capacity() <= (1+taskCount()) && !map.containsKey(r.input))
                        Util.nop(); //full
                    super.remember(r);
                    if (r.stored==null) {
                        System.err.println("rejected: " + r.input);
                        System.err.println("\t" + this.summary());
                    }
                }

                @Override
                protected int merge(MutableTruthProjection t, @Nullable NALTask victim, boolean removeVictim) {
                    return super.merge(t, victim, removeVictim);
                }
            }
             */
        );

        return c;
    }

    protected abstract void update(@Nullable Truth beliefTruth, @Nullable Truth goalTruth, Game g);

    public EternalDefaultTable goalDefault(Truth t, NAR n) {
        return EternalDefaultTable.add(concept, t, GOAL, n);
    }

    @Nullable
    public final TruthProjection reason() {
        assert (trace) : "trace must be enabled";
        return actionReason;
    }

    @Override
    public final double dexterity() {
        return DEX_SMOOTH ? dexMean.mean() : _dexterity();
    }

    private double _dexterity() {
        return actionTruthOrganic.conf();
    }

    @Override
    public double coherency(long start, long end, float dur, NAR nar) {
        return concept.goals().coherency(start, end, dur, nar);
    }

    /**
     * whether to supply a belief truth measurement during update
     */
    protected boolean computeBeliefTruth() {
        return false;
    }

    public void feedback(@Nullable Truth nextFeedback, Game g) {
        concept.inputCommit(nextFeedback, sensing, g.time);

        //HACK additional goal link
        //g.focus().link(concept.term(), GOAL, concept.next.pri());
    }

    @Nullable
    private MutableTruth beliefTruth(Game g, float matchDur, float truthDur) {
        return beliefTruth.set(truth(g.actions.truth(concept.term, concept.beliefs(), matchDur, truthDur, null)));
    }


    private MutableTruth goalTruth(Game g, float... truthDurs) {
        assert(truthDurs.length > 0);

        Truth t = null;
        for (int i = 0, n = truthDurs.length; t == null && i < n; i++) {
            float truthDur = truthDurs[i];
            t = goalTruth(
                    g.actions.truth(concept.term, concept.goals(),
                            truthDur, truthDur, null)
            );
        }

        actionTruthOrganic.set(t);

        return actionTruth.set(truth(g, t));
    }

    private Truth truth(Game g, Truth organicTruth) {
        Truth curiosityTruth = curiosity(g);

        Truth truth = g.actions.curiosityReviseOrReplace ?
                Revision.revise(curiosityTruth, organicTruth) :
                (curiosityTruth == null ? organicTruth : curiosityTruth);

        //        if (ditherMotorInput && truth!=null)
        //            truth = truth.dither(resolution(), 0); //dither action truth

        return truth;
    }

    public static final double CURI_EVI_THRESHOLD = NAL.truth.EVI_MIN * 4;

    /** current curiosity goal */
    @Nullable private transient Truth curi = null;

    @Nullable private Truth curiosity(Game g) {
        RandomBits rng = g.rng();
        if (curi == null) {
            float curiosityProb = 1 / g.curiosityDurs.floatValue();
            if (rng.nextBoolean(curiosityProb)) {
                curi = $.t(rng.nextFloat(), e2c(CURI_EVI_THRESHOLD));
            }
        } else {
            float curiosityOffProb = 1/g.curiosityOffDurs.floatValue();
            if (rng.nextBoolean(curiosityOffProb))
                curi = null;
        }
        return curi;
    }

    @Nullable
    private Truth goalTruth(@Nullable TruthProjection x) {
        Truth organicTruth;

        if (x != null) {
            if (x instanceof MutableTruthProjection P)
                P.overlapIgnore = NAL.truth.ACTION_GOAL_OVERLAP_ALLOW;

            organicTruth = x.truth();
            actionPri = x.priWeighted();
        } else {
            organicTruth = null;
            actionPri = Double.NaN;
        }

        if (trace) {
            AbstractGoalAction.this.actionReason = x;
        } else {
            if (x != null) x.delete();
            AbstractGoalAction.this.actionReason = null;
        }
        return organicTruth;
    }

    /**
     * action discretization semantics
     *   poles=1: unipolar
     *   poles=2: bipolar
     *
     * default: bipolar (freq=0 and freq=1)
     */
    public int poles() {
        return poles;
    }

//    private Curiosity.CuriosityTable curiosityTable() {
//        return ((BeliefTables) concept.goals()).tableFirst(Curiosity.CuriosityTable.class);
//    }


    /**
     * determines the feedback belief when desire or belief has changed in a MotorConcept
     * implementations may be used to trigger procedures based on these changes.
     * normally the result of the feedback will be equal to the input desired value
     * although this may be reduced to indicate that the motion has hit a limit or
     * experienced resistence
     *
     * @param desired  current desire - null if no desire Truth can be determined
     * @param believed current belief - null if no belief Truth can be determined
     * @return truth of a new feedback belief, or null to disable the creation of any feedback this iteration
     */
    @FunctionalInterface
    public interface MotorFunction {

        float apply(@Nullable Truth believed, @Nullable Truth desired);

    }

}