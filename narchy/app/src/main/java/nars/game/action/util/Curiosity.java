//package nars.game.action.util;
//
//import jcog.Skill;
//import jcog.Util;
//import jcog.event.Off;
//import jcog.random.RandomBits;
//import jcog.signal.FloatRange;
//import jcog.thing.SubPart;
//import nars.NAL;
//import nars.NAR;
//import nars.Op;
//import nars.Term;
//import nars.concept.TaskConcept;
//import nars.focus.Focus;
//import nars.game.Game;
//import nars.game.action.AbstractAction;
//import nars.game.action.AbstractGoalAction;
//import nars.game.sensor.AbstractSensor;
//import nars.game.sensor.SignalConcept;
//import nars.table.BeliefTables;
//import nars.table.dynamic.MutableTasksBeliefTable;
//import nars.task.NALTask;
//import nars.task.SerialTask;
//import nars.task.util.series.NavigableMapTaskSeries;
//import nars.term.Termed;
//import nars.time.Tense;
//import nars.truth.MutableTruth;
//
//import java.util.Arrays;
//import java.util.Random;
//import java.util.concurrent.atomic.AtomicBoolean;
//import java.util.function.Predicate;
//
//import static java.lang.Math.max;
//import static jcog.Util.*;
//import static nars.Op.ETERNAL;
//import static nars.Op.GOAL;
//
///**
// * the control of the future time (shift) and duty cycle (range) of new CuriosityTasks
// * has the ability to characterize temporal flexibility with regard to motor control.
// * <p>
// * if the motor has an ideal operational frequency then this may discover it.
// */
//@Skill({"Curiosity", "Central_pattern_generator", "Phantom_limb", "Motor_babbling"})
//public class Curiosity implements SubPart<Game> {
//
//    public static final boolean REVISE_OR_REPLACE = false;
//    public static final int curiCapacityEach =
//            1;
//    public static final double EVI_THRESHOLD = NAL.truth.EVI_MIN * 4;
//    public static final Predicate<NALTask> notSerialFilter = z -> !(z instanceof SerialTask)/*CuriosityTask*/;
//    //2;
//    //3;
//    private static final boolean ditherFreq = true;
//    public final AtomicBoolean enable = new AtomicBoolean(true);
//    /**
//     * on duty cycle durs;  staccato, legato
//     * in curiosity periods
//     */
//    public final FloatRange onPeriod = new FloatRange(0.25f, 0, 1);
//    /**
//     * proportional to on-cycle
//     */
//    public final FloatRange shiftRanges = new FloatRange(0, 0, 256);
//
//    /**
//     * priority multiplier for curiosity tasks
//     */
//    public final FloatRange pri = new FloatRange(1, 0, 1);
//    private Off on;
//
//    @Override
//    public void startIn(Game game) {
//        on = game.onFrame(this::accept);
//    }
//
//    @Override
//    public void stopIn(Game game) {
//        on.close();
//        on = null;
//    }
//
//    private CuriosityTable curiosityTable(SignalConcept a, Term why, NAR nar) {
//        BeliefTables t = (BeliefTables)a.goals();
//        CuriosityTable e = t.tableFirst(CuriosityTable.class);
//        if (e == null)
//            t.add(e = new CuriosityTable(a, why, nar.evidence()));
//        return e;
//    }
//
//    @Deprecated
//    public void accept(Game g) {
//
//        if (!enable.getOpaque()) return;
//
//        var actions = g.actions;
//        Focus f = g.focus();
////        long now = g.time();
//        NAR nar = f.nar;
//        float dur = max(1, g.dur());
//
//
//
//        long nowEnd = g.time.e;
//
//        double eviMin = max(EVI_THRESHOLD, nar.confMin.evi());
//        double eviMax = nar.goalConfDefault.evi();
//        double evi = clampSafe(0, eviMin, eviMax);
//
//        RandomBits rng = new RandomBits(f.random());
//
//
//        float shiftRanges = this.shiftRanges.floatValue();
//        float curiCycles = dur * g.curiosityDurs.floatValue();
//        float onCycles = curiCycles * onPeriod.floatValue();
//
//        float priBase = this.pri.floatValue();
//
//        for (AbstractAction A : actions) {
//            for (Termed aa : A.components()) {
//
//                if (!(aa instanceof TaskConcept a))
//                    continue;
//
//                CuriosityTable ct = curiosityTable((SignalConcept) a,
//                    ((AbstractSensor)A).sensing.why(), nar);
//                ct.update();
//
//                ActionCuriosity c = ct.model;
//                if (c != null) {
//                    if (!c.update(nowEnd)) {
//                        ct.delete();
//                        ct.model = null;
//                        c = null;
//                    }
//                }
//
//                if (c != null || ct.onNext <= nowEnd) {
//
//                    if (c == null) {
//
//                        float rangeCycles = Math.max(dur, onCycles * rng.nextFloat());
//                        float shiftCycles = rangeCycles * shiftRanges;
//
//                        long _start = nowEnd + Math.round(shiftCycles);
//                        long _end = Math.round(_start + rangeCycles);
//
//                        (ct.model = c = curiosity(rng))
//                            .start(_start, _end, evi, ct, A.resolution(), nar);
//
//                        double delay = rng.rng.nextGaussian() * curiCycles / 2 + curiCycles;
//                        ct.onNext = Math.max(
//                                nowEnd + Math.max(1, Math.round(delay)),
//                                _end + 1);
//                    }
//
//                    float pri = priBase * ((AbstractGoalAction)A).pri.pri();
//                    c.update(evi, pri, f);
//                }
//            }
//        }
//
//    }
//
//    private ActionCuriosity curiosity(RandomBits rng) {
////        int steps = rng.nextInt(curiCapacityEach) + 1;
//        return
//                //switch (rng.nextInt(2)) {
//                //case 0 ->
//                new PointCuriosity(rng.nextFloat());
//        //new LERPCuriosity(rng.nextFloat(), rng.nextFloat());
////            case 1 -> new SineCuriosity(rng);
////            default -> throw new UnsupportedOperationException();
////        };
//    }
//
//    public static class CuriosityTable extends MutableTasksBeliefTable {
//
//        /**
//         * also compares by stamp to detect proxies, projections, etc.
//         */
//        public final Predicate<NALTask> filter =
//                notSerialFilter.and(z -> !Arrays.equals(z.stamp(), sharedStamp));
//        private final SignalConcept a;
//        private final Term why;
//        @Deprecated
//        public ActionCuriosity model;
//        long onNext = ETERNAL;
//
//        CuriosityTable(SignalConcept a, Term why, long[] stamp) {
//            super(a.term(), false,
////                Curiosity.curiCapacityEach == 1 ?
////                    new RingTaskSeries<>(1) :
//                    new NavigableMapTaskSeries<>(Curiosity.curiCapacityEach)
//            );
//            this.a = a;
//            this.why = why;
//            sharedStamp = stamp;
//        }
//
//        @Override
//        public SerialTask task(long start, long end, float f, double evi, long[] stampIgnored) {
//            SerialTask T = new SerialTask(
//                a.term(), beliefOrGoal ? Op.BELIEF : GOAL,
//                new MutableTruth(f, evi),
//                start, end,
//                sharedStamp
//            );
//            T.why(why);
//            return T;
//        }
//
//
//        public void update() {
//
//
//        }
//    }
//
//    abstract static class ActionCuriosity {
//
//        protected double evi;
//        long endAt = Long.MAX_VALUE;
//        private transient float freqRes;
//        private CuriosityTable table;
//
////        long now = TIMELESS;
////        protected boolean ended(NALTask x) {
////            return x.end() < now;
////            //if (x.mid() < now) {
////            //if (x.start() < now) {
////        }
//
//        boolean update(long now) {
//            return this.endAt > now;
//            //this.now = now;
//
////            table.removeIf(x -> {
////                if (!ended(x)) {
////                    return false;
////                } else {
////                    return true;
////                }
////            }, true);
////            return !table.isEmpty();
//        }
//
//        public void update(double evi, float p, Focus f) {
//            table.forEachTask(x -> {
//                ((MutableTruth) x.truth()).evi(evi);
//                x.pri(p);
//                if (x.uncreated())
//                    f.link((SerialTask) x, null);
//            });
//        }
//
//        public void start(long s, long e, double evi, CuriosityTable table, float res, @Deprecated /* TODO use Truther */ NAR nar) {
//            this.table = table;
//            this.evi = evi;
//            this.freqRes = Math.max(res, nar.freqResolution.floatValue());
//            this.endAt = e;
//
//            int dtDither = nar.dtDither();
//            if (dtDither > 1) {
//                s = Tense.dither(s, dtDither);
//                e = Tense.dither(e, dtDither);
//            }
//
//            start(s, e);
//        }
//
//        protected SerialTask add(float f, long s, long e) {
//            return table.setOrAdd(s, e,
//                    //TODO table.a.truther(game)
//                    unitizeSafe(ditherFreq ? Util.round(f, freqRes) : f),
//                    evi, 0);
//        }
//
//        protected abstract void start(long s, long e);
//
//        public void delete() {
//            table.clear();
//            table = null;
//        }
//
//    }
//
//    abstract static class FunctionCuriosityModel extends ActionCuriosity {
//
////        final int steps;
////        FunctionCuriosityModel(int steps) {
////            this.steps = steps;
////        }
//
//        @Override
//        protected void start(long s, long e) {
//            add((f(0) + f(1)) / 2 /* HACK */, s, e);
//
////            long a = s;
////            long w = (long) ((e - s) / ((double) steps));
////
////            if (steps == 1) {
////                add((f(0) + f(1)) / 2, s, e);
////            } else {
////                //this.evi /= steps;
////                this.pri /= steps;
////                for (int i = 0; i < steps; i++) {
////                    float x = i / (steps - 1f);
////                    float y = f(x);
////                    add(y, a, a + w);
////                    a += w + 1;
////                }
////            }
//        }
//
//        public abstract float f(float x);
//    }
//
//    private static final class PointCuriosity extends FunctionCuriosityModel {
//        public float freq;
//
//        PointCuriosity(float freq) {
//            this.freq = freq;
//        }
//
//        @Override
//        public float f(float x) {
//            return freq;
//        }
//
//    }
//
//    static class LERPCuriosity extends FunctionCuriosityModel {
//        public float from, to;
//
//        LERPCuriosity(float from, float to) {
//            this.from = from;
//            this.to = to;
//        }
//
//        @Override
//        public float f(float x) {
//            return lerpSafe(x, from, to);
//        }
//
//    }
//
//    static class SineCuriosity extends FunctionCuriosityModel {
//
//
//        private final float freq, amp;
//
//        SineCuriosity(Random rng) {
////            super(steps);
//            float bandwidth = 8;
//            freq = (float) (rng.nextFloat() * bandwidth * Math.PI / 2);
//            amp = rng.nextFloat();
//        }
//
//        @Override
//        public float f(float x) {
//            return (float) ((amp * (Math.sin((x) * freq)) / 2 + 0.5));
//        }
//    }
//}