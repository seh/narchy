package nars.game.action;

import jcog.Util;
import jcog.data.iterator.ArrayIterator;
import nars.Term;
import nars.game.Game;
import nars.game.sensor.AbstractSensor;
import nars.term.Termed;
import nars.truth.MutableTruth;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import static nars.Op.SETe;

/** a vector-determined action */
public abstract class CompoundAction extends AbstractSensor {

    public final CompoundActionComponent[] concepts;

    private float momentum = 0;

    private CompoundAction(Term id, CompoundActionComponent[] concepts) {
        super(id);
        this.concepts = concepts;
    }

    public CompoundAction(Term... ids) {
        this(SETe.the(ids),
            Util.map(z -> new CompoundActionComponent(z),
                    new CompoundActionComponent[ids.length], ids)
        );
    }

    @Override
    public final Iterable<? extends Termed> components() {
        return ArrayIterator.iterable(concepts);
    }

    @Override public void start(Game g) {
        super.start(g);

        sensing.why(g.nar.control.newCause(term()).ID);

        for (CompoundActionComponent a : concepts) {
            a.sensing = sensing;
            g.actions.addAction(a);
        }

        g.afterFrame(this);
    }

    /** TODO in terms of low-pass filter frequency (game durs) */
    public CompoundAction momentum(float m) {
        this.momentum = m;
        return this;
    }

    /** to be called at end of 'accept' impl */
    public void set(Truth... t) {
        if (t.length!=concepts.length)
            throw new ArrayIndexOutOfBoundsException();

        float res = resolution();
        for (int i = 0; i < t.length; i++) {
            MutableTruth c = concepts[i].f;
            c.setLerp(t[i], momentum);
            c.freq(Util.round(c.freq(), res));
        }
    }

    public static final class CompoundActionComponent extends AbstractGoalAction {

        /** feedback */
        private final MutableTruth f = new MutableTruth().clear();

        CompoundActionComponent(Term pos) {
            super(pos);
            //poles=1;
        }

        @Override
        protected Truth feedback(Game g) {
//            return g.truther()/*concept.truther(game)*/.truth(f.freq(), resolution());
            return f;
        }


        @Override
        protected void update(@Nullable Truth beliefTruth, @Nullable Truth goalTruth, Game g) {

        }

    }
}