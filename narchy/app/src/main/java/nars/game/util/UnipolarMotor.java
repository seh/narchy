package nars.game.util;

import nars.game.action.AbstractGoalAction;
import nars.truth.Truth;
import org.eclipse.collections.api.block.function.primitive.FloatToFloatFunction;
import org.jetbrains.annotations.Nullable;

public class UnipolarMotor implements AbstractGoalAction.MotorFunction {

    private final boolean freqOrExp;
    private final FloatToFloatFunction ifGoalMissing;
    private final FloatToFloatFunction update;
    float goalPrev;

    public UnipolarMotor(boolean freqOrExp, FloatToFloatFunction ifGoalMissing, FloatToFloatFunction update) {
        this.freqOrExp = freqOrExp;
        this.ifGoalMissing = ifGoalMissing;
        this.update = update;
        this.goalPrev =
            0.5f;
            //Float.NaN;
    }

    @Override
    public float apply(@Nullable Truth b, @Nullable Truth g) {
        float goal = (g != null) ?
                (float) (freqOrExp ? g.freq() : g.expectation()) : ifGoalMissing.valueOf(goalPrev);

        goalPrev = goal;

        return update.valueOf(goal);
    }
}