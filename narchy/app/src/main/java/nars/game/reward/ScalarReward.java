package nars.game.reward;

import jcog.Util;
import jcog.signal.FloatRange;
import nars.$;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.action.memory.Remember;
import nars.focus.Focus;
import nars.game.Game;
import nars.game.Rewards;
import nars.game.sensor.ScalarSensor;
import nars.table.BeliefTable;
import nars.table.BeliefTables;
import nars.table.dynamic.MutableTasksBeliefTable;
import nars.table.eternal.EternalDefaultTable;
import nars.task.EternalTask;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.term.Termed;
import nars.time.Tense;
import nars.time.When;
import nars.truth.MutableTruth;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

import static java.lang.Float.NaN;
import static nars.Op.*;

/**
 * base class for reward which represents current belief truth as the reward value
 */
public abstract class ScalarReward extends Reward {

    /**
     * each has unique stamp
     */
    private static final int goalCapacity = 1;

    private static final boolean GOAL_OCC_FOCUS_RADIUS = true;

    /**
     * ideal (goal) reward value
     */
    public final FloatRange freq = new FloatRange(1, 0, 1);
    /**
     * adjustable goal truth
     */
    private final MutableTruth goalTruth = new MutableTruth(1, 0);
    /**
     * factor to multiple pri() for question inputs
     * avoid distracting from the goal concept
     * 0 to disable
     *
     * TODO inversely proportional to dexterity
     * TODO move this to separate class that manages questions for all game's rewards
     */
    @Deprecated private final FloatRange questionProb =
            new FloatRange(0.02f, 0, 1);

    public boolean eternal;

//    /** goal durs range.  horizon/focus of goal attention */
//    public final FloatRange range = new FloatRange(1, 0, 6);
    /**
     * feedback beliefs
     */
    public ScalarSensor sensor;

    /**
     * actual current reward value
     */
    public volatile float reward = NaN;

    @Deprecated
    private float _resolution = -1;

    @Nullable
    private SerialTask how;

    private RewardGoalTable out;

    /**
     * for 'sparse' rewards, where a particular value is expected
     * to occurr significantly more frequently than any other value.
     *
     * a value set at construction time that indicates the most 'usual'
     * or 'default' reward value to be expected, so that when it happens, the
     * ordinary behavior of creating temporal signal tasks is elided.
     *
     * at the same time, an eternal 'background' truth, with the usual value
     * will be present so that it resolves the expected default value accurately.
     *
     * this helps to reduce the number of tasks created but also helps to emphasize
     * the important times when the signal is 'unusual'.
     *
     * disabled by default (NaN).
     */
    private float freqUsually = NaN;

    protected ScalarReward(Term id) {
        super(id);
    }

    @Override
    public final double reward() {
        return reward;
    }

    /**
     * sets the ideal (goal) freq
     */
    public final ScalarReward freq(float g) {
        freq.set(g);
        return this;
    }

    public final ScalarReward amp(float a) {
        sensor.sensing.amp(a);
        return this;
    }

    @Override
    public final void accept(Game g) {

        sensor.accept(g);

        float pri = sensor.pri.pri();
        long[] goalOcc = goalOcc(g);
        Focus f = g.focus();
        f.link(rewardGoalTable().setOrAdd(goalOcc,
                goalTruth.freq(freq.floatValue()).evi(goalEvi(g)),
                pri
        ), sensor.concept);

        if (f.random().nextFloat() < questionProb.floatValue()) {
            f.rememberNow(how
                    .occ(goalOcc[0], goalOcc[1])
                    .withPri(pri /* * qr */ /*strength() *.. */)
            );
        }
    }

//    /** temporal range for the reward goal */
//    private float goalRange(Game g) {
//        return
//            //g.focus().dur() //focus (dynamic)
//            g.dur() //game loop (fixed)
//        ;
//    }

    private double goalEvi(Game g) {
        return strength() * g.nar.goalConfDefault.evi();
    }

    private long[] goalOcc(Game g) {
        if (eternal)
            return new long[]{ETERNAL, ETERNAL};
        else {
            long[] w =
                GOAL_OCC_FOCUS_RADIUS ?
                    goalOccFocusRadius(g) :
                    goalOccFocus(g);
            return Tense.dither(w, g.nar.dtDither());
        }
    }

    private long[] goalOccFocus(Game g) {
        return g.focus().when();
    }

    /** stretches goalOccFocus to include the present */
    private long[] goalOccFocusRadius(Game g) {
        long[] o = goalOccFocus(g);
        long now = g.time();

        //full radius: past AND future
        long r = Math.max(Math.abs(o[0] - now), Math.abs(o[1] - now));
        o[0] = now - r;
        o[1] = now + r;

        //half radius: past OR future
//        if (o[0] > now) o[0] = now;
//        if (o[1] < now) o[1] = now;

        return o;


//        long[] w = g.focus().when();
//        long rad = //Math.max(abs(now - w[0]), abs(now - w[1]));
//                    w[1] - w[0];
//        float radMult = 1;
//        long dur = Math.round(rad * radMult);
//        w[0] = now-dur;
//        w[1] = now+dur;
//        return w;
    }
//    public final long[] goalStamp() {
//        return rewardGoalTable().goalEvi;
//    }

    private RewardGoalTable rewardGoalTable() {
        //return ((BeliefTables) in.goals()).tableFirst(RewardGoalTable.class);
        return out;
    }

    public final ScalarReward resolution(float r) {
        if (sensor == null)
            _resolution = r; //HACK
        else
            sensor.resolution(r);
        return this;
    }

    protected abstract float reward(Game a);

    @Override
    public final double happy(long start, long end, float dur) {
        ScalarSensor i = this.sensor;

        Truth actual;
        if (i == null) {
            actual = null;
        } else {
            //TODO ignore Eternal freqUsually if temporals exist in the interval
            actual = beliefTruth(start, end, dur, true,
                    i.concept.beliefs());
            if (actual == null) {
                //TODO try again, accept any belief
                //if (NAL.belief.REWARD_SERIAL_FILTER...)
                //actual = beliefTruth(start, end, dur, false, rewardBelief);
            }
        }
        //* Math.min(1, b.conf() / game._confDefaultBelief)

        return actual == null ? Double.NaN :
                1 - i.truther(game).dist(goalTruth, actual);
    }

    @Nullable
    private Truth beliefTruth(long start, long end, float dur, boolean onlySerial, BeliefTable rewardBelief) {
        return rewardBelief.truth(start, end, null,
                onlySerial ? t -> t instanceof SerialTask || t instanceof EternalTask
                        : null, dur, nar());
    }

    @Override
    public Iterable<? extends Termed> components() {
        return List.of(sensor);
        //return List.of(id);
        //return sensor.components();
    }

    /** TODO not completely working yet */
    public final ScalarReward usually(float freq) {
        this.freqUsually = freq;
        return this;
    }

    @Override
    public void start(Game g) {
        super.start(g);

        NAR nar = g.nar;


        //this.why = nar...

        sensor = new RewardScalarSensor();

        sensor.start(g);
        ((BeliefTables) sensor.concept.goals()).addFirst(out = new RewardGoalTable(nar));

        long now = nar.time();
        how = //new MutableSingleQuestionTable(
                new SerialTask(
                        NALTask.taskTerm(IMPL.the($.varQuery(1), XTERNAL, term()), QUESTION),
                        QUESTION, null, now, now, nar.evidence()
                );
        //);
        //TODO ((QuestionTables)in.questions())...


        if (_resolution > 0)
            sensor.resolution(_resolution);

        if (freqUsually == freqUsually) {
            nar.runLater(() -> { //HACK
                Truth t = $.t(freqUsually, nar().beliefConfDefault.conf() * NAL.signal.REWARD_USUALLY_CONF_FACTOR);

                //for (Termed c : components()) {
                    //TODO assert that it has no eternal tables already
                    EternalDefaultTable.add(sensor.concept, t, nar);
                //}
            });
        }
    }

//    protected int rewardShift(Game g) {
//        return -Math.round(g.dur()/2);
//    }

    private final class RewardGoalTable extends MutableTasksBeliefTable {


        RewardGoalTable(NAR nar) {
            super(ScalarReward.this.id, false, goalCapacity);
            sharedStamp = nar.evidence();
        }

        @Override
        public void remember(Remember r) {
            if (eternal) {
                //HACK prevent eternal duplicate store in eternalbelieftable
                SerialTask first = series.first();
                if (first != null && Arrays.equals(first.stamp(), r.input.stamp() /*r.input.equals(first)*/)) {
                    r.store(first);
                }
            }
        }

        @Override
        protected Truth taskTruth(float f, double evi) {
            return new MutableTruth(f, evi);
        }

        @Override
        public SerialTask task(long start, long end, float f, double evi, long[] stampIgnored) {
            SerialTask x = super.task(start, end, f, evi, sharedStamp);
            //x.why(why.ID); //TODO
            return x;
        }

        SerialTask setOrAdd(long[] se, MutableTruth goalTruth, float pri) {
            return setOrAdd(se[0], se[1],
                    goalTruth.freq(), goalTruth.evi(),
                    pri);
        }

    }

    private final class RewardScalarSensor extends ScalarSensor {

        RewardScalarSensor() {
            super(ScalarReward.this.id);
        }

        @Override
        public void accept(Game g) {
            float reward = ScalarReward.this.reward = ScalarReward.this.reward(g);
            accept(reward(reward, g), g);
        }

        /** @param x current reward frequency */
        private float reward(float x, Game g) {
            float y;
            if (x == x && freqUsually == freqUsually &&
                    Util.equals(x, freqUsually, g.nar().freqResolution.asFloat())) {
                y = NaN; //masked; absorbed by EternalTable (and also cancels any ongoing Serial stretch) */
            } else
                y = x;
            return y;
        }

        /**
         * prevent reward and action occurring simultaneously
         * which can confuse action prediction.
         */
        @Override
        protected When<Focus> when(Game g) {
            return g.time.addDurs(Rewards.REWARD_OFFSET_DURS);
        }
    }

//    public void addGuard(boolean log, boolean forget) {
//
//        ((BeliefTables) sensor.goals()).add(0, new EmptyBeliefTable() {
//            @Override
//            public void remember(Remember r) {
//                NALTask i = r.input;
//
//                float diff = abs(i.freq() - goalTruth.freq());
//                if (diff >= 0.5f) {
//                    if (log) {
//                        //logger.info("goal contradicts reward:\n{}", i.proof());
//                        System.out.print("goal contradicts reward\t");
//                        r.nar().proofPrint(i);
//                        System.out.println();
//                    }
//                    if (forget) {
//                        r.unstore(i);
//                    }
//                } else if (diff < 0.25f) {
//                    //good
////                    if (log) {
////                        //logger.info("goal contradicts reward:\n{}", i.proof());
////                        System.out.print("goal supports reward\t"); r.nar().proofPrint(i);
////                    }
//                }
//
//            }
//        });
//    }


}