package nars.game.reward;

import jcog.signal.FloatRange;
import nars.NAR;
import nars.Term;
import nars.game.Game;
import nars.game.sensor.FocusLoop;

/** TODO extend UniSignal */

public abstract class Reward implements FocusLoop<Game> {

    @Deprecated protected Game game;
	public final Term id;

	/** determines reinforcement confidence */
    public final FloatRange strength = new FloatRange(1, 0, 1);


	protected Reward(Term id) {
    	this.id = id;
    }

    @Override public String toString() {
    	return id + ":" + getClass().getSimpleName();
	}

    /** raw value of last reward, may be NaN if not available.
	 *  if multiple components, this can return their sum reward()
	 * */
    public abstract double reward();

    public final double rewardOrZero() {
    	double r = reward();
		return r != r ? 0 : r;
	}

    public final Reward strength(float s) {
    	this.strength.set(s);
    	return this;
	}

    public float strength() {
    	return strength.floatValue();
	}

	@Override
	public final Term term() {
		return id;
	}


    /** estimated current happiness/satisfaction of this reward
     *
     * happiness = 1 - Math.abs(rewardBeliefExp - rewardGoalExp)/Math.max(rewardBeliefExp,rewardGoalExp)
	 * NaN if unknown
     * */
	public abstract double happy(long start, long end, float dur);


    public final NAR nar() { return game.nar(); }


	public void start(Game g) {
		this.game = g;
	}


}