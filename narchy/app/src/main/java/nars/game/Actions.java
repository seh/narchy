package nars.game;

import com.google.common.collect.Streams;
import jcog.TODO;
import jcog.data.list.FastCoWList;
import jcog.data.list.Lst;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.concept.Concept;
import nars.focus.PriAmp;
import nars.game.action.AbstractAction;
import nars.game.sensor.FocusLoop;
import nars.table.TaskTable;
import nars.task.NALTask;
import nars.task.util.Answer;
import nars.time.Moment;
import nars.truth.proj.TruthProjection;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static nars.NAL.answer.ANSWER_TRIES_PER_CAPACITY;
import static nars.Op.TIMELESS;

public class Actions implements Iterable<AbstractAction> {

    /** relative time to shift goal pickup, in durs */
    private static final float GOAL_FOCUS_SHIFT =
        0;
        //-1;
        //-0.5f;

    /**
     * TODO Map<> for O(1) dynamic add/remove
     */
    public /* TODO private */ final FastCoWList<AbstractAction> actions =
            new FastCoWList<>(AbstractAction[]::new);

    public PriAmp pri;

    private Answer goalMatch;

    public static final boolean curiosityReviseOrReplace = false;

    /** deferred initialization for compound actions */
    @Deprecated transient final List<FocusLoop<Game>> _actions = new Lst<>();

    void actBefore(Game g) {
        updateGoalMatch(g);

        actions.forEach(g::update);
    }
    
    void actAfter(Game g) {
        actions.forEach(a -> a.actAfter(g));
    }
    
    private void updateGoalMatch(Game g) {
        if (goalMatch == null)
            goalMatch = newGoalMatch(g);

        Moment w = g.time;
        int shift = Math.round(GOAL_FOCUS_SHIFT * w.dur);
        goalMatch.time(w.s + shift, w.e + shift);
    }

    private Answer newGoalMatch(Game g) {
        return Answer.answer(true, null,
                        NAL.answer.ACTION_ANSWER_CAPACITY, TIMELESS, TIMELESS,
                        0, null,
                        g.nar)
            .eviMin(0);
    }

    @Nullable public TruthProjection truth(Term term, TaskTable t, float matchDur, float truthDur, @Nullable Predicate<NALTask> filter) {
        return t == null || t.isEmpty() ? null : goalMatch
                .dur(matchDur)
                .clear(ANSWER_TRIES_PER_CAPACITY * NAL.answer.ACTION_ANSWER_CAPACITY)
                .term(term)
                .filter(filter)
                .match(t)
                .truthSpecificTime(truthDur);
    }

    //	/** dur sensitivity detection range */
//	public final FloatRange actionDurs = new FloatRange(1, 0, 4);


//    /**
//     * relative time when the desire is actually read (negative values mean past, ex: half duration ago),
//     * determines when desire is answered to produce feedback in the present frame.
//     * represents a natural latency between decision and effect.
//     */
//    public final FloatRange desireShift = new FloatRange(
//            0
//            //-1f
//            //-0.5f
//            //+0.5f
//            , -2, +2);


    /**
     * dexterity = mean(conf(action))
     * evidence/confidence in action decisions, current measurement
     */
    public final double dexterity() {
        int a = actions.size();
        return a == 0 ? 0 : actions.sumBy(AbstractAction::dexterity) / a;
    }

    public final double coherency(long start, long end, float dur, NAR nar) {
        return actions.meanBy(x -> x.coherency(start, end, dur, nar));
    }

    /**
     * compares the truth frequencies of the implication pair:
     * (  outcome ==>-dt action)
     * (--outcome ==>-dt action)
     * <p>
     * if both are not defined, return NaN
     * this can be used to test how polarized the belief that an outcome depends on the
     * particular value of this action.  if the two impl freq are similar, this
     * indicates the outcome is expected to depend less on this action's choice
     * than if the freq are different.
     */
    public double opinionation(Term outcome, int dt, long start, long end, float dur, NAR nar) {
        throw new TODO();
    }


//    /**
//     * avg action frustration
//     */
//    @Research
//    public final double frustration(long start, long end, float dur, NAR nar) {
//        return actions.meanBy(rr -> 1 - rr.happy(start, end, dur, nar));
//    }

    public final <A extends AbstractAction> A addAction(A c) {
        Term ct = c.term();
        if (actions.OR(e -> ct.equals(e.term())))
            throw new RuntimeException("action exists with the ID: " + ct);
        actions.add(c);
        return c;
    }

    public final int size() {
        return actions.size();
    }

    @Override
    public final Iterator<AbstractAction> iterator() {
        return actions.iterator();
    }

    @Override
    public final void forEach(Consumer<? super AbstractAction> action) {
        actions.forEach(action);
    }

    public final Stream<AbstractAction> stream() {
        return actions.stream();
    }

    @Deprecated public Stream<? extends Concept> concepts() {
        return stream().flatMap(z -> Streams.stream(z.components()))
                .filter(z -> z instanceof Concept).map(z -> ((Concept)z));
    }

    public <A extends FocusLoop<Game>> A addAction(A a) {
        if (a instanceof AbstractAction)
            addAction((AbstractAction)a);
        else {
            _actions.add(a);
        }
        return a;
    }
}