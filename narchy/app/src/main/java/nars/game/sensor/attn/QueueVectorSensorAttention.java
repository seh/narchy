package nars.game.sensor.attn;

import jcog.signal.FloatRange;
import nars.Term;
import nars.focus.Focus;
import nars.game.sensor.SignalConcept;
import nars.game.sensor.VectorSensor;
import nars.time.When;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * FIFO
 */
public class QueueVectorSensorAttention extends VectorSensorAttention /*implements Controllable*/ {

    final Deque<SignalConcept> queue = new ArrayDeque<>();
    public final FloatRange inputRate = new FloatRange(0.5f, 0, 1);


    public QueueVectorSensorAttention(VectorSensor v, Term why) {
        super(v, why);
    }

    @Override
    public void activate(SignalConcept s) {
        boolean alreadyActive = s.activateIfDeactivated();
        if (!alreadyActive) {
            queue.addFirst(s);
        }
    }

    @Override
    public void commit(VectorSensor v, When<Focus> w) {
        if (queue.isEmpty())
            return;

        int total = v.size();
        int qSize = queue.size();
        int toActivate = Math.min(qSize, activationLimit(qSize, total));

        activate(v.priComponent(), toActivate, w.x);
        trim(total);

        //System.out.println(v + " amp=" + n4(v.pri.amp.floatValue()) + " activated=" + toActivate + ", queue=" + queue.size());
    }

    protected void activate(float priEach, int toActivate, Focus ww) {
        for (int i = toActivate; i > 0; i--)
            pop().remember(priEach, ww);
    }

    private void trim(int total) {
        //trim queue to capacity
        int toRemove = queue.size() - total;
        while (toRemove-- > 0)
            pop();
    }

    private int activationLimit(int pending, int total) {
        return (int) Math.ceil(total * inputRate.floatValue());
        //return (int) Math.ceil(total * amp * inputRate.floatValue());
    }

    SignalConcept pop() {
        SignalConcept s = queue.removeLast();
        s.deactivate();
        return s;
    }

//        @Override
//        public void controlled(Game g) {
//            g.actionUnipolar()
//        }
}