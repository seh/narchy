package nars.game.sensor;

import com.google.common.collect.Iterables;
import jcog.math.FloatSupplier;
import nars.$;
import nars.NAR;
import nars.Term;
import nars.focus.Focus;
import nars.func.Factorize;
import nars.game.Game;
import nars.game.sensor.attn.DirectVectorSensorAttention;
import nars.game.sensor.attn.VectorSensorAttention;
import nars.subterm.Subterms;
import nars.term.Termed;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.term.util.transform.RecursiveTermTransform;
import nars.time.When;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import static nars.Op.SETe;

/**
 * base class for a group of concepts representing a sensor 'vector'
 */
public abstract class VectorSensor extends AbstractSensor implements Iterable<SubSignalComponent> {

    public final VectorSensorAttention model;

    protected VectorSensor(Term[] id) {
        this(sensorID(id));
    }

    private static final RecursiveTermTransform MASK = new RecursiveTermTransform() {
        @Override
        public Term applyAtomic(Atomic x) {
            return x instanceof Variable ? nextUnknown() : x;
        }
    };

    protected VectorSensor(Term id) {
        super(id.hasVars() ? MASK.apply(id) : id);

        this.model =
            new DirectVectorSensorAttention(this, /*why?*/this.id);
            //new QueueVectorSensorAttention(this, this.id);
    }


    @Override
    public void start(Game game) {
        super.start(game);
        sensing.why(game.nar.control.newCause(term()).term());
    }

//    public final Sensor model(VectorSensorAttention model) {
//        this.model = model;
//        return this;
//    }

    private static Term sensorID(Term[] states) {
        Subterms f = Factorize.applyConj(states, $.varDep(1));
        if (f!=null)
            return f.sub(1); //CONJ.the(f); //pattern found

        //fail-safe
        return SETe.the(states);
    }


    /**
     * best to override
     */
    public int size(){return Iterables.size(this);}

//    /** surPRIse */
//    public double surprise() {
//        double s = 0;
//        for (Signal c : this)
//            s += ((SensorBeliefTables)c.beliefs()).surprise();
//        return s;
//    }

    @Override
    public void accept(Game g) {
        When<Focus> ww = g.time;
        input(g, ww);
        model.commit(this, ww);
    }

    private void input(Game g, When<Focus> ww) {
        model.input(ww, truther(g), resolution(), sensing.sleepDurs, g);
    }



//    protected float priComponent(float pri) {
//        //post-commit phase
//        return pri / Math.max(1,
//            1
//            //active
//            //size()
//        );
//    }

    protected SubSignalComponent component(Term id, FloatSupplier f, NAR nar) {
        return new MyComponentSignal(id, f, nar);
    }

    /** default: input everything , but subclasses can override to return sub-ranges */
    public Iterator<SubSignalComponent> inputIterator() {
        return iterator();
    }

    @Override
    public final Iterable<? extends Termed> components() {
        return this;
    }


    /**
     * consider the fairness of prioritization of each component, wrt to other sensors
     * TODO move to abstract sensor budgeting interface */
    public float priComponent() {
        return sensing.pri();
        //return sensing.pri()/size();
        //return sensing.pri() / Util.sqrt(size());
        //TODO others
    }


    private static final AtomicInteger unknowns = new AtomicInteger();

    @Deprecated
    private static final String unknownPrefix = "⁉";

    @Deprecated
    private static synchronized Term nextUnknown() {
        return Atomic.atom(unknownPrefix + Character.toString(unknowns.getAndIncrement() + 'a'));
    }

    private static final class MyComponentSignal extends SubSignalComponent {

        private final FloatSupplier f;

        MyComponentSignal(Term id, FloatSupplier f, NAR nar) {
            super(id, nar);
            this.f = f;
        }

        @Override
        public float value(Game g) {
            return f.asFloat();
        }

    }
}