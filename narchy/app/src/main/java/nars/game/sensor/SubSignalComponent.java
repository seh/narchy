package nars.game.sensor;

import nars.NAR;
import nars.Term;
import nars.game.Game;

/** one of potentially several component signals in a vector sensor */
public abstract class SubSignalComponent extends SignalConcept {

	public SubSignalComponent(Term componentID, NAR nar) {
		super(componentID, nar);
	}

	public abstract float value(Game g);

}