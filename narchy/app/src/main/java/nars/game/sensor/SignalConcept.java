package nars.game.sensor;

import jcog.TODO;
import jcog.Util;
import jcog.math.FloatSupplier;
import nars.$;
import nars.NAR;
import nars.Term;
import nars.concept.PermanentConcept;
import nars.concept.TaskConcept;
import nars.concept.util.ConceptBuilder;
import nars.focus.Focus;
import nars.table.BeliefTable;
import nars.table.dynamic.SensorBeliefTables;
import nars.table.question.QuestionTable;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.time.Moment;
import nars.time.When;
import nars.truth.Truth;
import org.eclipse.collections.api.block.function.primitive.FloatFloatToObjectFunction;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.VarHandle;
import java.util.function.Function;

import static jcog.math.LongInterval.TIMELESS;


/**
 * primarily a collector for belief-generating time-changing (live) input scalar (1-d real value) signals
 *
 *
 * In vector analysis, a scalar quantity is considered to be a quantity that has magnitude or size, but no motion. An example is pressure; the pressure of a gas has a certain value of so many pounds per square inch, and we can measure it, but the notion of pressure does not involve the notion of movement of the gas through space. Therefore pressure is a scalar quantity, and it's a gross, external quantity since it's a scalar. Note, however, the dramatic difference here between the physics of the situation and mathematics of the situation. In mathematics, when you say something is a scalar, you're just speaking of a number, without having a direction attached to it. And mathematically, that's all there is to it; the number doesn't have an internal structure, it doesn't have internal motion, etc. It just has magnitude - and, of course, location, which may be attachment to an object.
 * http://www.cheniere.org/misc/interview1991.htm#Scalar%20Detector
 */
public class SignalConcept extends TaskConcept implements PermanentConcept {

    public transient SerialTask /*prev = null,*/ next;

    protected static final VarHandle ACTIVE = Util.VAR(SignalConcept.class, "active", boolean.class);
    /** whether link activation is pending */
    @SuppressWarnings("unused") private volatile boolean active;

    public SignalConcept(Term term, NAR n) {
        this(term,
                beliefTable(term, true, true, n),
                beliefTable(term, false, false, n),
                n);
    }

    public SignalConcept(Term term, BeliefTable beliefTable, BeliefTable goalTable, NAR n) {
        super(term, beliefTable, goalTable,
                questionTable(term, true, n),
                questionTable(term, false, n)
        );
        n.add(this);
    }

    static BeliefTable beliefTable(Term term, boolean beliefOrGoal, boolean sensor, NAR n) {
        return beliefTable(term, beliefOrGoal, sensor, n.conceptBuilder);
    }

    private static BeliefTable beliefTable(Term term, boolean beliefOrGoal, boolean sensor, ConceptBuilder c) {
        return sensor ?
                new SensorBeliefTables(term, beliefOrGoal, c.temporalTable(term, beliefOrGoal))
                :
                c.beliefTable(term, beliefOrGoal);
    }

    private static QuestionTable questionTable(Term term, boolean beliefOrGoal, NAR n) {
        return n.conceptBuilder.questionTable(term, beliefOrGoal);
    }


    /** pre-commit phase */
    public final boolean input(@Nullable Truth value, Term why, float sleepDurs, When<Focus> w) {
        SerialTask
            prev = this.next,
            next = ((SensorBeliefTables)beliefs()).add(value, w);

        boolean newTask = (next != null && next != prev);
        if (newTask) {
            next.why(why);
            this.next = next;
        }

        return recreateIfAgeExceeds(this.next, sleepDurs, w);
    }

    private static boolean recreateIfAgeExceeds(@Nullable NALTask next, float durs, Moment w) {
        if (next==null)
            return false;
        else {
            long creation = next.creation();
            return creation == TIMELESS || (w.s - creation) >= durs * w.dur;
        }
    }



    public final void remember(float pri, Focus w) {
        //assert(pri == pri && pri >= 0);

        SerialTask x = this.next;
        if (x == null) return;

        x.pri(pri);
        w.link(x, this);
    }


    //float activation = pri * lerpSafe(surprise(this.prev, next, dur), minSurprise, 1);

//    /**
//     * used to determine the priority factor of new signal tasks,
//     * with respect to how significantly it changed from a previous value,
//     * OR the time duration since last received signal
//     */
//    static float surprise(NALTask prev, NALTask next, float dur) {
//
//        boolean NEW = prev == null;
//        if (NEW)
//            return 1;
//
//        boolean stretched = prev == next;
//        if (stretched)
//            return 0;
//
//        float deltaFreq = Math.abs(prev.freq() - next.freq()); //TODO use a moving average or other anomaly/surprise detection
//
//        long sepCycles = Math.abs(next.start() - prev.end());
//        float deltaTime = sepCycles / dur; //(float) (1 - NAL.evi(1, sepCycles, dur));
//
//        return Util.or(deltaFreq, Math.max(0, 1 - deltaTime));
//    }


    /**
     * update directly with next value
     */
    public static Function<FloatSupplier, FloatFloatToObjectFunction<Truth>> SET = (conf) ->
        ((prev, next) -> next == next ? $.t(next, conf.asFloat()) : null);
    /**
     * first order difference
     */
    public static Function<FloatSupplier, FloatFloatToObjectFunction<Truth>> DIFF = (conf) ->
        ((prev, next) -> ($.t(next == next ? prev == prev ? (next - prev) / 2f + 0.5f : 0.5f : 0.5f, conf.asFloat())));


    @Nullable public final Truth truth() {
        SerialTask v = next;
        return v == null ? null : v.truth();
    }

    public final float freq() {
        Truth t = truth();
        return t!=null ? t.freq() : Float.NaN;
    }

    public byte punc() {
        SerialTask v = next;
        return v != null ? v.punc() : 0;
    }

    public final void activate() {
        ACTIVE.setVolatile(this, true);
    }
    public final boolean activeTake() {
        return ACTIVE.compareAndSet(this, true, false);
    }

    public final boolean activateIfDeactivated() {
        return ACTIVE.compareAndSet(this, false, true);
    }

    public final boolean active() {
        return (boolean) ACTIVE.getOpaque(this);
    }

    public final void deactivate() {
        ACTIVE.setVolatile(this,false);
    }



    /** reverse-estimates an input value from beliefs (at any target time) */
    public double predict(long s, long e) {
        throw new TODO();
    }

}