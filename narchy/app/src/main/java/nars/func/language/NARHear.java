package nars.func.language;

import com.google.common.io.Resources;
import jcog.data.list.Lst;
import jcog.exe.Loop;
import jcog.io.Twokenize;
import nars.*;
import nars.concept.Operator;
import nars.func.language.util.Twenglish;
import nars.task.NALTask;
import nars.task.util.TaskException;
import nars.term.Functor;
import nars.term.atom.Atomic;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static nars.Op.BELIEF;


/**
 * sequential hearing and reading input abilities
 * TODO extend NARPart, add Loop as field
 */
public class NARHear extends Loop {

	static final Atomic START = Atomic.the("start");

	private final NAR nar;


	private final List<Term> tokens;

	@Nullable private final Term src;

    //public final Off onReset;
	int token;

	float priorityFactor = 1f;
	float confFactor = 1f;


	public NARHear(NAR nar, List<Term> msg, @Nullable String src, int wordDelayMS) {
		this(nar, msg, src!=null ? Atomic.atom(src) : null, wordDelayMS);
	}

	public NARHear(NAR nar, List<Term> msg, @Nullable Term src, int wordDelayMS) {
		super();
		this.nar = nar;
        this.src = src;
		//onReset = nar.eventClear.onWeak(this::onReset);
		tokens = msg;


		if (wordDelayMS > 0) {
			setPeriodMS(wordDelayMS);
		}
//

//		Term prev = null;
//		for (Term x : msg) {
//			hear(prev, x);
//			prev = x;
//		}

	}

	public static Loop hear(NAR nar, String msg, String src, int wordDelayMS) {
		return hear(nar, msg, src, wordDelayMS, 1f);
	}

	/**
	 * set wordDelayMS to 0 to disable twenglish function
	 */
	public static Loop hear(NAR nar, String msg, String src, int wordDelayMS, float pri) {
		return hearIfNotNarsese(nar, msg, src, (m) -> hearText(nar, msg, src, wordDelayMS, pri));
	}

	public static Loop hearText(NAR nar, String msg, @Nullable String src, int wordDelayMS, float pri) {
		assert (wordDelayMS > 0);
		List<Term> tokens = tokenize(msg);
		if (!tokens.isEmpty()) {
			NARHear hear = new NARHear(nar, tokens, src, wordDelayMS);
			hear.priorityFactor = pri;
			return hear;
		} else {
			return null;
		}
	}

	public static Loop hearIfNotNarsese(NAR nar, String msg, String src, Function<String, Loop> ifNotNarsese) {

		List<Task> parsed = new Lst<>();

		List<Narsese.NarseseException> errors = new Lst<>();

		try {
			Narsese.tasks(msg, parsed, nar);
		} catch (Narsese.NarseseException ignored) {

		}

		if (!parsed.isEmpty() && errors.isEmpty()) {

			nar.input(parsed);
			return null;
		} else {
			return ifNotNarsese.apply(msg);
		}
	}

//    protected void onReset(Timed n) {
//        stop();
//        onReset.close();
//    }

	public static List<Term> tokenize(String msg) {
		List<Term> list = Twokenize.tokenize(msg).stream().map(Twenglish::spanToTerm).collect(Collectors.toList());
		return list;
	}

	public static void readURL(NAR nar) {
		nar.add(Atomic.atom("readURL"), (t, n) -> {

			Term[] args = Functor.args(t.term()).arrayClone();
			try {


				return readURL(n, $.unquote(args[0]));

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		});
	}

	public static Task readURL(NAR n, String url) throws IOException {


		String html = Resources.toString(new URL(url), Charset.defaultCharset());

		html = StringEscapeUtils.unescapeHtml4(html);
		String strippedText = html.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ").toLowerCase();


		NARHear.hear(n, strippedText, url, 250, 0.1f);

		return Operator.log(n.time(), "Reading " + url + ':' + strippedText.length() + " characters");
	}

	public static void hear(NAR nar, String t, String source) {
		try {
			try {
				nar.input(t);
			} catch (Narsese.NarseseException e) {

				try {
					NARHear.hear(nar, t, source, 0);
				} catch (Exception e1) {
					nar.input(Operator.log(nar.time(), e1));
				}

			}
		} catch (TaskException tt) {
			nar.input(Operator.log(nar.time(), $.p(t, tt.toString())));
		}

	}

	public static void hear(String text, String src, NAR nar) {

		hearIfNotNarsese(nar, text, src, (t) ->
			new NARHear(nar, tokenize(t.toLowerCase()), src, (int)(nar.dur() * 2)));
	}

	@Override
	public boolean next() {
		if (token >= tokens.size()) {
			stop();
			return true;
		}


		hear(token > 0 ? tokens.get(token - 1) : START, tokens.get(token++));
		return true;
	}

	private void hear(Term prev, Term next) {

		Term term =
			src != null ?
				$.func("that", next, src) :
				$.func("that", next);

		long now = nar.time();
		//            new TruthletTask(
		//                target,
		//                BELIEF,
		//                Truthlet.impulse(
		//                        now, now+1 /* TODO use realtime to translate wordDelayMS to cycles */, 1f, 0f,
		//                        c2w(nar.confDefault(BELIEF)*confFactor)
		//                ),
		//                nar)
		nar.input(
			(Task)NALTask.taskUnsafe(term, BELIEF, $.t(1, (nar.confDefault(BELIEF) * confFactor)), now,
				Math.round(now + nar.dur()), nar.evidence()).withPri(nar.priDefault(BELIEF) * priorityFactor)
		);
	}
}