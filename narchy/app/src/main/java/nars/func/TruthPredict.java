package nars.func;

import com.google.common.collect.Iterables;
import jcog.Util;
import jcog.math.FloatSupplier;
import jcog.predict.LivePredictor;
import jcog.predict.Predictor;
import jcog.signal.FloatRange;
import jcog.signal.anomaly.ewma.Ewma;
import nars.NAL;
import nars.NAR;
import nars.NARPart;
import nars.Term;
import nars.concept.Concept;
import nars.game.Game;
import nars.table.BeliefTable;
import nars.table.BeliefTables;
import nars.table.dynamic.MutableTasksBeliefTable;
import nars.task.NALTask;
import nars.term.Termed;
import nars.truth.MutableTruth;
import nars.truth.Truth;
import org.eclipse.collections.api.block.function.primitive.IntIntToObjectFunction;
import org.eclipse.collections.api.block.function.primitive.LongToFloatFunction;

import java.util.Random;
import java.util.function.Predicate;

import static jcog.Util.map;
import static nars.Op.BELIEF;
import static nars.Op.GOAL;

/**
 * numeric prediction support
 * TODO configuration parameters for projections
 * --number
 * --time width (duty cycle %)
 * --confidence fade factor
 */
public class TruthPredict extends NARPart {


	/** evidence level meter */
	final Ewma evi;

	/**
	 * multiplier to the learned target's average measured evidence,
	 * and normalized (divided) among # of futures
	 * (TODO normalize by futures factors integral which is a curve determined by eviSustain) */
	public final FloatRange eviFactor = new FloatRange(1f, 0, 2f);

	public final FloatRange eviSustain = new FloatRange(1, 0, 1f);

	final boolean beliefOrGoal; //TODO
	protected final LivePredictor predictor;

	/**
	 * duration, in cycles
	 */
	private final FloatSupplier dur;

	/** belief tables where predictions are written to */
	private final MyMutableTasksBeliefTable[] predicting;

	/** true = all concept predictions share same stamp
	 *  false = each concept has a unique stamp
	 */
	private final boolean stampShared = false;


	//    private final Cause why;
    int futures;

	private float gameDur = 1;


	public TruthPredict(Iterable<? extends Termed> in, Iterable<? extends Termed> out, int history, FloatSupplier dur, int futures, IntIntToObjectFunction<Predictor>  m, Game g, boolean beliefOrGoal) {
		this(
			Iterables.toArray(in, Termed.class), Iterables.toArray(out, Termed.class), history, dur, futures, m, beliefOrGoal,
			g.nar);

		g.afterFrame(this::predict);
	}

	/** TODO for pasts, use Sensors like configured in Reflex */
	public TruthPredict(Termed[] in, Termed[] out, int pasts, FloatSupplier dur, int futures, IntIntToObjectFunction<Predictor> m, boolean beliefOrGoal, NAR nar) {
		super();
		this.beliefOrGoal = beliefOrGoal;

        this.futures = futures;
		this.dur = dur;
		this.predicting = new MyMutableTasksBeliefTable[out.length];

		eviSustain.set(1 - 1f/(1+pasts)); //TODO refine using log(..)/..

		long[] sharedStamp = stampShared ? nar.evidence() : null;
		for (int i = 0, n = out.length; i < n; i++) {
			long[] stamp = sharedStamp!=null ? sharedStamp : nar.evidence();
			Concept c = nar.conceptualize(out[i].term());
			((BeliefTables) ((beliefOrGoal ? c.beliefs() : c.goals())))
				.add(predicting[i] = new MyMutableTasksBeliefTable(c.term(), futures, beliefOrGoal, stamp));
		}

		evi = //new FloatAveragedWindow(out.length * 2, 0.5f).mode(FloatAveragedWindow.Mode.Mean);
			new Ewma(0.5f);

		/* always belief */
		boolean inputPunctuation = true;

		/* learns the truth of the generated type */
		boolean outputPunctuation = beliefOrGoal;

		predictor = new LivePredictor(new LivePredictor.DenseFramer(
			map(c -> truth(c, inputPunctuation, nar), LongToFloatFunction[]::new, in),
			pasts,
			dur,
			map(c -> truth(c, outputPunctuation, nar), LongToFloatFunction[]::new, out)
		), m);
	}

//	@Override
//	protected void starting(NAR nar) {
//		super.starting(nar);
//
//		//on(nar.onDur(this::predict)); //TODO maybe What.dur
//	}

	private void predict(Game g) {
		long now = g.time();

		gameDur = g.dur();
		Random rng = g.random();
		float beliefPriMax = nar.priDefault(beliefOrGoal ? BELIEF : GOAL);
		//   / ((futures +1) * predicting.length); //TODO use Cause-based prioritization

		float fade = eviSustain.floatValue();


		int shift = Math.round(gameDur / 2);
		long start = now + shift; //se[0];

		try {
			//temporarily disable tables to avoid influencing the prediction HACK
			for (MyMutableTasksBeliefTable t : predicting) t.enabled(false);

			double[] p = predictor.next(now);

			double eviMax = this.evi.mean() * eviFactor.doubleValue() / futures;
			if (eviMax != eviMax || eviMax < NAL.truth.EVI_MIN)
				return; //no evidence actually accumulated

			float dur = this.dur.asFloat();
			for (int i = 0; i < futures; i++) {

				long s = Math.round(start + i * dur);
				long e = Math.max(s, Math.round(s + dur) - 1);

				if (i > 0)
					p = predictor.project(s, p);

				double evi = eviMax * Math.pow(fade, i);
				float pri = (float) (beliefPriMax * (evi / eviMax));

				for (int j = 0; j < p.length; j++)
					predict(i, j, s, e, p, evi, pri, rng);
			}

		} finally {
			//re-activate tables
			for (MyMutableTasksBeliefTable t : predicting) t.enabled(true);
		}



	}

	private void predict(int i, int j, long s, long e, double[] p, double evi, float pri, Random rng) {
		float F;
		double E;

		double f = p[j];
		if (Double.isFinite(f)) {
			F = (float) Util.unitizeSafe(f);
			E = evi;
		} else {
			//HACK TODO
			F = rng.nextFloat();// TV snow signal
			//F = 0.5f;
			E = NAL.truth.EVI_MIN; //non-zero to be careful
		}

		predicting[j].set(i, s, e, F, E, pri);
	}

	/** excludes prediction tasks (and curiosity tasks) when evaluating truth for learning */
	private static final Predicate<NALTask> filter =
		null;
		//t-> !(t instanceof SeriesBeliefTable.SeriesTask) || (!(t.truth() instanceof MutableTruth)); //HACK


	private LongToFloatFunction truth(Termed x, boolean beliefOrGoal, NAR nar) {

		return new LongToFloatFunction() {

			final Term xx = x.term();
			private Concept c;
			private BeliefTable table;

			@Override
			public float valueOf(long when) {
				if (c == null || c.isDeleted()) {
					c = nar.conceptualizeDynamic(x);
					table = beliefOrGoal ? c.beliefs() : c.goals();
				}

				float dur = TruthPredict.this.dur.asFloat();

				long start = when, end = Math.round(start + dur);
				Truth t = table.truth(
					start, end,
					xx,
					filter, dur /*gameDur*/, nar); //TODO filter predictions (PredictionTask's) from being used in this calculation
				float f, e;
				if (t != null) {
					f = truth(t.freq());
					e = (float) t.evi();
				} else {
					f = 0.5f; /* or random? */
					e = 0;
				}

				evi.accept(e);

				return f;
			}

			private float truth(float freq) {
				return freq;
				//return (freq-0.5f)*2;
			}
		};
	}

	private static final class MyMutableTasksBeliefTable extends MutableTasksBeliefTable {


		MyMutableTasksBeliefTable(Term t, int capacity, boolean beliefOrGoal, long[] sharedStamp) {
			super(t, beliefOrGoal, capacity);
			this.sharedStamp = sharedStamp;
		}

		@Override
		protected Truth taskTruth(float f, double evi) {
			return new MutableTruth(f, evi);
		}

	}
}