package nars.func.stm;

import jcog.Fuzzy;
import jcog.Util;
import jcog.data.bit.MetalBitSet;
import jcog.data.list.Lst;
import jcog.pri.PLink;
import jcog.random.RandomBits;
import jcog.signal.FloatRange;
import jcog.signal.IntRange;
import jcog.signal.anomaly.ewma.Ewma;
import nars.NAL;
import nars.NAR;
import nars.Op;
import nars.Term;
import nars.action.transform.TemporalComposer;
import nars.bag.BagClustering;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.derive.reaction.Reaction;
import nars.derive.util.DeriverTaskify;
import nars.focus.Focus;
import nars.task.NALTask;
import nars.task.proxy.SpecialNegTask;
import nars.task.proxy.SpecialTermTask;
import nars.task.util.TaskList;
import nars.term.util.Image;
import nars.term.util.transform.VariableShift;
import nars.truth.dynamic.DynConj;
import nars.truth.dynamic.DynImpl;
import nars.truth.dynamic.DynTruth;
import nars.unify.constraint.TermMatch;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.lang.Math.abs;
import static jcog.Util.pctDiff;
import static nars.Op.*;

public class CompoundClustering extends NativeReaction implements TemporalComposer {

    private static final boolean allowVars = true, allowSeq = true;
    private static final boolean
        conj = true,
        disj = NAL.temporal.TEMPORAL_INDUCTION_DISJ,
        impl = NAL.temporal.TEMPORAL_INDUCTION_IMPL
    ;

    static { assert(conj || disj); }

    /** >1 increases the dithering applied to the clustered tasks,
     *     increasing the probability of parallel sub-conditions */
    public final IntRange overDither = new IntRange(1, 1, 8);

    public final IntRange condMin = new IntRange(2, 2, 4);

    public final IntRange condMax = new IntRange(2, 2, 16);

    /** output to input cond count ratio: 1.0 is balanced */
    public final FloatRange outputRatio = new FloatRange(1, 0.1f, 4);

    public final FloatRange forgetRate = new FloatRange(1, 0, 2);

    /*
    TODO stop adding neighbors if frequency becomes too dilute:
    public final FloatRange freqMin = FloatRange.unit(Util.PHI_min_1f);
    */

    /** TODO per centroid, centroid^0.5 ? */
    public final IntRange clusterIterations = new IntRange(8, 1, 64);

    /**
     * fraction of volMax to stop clustering
     */
    public final FloatRange volFractionLimit = new FloatRange(0.9f /*1*/, 0, 1);

    @Deprecated
    final int CENTROIDS, CAPACITY;
    final BagClustering.Dimensionalize<NALTask> model = new BagClustering.Dimensionalize<>(
            //2
            3
            //2
    ) {


        //        @Override
//        public void accept(NALTask t, double[] c) {
//            long s = t.start(), e = t.end();
//            c[0] = ((s + e) / 2L); //mid
//            c[1] = (e - s); //range
//            //c[2] = t.volume();
//        }
//
//        @Override
//        public double distanceSq(double[] a, double[] b) {
//            final double ar = a[1], br = b[1];
//            double dur = 1 +
//                    //Math.min(ar, br)
//                    Util.mean(ar, br);
//            return sqr(abs(a[0] - b[0]) / dur) //dMid
//                    + sqr(abs(ar - br) / dur) //dRange
//                    ;
//        }
        @Override
        public void accept(NALTask t, double[] c) {
            c[0] = t.start();
            c[1] = t.end();
//            Truth tt = t.truth();
//            c[2] = tt.conf();
//            c[3] = tt.polarity();

            c[2] = t.evi();
            //c[3] = t.volume();
        }

        @Override
        public double distance(double[] a, double[] b) {
            //double range = 1+Math.min(a[1]-a[0], b[1]-b[0]);
            //double ds = abs(a[0] - b[0]), de = abs(a[1] - b[1]);
            double dCenter = abs(Fuzzy.mean(a[0],a[1]) - Fuzzy.mean(b[0],b[1]));
            //double dRangePct = pctDiff( a[1] - a[0], b[1] - b[0]);

            /* center, a 3rd-point */ //TODO helpful?
            //double dc = abs( ((a[0] + a[1]) - (b[0] + b[1]))/2 );

            //double dv = abs(a[2] - b[2]);
            //double dVolPct = pctDiff(a[3], b[3]);

            double dEviPct = pctDiff(a[2], b[2]);

            //double dr = abs((a[1] - a[0]) - (b[1] - b[0]));

//            double dc =
//                //cluster similar confidence to conserve conf
//                Util.pctDiff(a[2], b[2]);
//                //abs(a[2] - b[2]);

//            double dp =
//                    //cluster similar confidence to conserve conf
//                    abs(a[2] - b[2]);

            return
                  //(1 + (ds*ds + de*de + dc*dc))
                  //(1 + (ds + de /*+ dc*/))
                  (1 + dCenter)
                  //  (1 + (ds+de))
                //* (1 + dVolPct * 1)
                //* (1 + dRangePct * 0.5f)
                * (1 + dEviPct * 0.5)
                //(1 + (ds + de)/range)
                //* (1 + dc) * (1 + dp)
            ;
        }

    };

    private final Predicate<NALTask> filter;
    float learningPeriodDurs = 1;
    double dur;

    public CompoundClustering(byte puncIn, int centroids, int capacity) {
        this(puncIn, centroids, capacity, t -> true);
    }

    /**
     * default that configures with belief/goal -> question/quest output mode
     */
    public CompoundClustering(byte punc, int centroids, int capacity, Predicate<NALTask> filter) {
        super();

        single();

        assert(Op.BELIEF_OR_GOAL(punc));
        taskPunc(punc);

        taskEternal(false);

        if (!NAL.term.CONJ_INDUCT_IMPL)
            hasNot(PremiseTask, IMPL);

        if (!allowSeq)
            iff(PremiseTask, TermMatch.SEQUENCE, false);

        if (!allowVars) {
            hasAny(PremiseTask,
                    Variables //<-- TODO requires multi-normalization (shifting offsets)
                    , false);
        }


        this.filter = filter;
        this.CENTROIDS = centroids;
        this.CAPACITY = capacity;

        condMean = new Ewma().period(CAPACITY/*TODO refine*/).with(Fuzzy.mean(condMin.floatValue(), condMax.floatValue()));
    }

    private static void separateVariables(NALTask[] n) {

        int nSize = n.length;
        MetalBitSet hasVars = MetalBitSet.bits(nSize);

        short[] varCounts = null;
        for (int i = 0; i < nSize; i++) {
            if (i == nSize - 1 && varCounts == null)
                break; //final term, no vars yet, leave school early

            int xs = n[i].term().structure();
            if (Op.hasAny(xs, Variables)) {
                hasVars.set(i);
                if (varCounts == null) varCounts = new short[VarTypes];
                if (Op.hasAny(xs, VAR_PATTERN)) varCounts[0]++;
                if (Op.hasAny(xs, VAR_QUERY)) varCounts[1]++;
                if (Op.hasAny(xs, VAR_INDEP)) varCounts[2]++;
                if (Op.hasAny(xs, VAR_DEP)) varCounts[3]++;
            }

        }
        if (varCounts == null || Util.max(varCounts) < 2)
            return;

        //TODO sort by max vars first to reduce effort
        //n.sortThisByInt((NALTask x) -> -x.term().vars());

        int vCommon = 0;
        if (varCounts[0]>1) vCommon |= VAR_PATTERN.bit;
        if (varCounts[1]>1) vCommon |= VAR_QUERY.bit;
        if (varCounts[2]>1) vCommon |= VAR_INDEP.bit;
        if (varCounts[3]>1) vCommon |= VAR_DEP.bit;

        VariableShift v = new VariableShift(vCommon);
        for (int i = 0; i < nSize; i++) {
            if (!hasVars.test(i)) continue;

            NALTask X = n[i];
            Term x = X.term();
            if (v.offset() > 0) {
                Term y = v.apply(x);
                //assert(!y.equals(x));
//                if (i==0 || x.equals(y))
//                    throw new WTF(); //TEMPORARY
                n[i] = SpecialTermTask.proxy(X, y).copyMeta(X); //TODO re-use a VariableShift TermTransform
            }
            v.shift(x);
        }

    }

    public Stream<MyBagClustering> clusters(NAR n) {
        return n.focus.stream().map(z -> z.get().focusLocal(this));
    }

    @Override
    protected final void run(Deriver d, Cause<Reaction> why) {

        NALTask x = d.premise.task();
        if (filter.test(x))
            clusters(d.focus).cluster(d, task(x));

    }

    private NALTask task(NALTask x) {
        Term xt = x.term();
        if (xt.INH()) {
            Term yt = Image.imageNormalize(xt);
            if (yt != xt) {
                return SpecialTermTask.proxy(x, yt).copyMeta(x);
                //if (!filter.test(x)) return;
            }
        }
        return x;
    }


    private void addAll(Lst<NALTask[]> xx, Deriver d) {

        for (NALTask[] x : xx) {

            if (allowVars) separateVariables(x);

            boolean success = false;
            if (conj)
                success |= taskify(x, DynConj.Conj, d);

            if (disj && (!conj || success))
                success |= taskify(x, DynConj.Disj, d);

            if (impl)
                success |= taskify(chooseTwo(x), DynImpl.DynImpl, d);


            condMean.accept(success ? x.length : 0);
        }

    }

    private static NALTask[] chooseTwo(NALTask[] x) {
        return x.length == 2 ? x :
                Arrays.copyOf(x, 2); //first two, since x will be shuffled already
    }

    private boolean taskify(NALTask[] x, DynTruth m, Deriver d) {
        var xx = new DeriverTaskify(m, d, x.clone());
        xx.ditherDT *= overDither.intValue();

        NALTask y = xx.taskClose();
        if (y != null) {
            d.add(y);
            return true;
        } else
            return false;
    }

    private MyBagClustering clusters(Focus f) {
        return f.focusLocal(this, s -> new MyBagClustering());
    }

    protected float pri(NALTask t) {
        return t.pri();

        //return (float) Fuzzy.meanGeo(t.polarity(), t.conf());

//        return (float) ((0.5f + 0.5f * t.polarity())
//                    * (0.5f + 0.5f * t.conf()))
//                        //* (0.5f + 0.5f * t.pri())
//                ;

        //return t.pri() * (0.5f + t.polarity()*0.5f);

//        float vMax = d.volMax;
//        return Math.max(0, ((vMax + 1) - t.volume()))/ vMax * (0.5f + t.polarity() / 2);

        //return 1/Util.sqrt(t.volume());
        //return (0.5f + t.polarity()/2) / Util.sqrt(t.volume());
        //return t.polarity();
            //t.priElseZero()
            //t.priElseZero() * t.polarity()
            //t.priElseZero() * Math.pow(t.originality(), 2)
            //t.priElseZero() / t.volume()
            //t.priElseZero() / Math.sqrt(t.volume())
            //1
//                //(1 + 0.5 * t.priElseZero()) *
//                (t.evi()) *
//                (1 + (t.range()-1)/d.nar.dur()) *
            // * (t.polarity())
            //(1 + 1f/t.volume())
            //* (1 + 0.5 * t.originality())
        //);
//                 * (1/(1f+t.volume()))
        //* TruthIntegration.evi(t);
    }

    final Ewma condMean;

    private class MyBagClustering extends BagClustering<NALTask> {


        final AtomicBoolean busy = new AtomicBoolean(false);
        volatile long nextLearn = Long.MIN_VALUE;

        MyBagClustering() {
            super(model, CENTROIDS, CAPACITY, NAL.taskPriMerge);
        }

        /**
         * TODO move to MyBagClustering
         */
        private void learn(Deriver d) {

            long now = d.now();

            //called by only one thread at a time:
            //TODO adjust update rate according to value
            if (now >= nextLearn) {
                nextLearn = now + (long) (Math.ceil(learningPeriodDurs * (dur = d.nar.dur())));
                learn(forgetRate.asFloat(), clusterIterations.intValue());
            }
        }

        void cluster(Deriver d, NALTask x) {
            PLink<NALTask> y = this.put(x, pri(x));
            if (y == null)
                return;

            if (this.size() < condMin.intValue())
                return;

            if (!this.busy.compareAndSet(false, true))
                return;

            Lst<NALTask[]> zz;
            try {
                zz = cluster(d);
            } finally {
                this.busy.set(false);
            }

            if (!zz.isEmpty()) {
                addAll(zz, d);
                zz.clear();

//                if (d.randomBoolean(0.01f))
//                    print();
            }

        }



        private Lst<NALTask[]> cluster(Deriver d) {

            this.net.random = d.random;
            this.learn(d);
            this.net.random = null;

            float batchSizeProb = Math.min(
                    outputRatio.floatValue() / condMean.meanFloat(),
                    net.clusters.size() //safety limit
            );

            final Lst<NALTask[]> zz = new Lst<>(0);
            int batchSize = d.rng.floor(batchSizeProb);

            for (int i = 0; i < batchSize; i++) {
                TaskList cc = cluster(null, d);
                if (cc == null)
                    continue;

                cc.trimToSize();
                var c = cc.array();
                zz.add(c);
            }

            return zz;
        }


        @Nullable private TaskList cluster(@Nullable PLink<NALTask> y, Deriver d) {
            int condMin = CompoundClustering.this.condMin.intValue();

            int centroid = y != null ? this.centroid(y) :
                    (net.clusters.isEmpty() ? -1 : centroidRandom(d.rng, condMin));

            return centroid < 0 ? null :
                    cluster(y != null ? y.id : null,
                            centroid, condMin, d);
        }

        private int centroidRandom(RandomBits rng, int condMin) {
            int clusters = net.clusters.size();
            int centroid = rng.nextInt(clusters);
            int tries = clusters;
            do {
                if (net.valueCount(centroid) >= condMin)
                    return centroid;

                //probe forward
                centroid++; if (centroid >= clusters) centroid = 0;

            } while (--tries > 0);

            return -1;
        }

        @Nullable
        private TaskList cluster(@Nullable NALTask x, int centroid, int condMin, Deriver d) {
            Lst<PLink<NALTask>> _b = net.valueList(centroid);
            if (_b.size() < condMin)
                return null;

            TaskList neighbors = null; //ConjList should allow failed adds
            int clusterMax = condMax.intValue(); //allows extreme case where every neighbor has volume=1
            int volMax = d.volMax - 1;
            int volThresh = (int) Math.floor(volFractionLimit.floatValue() * volMax - clusterMax);
            int volMarginEach = 1;
            Iterator<PLink<NALTask>> b = null;

            while (b == null || b.hasNext()) {
                PLink<NALTask> c = b == null ? null : b.next();
                NALTask t = b == null ? x : c.id;
                if (t == null) {
                    //choose random
                    PLink<NALTask> s = this.bag.sample(d.rng.rng);
                    if (s == null) break;
                    t = s.id;
                }

                if (b == null || x == null || !t.equals(x)) {

                    int ccv = t.volume();

                    boolean cNeg = t.NEGATIVE();
                    if (ccv + (cNeg ? 1 : 0) < ((neighbors != null && neighbors.size() > 1) ? volThresh : volMax)) { //check again

                        if (cNeg) {
                            t = new SpecialNegTask(t).copyMeta(t);
                            ccv++;
                        }

                        if (neighbors == null) {
                            neighbors = new TaskList(clusterMax);
                        } else {
                            if (neighbors.contains((Object)t))
                                continue; //avoid duplicates
                        }

                        int volCost = ccv - volMarginEach;
                        if (volThresh - volCost < 0)
                            continue; //adding would be too large

                        volThresh -= volCost;

                        neighbors.add(t);

                        if (neighbors.size() >= clusterMax)
                            break; //done

                    }

                }

                if (b == null) {
                    b = _b.clone().shuffleThis(d.rng).iterator();
                    //net.bag.sampleUniqueIterator(d.random);
                }
            }

            return neighbors == null || neighbors.size() < condMin ? null : neighbors;
        }

        public void print() {
//            net.clusters.forEach(c -> {
//                System.out.println(c);
//            });
            bag.print();
        }

    }


}