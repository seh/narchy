package nars.func.java;

import com.google.common.collect.ImmutableSet;
import jcog.TODO;
import jcog.Util;
import jcog.data.list.Lst;
import jcog.data.map.CustomConcurrentHashMap;
import nars.$;
import nars.Term;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.atom.Int;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.util.Set;
import java.util.*;
import java.util.stream.Collectors;

import static jcog.data.map.CustomConcurrentHashMap.*;
import static nars.Op.*;


/**
 * Created by me on 8/19/15.
 */
public class DefaultTermizer implements Termizer {


	public static final Variable INSTANCE_VAR = $.varDep("instance");
	//    static final Term TRUE_TERM =
//            Atomic.the("TRUE");
//            //Bool.True;
//    static final Term FALSE_TERM =
//            TRUE_TERM.neg();
//            //Bool.False;
	static final Set<Class> classInPackageExclusions = ImmutableSet.of(
		Class.class,
		Object.class,
		Float.class,
		Double.class,
		Boolean.class,
		Character.class,
		Long.class,
		Integer.class,
		Short.class,
		Byte.class,
		Class.class
	);
	final Map<Term, Object> termToObj = new CustomConcurrentHashMap<>(STRONG, EQUALS, STRONG /*SOFT*/, IDENTITY, 64);

	/*final HashMap<Term, Object> instances = new HashMap();
	final HashMap<Object, Term> objects = new HashMap();*/
	final Map<Object, Term> objToTerm = new CustomConcurrentHashMap<>(STRONG /*SOFT*/, IDENTITY, STRONG, EQUALS, 64);

	public DefaultTermizer() {
		termToObj.put(Bool.True, true);
		termToObj.put(Bool.False, false);
		objToTerm.put(true, Bool.True);
		objToTerm.put(false, Bool.False);
	}

	private static Bool booleanToBoolTerm(boolean b) {
		return b ? Bool.True : Bool.False;
	}

	protected static Term number(Number o) {
		return $.the(o);
	}

	private static boolean reportClassInPackage(Class oc) {
		if (classInPackageExclusions.contains(oc)) return false;
        else
            return !Term.class.isAssignableFrom(oc) && !oc.isPrimitive();
    }

	/**
	 * (#arg1, #arg2, ...), #returnVar
	 */

	private static Term[] getMethodArgVariables(Method m) {
		String varPrefix = m.getName() + '_';
		int n = m.getParameterCount();
		Term args = $.p(getArgVariables(varPrefix, n));

		return m.getReturnType() == void.class ? new Term[]{
			INSTANCE_VAR,
			args
		} : new Term[]{
			INSTANCE_VAR,
			args,
			$.varDep(varPrefix + "_return")
		};
	}

	private static Term[] getArgVariables(String prefix, int numParams) {
		List<Variable> list = new Lst<>(numParams);
		for (int i = 0; i < numParams; i++)
			list.add($.varDep(prefix + i));
		return list.toArray(EmptyTermArray);
	}

	public static Term classTerm(Class c) {
		return Atomic.atom(c.getSimpleName());
	}

	public static Term termClassInPackage(Class c) {
		return $.p(termPackage(c.getPackage()), classTerm(c));
	}

	public static Term termPackage(Package p) {
		return $.p(p.getName().split("\\."));
	}

	/**
	 * generic instance target representation
	 */
	public static Term instanceTerm(Object o) {
		return $.p(System.identityHashCode(o), 36);
	}

	private static boolean cacheableInstance(Object o) {
		return true;
	}

	public void put(Term x, Object y) {
		assert (x != y);
		termToObj.put(x, y);
		objToTerm.put(y, x);
	}

	public void remove(Term x) {
		objToTerm.remove(termToObj.remove(x));
	}

	public void remove(Object x) {
		termToObj.remove(objToTerm.remove(x));
	}

	/**
	 * dereference a target to an object (but do not un-termize)
	 */
	@Override
	public @Nullable Object object(Term t) {

		if (t == NULL) return null;
		else if (t.INT())
			return Int.the(t);
		else {
			Object x = termToObj.get(t);
			return x == null ? t : x;  /** if null, return the target intance itself */
		}

	}

	@Nullable
	Term obj2term(@Nullable Object o) {
		@Nullable Term result;

		if (o == null) {
			result = NULL;
		} else if (o instanceof Term) {
			result = (Term) o;
		} else if (o instanceof String) {
			result = $.quote(o);
		} else if (o instanceof Boolean) {
			result = booleanToBoolTerm((Boolean) o);
		} else if (o instanceof Character) {
			result = $.quote(String.valueOf(o));
		} else if (o instanceof Number) {
			result = number((Number) o);
		} else if (o instanceof Class oc) {
            result = classTerm(oc);
		} else if (o instanceof Path) {
			result = $.the((Path) o);
		} else if (o instanceof URI) {
			result = $.the((URI) o);
		} else if (o instanceof URL) {
			result = $.the((URL) o);
		} else if (o instanceof int[]) {
			result = $.p((int[]) o);
		} else if (o instanceof Object[]) {
			List<Term> arg = Arrays.stream((Object[]) o).map(this::term).collect(Collectors.toList());
			result = arg.isEmpty() ? EmptyProduct : $.p(arg);
		} else if (o instanceof List) {
			if (!((Collection) o).isEmpty()) {
				Collection c = (Collection) o;

				List<Term> arg = (List<Term>) new Lst(c.size());
				for (Object x : c)
					arg.add(term(x));

				if (!arg.isEmpty()) {
					result = $.p(arg);/*} else if (o instanceof Stream) {
            return Atom.quote(o.toString().substring(17));}*/
				} else
					result = EmptyProduct;
			} else
				result = EmptyProduct;
		} else if (o instanceof Set) {
			Collection<Term> arg = (Collection<Term>) ((Collection) o).stream().map(this::term).collect(Collectors.toList());
			result = arg.isEmpty() ? EmptyProduct : SETe.the(arg);
		} else if (o instanceof Map mapo) {

            List<Term> components = new Lst(mapo.size());
			mapo.forEach((k, v) -> {

				Term tv = obj2term(v);
				Term tk = obj2term(k);

				if ((tv != null) && (tk != null)) {
					components.add(
						INH.the(tv, tk)
					);
				}
			});
			result = components.isEmpty() ? EmptyProduct : SETe.the(components);
		} else {
			result = instanceTerm(o);
		}

		return result;
	}

	protected @Nullable Term classInPackage(Term classs, @Deprecated Term packagge) {

		return null;
	}

	public Term[] terms(Object[] args) {
		return Util.map(this::term, Term[]::new, args);
	}

	@Override
	public @Nullable Term term(@Nullable Object o) {
		if (o instanceof Term)
			return (Term) o;

		if (o == null)
			return NULL;
		else if (o instanceof Boolean) {
			return booleanToBoolTerm((Boolean) o);
		} else if (o instanceof Number) {
			if (o instanceof Byte || o instanceof Short || o instanceof Integer || (o instanceof Long && Math.abs((Long) o) < Integer.MAX_VALUE - 1)) {
				return Int.the(((Number) o).intValue());
			} else if (o instanceof Float || o instanceof Double) {
				return $.the(((Number) o).doubleValue());
			} else if (o instanceof Long /* beyond an Int's capacity */) {
				return $.the(Long.toString((Long) o));
			} else {
				throw new TODO("support: " + o + " (" + o.getClass() + ')');
			}
		} else if (o instanceof String) {
			return Atomic.the((String) o);
		}

		Term y = obj2termCached(o);
		if (y != null)
			return y;


		if (o instanceof Object[])
			return PROD.the(terms((Object[]) o));

		return null;


	}

	public @Nullable Term obj2termCached(@Nullable Object o) {

		if (o == null)
			return NULL;
		if (o instanceof Term)
			return ((Term) o);
		if (o instanceof Integer) {
			return Int.the((Integer) o);
		}


		Term oe;
		if (cacheableInstance(o)) {
			oe = objToTerm.get(o);
			if (oe == null) {
				Term ob = obj2term(o);
				if (ob != null) {
					objToTerm.put(o, ob);
					return ob;
				} else
					return $.the("Object_" + System.identityHashCode(o));
			}
		} else {
			oe = obj2term(o);
		}

		return oe;
	}

	protected void onInstanceChange(Term oterm, Term prevOterm) {

	}

	protected void onInstanceOfClass(Object o, Term oterm, Term clas) {

	}

//    
//    public static <T extends Term> Map<Atomic,T> mapStaticClassFields( Class c,  Function<Field, T> each) {
//        Field[] ff = c.getFields();
//        Map<Atomic,T> t = $.newHashMap(ff.length);
//        for (Field f : ff) {
//            if (Modifier.isStatic(f.getModifiers())) {
//                T xx = each.apply(f);
//                if (xx!=null) {
//                    t.put(Atomic.the(f.getName()), xx);
//                }
//            }
//        }
//        return t;
//    }


}