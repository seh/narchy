package nars.sensor;

import jcog.Util;
import jcog.math.v2;
import jcog.signal.wave2d.Bitmap2D;
import jcog.signal.wave2d.ColorMode;
import nars.$;
import nars.Term;
import nars.game.Controlled;
import nars.game.Game;

import static java.lang.Math.round;
import static jcog.Util.lerp;
import static jcog.Util.lerpSafe;

/**
 * 2D flat Raytracing Retina
 */
public class PixelBag /* TODO extends ProxyBitmap2D */ implements Bitmap2D, Controlled<Game> {

    private final int px, py;

    /**
     * Z = 0: zoomed in all the way
     * = 1: zoomed out all the way
     */
    public final v2 pos = new v2(0.5f, 0.5f);
    private final v2 posNext = new v2(pos);
    private final Bitmap2D src;
    public float Z = 1f;
    private float Znext = Z;


    protected float panRate = 0.5f;
    protected float zoomRate =
        1;
        //0.75f;
    boolean inBoundsOnly = true;


    public final float[][] pixels;

    /* > 0 */
    public float maxZoom;

    /**
     * increase >1 to allow zoom out beyond input size (ex: thumbnail size)
     */
    private float minZoom =  1f;

    private boolean actionHorizontal = false;
    private boolean actionVertical = false;
    private boolean actionZoom = false;
    private float actionRes = 0;
    private Term actionRoot;

    //public List<ActionSignal> actions;

    public PixelBag(Bitmap2D src, int px, int py) {
        this.src = src;
        this.px = px;
        this.py = py;
        this.pixels = new float[px][py];
        this.maxZoom = 1f / Math.min(px, py); //one pixel observation
    }

    @Override
    public ColorMode mode() {
        return src.mode();
    }

    /**
     * source width, in pixels
     */
    public int sw() {
        return src.width();
    }

    /**
     * source height, in pixels
     */
    public int sh() {
        return src.height();
    }

    @Override
    public void updateBitmap() {

        src.updateBitmap();

        int sw = sw(), sh = sh();


        pos.move(posNext, panRate);


        float X = pos.x, Y = pos.y;
        float Z = this.Z = Util.lerpSafe(zoomRate, this.Z, Znext); //TODO zoom lerp

        float visibleProportion = (float) lerp(Math.sqrt(1 - Z), maxZoom, minZoom);
        float ew = visibleProportion * sw;
        float eh = visibleProportion * sh;

        float minX, maxX, minY, maxY;
        if (inBoundsOnly) {
            float mw = ew > sw ? 0 : sw - ew;
            float mh = eh > sh ? 0 : sh - eh;
            minX = X * mw;
            maxX = minX + ew;
            minY = Y * mh;
            maxY = minY + eh;
        } else {
            minX = X * sw - ew / 2;
            maxX = X * sw + ew / 2;
            minY = Y * sh - eh / 2;
            maxY = Y * sh + eh / 2;
        }

        updateClip(sw, sh, minX, maxX, minY, maxY);
    }

    private void updateClip(int sw, int sh, float minX, float maxX, float minY, float maxY) {

        float px = this.px, py = this.py;
        //float cx = px / 2f, cy = py / 2f;

        float xRange = maxX - minX, yRange = maxY - minY;

        int supersamplingX = (int) (xRange / px / 2),
            supersamplingY = (int) (yRange / py / 2);

        for (int oy = 0; oy < py; oy++) {
            float sy = lerpSafe(oy / py, minY, maxY);

            for (int ox = 0; ox < px; ox++) {

                //TODO optimize sources which are already gray (ex: 8-bit grayscale)

                float sx = lerpSafe(ox / px, minX, maxX);


                /* sampled pixels in the original image (inclusive) */
                int x1 = Math.max(0, round(sx - supersamplingX));
                int x2 = Math.min(sw - 1, round(sx + supersamplingX + 1));
                int y1 = Math.max(0, round(sy - supersamplingY));
                int y2 = Math.min(sh - 1, round(sy + supersamplingY + 1));

                float v;
                if (x1 == x2 && y1 == y2) {
                    //simple case: the pixel exactly
                    v = src.value(x1, y2);
                } //else if (x2 - x1 == 2 && y2 - y1 == 2) {
                //TODO bicubic interpolation
                // }
                else {

                    //generic n-ary interpolation
                    float samples = 0;
                    float brightSum = 0;
                    //float R = 0, G = 0, B = 0;
                    for (int esx = x1; esx <= x2; esx++) {

                        float dpx = esx - sx;

                        for (int esy = y1; esy <= y2; esy++) {

                            //TODO gaussian blur, not just flat average
                            float b = src.value(esx, esy);
                            if (b == b) {

                                float dpy = esy - sy;

                                float a = kernelFade(dpx, dpy);
                                brightSum += b * a;
                                samples += a;
                            } //else: random?
                        }
                    }

                    v = samples > 0 ? brightSum / samples : Float.NaN;
                }

                pixels[ox][oy] = v==v ? v : missing();
            }

        }

    }


    /**
     * cheap sampling approximation
     */
    private static float kernelFade(float dpx, float dpy) {
        float manhattan = Math.abs(dpx) + Math.abs(dpy);
        return 1/(1 + manhattan);

//        float cartesian = (float) Math.sqrt((dpx*dpx) + (dpy*dpy));
//        return 1/(1+cartesian);
    }

    protected float missing() {
        //return rng.nextFloat();
        return Float.NaN;
    }

    public void setMaxZoom(float maxZoom) {
        this.maxZoom = maxZoom;
    }

    public void setMinZoom(float minZoom) {
        this.minZoom = minZoom;
    }

    @Override
    public int width() {
        return px;
    }

    @Override
    public int height() {
        return py;
    }

    @Override
    public float value(int x, int y) {
        return pixels[x][y];
    }

    public float setZoom(float f) {
        Znext = Util.round(Util.unitize(f), actionRes);
        return f;
    }

    public final float setXRelative(float f) {
        posNext.x = f;
        return f;
    }
    public final float setYRelative(float f) {
        posNext.y = f;
        return f;
    }

    private float setXRelativeFromAction(float f) {
        return setXRelative(Util.round(f, actionRes));
    }

    private float setYRelativeFromAction(float f) {
        return setYRelative(Util.round(f, actionRes));
    }

    public PixelBag actions(Term actionRoot, boolean horizontal, boolean vertical, boolean zoom, float freqRes) {
        this.actionRoot = actionRoot;
        this.actionHorizontal = horizontal;
        this.actionVertical = vertical;
        this.actionZoom = zoom;
        this.actionRes = freqRes;
        return this;
    }

    @Override
    public void controlStart(Game g) {

        if (actionHorizontal)
            g.action($.inh(actionRoot, "panX"), this::setXRelativeFromAction).resolution(actionRes);
        else
            pos.x = posNext.x = 0.5f;

        if (actionVertical)
            g.action($.inh(actionRoot, "panY"), this::setYRelativeFromAction).resolution(actionRes);
        else
            pos.y = posNext.y = 0.5f;

        if (actionZoom) {
            g.action($.inh(actionRoot, "zoom"),
                    this::setZoom
                    //(float z) -> this.setZoom(z*z)
            ).resolution(actionRes);
            //minZoom = 1.5f; //expand to allow viewing the entire image as summary
        } else
            Z = Znext = 0.5f;
    }
}