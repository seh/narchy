package nars.task;

import nars.NAR;
import nars.NARS;
import nars.Task;
import nars.Term;
import nars.action.memory.Remember;
import nars.concept.TaskConcept;
import nars.table.BeliefTable;
import nars.table.TaskTable;
import nars.table.temporal.NavigableMapBeliefTable;
import nars.term.Termed;
import org.junit.jupiter.api.Test;

import static nars.$.$$;
import static nars.Op.BELIEF;
import static nars.task.TaskTest.task;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TemporalBeliefTableTest {

//	@Test void RTreeBasicOperations() {
//		testAddRememberMisc(new RTreeBeliefTable());
//	}
	@Test void SkipListBasicOperations() {
		testAddRememberMisc(new NavigableMapBeliefTable());
	}

//	@Disabled
//	@Test void reviseImpl_PP() {
//		TemporalBeliefTable r = new NavigableMapBeliefTable();
//		NAR n = NARS.shell();
//		TaskConcept X = (TaskConcept) n.conceptualize(ab);
//		r.taskCapacity(1);
//		add(r, $$("(x ==>+1 y)"), 1f,0.5f,0, 1, n);
//		assertEquals(1, r.taskCount());
//		add(r, $$("(x ==>+5 y)"), 1f,0.5f,0, 1, n);
//
//		assertEquals(0, r.taskCount());
//
//		Concept c = n.concept("(x ==> (y &&+- y))");
//		assertNotNull(c);
//		assertEquals(1, c.tasks(true,false,false,false).count());
//		String rt = c.tasks(true, false, false, false).findFirst().get().toString();
//		assertTrue(rt.contains("(x ==>+1 (y &&+4 y))"));
//
//	}
//
//	@Disabled @Test void reviseImpl_PN() {
//		reviseImpl_PN(true);
//	}
//	@Disabled @Test void reviseImpl_NP() {
//		reviseImpl_PN(false);
//	}
//
//	private static void reviseImpl_PN(boolean fwd) {
//		TemporalBeliefTable r = new NavigableMapBeliefTable();
//		NAR n = NARS.shell();
//		r.taskCapacity(1);
//		add(r, $$("(x ==>+1 y)"), fwd ? 1 : 0,0.5f,0, 1, n);
//		assertEquals(1, r.taskCount());
//		add(r, $$("(x ==>+5 y)"), fwd ? 0 : 1,0.5f,0, 1, n);
//
//		assertEquals(0, r.taskCount());
//
//		Concept c = n.concept("(x ==> ((--,y) &&+- y))");
//		assertNotNull(c);
//		assertEquals(1, c.tasks(true,false,false,false).count());
//		String rt = c.tasks(true, false, false, false).findFirst().get().toString();
//		assertTrue(rt.contains(fwd ?
//				"(x ==>+1 (y &&+4 (--,y)))" :
//				"(x ==>+1 ((--,y) &&+4 y))"));
//	}


//	@Test void SeriesBasicOperations() {
//		testAddRememberMisc(new SeriesBeliefTable(ab, true, new RingBufferTaskSeries<>(8)));
//	}

//	@Test
//	void RTreeSubsumeContainedEvent() {
//		testSubsumeContainedEvent(true, new RTreeBeliefTable());
//	}
//
//	@Test
//	void RTreeSubsumeContainedByEvent() {
//		testSubsumeContainedEvent(false, new RTreeBeliefTable());
//	}

	static void testSubsumeContainedEvent(boolean forward, BeliefTable t) {

		NAR n = NARS.shell();
		TaskConcept AB = (TaskConcept) n.conceptualize(ab);
		t.taskCapacity(4);

		long as = 0;
		long ae = 3;
		long bs = 1;
		long be = 2;
		long sameStamp = 1;

		Task x = TemporalBeliefTableTest.add(t, AB, 1f, 0.9f, forward ?  as : bs, forward ? ae : be, sameStamp, n);
		assertNotNull(x);

		Task y = TemporalBeliefTableTest.add(t, AB, 1f, 0.9f, forward ?  bs : as, forward ? be : ae, sameStamp, n);
		assertNotNull(y);

		assertEquals(1, t.taskCount());
		assertEquals(4, t.taskStream().findFirst().get().range()); //HACK task range = bounds range + 1

//		if (t instanceof RTreeBeliefTable)
//			assertEquals(3, ((RTreeBeliefTable) t).bounds().range(0));
	}

	static final Term ab = $$("a:b");


	static NALTask add(TaskTable r, Termed x, float freq, float conf, long start, long end, long evi, NAR n) {
		NALTask a = task(x.term(), BELIEF, freq, conf).time(start, start, end).evidence(evi).apply(n);
		a.pri(0.5f);
		Remember s = new Remember(n.main());
		s.input(a);
		r.remember(s);
		s.link();
		return a;
	}

	static NALTask add(TaskTable r, Termed x, float freq, float conf, long start, long end, NAR n) {
		return add(r, x, freq, conf, start, end, n.evidence()[0], n);
	}

	private static void testAddRememberMisc(BeliefTable r) {
		NAR n = NARS.shell();
		TaskConcept X = (TaskConcept) n.conceptualize(ab);
		r.taskCapacity(4);

		assertEquals(0, r.taskCount());

		Term x = X.term();
		float freq = 1f;
		float conf = 0.9f;
//        int creationTime = 1;
		int start = 1, end = 1;

		NALTask a = add(r, x, freq, conf, start, end, n);
		assertEquals(1, r.taskCount());

		Remember s = new Remember(n.main());
		s.input(a);
		r.remember(s);
		s.link();

		r.print(System.out);
		assertEquals(1, r.taskCount());

		Task b = add(r, x, 0f, 0.5f, 1, 1, n);
		assertEquals(2, r.taskCount());

		Task c = add(r, x, 0.1f, 0.9f, 2, 2, n);
		assertEquals(3, r.taskCount());

		Task d = add(r, x, 0.1f, 0.9f, 3, 4, n);
		assertEquals(4, r.taskCount());

		System.out.println("at capacity");
		r.print(System.out);


		Task e = add(r, x, 0.3f, 0.9f, 3, 4, n);

		System.out.println("\nat capacity?");
		r.print(System.out);
		r.forEachTask(System.out::println);

//        assertEquals(4, r.taskCount());

		System.out.println("after capacity compress inserting " + e.toString(true));
		r.print(System.out);
	}

//	@Test
//	void testProjection() throws Narsese.NarseseException {
//		RTreeBeliefTable r = new RTreeBeliefTable();
//
//		NAR nar = NARS.shell();
//		Term ab = $.$("a:b");
//		TaskConcept AB = (TaskConcept) nar.conceptualize(ab);
//		r.setTaskCapacity(4);
//
//		add(r, AB, 1f, 0.9f, 0, 1, nar);
//		add(r, AB, 0f, 0.9f, 2, 3, nar);
//
//
//		assertEquals("%.83;.92%" /*"%1.0;.90%"*/, r.truth(0, 0, ab, null, nar).toString());
//		assertEquals("%.67;.93%" /*"%1.0;.90%"*/, r.truth(1, 1, ab, null, nar).toString());
//		assertEquals("%1.0;.90%", r.truth(0, 1, ab, null, nar).toString());
//
//		assertEquals("%0.0;.90%", r.truth(2, 3, ab, null, nar).toString());
//		assertEquals("%0.0;.90%", r.truth(3, 3, ab, null, nar).toString());
//
//		assertEquals("%.50;.90%", r.truth(1, 2, ab, null, nar).toString());
//
//		assertEquals("%.33;.87%", r.truth(4, 4, ab, null, nar).toString());
//		assertEquals("%.35;.85%", r.truth(4, 5, ab, null, nar).toString());
//		assertEquals("%.38;.83%", r.truth(5, 5, ab, null, nar).toString());
//		assertEquals("%.40;.79%", r.truth(6, 6, ab, null, nar).toString());
//		assertEquals("%.39;.79%", r.truth(5, 8, ab, null, nar).toString());
//		assertEquals("%.44;.67%", r.truth(10, 10, ab, null, nar).toString());
//
//	}

}