package nars.table;

import nars.$;
import nars.NAR;
import nars.NARS;
import nars.Narsese;
import nars.table.temporal.NavigableMapBeliefTable;
import nars.task.NALTask;
import nars.test.TestNAR;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static nars.$.$$;
import static nars.Op.BELIEF;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NavigableMapBeliefTableTest {

    final NAR n = NARS.shell();

    @Test
    @Disabled
    void ordering_different_sizes() {
        TestNAR t = new TestNAR(n);
//        NavigableMapBeliefTable table = ((BeliefTables)t.nar.conceptualize("x").beliefs()).tableFirst(NavigableMapBeliefTable.class);

        for (int w = 0; w < 4; w++)
            for (int i = 0; i < 5; i++)
                t.believe("x", 1, 0.5f, i, i + w);

//        System.out.println(Joiner.on("\n").join(table.map.keySet()));
        //TODO test
    }
    @Test
    void scan1() throws Narsese.NarseseException {
        TestNAR n = new TestNAR(this.n);

        BeliefTables b = (BeliefTables) this.n.conceptualize("x").beliefs();
        b.tablesArrayForStore();
        NavigableMapBeliefTable table = b.tableFirst(NavigableMapBeliefTable.class);


        n.believe("x", 1, 0.5f, 0, 0);
        n.believe("x", 1, 0.5f, 1, 1);
        n.believe("x", 1, 0.5f, 3, 3);
        n.believe("x", 1, 0.5f, 7, 7);

        assertScan(table, 1, "1..1 0..0 3..3 7..7");
        assertScan(table, 3, "3..3 1..1 0..0 7..7");
    }

    private void assertScan(NavigableMapBeliefTable table, int center, String log) {
        TimeLogger l = new TimeLogger();
        table.scanNear(center, l);
        assertEquals(log, l.toString().trim());
    }

    private static final class TimeLogger implements Predicate<NALTask> {

        final StringBuilder sb = new StringBuilder(128);
        @Override
        public boolean test(NALTask t) {
            sb.append(t.start()).append("..").append(t.end()).append(" ");
            return true;
        }
        public String toString() {
            return sb.toString();
        }
    }

    @Test
    void compression1() throws Narsese.NarseseException {
        TestNAR t = new TestNAR(n);

        BeliefTables b = (BeliefTables) n.conceptualize("x").beliefs();
        b.tablesArrayForStore(); //force un-lazy

        NavigableMapBeliefTable table = b.tableFirst(NavigableMapBeliefTable.class);

        t.believe("x", 1, 0.5f, 0, 0);
        t.believe("x", 1, 0.5f, 1, 1);
        t.believe("x", 1, 0.5f, 2, 2);
        t.believe("x", 1, 0.5f, 3, 3);
        assertEquals(4, table.taskCount());

        table.taskCapacity(3);
        assertTrue(table.taskCount() < 3);
        //System.out.println( taskTableStr(table) );
    }

    @Test
    void addMerge() throws Narsese.NarseseException {
        TestNAR t = new TestNAR(n);
        BeliefTables b = (BeliefTables) n.conceptualize("x").beliefs();
        b.tablesArrayForStore(); //force un-lazy

        NavigableMapBeliefTable table = b
            .tableFirst(NavigableMapBeliefTable.class);
        table.taskCapacity(16);

        long[] stamp = n.evidence();
        //intersect merge:
        n.input(NALTask.task($$("x"),  BELIEF, $.t(1, 0.5f), 0, 1, stamp));
        assertEquals(1, table.taskCount());
        n.input(NALTask.task($$("x"),  BELIEF, $.t(1, 0.5f), 1, 2, stamp));
        System.out.println( taskTableStr(table) );

        assertEquals(1, table.taskCount());


    }
    
    private static String taskTableStr(NavigableMapBeliefTable table) {
        StringBuilder sb = new StringBuilder(1024);
        table.taskStream().forEach(z -> TaskTable.println(sb, z));
        return sb.toString();
    }
}