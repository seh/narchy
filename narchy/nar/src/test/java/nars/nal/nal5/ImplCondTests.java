package nars.nal.nal5;

import org.junit.jupiter.api.Test;

class ImplCondTests extends AbstractNAL5Test {

    private static final int cycles = 50;

    @Test
    void deduction_disj_A() {
        test.volMax(8).confMin(0.7f)
                .believe("(a ==> x)")
                .believe("((x || y) ==> z)")
                .mustBelieve(cycles, "(a ==> z)", 1.00f, 0.81f);
    }
    @Test
    void deduction_conj_A() {
        test.volMax(8).confMin(0.7f)
                .believe("(a ==> (x && y))")
                .believe("(x ==> z)")
                .mustBelieve(cycles, "(a ==> z)", 1.00f, 0.81f);
    }
    @Test
    void deduction_conj_B() {
        test.volMax(8).confMin(0.7f)
                .believe("(a ==> (--x && y))")
                .believe("(--x ==> z)")
                .mustBelieve(cycles, "(a ==> z)", 1.00f, 0.81f);
    }
    @Test
    void deduction_disj_A_dont() {
        test.volMax(8).confMin(0.7f)
                .believe("(a ==> x)")
                .believe("(--x ==> z)")
                .mustNotBelieve(cycles, "(a ==> z)");
    }
    @Test
    void deduction_disj_A_neg() {
        test.volMax(8).confMin(0.8f)
                .believe("(a ==> --x)")
                .believe("((--x || y) ==> z)")
                .mustBelieve(cycles, "(a ==> z)", 1.00f, 0.81f);
    }

    @Test
    void deduction_disj_B() {
        test.volMax(8).confMin(0.8f)
                .believe("(a ==> (x && y))")
                .believe("(x ==> z)")
                .mustBelieve(cycles, "(a ==> z)", 1.00f, 0.81f);
    }

    @Test
    void deduction_conj_disj_dont_1() {
        test.volMax(8)
                .believe("(a ==> x)")
                .believe("((x && y) ==> z)")
                .mustNotBelieve(cycles, "(a ==> z)");
    }

    @Test
    void deduction_conj_disj_dont_2() {
        test.volMax(8).confMin(6)
                .believe("(a ==> --x)")
                .believe("((x && y) ==> z)")
                .mustNotBelieve(cycles, "(a ==> z)");
    }

    @Test
    void deduction_conj_disj_dont_3() {
        test.volMax(8)
                .believe("(a ==> --x)")
                .believe("((x || y) ==> z)")
                .mustNotBelieve(cycles, "(a ==> z)");
    }


    @Test
    void deduction_conj_disj_dont_4() {
        test.volMax(8)
                .believe("(a ==> (--x && y))")
                .believe("(x ==> z)")
                .mustNotBelieve(cycles, "(a ==> z)");
    }

    @Test
    void deduction_conj_disj_dont_5() {
        test.volMax(8)
                .believe("(a ==> (x && y))")
                .believe("(--x ==> z)")
                .mustNotBelieve(cycles, "(a ==> z)");
    }



    @Test
    void induction1() {
        test.volMax(8).confMin(0.4f)
            .believe("(x ==> a)")
            .believe("((x || y) ==> z)")
            .mustBelieve(cycles, "(a ==> z)", 1.00f, 0.81f);
    }

    @Test
    void induction2() {
        test.volMax(8).confMin(0.4f)
                .believe("(--x ==> a)")
                .believe("((--x || y) ==> z)")
                .mustBelieve(cycles, "(a ==> z)", 1.00f, 0.81f);
    }

    @Test void induction_question_fwd_a() {
        test.volMax(8).confMin(0.99f)
                .input("(--x ==> a)?")
                .input("((--x || y) ==> z).")
                .mustQuestion(cycles, "(  a ==> z)")
                .mustQuestion(cycles, "(--a ==> z)")
                .mustNotQuestion(cycles, "(--a ==>+- z)")
                .mustNotQuestion(cycles, "(  a ==>+- z)")
        ;
    }
    @Test void induction_question_fwd_b() {
        test.volMax(8).confMin(0.99f)
                .input("((--x || y) ==> z)?")
                .input("(--x ==> a).")
                .mustQuestion(cycles, "(  a ==> z)")
                .mustQuestion(cycles, "(--a ==> z)")
//                .mustNotQuestion(cycles, "(--a ==>+- z)")
//                .mustNotQuestion(cycles, "(  a ==>+- z)")
        ;
    }
    @Test void conditional_intersection_1() {
        test.volMax(11)
            .believe("((&&,a,b,c)   ==> z)")
            .believe("((&&,  b,c,d) ==> z)")
            .mustBelieve(cycles, "((&&,b,c) ==> z)", 1, 0.81f)
            .mustBelieve(cycles, "((&&,(a||d),b,c) ==> z)", 1, 0.81f)
        ;
    }
    @Test void conditional_intersection_1a() {
        test.volMax(13)
                .believe("((y-->(&&,a,b,c))   ==> z)")
                .believe("((y-->(&&,  b,c,d)) ==> z)")
                .mustBelieve(cycles, "((y-->(&&,b,c)) ==> z)", 1, 0.81f)
                .mustBelieve(cycles, "((y-->(&&,(a||d),b,c)) ==> z)", 1, 0.81f)
        ;
    }
    @Test void conditional_intersection_2() {
        test.volMax(14)
            .believe("(--(&&,a,b,c)   ==> z)")
            .believe("(--(&&,  b,c,d) ==> z)")
            .mustBelieve(cycles, "(--(&&,b,c) ==> z)", 1, 0.81f)
        ;
    }
}