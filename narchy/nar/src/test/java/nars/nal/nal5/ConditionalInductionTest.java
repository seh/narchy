package nars.nal.nal5;

import org.junit.jupiter.api.Test;

class ConditionalInductionTest extends AbstractNAL5Test {

    @Test
    void conditional_induction_described() {
        test.volMax(11);
        test.confMin(0.7f);
        test.believe("<(&&,(robin --> [chirping]),(robin --> [flying])) ==> a>");
        test.believe("<(robin --> [flying]) ==> (robin --> [withBeak])>", 0.9f, 0.9f);
        test.mustBelieve(cycles, "<(&&,(robin --> [chirping]),(robin --> [withBeak])) ==> a>",
                1.00f, 0.73f);
    }

    @Test
    void conditional_induction_conj() {
        test.volMax(5).confMin(0.2f)
            .believe("((x && a) ==> z)")
            .believe("(a ==> y)", 0.9f, 0.9f)
            .mustBelieve(cycles, "((x && y) ==> z)", 0.95f, 0.73f);
    }
//    @Test
//    void conditional_multi_deduction_conj() {
//        test.volMax(5).confMin(0.2f)
//            .believe("(z ==> (x && a))")
//            .believe("(a ==> y)", 0.9f, 0.9f)
//            .mustBelieve(cycles, "(z ==> (x && y))", 0.95f, 0.73f);
//    }

    @Test
    void conditional_induction_conj_mismatch() {
        test.volMax(5).confMin(0.2f)
                .believe("((x && --a) ==> z)")
                .believe("(a ==> y)", 0.9f, 0.9f)
                .mustNotBelieve(cycles, "((x && y) ==> z)");
    }

    @Test
    void conditional_induction_conj_mismatch2() {
        test.volMax(5).confMin(0.2f)
                .believe("((x && a) ==> z)")
                .believe("(--a ==> y)", 0.9f, 0.9f)
                .mustNotBelieve(cycles, "((x && y) ==> z)");
    }

    @Test
    void conditional_induction_conj_neg() {
        test.volMax(7).confMin(0.2f)
                .believe("((x && --a) ==> z)")
                .believe("(--a ==> y)", 0.9f, 0.9f)
                .mustBelieve(cycles, "((x && y) ==> z)", 0.95f, 0.73f);
    }

    @Test
    void conditional_induction_disj() {
        test.volMax(12).confMin(0.2f)
                .believe("((x || a) ==> z)")
                .believe("(a ==> y)", 0.9f, 0.9f)
                .mustBelieve(cycles, "((x || y) ==> z)", 0.95f, 0.73f);

    }

    @Test
    void conditional_induction_weak_conj() {
        test.volMax(5).confMin(0.2f)
                .believe("((x && a) ==> z)")
                .believe("(y ==> a)", 0.9f, 0.9f)
                .mustBelieve(cycles, "((x && y) ==> z)", 0.95f, 0.42f);
    }

    @Test
    void conditional_induction_inh_conj() {
        test.volMax(9).confMin(0.7f)
                .believe("(((x && a)-->d) ==> z)")
                .believe("((a-->d) ==> (y-->d))", 0.9f, 0.9f)
                .mustBelieve(cycles, "(((x && y)-->d) ==> z)", 1.00f, 0.73f);
    }

    @Test
    void conditional_induction_conj_neg_event() {
        test.volMax(7).confMin(0.7f)
                .believe("((x && --y) ==> z)")
                .believe("(--y ==> a)", 0.9f, 0.9f)
                .mustBelieve(cycles, "((a && x) ==> z)", 0.95f, 0.73f);
    }

    @Test
    void conditional_induction_conj_neg_condition() {
        test.volMax(9).confMin(0.3f)
                .believe("((x && y) ==> z)")
                .believe("(y ==> a)", 0.1f, 0.9f)
                .mustBelieve(cycles, "((--a && x) ==> z)", 0.95f, 0.73f);
    }


}