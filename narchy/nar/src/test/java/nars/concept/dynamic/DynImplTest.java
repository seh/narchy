package nars.concept.dynamic;

import nars.*;
import nars.table.BeliefTable;
import nars.task.NALTask;
import nars.truth.PreciseTruth;
import nars.truth.Truth;
import org.junit.jupiter.api.Test;

import static nars.$.$$;
import static nars.Op.ETERNAL;
import static nars.term.util.Testing.assertEq;
import static org.junit.jupiter.api.Assertions.*;

class DynImplTest extends AbstractDynTaskTest {

    @Test void Tables() {
        assertDynamicBeliefTable("(x==>y)");
        assertDynamicBeliefTable("(y==>x)");
        assertDynamicBeliefTable("(--y==>x)");
        assertDynamicBeliefTable("(--x==>y)");
    }

    @Test
    void testEternalPosPos() throws Narsese.NarseseException {

        n.input("x.");
        n.input("y.");

        assertTruthEq(1, 0.45f, "(x==>y)");
        assertTruthEq(1, 0.45f, "(y==>x)");
//        assertEquals(
//                $.t(0, 0.31f),  //contraposition
//                /*null*/ //no evidence
//                n.beliefTruth("(--x==>y)", ETERNAL));
    }

    @Test
    void testEternalPosPosSeqEte() throws Narsese.NarseseException {
        n.input("(x &&+1 z).");
        n.input("y.");
        assertTruthEq(1, 0.45f, "((x &&+1 z)==>y)");
    }

    @Test
    void testEternalNegPosSeqEte() throws Narsese.NarseseException {
        n.input("--(x &&+1 z).");
        n.input("y.");

        assertTruthEq(1, 0.45f, "(--(x &&+1 z)==>y)");
    }

    @Test
    void testEternalNegPosSeqTmp() throws Narsese.NarseseException {
        n.input("--(x &&+1 z). |");
        n.run(2);
        n.input("y. |");
        assertTruthEq(1, 0.45f, "(--(x &&+1 z) ==>+1 y)");
    }

    @Test
    void testEternalPosPosSeqTmp() throws Narsese.NarseseException {
        n.input("(x &&+1 z). |");
        n.run(2);
        n.input("y. |");
        assertTruthEq(1, 0.45f, "((x &&+1 z) ==>+1 y)");
    }

    @Test
    void testEternalNegPos() throws Narsese.NarseseException {
        n.input("--x.");
        n.input("y.");
        assertTruthEq(1, 0.45f, "(--x==>y)");
        assertTruthEq(0, 0.45f, "(y==>x)");
        assertNull(n.beliefTruth("(x==>y)", ETERNAL)); //no evidence
        assertNull(n.beliefTruth("(--y==>x)", ETERNAL)); //no evidence
    }

    @Test
    void testEternalPosNeg_defined() throws Narsese.NarseseException {
        n.input("x.");
        n.input("--y.");
        assertTruthEq(0, 0.45f, "(x==>y)");
    }

    @Test
    void testEternalPosNeg_undefined() throws Narsese.NarseseException {
        n.input("x.");
        n.input("--y.");
        assertNull(n.beliefTruth("(--x==>y)", ETERNAL)); //no evidence
    }

    @Test
    void testEternalNegNeg() throws Narsese.NarseseException {
        n.input("--x.");
        n.input("--y.");
        assertNull(n.beliefTruth("(x==>y)", ETERNAL)); //no evidence

        assertTruthEq(0, 0.45f, "(--x==>y)");
        assertEq("((--,x)==>y)", n.belief("(--x==>y)", ETERNAL).term());
    }

    @Test
    void testEternalPosConjPosPos() throws Narsese.NarseseException {
        n.input("x1.");
        n.input("x2.");
        n.input("y.");
        assertTruthEq(1, 0.42f, "((x1&&x2)==>y)");
        assertTruthEq(1, 0.42f, "(y==>(x1&&x2))");
    }
    @Test
    void testEternalPosConjPosNeg() throws Narsese.NarseseException {
        n.input("x1.");
        n.input("--x2.");
        n.input("y.");

        assertTruthEq(1, 0.45f, "(--x2==>y)");
        assertNull(n.beliefTruth("(  x2==>y)", ETERNAL));

        assertTruthEq(1, 0.42f, "(y==>(x1 && --x2))");
        assertTruthEq(0, 0.42f, "(y==>(x1 &&   x2))");

        //assertEquals($.t(1, 0.42f), n.beliefTruth("((x1 && --x2)==>y)", ETERNAL));

    }


    @Test
    void testTemporal1() throws Narsese.NarseseException {
        n.input("x. |");
        n.run(2);
        n.input("y. |");
        {
            NALTask t = n.belief($$("(x ==>+- y)"), 1, 2);
            assertNotNull(t);
            //System.out.println(t);
            assertEquals(0, t.start());
            assertEquals(0, t.end());
            assertEquals(2, t.stamp().length);
            assertEq("(x ==>+2 y)", t.term());
            Truth tt = t.truth();
            assertEquals(1, tt.freq(), 0.01f);
			assertEquals(0.25, (float) tt.conf(), 0.20f);
        }
        //assertEquals("(x==>y). 0 %1.0;.37%", n.belief($$("(x==>y)"), 0, 0).toStringWithoutBudget());
        assertEquals("(x ==>+2 y). 0 %1.0;.45%", n.belief($$("(x==>y)"), 0, 0).toStringWithoutBudget());
        assertEquals("(x ==>+1 y). 0 %1.0;.42%", n.belief($$("(x ==>+1 y)"), 0, 0).toStringWithoutBudget());
    }

    @Test void WeakPolarity() throws Narsese.NarseseException {
        n.input("x. %0.2%");
        n.input("y.");
        {
            Task t = n.belief($$("(x==>y)"));
//            System.out.println(t);
            assertEquals("(x==>y). %1.0;.14%",t.toStringWithoutBudget());
        }
        {
            Task t = n.belief($$("(--x==>y)"));
//            System.out.println(t);
            assertEquals("((--,x)==>y). %1.0;.39%",t.toStringWithoutBudget());
        }
    }

    @Test
    void testPolarityPreference() throws Narsese.NarseseException {
        n.input("x. %0.05%");
        n.input("x. %0.95%");
        n.input("y.");

//        var results = new HashBag();
        for (int i = 0; i < 10; i++) {
            Task belief = n.belief($$("(x==>y)"));
            assertNotNull(belief);
//            results.add(belief.toStringWithoutBudget());
        }
//        System.out.println(results.toStringOfItemToCount());
    }

    @Test void compareDynamicAndDerivedImplTruth() {
//        int cycles = 32;

//        TestNAR t = new TestNAR(n);

//        NAL.DEBUG= true;
//        t.logDebug();

        float xf = 0.9f, xc = 0.8f;
        long xw = 0;
        long yw = 3;
        n.inputAt(xw, "x. | %" + xf + ";" + xc + "%"); //0.90;0.8%");
        n.inputAt(yw, "y. | %0.70;0.9%");
        n.run(10);


        /*
            $0.0 (x ==>+3 y). 0 %.70;.39% {3: 1;2}
            $0.0 (y ==>-3 x). 3 %.90;.34% {3: 1;2}
         */

        Term xy = $$("(x ==>+3 y)");
//        final @Nullable NALTask[] xy0 = new NALTask[1];
//        n.runAt(yw+1, ()->{

//
//        });
//        t.mustNotOutput(cycles,"x", BELIEF, 0, xf-0.02f, 0, 1, (s,e)->true);
//        t.mustNotOutput(cycles,"x", BELIEF, xf+0.02f, 1,0, 1, (s,e)->true);
//        t.run();

        BeliefTable t = n.conceptualizeDynamic(xy).beliefs();
        NALTask xy0 =
                //((BeliefTables) t).tableFirst(DynamicTruthTable.class)
                t.task(0, 0, xy, null, n.dur(), NAL.answer.ANSWER_CAPACITY, n);
        assertTrue(xy0.toString().endsWith("(x ==>+3 y). 0 %.70;.39%"));

    }
    @Test
    void testInductionNegativeImplSubj() throws Narsese.NarseseException {


//        n.log();
        n.input("p. | %0.8;0.9%");
        n.input("n. | %0.2;0.9%"); //n == --p
        n.run(1);
        n.input("x. | %0.7;0.9%");
        n.run(1);

        PreciseTruth P = $.t(0.7f, 0.39f);
        assertTrue(P.equals(n.beliefTruth("(  p ==>+1 x)", 0), 0.01f));
        assertTrue(P.equals(n.beliefTruth("(--n ==>+1 x)", 0), 0.01f));

        PreciseTruth N = $.t(0.7f, 0.14f);
        assertTruthEq(0.7f, 0.14f, "(  n ==>+1 x)", 0);
        assertTruthEq(0.7f, 0.14f, "(--p ==>+1 x)", 0);


    }

    private void assertTruthEq(float f, float c, String t) throws Narsese.NarseseException {
        assertTruthEq(f, c, t, ETERNAL);
    }

    private void assertTruthEq(float f, float c, String t, long when) throws Narsese.NarseseException {
        assertTrue($.t(f, c).equals(n.beliefTruth(t, when), NAL.test.TEST_EPSILON));
    }


}