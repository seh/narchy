package nars.concept.dynamic;

import nars.Narsese;
import nars.task.NALTask;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/** TODO debug */
@Disabled
class DynContrapositionTest extends AbstractDynTaskTest {

    @Test
    void contrapositionPosDyn() throws Narsese.NarseseException {
        assertDynContrapositive("((--,B)==>A)", "(A ==> B)");
    }

    @Test
    void contrapositionPosTemporal1() throws Narsese.NarseseException {
        assertDynContrapositive("((--,B) ==>-1 A)", "(A ==>+1 B)");
    }

    @Test
    void contrapositionPosTemporal2() throws Narsese.NarseseException {
        assertDynContrapositive("((--,B) ==>-2 (A &&+1 C))", "((A &&+1 C) ==>+1 B)");
    }

    @Test
    void contrapositionPosTemporal3() throws Narsese.NarseseException {
        assertDynContrapositive("((--,(B &&+1 C)) ==>-2 A)", "(A ==>+1 (B &&+1 C))");
    }

    @Test
    void contrapositionPosTemporal4() throws Narsese.NarseseException {
        assertDynContrapositive("((--,(B &&+1 C)) ==>-3 (A &&+1 D))", "((A &&+1 D) ==>+1 (B &&+1 C))");
    }

    private void assertDynContrapositive(String y, String x) throws Narsese.NarseseException {
        n.believe(x, 0.9f, 0.9f);


        NALTask b = n.belief(y);
        assertNotNull(b);
        assertEquals(y + ". %0.0;.45%", b.toStringWithoutBudget());
    }
}