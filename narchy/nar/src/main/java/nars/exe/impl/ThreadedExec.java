package nars.exe.impl;

import jcog.Log;
import jcog.event.Off;
import jcog.exe.AffinityExecutor;
import jcog.exe.Exe;
import jcog.exe.WorkQueue;
import nars.NAR;
import nars.time.ScheduledTask;
import org.slf4j.Logger;

import java.util.concurrent.Semaphore;
import java.util.function.Supplier;

import static nars.Op.TIMELESS;

/**
 * N independent asynchronously looping worker threads
 */
public abstract class ThreadedExec extends MultiExec  {

    protected static final Logger logger = Log.log(ThreadedExec.class);

    static final int inputQueueCapacityPerThread = 256;

    protected final WorkQueue in;

    final boolean affinity;
    protected final AffinityExecutor exe;

    /** cached concurrency (thread count) */
//    private transient int concurrency = 1;
//    private boolean threadScaling = false;


    public ThreadedExec(int maxThreads, boolean affinity) {
        super(maxThreads);


        in = new WorkQueue(inputQueueCapacityPerThread * concurrencyMax);

        this.exe = new AffinityExecutor(maxThreads);
        this.affinity = affinity;


    }

    @Override
    public final synchronized void synch() {
        Semaphore r = exe.running;
        if (r.tryAcquire(exe.maxThreads)) {
            try {
                assert(this.exe.size() == 0);
                in.acceptAll(this::accept);
            } finally {
                r.release(exe.maxThreads);
            }
        }
    }

    public final int work() {
        return work(1);
    }

    /** called by a thread to do pending work.  completeness, in 0..1.0
     * determines the max amount of pending burden the callee will be responsible for
     * @return*/
    public final int work(float completeness) {
        return in.accept(this, completeness);
    }

    @Override
    protected final void executeSoon(ScheduledTask t) {
        t.next = TIMELESS;
        execute(t);
    }

    @Override
    protected final void execute(/*@NotNull */Object x) {
        //if (!in.relaxedOffer(x))
        if (!in.offer(x))
            executeJammed(x);
    }

    private void executeJammed(Object x) {

//        //experimental: help drain queue
//        Object helping = in.poll();
//        if (helping!=null) {
//            logger.error("{} queue jam help={}", this, helping);
//            executeNow(helping);
//        }

        //if (!in.offer(x)) { //try again
            logger.error("{} queue blocked offer={}", this, x);
            //TODO print queue contents, but only from one thread and not more than every N seconds
            accept(x); //else: execute (may deadlock)
        //}
    }



    public final int queueSize() {
        return in.size();
    }

    private void flush() {
        Object next;
        while ((next = in.poll()) != null) accept(next);
    }


    @Override
    public void starting(NAR n) {

        super.starting(n);

        exe.set(worker(), affinity);
        Exe.runLater(exe::runAll);

        //Exe.setExecutor(this); //ready

    }
    @Override
    protected void stopping(NAR nar) {
        exe.shutdownNow();
        super.stopping(nar);
    }

    protected abstract Supplier<Worker> worker();

    @Override
    public boolean delete() {
        if (super.delete()) {

//            if (Exe.executor() == this) //HACK
//                Exe.setExecutor(ForkJoinPool.commonPool()); //TODO use the actual executor replaced by the start() call instead of assuming FJP

            exe.shutdownNow();

            flush();

            return true;
        }
        return false;
    }


    protected interface Worker extends Runnable, Off {
    }

}