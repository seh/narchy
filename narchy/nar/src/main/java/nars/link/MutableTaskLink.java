package nars.link;

import jcog.Util;
import jcog.pri.Prioritized;
import jcog.pri.op.PriMerge;
import jcog.random.RandomBits;
import jcog.util.PriReturn;
import nars.$;
import nars.NAL;
import nars.Term;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.task.util.OpPri;
import nars.task.util.PuncBag;
import nars.task.util.TaskException;
import nars.term.Neg;
import nars.term.atom.Bool;
import org.eclipse.collections.api.block.function.primitive.FloatFloatToFloatFunction;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static jcog.util.PriReturn.Changed;
import static jcog.util.PriReturn.Delta;

/** base class for tasklinks which contain live, changing data */
public abstract class MutableTaskLink extends TaskLink {

	public static MutableTaskLink link(Term self) {
		return link(self, null);
	}

	/**
	 * tasklink endpoints are usualy the concept() form of a term
	 * (which could differ from the input if temporal or not normalized)
	 *
	 * source is reduced to concept
	 * target is reduced to root (not concept);
	 * 	  this is for separate links to the different
	 * 	  variable-containing unnormalized compounds
	 * */
	public static MutableTaskLink link(Term _source, Term target) {
		assert(!(target instanceof Neg));

		var source = linkComponent(_source);
		if (target == null) target = source;
		else target = (target==_source || target==source) ? source : linkComponent(target);

		return new AtomicTaskLink(source, target);
	}

	private static Term linkComponent(Term x) {
		return x.root();
		//return Image.imageNormalize(x).concept();
		//return x.concept();
	}

	/**
	 * source,target as a 2-ary subterm
	 */
	final Term from, to;
	final int hash;

//	public Termed why = null;

//	private static final VarHandle INVALID = Util.VAR(MutableTaskLink.class, "invalid", boolean.class);
//	transient volatile boolean invalid;

	MutableTaskLink(Term from, Term to) {
		if (NAL.DEBUG) assertValid(from, to);

		this.from = from;
		this.to = Util.maybeEqual(to, from); //share identical term if equal
		this.hash = Term.hashShort(from, this.to);
	}

	private static void assertValid(Term from, Term to) {
		NALTask.TASKS(from, (byte) 0, false); //asserts that it's a valid taskterm

		if (from instanceof Neg || !from.TASKABLE())
			throw new TaskException("tasklink 'from' invalid", from);
		if (to instanceof Neg || to instanceof Bool /* ... */)
			throw new TaskException("tasklink 'to' invalid", to);
	}

	MutableTaskLink(Term source, Term target, int hash) {
		super();

		this.from = source;
		this.to = target;
		this.hash = hash;
	}

	@Override
	public Term term() {
		Term f = from();
		Term t = to();
		return f.equals(t) ? f : $.pFast(f, t);
	}

	@Override
	public final Term why() {
		return null;
	}

	@Override
	public @Nullable Term other(Term x, int xHashShort, boolean reverse) {
		return !self() && ((xHashShort == (reverse ? toHash() : fromHash())) && x.equals(reverse ? to : from))
			?
			(reverse ? from : to) : null;
	}

	@Override
	public boolean self() {
		return from==to;
	}

	private int toHash() {
		return (hash >>> 16);
	}

	private int fromHash() {
		return (hash & 0xffff);
	}

	@Override
	public Premise id() {
		return this;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj instanceof TaskLink) {
			MutableTaskLink t = (MutableTaskLink) obj;
			return hash == t.hash && from.equals(t.from()) && to.equals(t.to());
		}
		return false;
	}

	@Override
	public final int hashCode() {
		return hash;
	}

	@Override
	public final Term from() {
		return from;
	}

	@Override
	public final Term to() {
		return to;
	}

	public abstract double variance();

//	@Override
//	public float pri() {
//		if ((boolean)INVALID.compareAndSet(this, true, false)) {
//			return commit();
//		} else {
//			return super.pri();
//		}
//	}

	private float commit() {
		float p = (float) (priSum() / 4);
		_pri(p);

//		//TEMPORARY
//		float t = 2*Epsilon16;
//		if (Util.equals(priPunc(Op.BELIEF), priPunc(Op.GOAL), t)
//			&& Util.equals(priPunc(Op.BELIEF), priPunc(Op.QUESTION), t)
//			&& Util.equals(priPunc(Op.BELIEF), priPunc(Op.QUEST), t)
//		)
//			Util.nop();

		return p;
	}

	@Override
	public void priAdd(float a) {
		if (Math.abs(a) < Prioritized.EPSILON)
			return; //HACK

		float p = pri();
		if (p < Prioritized.EPSILON) {
//			if (a >= Prioritized.EPSILON)
//				pri(a); //'white' fill
			return;
		}

		float prop = (p+a)/p;
		if (prop >= EPSILON)
			priMul(prop);
		else if (prop < -EPSILON)
			pri(0);
	}

	public void priScale(float p) {
		if (p == 0) {
			set(0);
			return;
		}

		float p0 = priElseZero();
		if (p0 < Prioritized.EPSILON) {
			set(p);
			return;
		}

		float factor = (float) (((double)p)/ p0);
		if (factor!=1)
			priMul(factor,factor,factor,factor); //HACK

		//TODO fill any remainder divided among components that have capacity
	}

//	@Override public final void set(float p) {
//	}

	private void _pri(float p) {
		super.set(p);
	}

	public abstract void priMul(float b, float q, float g, float Q);

	public final MutableTaskLink priMul(PuncBag punc, OpPri op) {
		return priMul(punc, op, 1);

	}
	public final MutableTaskLink priMul(PuncBag punc, OpPri op, double x) {
		double f = op.apply(from().opID());
		double t = op.apply(to().opID());
		double ft = Util.mean(f, t); //mix function

		ft *= x;

		double b = punc.belief.doubleValue() * ft;
		double q = punc.question.doubleValue() * ft;
		double g = punc.goal.doubleValue() * ft;
		double Q = punc.quest.doubleValue() * ft;

		priMul((float)b, (float)q, (float)g, (float)Q);
		return this;
	}


	@Override public @Nullable Term equalReverse(Predicate<Term> fromEquals, int fromHash, Predicate<Term> toEquals, int toHash) {
		int f = fromHash();
		return f != fromHash && f != toHash &&
			toHash() == toHash &&
			toEquals.test(this.to) &&
			!fromEquals.test(this.from) ? this.from : null;
	}



	protected final void invalidate() {
		//INVALID.setOpaque(this, true);
		commit();
	}

	@Override
	public void delete(byte punc) {
		priPunc(punc, 0);
	}

	@Override
	public final boolean delete() {
		set(0);
		return true;
	}

	protected abstract double priSum();

	/**
	 * merge a component; used internally.  does not invalidate so use the high-level methods like merge()
	 * implementations are not resonsible for calling invalidate() themselves.
	 */
	protected abstract float mergeDirect(int ith, float pri, FloatFloatToFloatFunction componentMerge, PriReturn returning);

//	/** fills all components with the provided value */
//	@Override public abstract void set(float pri);

	@Override
	public abstract String toString();

	public void priSet(TaskLink copied) {
		priSet(copied, 1.0f);
	}

	public abstract void priSet(TaskLink copied, float factor);

	/** @return this instance */
	public final MutableTaskLink priPunc(byte punc, float puncPri) {
		mergePunc(punc, puncPri, PriMerge.replace);
		return this;
	}

	@Override
	public final float merge(Premise _incoming, PriMerge merge, PriReturn returning) {

		TaskLink incoming = (TaskLink)_incoming;
		FloatFloatToFloatFunction m = merge::mergeUnitize;
		float o = switch (returning) {
			case Overflow, Delta -> mergeDelta(incoming, m);
			case Void -> mergeVoid(incoming, m);
			default -> throw new UnsupportedOperationException();
		};

		invalidate();

		return o;
	}

	/** use with caution */
	private float mergeVoid(TaskLink incoming, FloatFloatToFloatFunction m) {
		for (byte i = 0; i < 4; i++)
			mergeDirect(i, incoming.priComponent(i), m, null);
		return Float.NaN;
	}

	public final float mergeDelta(TaskLink incoming, FloatFloatToFloatFunction m) {
		double deltaSum = 0;
		for (byte i = 0; i < 4; i++)
			deltaSum += mergeDirect(i, incoming.priComponent(i), m, Delta);
		return deltaPost(deltaSum);
	}

	private float deltaPost(double deltaSum) {
		if (deltaSum != 0) invalidate();
		return (float) (deltaSum / 4);
	}

	public final float mergeDelta(float incoming, PriMerge M) {
		FloatFloatToFloatFunction m = M::mergeUnitize;
		double deltaSum = 0;
		for (byte i = 0; i < 4; i++)
			deltaSum += mergeDirect(i, incoming, m, Delta);
		return deltaPost(deltaSum);
	}
	public final float mergeDelta(float[] incoming, PriMerge M) {
		FloatFloatToFloatFunction m = M::mergeUnitize;
		double deltaSum = 0;
		for (byte i = 0; i < 4; i++)
			deltaSum += mergeDirect(i, incoming[i], m, Delta);
		return deltaPost(deltaSum);
	}

	public final void mergePunc(byte punc, float pri, PriMerge merge) {
		mergePunc(punc, pri, merge, null);
	}

	public final float mergePunc(byte punc, float pri, PriMerge merge, @Nullable PriReturn returning) {
		return mergeComponent(NALTask.i(punc), pri, merge, returning);
	}

	public final float mergeComponent(byte comp, float pri, PriMerge merge, @Nullable PriReturn returning) {

		float y = mergeDirect(comp, pri, merge::mergeUnitize, returning);

		if (y != 0 || returning != Delta) //delta==0 on individual component = unchanged
			invalidate();

		return y/4;
	}


	/** not atomic */
	public final MutableTaskLink priNormalize(float mag) {
		priMul(mag/pri());
		return this;
	}

	@Override
	public void priMul(float x) {
		//assert(x >= 0);
		if (x!=1) {

			boolean changed = false;
			//HACK not fully atomic but at least consistent
			for (int i = 0; i < 4; i++)
				changed |= this.mergeDirect(i, x, mult, Changed) != 0;

			if (changed)
				invalidate();
		}
	}


	/** TODO atomic if possible */
	public void addFill(float v, RandomBits b) {
		float p = pri();
		float overflow;
		if (p > AtomicTaskLink.EPSILON) {
			float pTarget = Math.min(1, p + v);
			if (Util.equals(p, pTarget, AtomicTaskLink.EPSILON)) return; //limit reached
			float div = pTarget / p;
			float diff = mergeDelta(div, PriMerge.and);
			overflow = v - diff;
		} else
			overflow = v;

		if (overflow >= AtomicTaskLink.EPSILON) {
			int r = b.nextBits(2);
			for (byte i = 0; i < 4; i++) {
				float diff = mergeDirect((byte) ((i + r) % 4), overflow, PriMerge.plus, Delta);
				overflow -= diff;
				if (overflow < EPSILON)
					break;
			}
			invalidate();

//			float minHeadroom = Float.POSITIVE_INFINITY, maxHeadroom = Float.NEGATIVE_INFINITY;
//			int fillable = 0;
//			for (byte i = 0; i < 4; i++) {
//				float pi = priComponent(i);
//				if (pi < 1-EPSILON) {
//					minHeadroom = Math.min(minHeadroom, pi);
//					maxHeadroom = Math.max(maxHeadroom, pi);
//					fillable++;
//				}
//			}
//			if (fillable > 0) {
//				for (byte i = 0; i < 4; i++) {
//
//				}
//			}
//

		}
	}
}