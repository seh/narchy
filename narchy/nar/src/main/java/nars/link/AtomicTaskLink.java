package nars.link;

import jcog.Util;
import jcog.pri.op.PriMerge;
import jcog.signal.tensor.AtomicFixedPoint4x16bitVector;
import jcog.util.PriReturn;
import nars.Term;
import nars.derive.reaction.Reaction;
import org.eclipse.collections.api.block.function.primitive.FloatFloatToFloatFunction;
import org.jetbrains.annotations.Nullable;


public class AtomicTaskLink extends MutableTaskLink {

    private AtomicTaskLink(Term source, Term target, int hash) {
        super(source, target, hash);
    }

    AtomicTaskLink(Term source, Term target) {
        super(source, target);
    }

    private final AtomicFixedPoint4x16bitVector punc = new AtomicFixedPoint4x16bitVector(); //OR: new MutableFloatArray(4);

    /** TODO */
    @Override public Reaction reaction() {
        return null;
    }

    @Override
    public final AtomicTaskLink clone() {
        AtomicTaskLink y = new AtomicTaskLink(from, to, hash);
        y.priSet(this);
        return y;
    }

    private AtomicTaskLink clone(Term from, Term to) {
        AtomicTaskLink y = new AtomicTaskLink(from, to);
        y.priSet(this);
//        clone.why = why;
        return y;
    }

    @Override public void priSet(TaskLink copied, float factor) {
        priSet(copied);
        priMul(factor);
    }

    @Override public void priSet(TaskLink copied) {
        punc.data(((AtomicTaskLink)copied).punc.data());
        invalidate();
    }

    @Override
    public double variance() {
        return Util.variance(punc.floatArray());
    }

    @Override
    protected double priSum() {
        return punc.sumValues();
    }

    @Override
    protected float mergeDirect(int ith, float pri, FloatFloatToFloatFunction componentMerge, @Nullable PriReturn returning) {
        return punc.merge(ith, pri, componentMerge, returning);
    }

    @Override
    public final float priComponent(byte c) {
        return punc.getAt(c);
    }

    @Override
    public void set(float pri) {
        punc.fill(pri);
        invalidate();
    }

    @Override
    public float[] priGet() {
        return punc.snapshot();
    }

    @Override
    public float[] priGet(float[] buf) {
        punc.writeTo(buf);
        return buf;
    }

    @Override
    public String toString() {
        Term f = from(), t = to();
        return toBudgetString() + ':' + punc + ' ' + (f.equals(t) ? f : (f + " " + t));
    }

    private static final FloatFloatToFloatFunction mult = PriMerge.and::mergeUnitize;
    private static final FloatFloatToFloatFunction replace = PriMerge.replace::mergeUnitize;

    @Override
    public final void priMul(float b, float q, float g, float Q) {
        AtomicFixedPoint4x16bitVector p = this.punc;
        p.merge(0, b, mult, null);
        p.merge(1, q, mult, null);
        p.merge(2, g, mult, null);
        p.merge(3, Q, mult, null);
        invalidate(); //assume changed
    }

}