package nars.link.flow;

import jcog.Util;
import jcog.data.list.Lst;
import jcog.pri.Prioritized;
import jcog.pri.bag.Bag;
import jcog.pri.op.PriMerge;
import nars.Term;
import nars.link.MutableTaskLink;
import nars.link.TaskLink;
import nars.premise.Premise;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.Random;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

/** support methods and classes for LinkFlow */
public enum LinkFlows { ;

    /** warning: this is not atomic */
    public static void activate(float[] pri, Lst<MutableTaskLink> y, Bag<Premise, Premise> b, PriMerge merge) {
        final int n = y.size();
        if (n > 1) Util.mul(1f/n, pri);

        if (Util.max(pri) < TaskLink.EPSILON)
            return;

        double deltaSum = 0;
        for (MutableTaskLink Y : y)
            deltaSum += Y.mergeDelta(pri, merge);

        if (deltaSum >= Prioritized.EPSILON)
            b.pressurize((float) deltaSum);
    }

    /** TODO use Snapshot */
    @Nullable private static Lst<MutableTaskLink> find(TaskLink x, /* TODO Iterable */ Bag<Premise, Premise> b, int max, Function<TaskLink, BiPredicate<Term, Term>> matcher, @Nullable Random rng) {
        assert(max > 0);

        BiPredicate<Term, Term> m = matcher.apply(x);
        if (m == null)
            return null; //HACK shouldnt happen

        Lst<MutableTaskLink> yy = null;
        Iterator<Premise> ii = rng!=null ? b.sampleUnique(rng) : b.iterator();
        int bs = b.size();
        while (ii.hasNext()) {
            MutableTaskLink Y = (MutableTaskLink) ii.next();
            if (m.test(Y.from(), Y.to())) {
                (yy != null ? yy : (yy = new Lst<>(Math.min(max, bs)))).add(Y);
                if (yy.size() >= max)
                    break;
            }
            bs--;
        }
        return yy;
    }

    public static void flow(Function<TaskLink, BiPredicate<Term, Term>> matcher, TaskLink x, float[/*4*/] pri, PriMerge merge, Bag<Premise, Premise> b, int max, @Nullable Random rng) {
        //x = imageNormalize(x);

        if (Util.max(pri) < TaskLink.TaskLinkEpsilon*max)
            return;

        Lst<MutableTaskLink> yy = find(x, b, max, matcher, rng);
        if (yy != null) {
            activate(pri, yy, b, merge);
            yy.delete();
        }
    }

    public static final Function<TaskLink, BiPredicate<Term, Term>> fromEqualsFrom = x -> {
        Predicate<Term> match = x.from().equals();
        return (yF, yT) -> {
            if (yF == yT) return false; // self
            return match.test(yF);
        };
    };
    public static final Function<TaskLink, BiPredicate<Term, Term>> fromEqualsFromOrTo = x -> {
        Predicate<Term> match = x.from().equals();
        return (yF, yT) -> {
            if (yF == yT) return false; // self
            return match.test(yF) || match.test(yT);
        };
    };
//
//            boolean ft; //true=yF, false=yT
//            if (match.test(yF)) ft = false;
//            else if (match.test(yT)) ft = true;
//            else return false;
//
//            return switch (/*other*/(ft ? yF : yT).op()) {
//                case CONJ, DELTA -> true;
//                case IMPL, EQ -> !ft;
//                default -> false;
//            };


    /** ensures acyclical flows */
    @Deprecated public static final Function<TaskLink, BiPredicate<Term, Term>> edgeToEdge = x -> {

        Term f = x.from(), t = x.to();
        if (f == t)
            return null; //shouldnt happen

        Predicate<Term> F = f.equals(), T = t.equals();

        return (yF, yT) -> {
            if (yF == yT) return false;
            return //F.test(yF) ||
                    F.test(yT) ||
                            T.test(yF)
                    //|| T.test(yT)
                    ;
        };
    };

}