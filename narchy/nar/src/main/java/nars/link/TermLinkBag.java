package nars.link;

import jcog.pri.PLink;
import jcog.pri.PriReference;
import jcog.pri.bag.impl.PriReferenceArrayBag;
import jcog.pri.op.PriMerge;
import nars.Term;
import nars.premise.Premise;

import java.util.Iterator;

/**
 * caches an array of tasklinks tangent to an atom
 * thread-safe but used in localized contexts, like per-concept etc
 */
public final class TermLinkBag extends PriReferenceArrayBag<Term, PriReference<Term>> {


    /**
     * enforce non-structural total ordering
     */
    private static final boolean ENFORCE_NON_STRUCTURAL =
            false;
            //true;

    public TermLinkBag(int capacity) {
        super(PriMerge.plus, capacity);
    }

    private static boolean invalid(Term y, int volMin) {
		return  y == null
				||
				//!y.TASKABLE()
				//||
				(ENFORCE_NON_STRUCTURAL && y.volume() <= volMin);
    }

    /**
     * cache an AtomLinks instance in the Concept's meta table, attached by a SoftReference
     */
    public void commit(Term x, Iterator<Premise> iterator, double pri, int ttl, float forgetRate) {

        if (!isEmpty())
            commit(forget(forgetRate)); //TODO maybe defer to before first put() attempt

        int volLimit = x.volume();

        int insertions = 0, rejections = 0, merges = 0;

        int cap = capacity();
        int xh = x.hashCodeShort();
        for ( ; iterator.hasNext() && ttl > 0; ttl--) {

           Premise t = iterator.next();

            Term y = t.other(x, xh, true);
            if (invalid(y, volLimit)) {
                y = t.other(x, xh, false);
				if (invalid(y, volLimit))
					continue;
            }

			//y = y.concept(); //HACK necessary?

			//float complxInc = 1 + (y.volume() - volLimit)/((float)volLimit);
//			double pri =
//					priFactor; //guaranteed non-zero, and maybe more fair

			PriReference<Term> a = new PLink<>(y, pri);
			PriReference<Term> b = put(a);
			if (b == null) rejections++;
			else {
				if (b == a) insertions++;
				else merges++;
				if (insertions + merges > cap)
					break; //activity exceeds capacity
			}
        }

    }


//		@Nullable Object[] ll = items();
//		int lls = Math.min(size(), ll.length);
//		if (lls == 0)
//			return null;
//		else {
//
//            int li = Roulette.selectRouletteCached(lls, i -> {
//
//				PLink<Term> x = (PLink<Term>) ll[i];
//				return x != null && filter.test(x.id) ?
//					Math.max(ScalarValue.EPSILON,
//					//x.priPunc(punc)
//					x.pri()
//					)
//					:
//					Float.NaN;
//
//			}, rng::nextFloat);
//
//            PLink<Term> l = li >= 0 ? (PLink<Term>) ll[li] : null;
//
//			return l != null ? l.id : null;
//		}


}