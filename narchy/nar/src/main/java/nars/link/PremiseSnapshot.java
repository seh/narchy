package nars.link;

import jcog.data.list.Lst;
import jcog.data.list.table.Table;
import nars.premise.Premise;

import java.util.Arrays;
import java.util.Iterator;

/** takes snapshots of a premies bag */
public class PremiseSnapshot implements Runnable, Iterable<Premise> {

	private final Table<?, Premise> active;
	protected final Lst<Premise> snapshot;
	protected transient Premise[] items = Premise.EmptyPremiseArray;

	public PremiseSnapshot(Table<?, Premise> active) {
		this.active = active;
		this.snapshot = new Lst<>(0, new Premise[active.capacity()]);
	}


	@Override
	public final void run() {
		synchronized (snapshot) {
			snapshot.clearFast();
			int c = active.capacity();
			//TODO resize bitmap
			snapshot.ensureCapacity(c);
			active.forEach(snapshot::addFast);
			items = snapshot.array();
			int s = snapshot.size();
			if (s < c)
				Arrays.fill(items, s, items.length,null); //clear remainder of array
			//commit();
		}
	}

//	abstract protected void commit();

	@Override
	public Iterator<Premise> iterator() {
		return snapshot.iterator();
	}
}
