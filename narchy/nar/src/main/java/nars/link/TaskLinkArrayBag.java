package nars.link;

import jcog.pri.bag.impl.ArrayBag;
import jcog.pri.op.PriMerge;
import jcog.util.PriReturn;
import nars.NAL;
import nars.premise.Premise;

public class TaskLinkArrayBag extends ArrayBag<Premise, Premise> {

    public TaskLinkArrayBag(int initialCapacity, PriMerge merge) {
        super(merge);
        setCapacity(initialCapacity);
    }

    @Override
    protected float merge(Premise existing, Premise incoming, float incomingPri) {
        return existing.merge(incoming, merge(), PriReturn.Delta);
    }
    @Override
    protected float sortedness() {
        return NAL.tasklinkSortedness;
    }

    @Override
    public Premise key(Premise value) {
        return value;
    }

}