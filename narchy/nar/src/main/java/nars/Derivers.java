package nars;

import nars.action.decompose.*;
import nars.action.link.STMLinker;
import nars.action.link.TermLinking;
import nars.action.link.index.CachedAdjacenctTerms;
import nars.action.link.index.EqualTangent;
import nars.action.resolve.AnswerBelief;
import nars.action.resolve.AnswerTask;
import nars.action.transform.ImageAlign;
import nars.action.transform.TemporalInduction;
import nars.action.transform.VariableIntroduction;
import nars.derive.reaction.Reactions;
import nars.focus.time.FocusTiming;
import nars.focus.time.TaskWhen;

import static nars.Op.*;
import static nars.action.resolve.Answerer.AnyTaskResolver;

/**
 * utility class and recipes for building Deriver's
 */
public class Derivers extends Reactions {

    /** standard ruleset */
    @Deprecated public static Derivers nal(int minLevel, int maxLevel, String... extraFiles) {

        Derivers r = new Derivers();

        for (int level = minLevel; level <= maxLevel; level++) {
            switch (level) {
                case 1 -> {
                    r.structural();
                    r.sim();
                }
                case 2 -> r.sets();
                case 3,5,6 -> r.procedural();
            }
        }

        r.files(extraFiles);

        return r;
    }

    /** TODO ensure called only once */
    public Derivers structural() {
        files(
                   "inh.nal"
                //,"inh.goal.nal"
                //"comparison.nal",
                //"comparison.question.nal"
        );

        addAll(
            //new ImageUnfold(),
            new ImageAlign.ImageAlignBidi()
//			new ImageAlign.ImageAlignUni_Root(),
//			new ImageAlign.ImageAlignUni_InCompound(true),
//			new ImageAlign.ImageAlignUni_InCompound(false),
        );

        return this;
    }
    /** similarity */
    public Derivers sim() {
        files(
            "sim.nal"
            ,"sim.goal.nal"
        );
        return this;
    }

    public Derivers sets() {
        files("nal2.compose.nal",
                "nal2.decompose.nal",
                "nal2.guess.nal");
        return this;
    }

    public Derivers procedural() {

        files(
                "cond.decompose.nal",
                "cond.decompose.must.nal",
                "cond.decompose.should.nal",
                "cond.decompose.would.nal",

                "nal3.question.nal",

                "contraposition.nal",
                "conversion.nal",

                "impl.syl.nal",
                "impl.syl.combine.nal",

                "impl.strong.nal",
                "impl.strongx.nal",

                "impl.compose.nal"
                //,"impl.compose.specific.nal"

              , "impl.decompose.nal"
              , "impl.decompose.self.nal"
              , "impl.decompose.specific.nal"

              , "impl.recompose.nal"

              , "analogy.anonymous.conj.nal"
              , "analogy.anonymous.impl.nal"

              , "analogy.mutate.nal"

              //,"analogy.goal.nal"

                //,"impl.decompose.inner.nal"
                //,"impl.decompose.inner.question.nal",




                //,"quest_induction.nal"
                //"equal.nal"
                //"xor.nal"
                //"impl.disj.nal"
                //"nal6.layer2.nal"
                //"nal6.mutex.nal"
        );
        return this;
    }

    public final Derivers core() {
        return core(true, true);
    }

    /**
     * standard derivation behaviors
     */
    public Derivers core(boolean temporalInduction, boolean stmLinker) {

        answerers();

        decomposers();

        termlinking();

        varIntro();

        if (temporalInduction) {
            temporalInductionImpl(
                    NAL.temporal.TEMPORAL_INDUCTION_IMPL_BOTH_SUBJS,
                    NAL.temporal.TEMPORAL_INDUCTION_IMPL_BIDI);
            temporalInductionConj(false, NAL.temporal.TEMPORAL_INDUCTION_DISJ);
        }

        if (stmLinker)
            stm();

        //OTHERS:

        //new Evaluate(),

//			new AnswerQuestionsFromConcepts.AnswerQuestionsFromTaskLinks(now)
//					.log(true)

        return this;
    }

    private void varIntro() {
        add(
                new VariableIntroduction()./*anon().*/taskPunc(true, true,
                        NAL.derive.VARIABLE_INTRODUCE_QUESTIONS,
                        NAL.derive.VARIABLE_INTRODUCE_QUESTIONS)
        );
    }

    private void termlinking() {
        add(

                new TermLinking(
                new CachedAdjacenctTerms(EqualTangent.the, false))
        );
    }

    private void stm() {
        add(new STMLinker(true, true, true, true));
    }

    private void temporalInductionConj(boolean full, boolean disj) {
        if (!full) {
            //auto-negate
            add(new TemporalInduction.ConjInduction(0, 0));

            if (disj)
                add(new TemporalInduction.DisjInduction(0, 0));
        } else {
            //disj = ignored

            addAll(
                      new TemporalInduction.ConjInduction(+1, +1)
                    , new TemporalInduction.ConjInduction(-1, +1)
                    , new TemporalInduction.ConjInduction(+1, -1)
                    , new TemporalInduction.ConjInduction(-1, -1)
            );

//					.iff(  TheTask, TermMatcher.ConjSequence.the, false)
//					.iff(TheBelief, TermMatcher.ConjSequence.the, false)
        }
    }

    private void temporalInductionImpl(boolean implBothSubjs, boolean bidi) {
        int implDir = bidi ? 0 : +1;
        if (implBothSubjs) {
            addAll(
				new TemporalInduction.ImplTemporalInduction(+1, implDir),
				new TemporalInduction.ImplTemporalInduction(-1, implDir)
            );
        } else {
            //AUTO
            add(
                new TemporalInduction.ImplTemporalInduction(0, implDir)
            );
        }
    }

    private void decomposers() {
        boolean implTwoStep = true;

        addAll(

                //default compound decomposition
                new Decompose1().taskIsNotAny(INH, SIM, IMPL, CONJ),

                new DecomposeCond().bidi(),

                new DecomposeStatement(INH),
                new DecomposeStatement(SIM).bidi()
        );


        if (implTwoStep) {
            addAll(
                //2-step IMPL decompose (progressive - less combinatorial explosion)
                new DecomposeImpl(false).bidi(),
                new DecomposeCondSubterm(true, IMPL),
                new DecomposeCondSubterm(false, IMPL)
            );
        } else {
            //1-step IMPL decompose (immediate)
            add(new DecomposeImpl(true).bidi());
        }
    }

    private void answerers() {
        TaskWhen when =
            new FocusTiming();
            //new JitterTiming(when),

        addAll(

                new AnswerTask(when, AnyTaskResolver),

                /* beliefs and goals */
                new AnswerBelief(
                        true, true, true, true,
                        when, AnyTaskResolver //AnyTaskResolver
                )

                //,new AnswerQuestionsFromBeliefTable(when, true, true, AnyTaskResolver)
            );
    }
    //				case 4 -> r.files(
//					//"nal4.nal"
//					//"nal4.composition.nal" //dangerous until positioning restriction is added
//					//"nal4.hol.nal"
//					//"relation_extraction.nal"
//				);
}