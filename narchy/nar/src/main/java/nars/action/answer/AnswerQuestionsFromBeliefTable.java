package nars.action.answer;

import nars.Op;
import nars.Term;
import nars.action.resolve.Answerer;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.Reaction;
import nars.focus.time.TaskWhen;
import nars.task.NALTask;

import static nars.task.NALTask.TASKS;

@Deprecated public class AnswerQuestionsFromBeliefTable extends AnswerQuestions {

	public static final boolean ACCEPT_REFINED_QUESTION = false;

	public AnswerQuestionsFromBeliefTable(TaskWhen timing, boolean question, boolean quest, Answerer.AbstractTaskResolver resolver) {
		super(timing, question, quest, resolver);
		assert(question||quest);

		hasAny(PremiseTask, Op.VAR_QUERY, false);

//		store(false);
	}

	@Override
	protected void run(Deriver d, Cause<Reaction> why) {
		NALTask Q = d.premise.task();

		Term q = Q.term();
		Term qq = d.nar.eval(q);
		if (q==qq || TASKS(qq, (byte)0, true)) {
            NALTask A = answer(Q, qq, true, ACCEPT_REFINED_QUESTION, () -> timing.whenRelative(Q, d), d);
			if (A != null)
				react(A, d, why);
		}
	}


}