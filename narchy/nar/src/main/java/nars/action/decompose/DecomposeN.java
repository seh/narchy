package nars.action.decompose;

import jcog.random.RandomBits;
import nars.Term;
import nars.term.Compound;
import org.jetbrains.annotations.Nullable;

public class DecomposeN extends DynamicDecomposer.WeightedDynamicCompoundDecomposer {
    final int depth;

    protected DecomposeN(int depth) {
        this.depth = depth;
    }

    @Override
    public @Nullable Term apply(Compound t, RandomBits rng) {
        return sampleDynamic(t, depth, rng);
    }
}