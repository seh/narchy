package nars.action.decompose;

import jcog.random.RandomBits;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.Img;
import nars.term.Neg;
import nars.term.atom.Bool;
import org.eclipse.collections.api.block.function.primitive.FloatFunction;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiFunction;

@Deprecated public abstract class DynamicDecomposer implements BiFunction<Compound, RandomBits, Term> {

    @Nullable
    protected Term sampleDynamic(Compound x0, int depthRemain, RandomBits rng) {
        assert(!(x0 instanceof Neg));
        Compound x = x0;
        while (true) {
            Term y = subterm(x, rng);

            /* || !u.CONCEPTUALIZABLE() */
            if (depthRemain <= 1 || !(y instanceof Compound)) {
                 if (valid(y))
                     return y;
                 else
                     return x == x0 ? null : x;
            }

            depthRemain--;
            x = (Compound) y;
        }
    }

    public static boolean valid(Term y) {
        return !(y instanceof Img || y instanceof Bool);
    }

    public Term subterm(Compound x, RandomBits rng) {

        return subterm(x.subtermsDirect(), null, rng);
    }

    public Term subterm(Compound x, RandomBits rng, Subterms tt) {
        return switch (tt.subs()) {
            case 0 -> x;
            case 1 -> tt.sub(0);
            default -> subterm(tt, x, rng);
        };
    }

    protected boolean unneg() {
        return true;
    }

    /** simple subterm choice abstraction TODO a good interface providing additional context */
    public abstract Term subterm(Subterms tt, Term parent, RandomBits rng);

    public abstract static class WeightedDynamicCompoundDecomposer extends DynamicDecomposer implements FloatFunction<Term> {

        @Override
        public Term subterm(Subterms s, @Nullable Term parent, RandomBits rng) {
            Term u = s.subRoulette(this, rng.rng);
            assert(u!=null);
            return u.unneg();
        }

        @Override public float floatValueOf(Term subterm) {

            return
                1; //flat
                //(float) Math.sqrt(subterm.unneg().volume());
                //subterm.unneg().volume();
                //1f/subterm.unneg().volume(); //seems bad
        }
    }
}