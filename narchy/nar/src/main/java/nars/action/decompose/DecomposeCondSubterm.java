package nars.action.decompose;

import nars.Op;
import nars.Term;
import nars.derive.Deriver;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.Variable;
import nars.unify.constraint.TermMatch;
import org.jetbrains.annotations.Nullable;

import static nars.derive.reaction.PatternReaction.HasEvents;

/** in-place decompose of a compound subterm that is a cond */
public class DecomposeCondSubterm extends DecomposeTerm {
    private final boolean fromOrTo;

    public DecomposeCondSubterm(boolean fromOrTo, Op other) {
        super(false);
        this.fromOrTo = fromOrTo;

        Variable src = fromOrTo ? PremiseTask : PremiseBelief;
        Variable oth = fromOrTo ? PremiseBelief : PremiseTask;
        iff(src, HasEvents);
        iff(src, new TermMatch.SubsMin((short) 1));
        isAny(oth, other);

        //ensures this appplies only to the result of a previous decomposition
        in(oth, src);
    }

    @Override
    protected Term x(Premise premise) {
        return fromOrTo ? premise.from() : premise.to();
    }

    @Override
    protected void run(Term x, Term y, Deriver d) {
        Premise p = d.premise;
        Term f, t;
        if (fromOrTo) {
            if (!NALTask.TASKS(y))
                return;
            f = y; t = p.to();
        } else {
            f = p.from(); t = y;
        }

//        if (f.equalConcept(t))
//            return;

        link(f, t, true, d);
    }

    @Override
    public @Nullable Term decompose(Compound src, Deriver d) {
        return DecomposeCond.decomposeCond(src,
                1, 0, //one layer at a time
                d.rng);
    }
}