package nars.action.decompose;

import nars.Op;
import nars.Term;
import nars.derive.Deriver;
import nars.term.Compound;
import org.jetbrains.annotations.Nullable;

import static nars.Op.*;

public class DecomposeStatement extends DecomposeTerm {

    public DecomposeStatement() {
        this(INH, SIM, IMPL);
    }

    public DecomposeStatement(Op... ops) {
       assert(ops.length>0);
       isAny(PremiseTask, ops);
    }

    @Override public @Nullable Term decompose(Compound root, Deriver d) {
        //assert(root.STATEMENT());
        return Decompose1.decomposer.subterm(root, d.rng);
    }

}