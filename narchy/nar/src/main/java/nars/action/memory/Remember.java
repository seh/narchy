package nars.action.memory;

import jcog.signal.meter.SafeAutoCloseable;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.concept.TaskConcept;
import nars.focus.Focus;
import nars.table.TaskTable;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.time.Tense;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

/**
 * conceptualize and attempt to insert/merge a task to belief table.
 * depending on the status of the insertion, activate links
 * in some proportion of the input task's priority.
 */
public class Remember implements SafeAutoCloseable {

    /**
     * input as perceived
     */
    public NALTask input;

    /** input as finally remembered */
    public @Nullable NALTask stored;

    @Deprecated
    public Focus focus;

    public transient TaskConcept concept;


    public Remember() {

    }

    public Remember(Focus f) {
        focus(f);
    }

    public void link(SerialTask x, TaskConcept c) {
        input = stored = x;
        concept = c;
        link();
    }


    public final Remember focus(Focus f) {
        this.focus = f;
        return this;
    }

    public final Remember input(NALTask x) {
        _input(x);

        boolean s;
        if (x instanceof SerialTask) {
            stored = x; //pretend to store it
            s = true;
        } else {
            verify(x);
            //noinspection AssignmentUsedAsCondition
            s = tryStore();
        }

        if (s)
            link();

        return this;
    }

    protected final Remember _input(NALTask x) {
        this.input = x;
        return this;
    }

    private boolean tryStore() {
        var concept = concept();
        return concept != null &&
               stored((this.concept = concept).table(input.punc(), true));
    }


    private void verify(NALTask x) {

        if (NAL.test.DEBUG_ENSURE_DITHERED_TRUTH && x.BELIEF_OR_GOAL())
            Truth.assertDithered(x.truth(), nar());

        if (NAL.test.DEBUG_ENSURE_DITHERED_DT || NAL.test.DEBUG_ENSURE_DITHERED_OCCURRENCE) {
            int d = nar().dtDither();
            if (d > 1) {
//                if (!x.isInput()) {
                if (NAL.test.DEBUG_ENSURE_DITHERED_DT)
                    Tense.assertDithered(x.term(), d);
                if (NAL.test.DEBUG_ENSURE_DITHERED_OCCURRENCE)
                    Tense.assertDithered(x, d);
//                }
            }
        }
    }

    /** TODO close() ? */
    @Override public final void close() {
        concept = null;
        clear();
        this.focus(null);
    }

    protected void clear() {
        this.stored = this.input = null;
        this.concept = null;
    }

    public void link() {
        focus.link(this);
    }


    public final Term target() {
        return concept != null ? concept.term() : stored.term();
    }


    @Override
    public String toString() {
        return Remember.class.getSimpleName() + '(' + input + ')';
    }

    @Nullable
    protected TaskConcept concept() {
        return nar().conceptualizeTask(input);
    }

    public final boolean stored() {
        return stored !=null;
    }

    public final void unstore(NALTask x) {
        if (stored == x)
            stored = null;
    }

    public final void store(NALTask x) {
        stored = x;
    }

    @Nullable /* HACK */ public NAR nar() {
        Focus f = this.focus;
        return f!=null ? f.nar : null;
    }

    /** current time */
    public final long time() {
        return nar().time();
    }

    public final float dur() {
        return nar().dur();
        //return what.dur();
    }


    /** materializes the task, if necessary */
    public final NALTask the() {
        return this.input.the();
    }


    public final void rememberNext(NALTask z) {
        input(z);
        clear();
    }

    public final boolean stored(TaskTable t) {
        stored = null; //HACK to be sure
        t.remember(this);
        return stored !=null;
    }

    public final float pri() {
        return input.priElseZero();
    }

    @Deprecated public final int dtDither() {
        NAR n = nar();
        return n!=null ? n.dtDither() : 1;
    }
}