package nars.action.memory;

import jcog.data.array.IntComparator;
import jcog.data.list.Lst;
import jcog.pri.bag.impl.ArrayBag;
import jcog.sort.QuickSort;
import nars.NAL;
import nars.NAR;
import nars.Op;
import nars.Term;
import nars.concept.TaskConcept;
import nars.focus.Focus;
import nars.task.NALTask;
import org.eclipse.collections.api.block.procedure.primitive.IntIntProcedure;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.stream.Stream;

import static jcog.util.ArrayUtil.swap;
import static nars.term.atom.Bool.Null;

/** batch impl */
public class RememberAll extends Remember implements IntComparator, IntIntProcedure {

    private final Lst<Term> concepts = new Lst<>(Op.EmptyTermArray);
    private final Lst<NALTask> tasks = new Lst<>(NALTask.EmptyNALTaskArray);
    private transient Term[] tmpConcepts;
    private transient NALTask[] tmpTasks;

    public RememberAll() {

    }

    public RememberAll(Stream<NALTask> t) {
        t.forEach(tasks::add);
    }


    public final RememberAll commit(Focus f) {
        if (NAL.REMEMBER_BATCHED)
            commitBatched(f);
        else
            commitDirect(f);
        return this;
    }

    private void commitDirect(Focus f) {
        //tasks.clear(f::rememberNow); //FAILSAFE

        tasks.clear(new Remember(f)::rememberNext);
    }

    /** pre-sorts the input, for batching tasks common to concepts.
     *  WARNING: can affect attention dynamics */
    private void commitBatched(Focus f) {
        focus(f);

        try {

            NAR nar = nar();
            for (NALTask xx : tasks)
                concepts.add(xx.term().concept());

            int n = concepts.size(); assert(tasks.size()==n);
            if (n == 0)
                return;

            tmpTasks = tasks.array();
            tmpConcepts = concepts.array();
            if (n > 1)
                QuickSort.quickSort(0, n, this, this);


            Term cc = Null;
            for (int i = 0; i < n; i++) {
                Term nc = tmpConcepts[i];

                //update concept
                if (!cc.equals(nc))
                    concept = nar.conceptualizeTask(cc = nc);

                if (concept!=null) {
                    //assert(tmpTasks[i].term().concept().equals(concept.term()));
                    input(tmpTasks[i]);
                    input = stored = null;
                }
            }

        } finally {
            concepts.clear();
            tasks.clear();
            tmpConcepts = null;
            tmpTasks = null;
            concept = null;
            close();
        }
    }


    @Override
    protected @Nullable TaskConcept concept() {
        return concept;
    }

    public final RememberAll drain(ArrayBag<?, NALTask> bag, int n) {
        tasks.ensureCapacity(n);
        bag.popBatch(n, 1, tasks::add);
        return this;
    }

    public final RememberAll drain(Map<?, NALTask> m) {
        int n = m.size();
        tasks.ensureCapacity(n);
        m.values().removeIf(z -> { tasks.add(z); return true; });
        return this;
    }

    /** swapper */
    @Override public void value(int a, int b) {
        swap(tmpConcepts, a, b);
        swap(tmpTasks, a, b);
    }

    /** sorter */
    @Override public int compare(int a, int b) {
        int t =
            tmpConcepts[a].compareTo(tmpConcepts[b]);
        if (t!=0) return t;

        return compare(tmpTasks[a], tmpTasks[b]);
    }

    private static int compare(NALTask A, NALTask B) {
        int p = Byte.compare(A.punc(), B.punc());
        if (p!=0) return p;
//        int s = Long.compare(A.start(), B.start());
//        if (s!=0) return s;
//
//        int e = Long.compare(A.end(), B.end());
//        if (e!=0) return e;

        return Integer.compare(
                System.identityHashCode(A),
                System.identityHashCode(B));
    }
}