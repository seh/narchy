package nars.action;

import nars.Term;
import nars.derive.Deriver;
import nars.task.NALTask;
import nars.task.proxy.SpecialTermTask;
import nars.term.util.TermCodec;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public abstract class TaskTermTransformAction extends TaskTransformAction {
    protected Supplier<TermCodec<?,?>> codec = ()->null;

    @Nullable public abstract Term apply(Term x, Deriver d);

    /** sets anon codec */
    public TaskTermTransformAction anon() {
        codec = ()->new TermCodec.AnonTermCodec(false, true);
        return this;
    }

    @Override
    protected final NALTask transform(NALTask t, Deriver d) {
        try (TermCodec c = this.codec.get()) {
            Term x0 = t.term();
            Term x = c!=null ? c.encodeSafe(x0) : x0;
            Term y = apply(x, d);

            if (y == null || y == x0 || !y.unneg().TASKABLE()) return null; //unchanged

            Term y0 = c!=null ? c.decode(y) : y;

            return valid(x0, y0, d) ? applyTask(t, y0) : null;
        }
    }

    private boolean valid(Term x0, Term y, Deriver d) {
        Term yu = y.unneg();
        return yu.TASKABLE() && yu.volume() <= d.volMax && !x0.equals(yu);
    }

    @Nullable
    protected final NALTask applyTask(NALTask x, Term y) {
        NALTask p = SpecialTermTask.proxy(x, y, true);
        return p;
        //return p!=null ? p.copyMeta(x) : p;
    }


}