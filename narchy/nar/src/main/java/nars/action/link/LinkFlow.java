package nars.action.link;

import jcog.pri.bag.Bag;
import jcog.pri.op.PriMerge;
import jcog.signal.IntRange;
import nars.Term;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.derive.reaction.Reaction;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.link.TaskLink;
import nars.link.flow.LinkFlows;
import nars.premise.Premise;
import org.jetbrains.annotations.Nullable;

import java.util.Random;
import java.util.function.BiPredicate;
import java.util.function.Function;

/**
 * TODO parameters for punc inputs and outputs. currently this is hardcoded for goal->goal
 * TODO use TermAdjacency
 */
public class LinkFlow extends NativeReaction {

//    public final PuncPri punc = new PuncPri(0, 2);
    private final Function<TaskLink, BiPredicate<Term /* yF */, Term /* yT */>> matcher;

    /**
     * computes punctuation priority activation,
     * for the given input tasklink,
     * to be applied to matches
     *
     * ex: l.priGet(punc) */
    private final Function<TaskLink, float[]> pri;

    public PriMerge merge;

    public final IntRange spread = new IntRange(1, 1, 8);


    public LinkFlow(Function<TaskLink, BiPredicate<Term, Term>> matcher, Function<TaskLink, float[]> pri, PriMerge merge) {
        tasklink();
        taskEqualsBelief();
        this.matcher = matcher;
        this.pri = pri;
        this.merge = merge;
    }

    @Override
    protected void run(Deriver d, Cause<Reaction> why) {
        //int MAX = (int) Math.ceil(Math.pow(((GraphBagFocus)f).bag.size(), 0.3f));
        flow(TaskLink.parentLink(d.premise), d.focus, spread.getAsInt(), d.random);
    }

    public void flow(TaskLink l, Focus f, int max, Random rng) {
        flow(l, ((BagFocus)f).bag, max, rng);
    }

    public void flow(TaskLink l, Bag<Premise, Premise> b, int max, @Nullable Random rng) {
        LinkFlows.flow(matcher, l, pri.apply(l), merge, b, max, rng);
    }




}