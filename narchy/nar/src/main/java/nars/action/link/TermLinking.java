package nars.action.link;

import jcog.signal.FloatRange;
import nars.Op;
import nars.Term;
import nars.action.link.index.AdjacentTerms;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.derive.reaction.Reaction;
import nars.link.MutableTaskLink;
import nars.premise.Premise;
import nars.task.NALTask;

import java.util.Iterator;

public class TermLinking extends NativeReaction {

	/** adjacency among ONLY image-normalized targets */
	private static final boolean NON_IMAGES_ONLY = true;

	/** restrict operation to links whose belief term
	 *  is smaller (by volume) than the fixed term */
	private static final boolean TASK_VOL_GREATER_THAN_BELIEF_VOL = false;

	public final FloatRange direction = FloatRange.unit(
		0.5f //balanced
		//1 //forward only
		//0.9f
	);

	private final AdjacentTerms adj;

	private static final int MAX = 1;
//	private final boolean tasklinking;

	public TermLinking(AdjacentTerms adj) {
//		this(adj, false /* true */);
//	}
//	public TermLinking(AdjacentTerms adj, boolean tasklink) {

		this.adj = adj;

//		if (TASK_VOL_GREATER_THAN_BELIEF_VOL)
//			constrain(new VolumeCompare(PremiseTask, PremiseBelief, false, -1).neg()); //belief.volume <= task.volume

//		this.tasklinking = tasklink;
//		if (tasklink)
			tasklink();
//		else
//			taskPunc(true,true,true,true);

		isAny(PremiseBelief, Op.Conceptualizables);
		hasAny(PremiseBelief, Op.AtomicConstant); //dont bother with variable-only terms

		hasAny(PremiseBelief, Op.Variables, false); //only constant terms, because unification is not tested

		if (NON_IMAGES_ONLY)
			condition(NonImages);
	}


	@Override
	protected final void run(Deriver d, Cause<Reaction> why) {

		Premise p = d.premise;

		Term x, y;
		Term from = p.from(), to = p.to();
		int fv = from.volume(), tv = to.volume();
		boolean dir;
		if (!TASK_VOL_GREATER_THAN_BELIEF_VOL || fv == tv) {
			dir = d.randomBoolean(direction);
		} else {
			dir = (fv > tv);
		}

		if (dir) {
			//forward
			x = from;
			y = to;
		} else {
			//reverse
			x = to;
			y = from;
		}

		Iterator<Term> zz = adj.adjacent(x, y, d);
		if (zz == null) return;

		int max = MAX;
		while (zz.hasNext()) {
			Term z = zz.next();

			if (dir || !NALTask.TASKS(z)) {
				link(x, z, d, why);
			} else {
				link(z, x, d, why);
			}

			if (--max <= 0) break;
		}

	}

	protected void link(Term from, Term to, Deriver d, Cause<Reaction> why) {
//		if (tasklinking) {
			d.link(MutableTaskLink.link(from, to),
					false);
//		} else {
//			//HACK assume forward
//			Premise p = d.premise;
//			d.add(NALPremise.the(
//					p.task(),
//					to,
//					true,
//					Why.why(p, why)));
//
//		}
	}



}