package nars.action.link.index;

import nars.Term;
import nars.derive.Deriver;

import java.util.Iterator;

/**
 * implementations resolve adjacent concepts to a concept in a context by a particular strategy
 */
public abstract class AdjacentTerms {


	public abstract Iterator<Term> adjacent(Term from, Term to, Deriver d);


}