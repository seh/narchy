package nars.action;

import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.Reaction;
import nars.derive.reaction.TaskReaction;
import nars.task.NALTask;
import org.jetbrains.annotations.Nullable;

public abstract class TaskTransformAction extends TaskReaction {


	protected abstract @Nullable NALTask transform(NALTask x, Deriver d);

	@Override
	protected final void run(Deriver d, Cause<Reaction> why) {
		react(transform(d.premise.task(), d), d, why);
	}
}