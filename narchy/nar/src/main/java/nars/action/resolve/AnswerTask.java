package nars.action.resolve;

import nars.control.Cause;
import nars.control.Why;
import nars.derive.Deriver;
import nars.derive.reaction.Reaction;
import nars.focus.time.TaskWhen;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.task.NALTask;

/**
 * resolves a tasklink to a single-premise
 * TODO share common parts with BeliefResolve
 */
public class AnswerTask extends Answerer {


    public AnswerTask(TaskWhen timing, AbstractTaskResolver resolver) {
        super(timing, resolver);

        tasklink();//taskCommand();
        hasBeliefTask(false);
    }

    @Override
    protected final void run(Deriver d, Cause<Reaction> why) {

        Premise p = d.premise;

        NALTask y = resolver.resolveTask(p.from(), d.puncSample(p),
                timing.whenAbsolute(d), d);
        if (y != null) {
            d.add(NALPremise.the(y, p.self() ?
                            y.term() :
                            p.to(),
                    false, Why.why(p, y, why)));
        }
    }



}