package nars.action.transform;

import jcog.Is;
import nars.NAL;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.reaction.NativeReaction;
import nars.derive.reaction.Reaction;
import nars.derive.util.DeriverTaskify;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.task.proxy.SpecialNegTask;
import nars.term.util.transform.VariableShift;
import nars.truth.dynamic.DynConj;
import nars.truth.dynamic.DynImpl;
import nars.truth.dynamic.DynTruth;
import org.jetbrains.annotations.Nullable;

import static nars.Op.*;

/**
 * B, A, --is(B,"==>") 				   |- polarizeBelief((polarizeTask(B) ==> A)), (Belief:AbductionDD, Time:Sequence)
 * B, A, --is(A,"==>") 				   |- polarizeTask((polarizeBelief(A) ==> B)), (Belief:InductionDD, Time:Sequence)
 * B, A, --is(A,"==>"), --is(B,"==>")  |- (polarizeTask(B) && polarizeBelief(A)),  (Belief:IntersectionDD, Time:Sequence)
 */
@Is("New_riddle_of_induction")
public abstract class TemporalInduction extends NativeReaction implements TemporalComposer {

	static final boolean allowVars = true;

	protected TemporalInduction(boolean task_beliefOrGoal) {
		taskPunc(task_beliefOrGoal ? BELIEF : GOAL);

		if (!allowVars) {
			hasAny(PremiseTask, Variables, false);
			hasAny(PremiseBelief, Variables, false);
		}

		//isAny(TheBelief, Op.Taskables);
		hasBeliefTask(true);
		noOverlap();
	}

	@Override
	protected final void run(Deriver d, Cause<Reaction> why) {
		final Premise p = d.premise;
		final NALTask x = p.task();
		final NALTask y = p.belief();
		NALTask xy = induct(x, y, d);
		if (xy!=null)
			d.add(xy);
	}

	/** a will be before or during b */
	private @Nullable NALTask induct(NALTask a, NALTask b, Deriver d) {

		int av = a.term().volume();
		int bv = b.term().volume();
		int mv = d.volMax;
		if (av + bv + 1 > mv)
			return null; //too large

		if (!fwd(a, b, d)) {
			//swap
			NALTask c = a;
			a = b;
			b = c;
		}

		a = filter(a, 0); if (a == null) return null;
		b = filter(b, 1); if (b == null) return null;

		return inductTask(a, b, d);
	}

	@Nullable private NALTask inductTask(final NALTask a, NALTask b, Deriver d) {

		if (allowVars)
			b = VariableShift.varShift(b, a.term(), true, true);

		return new DeriverTaskify(model(a, b), d, a, b).taskClose();
	}

	protected NALTask filter(NALTask x, int component) {
		return x;
	}

	protected boolean fwd(NALTask a, NALTask b, Deriver d) {
		return true;
	}

	protected abstract DynTruth model(NALTask a, NALTask b);

	public static final class ImplTemporalInduction extends TemporalInduction {

		/**
		 *  +1 forward or eternal
		 *   0 auto (time ordering)
		 *  -1 reverse
		 */
		private final int dir;

		/** +1 pos subject term
		 *   0 auto (truth freq)
		 *  -1 neg subject term
		 * */
		private final int subjPolarity;


		public ImplTemporalInduction(int subjPolarity, int dir) {
			super(true);

			this.dir = dir;
			this.subjPolarity = subjPolarity;

			isNot(PremiseTask, IMPL);
			isNot(PremiseBelief, IMPL);
			//this.fwd = fwd;

			//neq(TheTask, TheBelief);

			//in IMPL, prevent causal loops and inverting oscillators:
			//  (probably since it may confuse strong deduction results)
		}

		@Override
		protected boolean fwd(NALTask a, NALTask b, Deriver d) {
			return switch (dir) {
				case  0 -> d.randomBoolean();
				case +1 -> a.start() <= b.start();
				case -1 -> b.start() <= a.start();
				default -> throw new UnsupportedOperationException();
			};
		}

		@Override
		protected DynTruth model(NALTask a, NALTask b) {
			return DynImpl.DynImpl;
		}

		@Override
		protected NALTask filter(NALTask x, int component) {
			return component == 0 &&
				   subjPolarity == -1 || (subjPolarity == 0 && x.NEGATIVE()) ?
					new SpecialNegTask(x) : x;
		}
	}


	public static final class DisjInduction extends ConjInduction {

		public DisjInduction(int polarityX, int polarityY) {
			super(polarityX, polarityY);
		}

		@Override
		protected DynTruth model(NALTask a, NALTask b) {
			return DynConj.Disj;
		}

	}
	public static class ConjInduction extends TemporalInduction {

		private final int polarityX, polarityY;

		public ConjInduction(int polarityX, int polarityY) {
			super(true);

			this.polarityX = polarityX;
			this.polarityY = polarityY;

			if (!NAL.term.CONJ_INDUCT_IMPL) {
				hasNot(PremiseTask, IMPL);
				hasNot(PremiseBelief, IMPL);
			}

		}

		@Override
		protected DynTruth model(NALTask a, NALTask b) {
			return DynConj.Conj;
		}

		@Override
		protected NALTask filter(NALTask x, int component) {
			int p = component == 0 ? polarityX : polarityY;
			return p < 0 || (p == 0 && x.NEGATIVE()) ? new SpecialNegTask(x) : x;
		}
	}

}