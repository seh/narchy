package nars.truth;

import jcog.Research;
import jcog.data.list.Lst;
import jcog.signal.meter.SafeAutoCloseable;
import jcog.sort.QuickSort;
import jcog.util.ArrayUtil;
import nars.NAR;
import nars.Term;
import nars.table.BeliefTable;
import nars.table.TaskTable;
import nars.task.NALTask;
import nars.task.util.Answer;
import nars.term.Compound;
import nars.time.Tense;
import nars.truth.dynamic.DynTruth;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static jcog.Util.filter;
import static nars.NAL.truth.DYN_DT_DITHER;

/**
 * Dynamic Taskify
 * <p>
 * uses dynamic truth models and recursive dynamic belief evaluation to compute
 * accurately truthed, timed, and evidentially stamped composite/aggregate truths
 * with varying specified or unspecified internal temporal features described by
 * a template target.
 * <p>
 * additionally tracks evidential overlap while being constructed, and provide the summation of evidence after
 */
@Research
public final class AnswerTaskify extends DynTaskify {

    private final Answer a;
    private Lst<Component> components;

    public AnswerTaskify(DynTruth model, boolean beliefOrGoal, Answer a) {
        super(model, beliefOrGoal, DYN_DT_DITHER ? a.nar.dtDither() : 0, 0);

        this.a = a;
    }

    @Override
    public void close() {
        super.close();
        if (components != null) {
            components.forEach(Component::close);
            components.delete();
            components = null;
        }
    }

    public boolean accept(Term subterm, long start, long end) {
        if (!subterm.unneg().TASKABLE())
            return false;
        else {
            if (ditherDT > 1 && model.ditherComponentOcc()) {
                start = Tense.dither(start, ditherDT);
                end   = Tense.dither(end,   ditherDT);
            }
            _components().add(new Component(subterm, start, end));
            return true;
        }
    }

    private Lst<Component> _components() {
        Lst<Component> c = this.components;
        if (c == null)
            c = this.components = new Lst<>(0, new Component[model.componentsEstimate()]);
        return c;
    }

    @Override
    public Truth taskTruth(int i) {
        Truth t = components.get(i).truthProjected;
        return t == null ? get(i).truth() : t;
    }

    @Override
    public NAR nar() {
        return a.nar;
    }

    private boolean commit() {
        byte[] o = ord();
        if (o != null) {
            Component[] c = components.array();
            NAR n = a.nar;

            //STAGE 0
            if (stage0(o, c, n)) {
                double compEviMin = model.componentEviMin(a.eviMin);

                if (stage1(o, c, n))
                    if (stage2(o, c, compEviMin))
                        return true;
            }
        }

        return false;
    }

    private boolean stage0(byte[] ord, Component[] c, NAR n) {
        for (byte b : ord) {
            if (!c[b].table(beliefOrGoal, n))
                return false;
        }
        return true;
    }

    private boolean stage1(byte[] ord, Component[] c, NAR nar) {
        int cn = ord.length;
        int subMatchCapacity = subMatchCapacity(cn);
        float matchDur = a.dur();
        Predicate<NALTask> aFilter = a.filter;

        for (byte b : ord) {
            if (!c[b].match(filter(aFilter,
                        model.preFilter(b, this)),
                    matchDur, subMatchCapacity, nar))
                return false;
        }
        return true;
    }

    private boolean stage2(byte[] ord, Component[] c, double eMin) {

        Predicate<NALTask> f = this::postFilter;

        //TODO refine
        boolean occSpecific = occSpecific();

        float truthDur = truthDur();

        for (int compOrd = 0, cn = ord.length; compOrd < cn; compOrd++) {
            byte b = ord[compOrd];

            NALTask z = c[b].taskify(f, eMin, occSpecific, truthDur);
            if (z == null)
                return false;

//            //TEMPORARY
//            if (model instanceof DynImpl)
//                System.out.println(compOrd + ": " + z.start() + " " + z.end() + "\t" + c[b].s + " " + c[b].e + "\t" + z);

            if (compOrd == 0)
                ensureCapacity(cn);

            items[b] = z;
            size++;
        }
        return true;
    }

    protected float truthDur() {
        return nar().dur();
    }

    protected boolean occSpecific() {
        return !model.loose(template);
    }

    @Nullable private byte[] ord() {
        Lst<Component> c = this.components;
        int cn = c!=null ? c.size() : 0;
        return switch (cn) {
            case 0 -> null;
            case 1 -> ArrayUtil.BYTE_ARRAY_ONLY_ZERO;
            default -> ordN(cn);
        };
    }

    private byte[] ordN(int cn) {
        var ord = ArrayUtil.byteOrdinals(cn);

        QuickSort.sort(ord, this::volume /* ascending */);

        return ord;
    }

    private int volume(int j) {
        return components.get(j).termVolume;
    }

    /** determines effort, recursively, in sub matches */
    protected int subMatchCapacity(int components) {
        int baseCapacity = a.tasks.capacity();
        return baseCapacity; //inherit from super
        //return Math.max(1, baseCapacity - 1); //decreasing one per level

//        //return Math.max(1, baseCapacity/components); //decreasing per # components
    }

    @Override
    public @Nullable NALTask task() {
        return commit() ? super.task() : null;
    }

    @Override
    protected double eviMin() {
        return a.eviMin;
    }

    /** no overlap */
    private boolean postFilter(NALTask x) {

        if (size==0) return true;

        int ii = items.length;
        NALTask[] items = this.items;

        Component[] components = this.components.array();

        for (int i = 0; i < ii; i++) {

            NALTask y = items[i];
            if (y == null)
                continue;
            if (x == y)
                return false;

            Component c = components[i];
            Predicate<NALTask> co = c.overlapping;
            if (co == null)
                co = c.overlapping = y.stampOverlapping();

            if (co.test(x))
                return false;

        }
        return true;
    }

    public NALTask task(Compound template) {
        return task(template, a.start(), a.end());
    }

    private static final class Component implements SafeAutoCloseable {
        final Term x;

        /** target start, end; may not equal the actual result's occurrence */
        final long s, e;

        final int termVolume;

        @Nullable transient Truth truthProjected;
        @Nullable transient Predicate<NALTask> overlapping;
        @Nullable transient TaskTable table;
        @Nullable transient Answer match;


        @Override public void close() {
            if (match!=null) {
                match.close();
                match = null;
            }
            table = null;
            overlapping = null;
            truthProjected = null;
        }

        Component(Term x, long s, long e) {
            this.s = s;
            this.e = e;
            this.x = x;
            this.termVolume = x.volume();
        }

        boolean table(boolean beliefOrGoal, NAR n) {
            BeliefTable table = n.table(x, beliefOrGoal);
            if (table != null && !table.isEmpty()) {
                this.table = table;
                return true;
            } else
                return false;
        }

        /** match answer */
        boolean match(Predicate<NALTask> filter, float dur, int subMatchCapacity, NAR nar) {
            TaskTable t = this.table;
            this.table = null; //release
            Answer a = t.match(s, e, x.unneg(), filter, dur, subMatchCapacity, nar);


            if (invalid(a)) {
                if (a!=null)
                    a.close();
                return false; //no chance
            } else {
                this.match = a;
                return true;
            }
        }

        /** generate task */
        @Nullable NALTask taskify(Predicate<NALTask> filter, double componentEviMin, boolean occSpecific, float dur) {
            Answer a = this.match;
            this.match = null; /* detach: to help GC if possible */
            return a.taskify(x, filter, componentEviMin, occSpecific, dur, a.tasks.capacity() /* TODO ? */);
        }

        private static boolean invalid(Answer a) {
            return a == null || a.isEmpty()
                //|| (componentEviMin > NAL.truth.EVI_MIN && !Util.sumExceeds(NALTask::evi, componentEviMin, a.tasks)) ////compare to most optimistic case: sum component evidence
            ;
        }

    }

}