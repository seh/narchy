package nars.truth.dynamic;

import nars.truth.DynTaskify;
import nars.truth.MutableTruth;
import nars.truth.Truth;
import nars.truth.func.NALTruth;

public abstract class DynSect extends DynTruth {

    @Override
    public final Truth truth(DynTaskify l) {
        MutableTruth y = null;
        NALTruth truthFn = truthFn();
        boolean negComponents = truthNegComponents();

        for (int i = 0, dSize = l.size(); i < dSize; i++) {
            Truth x = l.taskTruth(i);

            if (y == null) {
                y = new MutableTruth(x);
                if (negComponents)
                    y.negThis();
            } else {
                Truth yy = truthFn.truth(y, negComponents ? x.neg() : x);
                if (yy == null)
                    return null;
                y.set(yy);
            }
        }

        return negResult() ? y.negThis() : y;
    }

    protected NALTruth truthFn() {
        return NALTruth.Intersection;
    }

    protected boolean truthNegComponents() {
        return false;
    }

    protected boolean negResult() {
        return false;
    }

    @Override
    public int componentsEstimate() {
        return 4;
    }
}