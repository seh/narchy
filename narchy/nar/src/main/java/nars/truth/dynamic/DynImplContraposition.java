package nars.truth.dynamic;

import jcog.util.ObjectLongLongPredicate;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.Neg;
import nars.time.Tense;
import nars.truth.DynTaskify;
import nars.truth.Truth;
import nars.truth.func.NALTruth;

import static nars.Op.DTERNAL;
import static nars.Op.IMPL;

/*
TODO debug

contraposition {
// Contraposition
//    a→b == ¬b→¬a		# contra positition [Lex contrapositionis]
// "If not smoking causes health, being not healthy may be the result of smoking"
// "If smoking causes not health, being healthy may be the result of not smoking"
//    contraposition truth fn inverts frequency, so (--B ==> --A) is wrong
    (--B ==> A), (A ==> B), task("?"), --isVar(B) |- (--B ==> A), (Belief:Contraposition, Punctuation:Belief, Time:Belief)
 */
public class DynImplContraposition extends DynTruth {

    public static final DynTruth DynImplContraposition = new DynImplContraposition();

    private DynImplContraposition() { }

    @Override
    public boolean loose(Compound template) {
        return true;
    }

    @Override
    public boolean ditherComponentOcc() {
        return true;
    }

    public static boolean validSubject(Term s) {
        return s instanceof Neg;
        // && !s.unneg().IMPL() /* prevent recursive impl */;
    }

    @Override
    public Truth truth(DynTaskify d) {
        Truth truth = d.get(0).truth();
        return NALTruth.Contraposition.apply(null, truth, (float) NAL.truth.CONF_MIN);
        //return neg(NALTruth.Contraposition.apply(null, truth.neg(), (float) NAL.truth.CONF_MIN));
    }

    @Override
    public boolean decompose(Compound x, long start, long end, ObjectLongLongPredicate<Term> each) {
        Subterms xx = x.subterms();
        Term subj = xx.subUnneg(0); //assert(x.sub(0) instanceof Neg);
        Term pred = xx.sub(1);

        int sdt = x.dt(); if (sdt==DTERNAL) sdt = 0; //HACK
        int dt = (sdt== Op.XTERNAL) ?
                Op.XTERNAL :
                sdt + subj.seqDur() - pred.seqDur();

        Term y = IMPL.the(pred, dt, subj);
        if (!y.IMPL())
            return false; //failure to construct

        //int shift = ((dt == XTERNAL) ? 0 : dt) + sRange; //TODO test

        return each.accept(y, start /*- shift*/, end /*- shift*/);
//        throw new TODO();
    }


//    @Override public long[] occ(DynTaskify d) {
//        long[] se = super.occ(d);
//        if (se[0]!= Op.ETERNAL) {
//            Term x = d.get(0).term();
//            int dt = x.dt();
//            int shift = ((dt == Op.DTERNAL) ? 0 : dt) + ((Compound)x).seqDurSub(0);
//            if (shift!=0) {
//                se[0] += shift; se[1] += shift;
//            }
//        }
//        return se;
//    }

    @Override
    public Term recompose(Compound superterm, DynTaskify d) {
        Term x = d.get(0).term();
        Term subj = x.sub(0), pred = x.sub(1);

        int sdt = x.dt();
        int dt = (sdt== Op.XTERNAL) ?
                Op.XTERNAL :
                Tense.dither(-(sdt == DTERNAL ? 0 : sdt) - subj.seqDur() - pred.seqDur(), d.ditherDT);

        return IMPL.the(pred.neg(), dt, subj);
    }

    @Override
    public int componentsEstimate() {
        return 1;
    }
}