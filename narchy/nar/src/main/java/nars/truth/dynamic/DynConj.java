package nars.truth.dynamic;

import jcog.Util;
import jcog.data.bit.MetalBitSet;
import jcog.util.ObjectLongLongPredicate;
import nars.Term;
import nars.subterm.Subterms;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.Variable;
import nars.term.atom.Bool;
import nars.term.util.conj.ConjBuilder;
import nars.term.util.conj.ConjBundle;
import nars.term.util.conj.ConjList;
import nars.time.Tense;
import nars.truth.DynTaskify;
import org.eclipse.collections.api.block.predicate.primitive.LongObjectPredicate;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;

import static nars.Op.*;
import static nars.time.Tense.dither;

public class DynConj extends DynSect {

	public static final DynConj Conj = new DynConj(true);

	public static final DynConj Disj = new DynConj(true) {
		@Override
		protected boolean truthNegComponents() {
			return true;
		}

		@Override
		protected boolean negResult() {
			return true;
		}
	};

	private final boolean loose;

	private DynConj(boolean loose) {  this.loose = loose; }

	@Override
	public boolean loose(Compound template) {
		return loose;
	}

	@Override
	public boolean ditherComponentOcc() {
		return true;
	}

	@Override
	public long[] occ(DynTaskify d) {
		long s = d.earliestStart();
		return new long[] { s, s + d.rangeIntersection()};
	}

	@Override
	public Term recompose(Compound superterm, DynTaskify d) {

		//long[] occ = occ(d);
//			long start = occ[0], endEarly, endLate, r;
//			boolean seqEternal = start == Op.ETERNAL;
//			if (!seqEternal) {
//				r = occ[1] - start;
//				endEarly = d.latestStart() + r;
//				endLate = d.latestEnd();
//			} else {
//				endEarly = endLate = Op.ETERNAL;
//				r = 0;
//			}
//			ConjList echos = NAL.term.CONJ_RANGE_ECHO && (endLate > endEarly) ? new ConjList(d.size()) : null;

		boolean negComponents = truthNegComponents();


		ConjBuilder c =
			//new ConjTree();
			new ConjList(d.size());

		int ditherDT = d.ditherDT;
		boolean dither = ditherDT > 1;

		long ds = d.earliestStart(), de = d.latestStartPlusRange();
		if (dither) {
			ds = dither(ds, ditherDT);
			de = dither(de, ditherDT);
		}

		try {

			for (NALTask ii : d) {
				long is = ii.start();
				Term ix = ii.term();
//				//NOT WORKING 100%:
//				if (is != ETERNAL && !(ix.unneg().SEQ())) {
//					long is0 = is;
				is = dither ? dither(is, ditherDT) : is;
					if (is <= ds && dither(
							ii.end()
							//is0 + ix.seqDur()
						, ditherDT) >= de)
						is = ETERNAL;  //condition spans entire conj
//				}
				Term IX = ix.negIf(negComponents);
				if (!c.add(is, IX)) {  //TODO c.addEventNeg
//					if (NAL.DEBUG)
//						throw new TermTransformException("DynConj fail when adding component", superterm, IX);
					return null;
				}
			}


			Term y = c.term();

			if (!y.unneg().CONDS()) {
//				if (NAL.DEBUG)
//					throw new TermTransformException("DynConj fail when adding component", superterm, y);
				return null;
			}


//				if (eternals!=null) {
//					eternals.add(y);
//					y = CONJ.the((Subterms)eternals);
//					eternals.clear();
//				}
//				if (y.opID()!=expectedOp)
//					return null;

			boolean nr = negResult();
			int volMax = d.volMax() - (nr ? 1 : 0);
			if (y.volume() > volMax)
				return null;

//				if (echos != null) {
//					assert(!echos.isEmpty());
//					//try attaching echos
//					if (echos.add(start, y)) {
//						Term y2 = echos.term();
//						if (!(y2 instanceof Bool) && y2.volume() <= volMax)
//							y = y2;
//					}
//				}

			return y.negIf(nr);
		} finally {
			c.clear();
//				if (echos!=null) echos.clear();
		}
	}


	@Override
	public boolean decompose(Compound c, long start, long end, ObjectLongLongPredicate<Term> each) {

		ConjList cc = new ConjList();
		try {

			int dt = c.dt();
			boolean dtx = dt == XTERNAL;

			if (dtx && eventsXternal(cc, c.subtermsDirect(), start, end)) {
				//
			} else if (!dtx && c.SEQ()) {
				decomposeSeq(c, start==ETERNAL ? 0 : start, cc);
			} else {
				cc.addAllDirect(start, c);
			}

			if (c.hasAny(VAR_DEP) && !pairVars(cc))
				return false; //give up

			return decompose(cc, end-start, each);

		} finally {
			cc.delete();
		}
	}

	private static void decomposeSeq(Compound conj, long start, ConjList c) {
//		if (conj.hasAny(VAR_DEP) || conj.dt()==DTERNAL /* factored */) {
			decomposeSeqN(conj, start, c);
//		} else {
//			decomposeSeq2(conj, start, c);
//		}
	}

//	/** 2-ary sequence decompose */
//	private static void decomposeSeq2(Compound conj, long start, ConjList c) {
//
//
//		var cs = conj.subtermsDirect();
//		assert(cs.subs()==2);
//		Term a = conj.sub(0);
//		Term b = conj.sub(1);
//		int dt = conj.dt();
//		if (dt >= 0) {
//			c.addDirect(start, a);
//			c.addDirect(start + dt + a.seqDur(), b);
//		} else {
//			c.addDirect(start, b);
//			c.addDirect(start - dt + b.seqDur(), a);
//		}
//	}

	/** n-ary sequence decompose */
	private static void decomposeSeqN(Compound conj, long start, ConjList c) {
		c.addConjEventsDirect(start, conj, false, false, null);
	}

	private static boolean decompose(ConjList c, long range, ObjectLongLongPredicate<Term> each) {
		int n = c.size();
		if (n < 2) {
			assert(false);
			return false;
		}

		//return c.AND((ws, what) -> each.accept(what, ws, ws + range));
		long[] cw = c.when;
		Term[] ct = c.array();
		for (int i = 0; i < n; i++) {
			long w = cw[i];
			long s, e;
			if (w!=ETERNAL) {
				s = w; e = w + range;
			} else {
				s = c.whenEarliest();
				if (s == ETERNAL)
					e = ETERNAL;
				else
					e = c.whenLatest() + range;
			}
			if (!each.accept(ct[i], s, e))
				return false;
		}
		return true;
	}

	private static boolean eventsXternal(ConjList c, Subterms conj, long start, long end) {
		if (start != end && conj.subs() == 2 && conj.sub(0).equalsPN(conj.sub(1))) {
			//random temporal order for fairness
			//				Random rng =
			//					ThreadLocalRandom.current();
			//					//d.random()
			//				int z = rng.nextInt(2);
			int z = Math.abs(Util.hashCombine(System.identityHashCode(c), System.identityHashCode(conj))) % 2; //HACK GOOD
			c.ensureCapacity(2);
			c.addDirect(start, conj.sub(z));
			c.addDirect(end, conj.sub(1 - z));
			return true;
		}
		return false;
	}


//		@Nullable
//		private Compound evalEternalComponents(Compound conj, long start, long end, ObjectLongLongPredicate<Term> each) {
//			Compound seq = null;
//			int range = end == Op.ETERNAL ? 0 : conj.eventRange();
//			for (Term x : conj) {
//				if (x.SEQUENCE()) {
//					assert (seq == null) : "only one temporal in factored seq";
//					seq = (Compound) x;
//				} else {
//					if (!each.accept(x, start, end + range))
//						return null;
//				}
//			}
//			return seq;
//		}




	/**
	 * (special case)
	 * variable (#) events that need paired with other non-variable events before evaluating
	 * TODO improve this
	 */
	private static boolean pairVars(ConjList c) {
		int n = c.size();
		if (n < 2) return false;
		MetalBitSet vars = null;
		for (int i = 0; i < n; i++) {
			Term X = c.get(i);
			if (X.unneg().VAR_DEP()) {
				if (vars == null) vars = MetalBitSet.bits(n);
				vars.set(i);
			}
		}
		if (vars == null)
			return true; //no variable events

		Random rng = null;
		for (int x = 0; x < n; x++) {
			if (!vars.test(x))
				continue;
			Term X = c.get(x);
//			if (X == null)
//				continue;
			long wX = c.when(x);
			if (wX == ETERNAL)
				return false; //TODO attach to all non-eternal events.


			//choose random other event to pair with
			//TODO prefer smaller non-conj, non-disj event

			boolean removed = false;
			if (rng == null) rng = ThreadLocalRandom.current();
			long[] w = c.when;
			for (int r = 0; r < n*2; r++) { //max tries
				int y = rng.nextInt(n);
				if (y == x || vars.test(y)) continue;
				Term Y = c.get(y);
				if (Y != null) {
					long wY = w[y];
					if (wY == ETERNAL)
						continue;
					int dt = Tense.occToDT(wX - wY);
//						if (dt == XTERNAL)
//							throw new WTF();
					Term xy = CONJ.the(dt, Y, X);
					if (!(xy instanceof Bool)) {
						//vars.clear(x);
						c.setNull(x);
						removed = true;
						c.setFast(y, xy);
						w[y] += (dt >= 0) ?
							/* var after */  0 :
							/* var before */ -dt;
						break;
					}
				}
			}
			if (!removed)
				return false; //failed to pair
		}
		c.removeNulls();
		return true;
	}

	public static boolean condDecomposeable(Term c, boolean event) {
		return condDecomposeable(c, event, Integer.MAX_VALUE);
	}

	public static boolean condDecomposeable(Term c, boolean event, int maxEvents) {
		return c.CONDS()
				&& !(c.CONJ() && ((Compound)c).ORunneg(Term::EQ)) //HACK dont try to decompose when sub-event is =
				&& _conjDecomposeable(c, event, maxEvents);
	}

	/** TODO test for variable independence among the events, not just that there are <=1 variables total. */
	@Deprecated
	private static boolean _conjDecomposeable(Term c, boolean event, int maxEvents) {
		return Util.inIncl(new CondDecomposeability(c, event).events, 2, maxEvents);
	}

	private static final class CondDecomposeability implements LongObjectPredicate<Term>, Predicate<Term> {

		private final boolean event;
		int events = 0, vars = 0;

		CondDecomposeability(Term conj, boolean event) {
			this.event = event;

			boolean y;
			if (conj.CONJ()) {
				int cdt = conj.dt();
				y = ((Compound) conj).condsAND(this, 0, cdt == DTERNAL, cdt == XTERNAL);
			} else {
				//assert(conj.INH() && ConjBundle.bundled(conj));
				y = ConjBundle.eventsAND(conj,this);
			}
			if (!y) events = -1;
		}

		@Override
		public boolean test(Term x) {
			return accept(0, x);
		}

		@Override
		public boolean accept(long when, Term x) {
			x = x.unneg();

			boolean var = (x instanceof Variable);
			if (!var) {
				if (event && !x.TASKABLE())
					return false;
			}

			if (var || x.hasVars())
				if (++vars > 1) //depvar
					return false; //too many vars

			if (!var)
				events++;
			return true;
		}
	}



}