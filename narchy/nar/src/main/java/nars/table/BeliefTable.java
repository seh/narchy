package nars.table;

import jcog.TODO;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.table.eternal.EternalTable;
import nars.table.temporal.NavigableMapBeliefTable;
import nars.task.NALTask;
import nars.task.util.Answer;
import nars.time.Moment;
import nars.truth.Truth;
import nars.truth.proj.TruthProjection;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static java.lang.Float.NaN;

/**
 * A model storing, ranking, and projecting beliefs or goals (tasks with TruthValue).
 * It should iterate in top-down order (highest ranking first)
 * <p>
 * TODO make an abstract class not interface
 */
public interface BeliefTable extends TaskTable {

    BeliefTable[] EmptyBeliefTableArray = new BeliefTable[0];

    static double eternalTaskValue(NALTask eternal) {
        return eternal.evi();
    }

    static double eternalTaskValueWithOriginality(NALTask eternal) {
        return eternalTaskValue(eternal) * eternal.originality();
    }

    public static Predicate<BeliefTable> mutableTable(boolean eternal) {
        return eternal ?
                z -> z instanceof EternalTable :
                z -> z instanceof NavigableMapBeliefTable;
    }

    @Nullable
    default Truth truth(long s, long e, NAR nar) {
        return truth(s, e, nar.dur(), nar);
    }


    @Nullable
    default Truth truth(Moment w, NAR nar) {
        return truth(w.s, w.e, w.dur, nar);
    }

    @Nullable
    default Truth truth(long start, long end, float dur, NAR nar) {
        return truth(start, end, null, null, dur, nar);
    }


    //    @Nullable default Truth truth(long start, long end, @Nullable Term template, @Nullable Predicate<NALTask> filter, NAR n) {
//        return truth(start, end, template, filter,
//            0
//            //(end-start)+1 //TODO experimental
//            //n.dur()
//            , n);
//    }
    @Nullable
    default Truth truth(long start, long end, @Nullable Term template, @Nullable Predicate<NALTask> filter, float dur, NAR n) {
        return isEmpty() ? null :
                Answer.answer(true, template, NAL.answer.ANSWER_CAPACITY, start, end, dur, filter, n)
                        .eviMin(0)
                        .match(this)
                        .truth();
    }

    default float freq(long start, long end, float dur, NAR nar) {
        Truth t = truth(start, end, dur, nar);
        return t != null ? t.freq() : NaN;
    }

    default double coherency(long s, long e, float dur, NAR nar) {
        @Nullable TruthProjection t = null;
        if (!isEmpty()) {
            Answer a = Answer.answer(true, null, NAL.answer.ANSWER_CAPACITY, s, e,
                    dur, null, nar).match(this);

            t = a.isEmpty() ? null : a.truthSpecificTime(0);
        }

        throw new TODO(t.toString());
        //return t == null || !t.commit(false) ? Double.NaN : t.coherency();
    }

}










































    /* when does projecting to now not play a role? I guess there is no case,
    
    
    
    Ranker BeliefConfidenceOrOriginality = (belief, bestToBeat) -> {
        final float confidence = belief.getTruth().getConfidence();
        final float originality = belief.getOriginality();
        return or(confidence, originality);
    };*/