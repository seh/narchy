package nars.table.eternal;

import jcog.Util;
import jcog.data.list.Lst;
import jcog.sort.SortedArray;
import jcog.util.ArrayUtil;
import jcog.util.LambdaStampedLock;
import nars.NAL;
import nars.Task;
import nars.action.memory.Remember;
import nars.table.BeliefTable;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.util.Answer;
import nars.truth.Stamp;
import nars.truth.Truth;
import nars.truth.proj.IntegralTruthProjection;
import nars.truth.proj.MutableTruthProjection;
import org.eclipse.collections.api.block.function.primitive.FloatFunction;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static nars.table.BeliefTable.eternalTaskValueWithOriginality;


/**
 * Created by me on 5/7/16.
 */
public class EternalTable extends SortedArray<NALTask> implements BeliefTable, FloatFunction<NALTask> {

    private final LambdaStampedLock lock = new LambdaStampedLock();

    /** initializes with zero capacity. */
    public EternalTable() {
        super();
        items = NALTask.EmptyNALTaskArray; //HACK
        //taskCapacity(initialCapacity);
    }

    static boolean stronger(NALTask a, NALTask b) {
        return eternalTaskValueWithOriginality(b)
                <=
                eternalTaskValueWithOriginality(a);
    }

    public static boolean cantContain(NALTask input) {
        return input instanceof SerialTask || !input.ETERNAL();
    }

    @Override
    public final void forEachTask(long minT, long maxT, Consumer<? super NALTask> x) {
        forEachTask(x);
    }

    @Override
    public final void forEachTask(Consumer<? super NALTask> x) {
        forEach(x);
    }

    @Override
    public void forEach(Consumer<? super NALTask> each) {
        lock.read(() -> {
            int s = size;
            NALTask[] xx = items;
            for (int i = 0; i < s; i++)
                each.accept(xx[i]);
        });
    }

    @Override
    public final int taskCount() {
        return size();
    }

    @Override
    public final Stream<? extends NALTask> taskStream() {


        return stream();
    }

    @Override
    public void match(Answer a) {
        long r = lock.readLock();
        try {
            //lock.read(() -> whileEach(a));
            whileEach(a);
        } finally {
            lock.unlockRead(r);
        }
    }

    @Override
    public void taskCapacity(int c) {
        assert (c >= 0);

        Lst<Task> trash = null;

        long l = lock.writeLock();
        try {
            int wasCapacity = this.capacity();
            if (wasCapacity == c)
                return;

            int s = size;
            int excess = s - c;
            if (excess > 0) {
                for (int e = 0; e < excess; e++) {
                    if (!compressFast(null))
                        break; //stuck
                }

                excess = size - c;
                if (excess > 0) {
                    //evict
                    trash = new Lst<>(excess);
                    do {
                        trash.addFast(removeLast());
                    } while (--excess > 0);
                }
            }

            capacity(c);
        } finally {
            lock.unlock(l);
        }

        if (trash != null)
            trash.forEach(Task::delete);
    }

    @Override
    public NALTask[] taskArray() {

        //long l = lock.readLock();
//        try {
        int s = this.size;
        if (s == 0)
            return NALTask.EmptyNALTaskArray;
        else {
            NALTask[] list = this.items;
            return Arrays.copyOf(list, Math.min(s, list.length));
        }
//        } finally {
//            lock.unlock(l);
//        }

    }

    //    @Override
    public void clear() {

        long l = lock.writeLock();
        try {
            if (size() > 0) {

                //forEach(ScalarValue::delete);
                super.delete();
            }
        } finally {
            lock.unlock(l);
        }

    }


    /**
     * for ranking purposes.  returns negative for descending order
     */
    @Override
    public final float floatValueOf(NALTask w) {
        return (float) -eternalTaskValueWithOriginality(w);
    }

    /**
     * direct insert; not ordinarily invoked from external
     */
    public void insert(NALTask incoming) {
        int r = add(incoming, this);
        assert (r != -1);
    }

    /** weakest to weakest, first valid is taken */
    private boolean compressFast(@Nullable NAL nar) {
        int s = size();

        EternalReviser ar = new EternalReviser(nar!=null ? nar.dtDither() : 1);

        outer:
        for (int a = s - 1; a >= 1; a--) {
            NALTask A = get(a);
            ar.set(A);
            for (int b = a - 1; b >= 0; b--) {
                NALTask B = get(b);
                NALTask AB = ar.apply(B);
                if (AB != null) {
                    //assert(a > b); //remove the higher index, A, first and B remains at same index
                    remove(a);
                    remove(b);
                    if (!contains(AB))
                        insert(AB);
                    return true;
                }
            }
        }
        return false;
    }

    public final Truth truth() {
        NALTask s = first();
        return s != null ? s.truth() : null;
    }

    @Override
    public boolean remove(NALTask x, boolean delete) {

        if (cantContain(x))
            return false;

        Task removed = null;

        //TODO use optimistic read here
        long l = lock.writeLock();
        try {

            NALTask[] items = this.items;

            int index = indexOf(x, this);

            if (index != -1) {
                NALTask xx = items[index];

                if (items[index] != xx) //moved while waiting for lock, retry:
                    index = indexOf(x, this);

                if (index != -1) {
                    boolean wasRemoved = removeFast(xx, index);
                    assert (wasRemoved);
                    removed = xx;
                }
            }

        } finally {
            lock.unlock(l);
        }

        if (removed != null) {
            if (delete)
                removed.delete();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public final void remember(Remember r) {
        NALTask _x = r.input;
        if (cantContain(_x))
            return;

        long l = lock.readLock();
        try {
            int s = size;
            if (s > 0) {
                NALTask[] ii = items;
                //scan list for existing equal task //TODO containment can be quickly tested by assuming the tasks are sorted by conf, and whether the conf is in the current range
                int existing = ArrayUtil.indexOf(ii, _x, 0, Math.min(s, ii.length));
                if (existing!=-1) {
                    r.store(ii[existing]);
                    return;
                }
            }

            int c = capacity();
            if (c == 0) {
                if (s > 0)
                    clear();
                return;
            }

            if (s >= c) {
                l = Util.readToWrite(l, lock);
                if (s > 1)
                    compressFast(r.nar());
                int excess = 1 + (s = size()) - c;
                if (excess > 0) {
                    //compression failed; evict
                    NALTask weakestPresent = last();
                    if (weakestPresent != null) {
                        if (!stronger(_x, weakestPresent)) {
                            //rejected
                            r.unstore(_x);
                            if (--excess <= 0)
                                return;
                        } else {
                            evict(excess, r);
                            //continue...
                        }
                    }
                }
            }

            NALTask x = r.the();

            l = Util.readToWrite(l, lock);

            if (capacity() > 0) { //check again in case it got deleted while waiting for lock
                insert(x);
                r.store(x);
            }

        } finally {
            if (l != 0)
                lock.unlock(l);
        }

    }



    private void evict(int n, Remember r) {
        for (int i = 0; i < n; i++)
            r.unstore(removeLast());
    }

    private static final class EternalReviser implements Function<NALTask,NALTask> {


        long[] xStamp;
        private NALTask x;

        MutableTruthProjection l;
        int dtDither;

        /** ditherDT used in intermpolation */
        EternalReviser(int dtDither) {
            this.dtDither = dtDither;
        }

        public void set(NALTask x) {
            this.x = x;
            this.xStamp = x.stamp();
        }

        @Override
        public NALTask apply(NALTask y) {
            if (overlaps(y)) //quick stamp test
                return null;
            else {
                return projection(y);
            }
        }

        private NALTask projection(NALTask y) {
            if (l == null) {
                l = new IntegralTruthProjection(2);
                l.add(x);
            }

            //l.removeIf(z -> z != x); //remove all but 'x'
            l.add(y);
            return l.task();
        }

        private boolean overlaps(NALTask y) {
            return Stamp.overlapsAny(xStamp, y.stamp());
        }


//            if (Stamp.overlapsAny(inputStamp, x.stamp()))
//                return null;
//
//
//            Truth yt;
//            Term yc;
//
//            if (temporal) {
//                Term xc = x.term();
//                boolean termsDiffer = !ic.equals(xc);
//                if (termsDiffer) {
////                    if (belowCapacity) {
////                        //dont revise if terms differ
////                        //TODO choose ideal intermpolation result
////                    } else {
//                    yc = Intermpolate.intermpolate(
//                            (Compound) ic, (Compound) xc,
//                            (float) (ie / (ie + x.evi())),
//                            ditherDT
//                    );
//                    if (yc == null || (yc != ic && (!yc.TASKABLE() || !yc.root().equals(ic.root()))))
//                        return null;
//
////                            aProp = _aProp;
////                    }
//                } else {
////                        aProp = (float) (ie / (ie + x.evi()));
//                    yc = xc;
//                }
//            } else {
////                    aProp = (float) (ie / (ie + x.evi()));
//                yc = ic;
//            }
//
//            Truth xt = x.truth();
//            yt = Revision.revise(input.truth(), xt);
//            if (yt == null)
//                return null;
//
////            } else {
////				if ((!temporal || inputTerm.equals(x.term()))
////                    && Arrays.equals(xStamp, inputStamp) &&
////                        Util.equals((float) x.conf(), (float) input.conf(), nar.confResolution.floatValue())
////                        //&& !x.isCyclic() && !input.isCyclic()
////                ) {
////                    //HACK interpolate truth if only freq differs
////					yt = $.t((x.freq() + input.freq()) / 2, x.conf());
//////                    aProp = 0.5f;
////                    nt = inputTerm;
////                } else
////                    yt = null;
////            }
//
//
//            yt = yt.dither(fr, cr);
//            if ((!temporal || yc.equals(ic)) && (yt.equalTruth(xt, fr, cr) || yt.equalTruth(input.truth(), fr, cr)))
//                return null;
//
//            //a previous conclusion exists; try if by originality this one is stronger
////                if (conclusion != null && conclusion.evi() * x.originality() >= yt.evi() * x.originality())
////                    continue;
//
//            byte punc = input.punc();
//
//            NALTask revised = NALTask.taskUnsafe(
//                    NALTask.taskValid(yc, punc, yt),
//                    punc, yt, ETERNAL, ETERNAL,
//                    Stamp.merge(inputStamp, x.stamp()/*, aProp*/)
//            );

//
//            revised.pri(Math.max(x.priElseZero(), input.priElseZero())); //TODO maybe based on relative evidence
//            revised.why(Why.why(input, x));
//            return revised;


    }


}