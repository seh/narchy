package nars.table.temporal;

import jcog.Is;
import jcog.math.LongInterval;
import nars.table.BeliefTable;
import nars.task.NALTask;
import org.jetbrains.annotations.Nullable;

import java.util.DoubleSummaryStatistics;
import java.util.Iterator;
import java.util.function.Predicate;


/**
 * https://en.wikipedia.org/wiki/Compressed_sensing
 */
@Is("Compressed_sensing")
public interface TemporalBeliefTable extends BeliefTable {

    class TemporalBekiefTableStats  {
        public final LongInterval occ;
        public final int size, capacity;
        //TODO descriptive statistics: freq, conf, etc..
        public final DoubleSummaryStatistics freq, conf;

        public TemporalBekiefTableStats(LongInterval occ, int size, int capacity, Iterator<? extends NALTask> tasks) {
            this.occ = occ;
            this.size = size;
            this.capacity = capacity;
            freq = new DoubleSummaryStatistics();
            conf = new DoubleSummaryStatistics();
            for (; tasks.hasNext(); ) {
                NALTask t = tasks.next();
                var tt = t.truth();
                if (tt == null)
                    break; //assume the table is question or quest
                freq.accept(tt.freq());
                conf.accept(tt.conf());
            }
        }

        @Override
        public String toString() {
            return (occ!=null ? "@" + occ : "") +
                    " size=" + size + "/" + capacity +
                            (freq.getCount() > 0 ? (", freq=" + freq +
                    ", conf=" + conf) : "");
        }
        public TemporalBekiefTableStats(TemporalBeliefTable t) {
            this(t.range(), t.taskCount(),
                    t.capacity(), t.taskStream().iterator());
        }
    }

    default TemporalBekiefTableStats stats() {
        return new TemporalBekiefTableStats(this);
    }

    @Nullable LongInterval range();

    void taskCapacity(int temporals);


    void whileEach(Predicate<? super NALTask> each);

    void removeIf(Predicate<NALTask> remove, long finalStart, long finalEnd);

    @FunctionalInterface interface TimeTruthProcedure {
        void accept(long when, float freq, float conf);
    }

    default void plot(TimeTruthProcedure each) {
        forEachTask(t -> {
            float freq = t.freq(), conf = (float) t.conf();
            long e = t.end();
            for (long tt = t.start(); tt <= e; tt++) {
                each.accept(tt, freq, conf);
                //System.out.println(tt + ", " + freq + "," + conf);
            }
        });
    }

    int capacity();
}