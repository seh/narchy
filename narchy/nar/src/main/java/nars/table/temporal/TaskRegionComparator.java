package nars.table.temporal;

import jcog.data.byt.DynBytes;
import jcog.math.LongInterval;
import nars.io.IO;
import nars.task.NALTask;
import nars.task.util.TaskOccurrence;
import nars.task.util.TaskRegion;

import java.util.Comparator;

public final class TaskRegionComparator implements Comparator<TaskRegion> {

    static final TaskRegionComparator the = new TaskRegionComparator();

    private TaskRegionComparator() {
    }

    @Override
    public int compare(TaskRegion a, TaskRegion b) {
        if (a == b)
            return 0;

        //compare Disjoint
        long ae = a.end(), bs = b.start();
        if (ae < bs)
            return -1; //a entirely before b
        long as = a.start(), be = b.end();
        if (be < as)
            return +1; //b entirely before a

        //if not disjoint, then intersects:
        if (a instanceof TaskOccurrence || b instanceof TaskOccurrence)
            return 0;

        int mid = LongInterval.compare(as, ae, bs, be);
        if (mid != 0)
            return mid;

        return compareEquality(a, b);
    }


    private static int compareEquality(TaskRegion a, TaskRegion b) {
        //this naive hashcode compare is not 100% bulletproof but should work for 99.99+% cases where truth, stamp or term causes the hash to differ

        int m = Integer.compare(a.hashCode(), b.hashCode());
        if (m != 0) return m;
        if (a.equals(b)) return 0;

        return compareSerialized((NALTask) a, (NALTask) b);
        //return compareIdentity(a, b); //last resort
    }

    /**
     * for rare hash collisions
     */
    private static int compareSerialized(NALTask a, NALTask b) {
        return serialize(a).compareTo(serialize(b));
    }

    private static DynBytes serialize(NALTask a) {
        return IO.bytes(a, false, false, new DynBytes(32));
    }

//        private int compareIdentity(TaskRegion a, TaskRegion b) {
//            return Integer.compare(System.identityHashCode(a), System.identityHashCode(b));
//        }
}