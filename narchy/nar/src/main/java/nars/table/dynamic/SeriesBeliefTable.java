package nars.table.dynamic;

import jcog.TODO;
import jcog.Util;
import nars.NAL;
import nars.Term;
import nars.focus.Focus;
import nars.table.BeliefTable;
import nars.table.TaskTable;
import nars.table.eternal.EternalTable;
import nars.table.temporal.TemporalBeliefTable;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.util.Answer;
import nars.time.When;
import nars.truth.PreciseTruth;
import nars.truth.Truth;
import nars.truth.evi.EviInterval;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static nars.Op.ETERNAL;
import static nars.Op.TIMELESS;

/**
 * adds a TaskSeries additional Task buffer which can be evaluated from, or not depending
 * if a stored task is available or not.
 */
abstract public class SeriesBeliefTable extends DynBeliefTable {

//    /** TODO top-level */
//    abstract public static class TaskRingSeriesBeliefTable extends SeriesBeliefTable {
//    }

    static final private boolean ANSWER_MEAN = true;
    static final private boolean ANSWER_SOME = false;
    private static final float MEAN_MARGIN_PCT =
        0.05f;
        //0.01f;

    public float stretchDurs = NAL.signal.SIGNAL_STRETCH_DURS_MAX;

//    private static final VarHandle ENABLED = Util.VAR(SerialBeliefTable.class, "enabled", boolean.class);
//    @SuppressWarnings({"unused", "FieldMayBeFinal"})
//    private volatile boolean enabled = true;
    private boolean enabled = true;


    /** TODO protected or private */
    public transient long[] sharedStamp;

    protected SeriesBeliefTable(Term c, boolean beliefOrGoal) {
        super(c, beliefOrGoal);
    }

    static boolean continuePrev(@Nullable Truth next, float dur, long nextStart, long nextEnd, SerialTask prev, float stretchDurs, float freqRes) {
        long prevEnd = prev.end();

        float gapThresh = NAL.signal.SIGNAL_LATCH_LIMIT_DURS * dur;

        if (next==null || nextStart - prevEnd <= gapThresh) {

            if (stretchDurs > 0 && next != null) {
                if (prev.truth().equals(next, freqRes, NAL.truth.TRUTH_EPSILON)) {
                    if (nextStart - prev.start() < stretchDurs * dur) {
                        //continue if not excessively long
                        prev.setEnd(Math.max(prevEnd, nextEnd));
                        return true;
                    }
                }
            }

            //form new task either because the value changed, or because the latch duration was exceeded
            if (prevEnd < nextStart - 1 && prevEnd > nextStart - gapThresh)
                prev.setEnd(nextStart - 1);
        }

        return false;
    }

    @Override abstract public boolean isEmpty();

    abstract public boolean isEmpty(long s, long e);

    @Override
    public boolean remove(NALTask x, boolean delete) {
        return !delete || x.delete(); //HACK until RingBufferTaskSeries can remove items
    }

    public final void enabled(boolean e) {
        //ENABLED.setOpaque(this, e);
        enabled = e;
    }

    @Override
    public final void match(Answer a) {
        if (!enabled)
            return;
        //if (!((boolean)ENABLED.getOpaque(this)))
            //return;

        long s = a.start(), e;
        if (s == ETERNAL || s == TIMELESS) {
            int durHalf = (int) Math.ceil(a.dur() / 2);
            long now = a.time();
            s = now - durHalf;
            e = now + durHalf;
        } else {
            e = a.end();
        }

        answer(a, s, e);
    }

    private void answer(Answer a, long s, long e) {
        if (ANSWER_MEAN && e > s && taskCount() > 1) {
            answerMean(a, s, e);
            //if (answerMean(a, s, e)) return;
        }

        if (ANSWER_SOME)
            answerSome(a, s, e);
        else
            answerOne(a, s, e);
    }

    private boolean answerMean(Answer a, long s, long e) {
        float dur = a.dur();
        NALTask m = mean(s, e, dur > 0 ? MEAN_MARGIN_PCT : 0,
                dur, a.eviMin);
        return m != null && a.accept(m);
    }

    abstract protected void answerOne(Answer a, long s, long e);

    protected void answerSome(Answer a, long s, long e) {
         whileEach(s, e, false, a);
    }

    /**
     * compute an aggregate task numerically for the mean frequency of the computed values in the range. assumes tasks do not overlap
     */
    @Nullable private NALTask mean(long s, long e, float marginPct, float dur, double eviMin) {
        Mean m = new Mean(s, e, dur);
        long margin;
        if (dur == 0) margin = 0; //no need to calculate margin
        else margin = (long)Math.max(dur, marginPct * (e-s));

        whileEachLinear(s - margin, e + margin, m);

        return m.task(this, eviMin);
    }

    /** scans linearly from start to end */
    abstract public void whileEachLinear(long s, long e, Predicate<? super SerialTask> m);

    abstract public void whileEach(long s, long e, boolean intersectRequired, Predicate<? super SerialTask> each);


    @Override abstract public Stream<? extends NALTask> taskStream();

//    @Override abstract public void forEachTask(long minT, long maxT, Consumer<? super NALTask> x);

    @Override
    public abstract void forEachTask(Consumer<? super NALTask> action);

    /**
     * TODO only remove tasks which are weaker than the sensor
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    public void clean(List<BeliefTable> tables, long sEnd) {
        assert (beliefOrGoal);

        long sStart = start();
        if (sStart < sEnd) {

            Predicate<NALTask> cleaner = null;

            for (int i = 0, tablesSize = tables.size(); i < tablesSize; i++) {
                TaskTable b = tables.get(i);
                if (!(b instanceof DynBeliefTable) && !(b instanceof EternalTable) && !b.isEmpty()) {
                    if (cleaner == null)
                        cleaner = t -> absorbNonSignal(t, sStart, sEnd);

                    ((TemporalBeliefTable) b).removeIf(cleaner, sStart, sEnd);
                }
            }
        }
    }

    /**
     * used for if you can cache seriesStart,seriesEnd for a batch of calls
     * TODO only remove tasks which are weaker than the sensor
     */
    boolean absorbNonSignal(NALTask t, long seriesStart, long seriesEnd) {

        //assert (beliefOrGoal && !t.GOAL());

        if (t.isDeleted())
            return true;

        long tStart = t.start();
        if (tStart == ETERNAL)
            return false;

        if (seriesEnd < tStart)
            return false; //occurs after series ends

        long tEnd = t.end();
        if (seriesStart > tEnd)
            return false; //occurs before series starts

        if (tEnd > seriesEnd)
            return false; //predicts future of series


        //TODO actually absorb (transfer) the non-series task priority in proportion to the amount predicted, gradually until complete absorption
        //TODO store ranges tested for series rather than keep scanning for each one
        return !isEmpty(tStart, tEnd);
    }


    abstract public long start();
    abstract public long end();

    /**
     * dur can be either a perceptual duration which changes, or a 'physical duration' determined by
     *            the interface itself (ex: clock rate)
     */
    public SerialTask add(@Nullable Truth next, When<Focus> x) {
        if (sharedStamp ==null)
            sharedStamp = x.x.nar.evidence(); //HACK

        return add(next, x.s, x.e, x.dur /* TODO ,freqRes */ );
    }

//    @Deprecated final HistogramAnomaly h = new HistogramAnomaly(4);
//
//    /** TEMPORARY */
//    @Deprecated private @Nullable Truth truth(@Nullable Truth t) {
//        if (t!=null) {
//            float novelty = h.acceptAndGetAnomaly(t.freq());
//            novelty = Util.round(novelty, 0.1f);
//            float noveltyMult = Util.lerpSafe(novelty, 0.5f, 1f);
//            t = t.eviMult(noveltyMult, 0);
//        }
//        return t;
//    }


    @Deprecated public SerialTask add(@Nullable Truth next, long s, long e, float dur) {
        return add(next, s, e, dur, NAL.truth.TRUTH_EPSILON/2);
    }

    abstract public SerialTask add(@Nullable Truth next, long s, long e, float dur, float freqRes);

//    private void findDeleted() {
//        series.forEach(z -> {
//            if (z.isDeleted())
//                throw new WTF();
//        });
//    }


    public final SerialTask task(long start, long end, float f, double evi) {
        return task(start, end, f, evi, stamp());
    }

    /** creates a new task for storage */
    protected SerialTask task(long start, long end, float f, double evi, long[] stamp) {
        return new SerialTask(term, punc(), taskTruth(f, evi), start, end, stamp);
    }
//    protected NALTask taskAggregate(long start, long end, float f, double evi, long[] stamp) {
//        return NALTask.task(term, punc(), taskTruth(f, evi), start, end, stamp);
//    }

    protected Truth taskTruth(float f, double evi) {
        return PreciseTruth.byEvi(f, evi);
    }

    protected final long[] stamp() {
//        //return when.x.nar.evidence();
//        if (when != null && sharedEvi == null)
//            sharedEvi = when.x.nar.evidence();
        if (sharedStamp == null)
            throw new NullPointerException();
        return sharedStamp;
    }




    /**
     * mean truth aggregation by linear combination
     */
    private static final class Mean extends EviInterval implements Predicate<SerialTask> {


        double freqEviRangeWeightedSum = 0, eviIntegral = 0, priWeightedSum = 0;
        int count = 0;

        private static final int MIN_MEAN_COMPONENTS = 2;

        Mean(long s, long e, float dur) {
            this(s, e, s, e, dur);
        }

        Mean(long s, long e, long scanStart, long scanEnd, float dur) {
            super(s, e, dur);
        }

        @Override public boolean test(SerialTask x) {
//            this.current = null;
            double eviInteg = eviInteg(x);
            /*/d.range()*/
            if (eviInteg/*/d.range()*/ >= NAL.truth.EVI_MIN) {
                this.eviIntegral += eviInteg;
                this.priWeightedSum = Util.fma(x.priElseZero(), eviInteg, priWeightedSum);
                this.freqEviRangeWeightedSum = Util.fma(x.freq(), eviInteg, freqEviRangeWeightedSum);
                this.count++;
            }
            return true;



        }

        @Nullable
        public NALTask task(SeriesBeliefTable x, double eviMin) {
            return count < MIN_MEAN_COMPONENTS ? null : task(x, eviMin, this.s, this.e);
        }

        @Nullable private NALTask task(SeriesBeliefTable x, double eviMin, long s, long e) {
            double eviIntegral = this.eviIntegral;
            long R = e - s + 1;
            double eviMean = eviIntegral / R;
            if (eviMean < eviMin) return null;

            float freq = (float)(freqEviRangeWeightedSum / eviIntegral);
            double pri = priWeightedSum / eviIntegral;
            return NALTask.taskUnsafe(x.term, x.punc(), x.taskTruth(freq, eviMean), s, e, x.stamp()).withPri((float) pri);
        }

        @Nullable public PreciseTruth truth(double eviMin) {
            throw new TODO();
        }


//        /** for debugging */
//        private Predicate<SerialTask> unique() {
//            return new Predicate<>() {
//
//                Set s;
//
//                @Override
//                public boolean test(SerialTask x) {
//                    if (s == null) s = new UnifiedSet<>(4);
//                    boolean unique = s.add(x);
//                    if (!unique)
//                        System.out.println(unique);
//                    return unique && Mean.this.test(x);
//                }
//            };
//        }
    }

}