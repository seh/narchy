package nars.table.dynamic;

import jcog.Fuzzy;
import jcog.TODO;
import jcog.math.LongInterval;
import nars.Term;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.util.Answer;
import nars.task.util.series.IntervalSeries;
import nars.task.util.series.RingIntervalSeries;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class TaskSeriesSeriesBeliefTable extends SeriesBeliefTable {
    public final IntervalSeries<SerialTask> series;

    public TaskSeriesSeriesBeliefTable(Term c, boolean beliefOrGoal, IntervalSeries<SerialTask> s) {
        super(c, beliefOrGoal);
        this.series = s;
    }

    @Override
    public final long start() {
        return series.start();
    }

    @Override
    public final long end() {
        return series.end();
    }

    @Override
    public final boolean isEmpty() {
        return series.isEmpty();
    }

    @Override
    public Stream<? extends NALTask> taskStream() {
        return series.stream();
    }

    public void forEachTask(Consumer<? super NALTask> each) {
        series.forEach(each);
    }

    @Override
    public void whileEach(long s, long e, boolean intersectRequired, Predicate<? super SerialTask> each) {
        if (series instanceof RingIntervalSeries)
            ((RingIntervalSeries) series).whileEach(s, e,
                    Fuzzy.mean(s, e) //TODO NAL.SCAN_START_RANDOM_OR_MID
                    , true, each);
        else
            series.whileEach(s, e, intersectRequired, each);
    }

    @Override
    public void whileEachLinear(long s, long e, Predicate<? super SerialTask> m) {
        if (series instanceof RingIntervalSeries)
            ((RingIntervalSeries) series).whileEach(s, e, s, true, m);
        else
            whileEach(s, e, false, m);
    }

    @Override
    public boolean isEmpty(long s, long e) {
        return series.isEmpty(s, e);
    }

    @Override
    public final void forEachTask(long minT, long maxT, Consumer<? super NALTask> x) {

        //if (series.isEmpty()) return;

        whileEachLinear(minT, maxT, t -> {
            x.accept(t);
            return true;
        });
    }


    @Override
    public int taskCount() {
        return series.size();
    }

    @Override
    protected void answerOne(Answer a, long s, long e) {
        if (series instanceof RingIntervalSeries) {
            LongInterval x = ((RingIntervalSeries) series).findNearest(a.rng().nextLong(s, e));
            if (x != null)
                a.accept((NALTask) x);
        } else
            throw new TODO();
    }

    @Override
    public void clear() {
        series.clear();
    }

    @Override
    public SerialTask add(@Nullable Truth next, long s, long e, float dur, float freqRes) {
        SerialTask prev = series.last();
        if (prev != null) {
            if (continuePrev(next, dur, s, e, prev, stretchDurs, freqRes))
                return prev;

            s = Math.min(Math.max(prev.end() + 1, s), e); //avoid overlap
        }

        if (next == null) return null;

        float nextFreq = next.freq();
        double nextEvi = next.evi();
        return prev != null && series.capacity() == 1 ?
                prev.set(s, e, nextFreq, nextEvi, Float.NaN)
                :
                series.add(task(s, e, nextFreq, nextEvi))
                ;
    }

//        @Override public boolean removeIf(Predicate<SerialTask> p, boolean delete) {
//            //HACK
//            return ((NavigableMapTaskSeries<SerialTask>)series).removeIf(p, delete);
//        }

}