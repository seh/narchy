package nars.table.dynamic;

import jcog.WTF;
import nars.NAL;
import nars.Term;
import nars.task.NALTask;
import nars.task.util.Answer;
import nars.term.Compound;
import nars.truth.AnswerTaskify;
import nars.truth.dynamic.DynTruth;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiConsumer;


/**
 * computes dynamic truth according to implicit truth functions
 * determined by recursive evaluation of the compound's sub-component's truths
 */
public final class DynTruthBeliefTable extends DynBeliefTable {

    static final private boolean cache = true;

    private final DynTruth model;

    public DynTruthBeliefTable(Term c, DynTruth model, boolean beliefOrGoal) {
        super(c, beliefOrGoal);
        this.model = model;
    }

    @Override
    public final void match(Answer a) {
        match(a, null);
    }

    public void match(Answer a, @Nullable BiConsumer<NALTask,Answer> cacheTarget) {
        NALTask y;
        Compound xx = term(a);
        try (AnswerTaskify t = new AnswerTaskify(model, beliefOrGoal, a)) {
            y = t.task(xx);
        }
        if (y != null) {

            Term yy = y.term();
            if (yy instanceof Compound != this.term instanceof Compound) {
                if (NAL.DEBUG) throw new WTF();//???
                return;
            }

            boolean accepted = a.accept(y);

            if (accepted && cache && cacheTarget!=null && yy.equalConcept(xx))
                cacheTarget.accept(y, a);
        }
    }

    private Compound term(Answer a) {
        Term template = a.term();
        return (Compound) (template != null ? template : term);
    }



}