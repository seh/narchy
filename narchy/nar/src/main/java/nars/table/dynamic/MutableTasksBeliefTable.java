package nars.table.dynamic;

import nars.Term;
import nars.task.SerialTask;
import nars.task.util.series.IntervalSeries;
import nars.task.util.series.RingIntervalSeries;


/**
 * overlay table with N mutable recycled tasks (stored in a sequence or near sequence)
 * that can be manipulated directly in access orders such as FIFO or LIFO
 */
public class MutableTasksBeliefTable extends TaskSeriesSeriesBeliefTable {

    @Deprecated public MutableTasksBeliefTable(Term c, boolean beliefOrGoal, int cap) {
        this(c, beliefOrGoal, new RingIntervalSeries<>(cap));
    }

    public MutableTasksBeliefTable(Term c, boolean beliefOrGoal, IntervalSeries<SerialTask> series) {
        super(c, beliefOrGoal, series);
    }

    private SerialTask peek() {
        return series.first();
    }
    private SerialTask pop() {
        return series.poll();
    }
    private SerialTask last() {
        return series.last();
    }

    public final SerialTask setOrAdd(long start, long end, float f, double evi, float pri) {
        int c = series.capacity();
        SerialTask x;
        if (c == 1)
            x = peek();
        else
            x = series.size() >= c ? pop() : null; //re-use oldest

        if (x!=null) {
            x.set(start, end, f, evi, pri);
        } else {
            series.add(x = task(start, end, f, evi, pri));
        }
        return x;
    }


    public final SerialTask set(int i, long start, long end, float f, double evi, float pri) {
        SerialTask x = ((RingIntervalSeries<SerialTask>) series).peek(i);
        if (x == null) {
            series.add(x = task(start, end, f, evi, pri));
        } else {
            x.set(start, end, f, evi, pri);
        }
        return x;
    }

//    public final SerialTask push(long start, long end, float f, double evi, float pri) {
//        SerialTask t = task(start, end, f, evi, pri);
//        series.add(t);
//        return t;
//    }

    public final SerialTask task(long start, long end, float f, double evi, float pri) {
        return task(start, end, f, evi).withPri(pri);
    }


}