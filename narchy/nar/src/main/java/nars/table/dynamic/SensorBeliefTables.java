package nars.table.dynamic;

import nars.NAL;
import nars.Op;
import nars.Term;
import nars.action.memory.Remember;
import nars.focus.Focus;
import nars.table.BeliefTable;
import nars.table.BeliefTables;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.util.series.IntervalSeries;
import nars.task.util.series.RingIntervalSeries;
import nars.time.When;
import nars.truth.Truth;

/**
 * special belief tables implementation
 * dynamically computes matching truths and tasks according to
 * a lossy 1-D wave updated directly by a signal input
 */
public class SensorBeliefTables extends BeliefTables {

	public final SeriesBeliefTable series;

	public SensorBeliefTables(Term c, boolean beliefOrGoal, BeliefTable mutableTemporalTable) {
		this(c, beliefOrGoal,
					//TODO impl time series with concurrent ring buffer from gluegen
					//new ConcurrentSkiplistTaskSeries<>(capacity)
					new RingIntervalSeries<>(NAL.signal.SIGNAL_BELIEF_TABLE_SERIES_SIZE),
				mutableTemporalTable);
	}

	private SensorBeliefTables(Term term, boolean beliefOrGoal, IntervalSeries<SerialTask> s, /*Temporal*/BeliefTable mutableTemporalTable) {
		super(new TaskSeriesSeriesBeliefTable(term, beliefOrGoal, s));

		this.series = tableFirst(SeriesBeliefTable.class);
		assert (series != null);

		tables.add(mutableTemporalTable);

		/* 0=series will override the r-tree table */
		//matchMode = 0;
	}


	@Override
	public final void remember(Remember r) {
		NALTask x = r.input;
		if (x instanceof SerialTask)
			return; //already inserted when constructed, dont allow any other way
		assert(x.BELIEF());


		if (NAL.signal.SIGNAL_TABLE_FILTER_NON_SIGNAL_TEMPORAL_TASKS_ON_REMEMBER && !x.ETERNAL() /*&& !x.isInput()*/) {
			if (absorbNonSignal(r)) {
				r.unstore(x);
				x.delete();
				return;
			}
		}

		super.remember(r);
	}

	private boolean absorbNonSignal(Remember r) {
		long seriesStart = series.start();
		if (seriesStart == Op.TIMELESS)
			return false;
		long seriesEnd = series.end();
		if (seriesEnd == Op.TIMELESS)
			return false; //seriesEnd = seriesStart;

		//r.dur();
		int cleanMargin = 0;
		long ss = seriesStart + cleanMargin;
		long ee = seriesEnd - cleanMargin;
		return ss <= ee && series.absorbNonSignal(r.input, ss, ee);
	}


	public final SerialTask add(Truth value, When<Focus> w) {
		SerialTask t = series.add(value, w);

		if (NAL.signal.SIGNAL_TABLE_FILTER_NON_SIGNAL_TEMPORAL_TASKS_ON_SIGNAL)
			series.clean(tables, w.e - 1);

		return t;
	}
}