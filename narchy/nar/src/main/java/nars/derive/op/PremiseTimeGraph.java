package nars.derive.op;

import jcog.data.set.ArrayHashSet;
import jcog.decide.Roulette;
import nars.NAL;
import nars.Term;
import nars.derive.Deriver;
import nars.term.Compound;
import nars.term.util.Image;
import nars.time.Tense;
import nars.time.TimeGraph;
import nars.truth.MutableTruthInterval;
import org.eclipse.collections.api.block.predicate.Predicate;
import org.eclipse.collections.api.tuple.Pair;
import org.jetbrains.annotations.Nullable;

import static nars.Op.TIMELESS;
import static org.eclipse.collections.impl.tuple.Tuples.pair;

public final class PremiseTimeGraph extends TimeGraph {

    private transient int patternVolumeMin, patternVolumeMax, ditherDT, ttl;

    public PremiseTimeGraph(Deriver d) {
        super(d.rng);
    }

    private static int filter(boolean occ, ArrayHashSet<Event> s) {
        int total = s.size();
        if (total <= 1) return total;

        Predicate<Event> bad = occ ? x -> !(x instanceof Absolute) : x -> x.id.TEMPORAL_VAR();
        int good = total - s.list.count(bad);
        if (good > 0 && good < total) {
            if (s.removeIf(bad))
                total = s.size();
        }
        return total;
    }

    void init(Term x, Deriver d) {
        this.patternVolumeMax = d.volMax; //+1 for possible negation/unnegation?
        this.ditherDT = d.ditherDT();
        this.patternVolumeMin = (int) (NAL.derive_temporal.TIMEGRAPH_DEGENERATE_SOLUTION_THRESHOLD_FACTOR * x.volume());
    }

    @Nullable
    private Pair<Term, long[]> time(Term x, boolean occ) {

        Event e = solve(x, NAL.derive_temporal.TIMEGRAPH_ITERATIONS, occ);
        if (occ && e == null)
            return null;

        return pair(
            e != null ? e.id : x,
            occ ? new long[]{e.start(), e.end()} : null);
    }


//    @Override
//    protected boolean autoNeg(Term x) {
//        if (super.autoNeg(x)) {
//            Term xu = x.unneg();
//            Term tu = target.unneg();
//            return tu.equalsRoot(xu)
//                   ||
//                   tu.containsRecursively(xu)
//                   ||
//                   xu.containsRecursively(tu);
//        }
//        return false;
//    }

    private @Nullable Event solve(Term target, int ttl, boolean occ) {

        this.ttl = ttl;

        solve(target, occ);

        ArrayHashSet<Event> s = solutions;

        int s0 = s.size();
        if (s0 == 0) return null;
        if (s0 == 1) return s.first();

        int s1 = target.TEMPORAL_VAR() ? filter(false, s) : s.size();
        if (occ)
            s1 = filter(true, s);

        switch (s1) {
            case 0:
                return null;
            case 1:
                return s.get(0);
            default:
                int volIdeal = this.target.volume();
                int structIdeal = this.target.structure();
                float[] score = new float[s1];
                for (int i = 0; i < s1; i++)
                    score[i] = OccurrenceSolver.score(s.get(i), volIdeal, structIdeal, occ);
                return solutions.get(Roulette.selectRoulette(s1, i -> score[i], rng.rng));
        }
    }

    @Override
    public long t(long when) {
        return NAL.derive_temporal.TIMEGRAPH_DITHER_EVENTS_INTERNALLY ? Tense.dither(when, ditherDT) : when;
    }

    @Override protected long tExternal(long t) {
        return NAL.derive_temporal.TIMEGRAPH_DITHER_EVENTS_EXTERNALLY ? Tense.dither(t, ditherDT) : t;
    }
    @Override protected int tExternal(int t) {
        return NAL.derive_temporal.TIMEGRAPH_DITHER_EVENTS_EXTERNALLY ? Tense.dither(t, ditherDT) : t;
    }

    @Nullable private Event know(Term x, long s, long e, boolean decompose, Deriver d) {
        if (!x.CONDABLE())
            return null;
        Event y = (s == TIMELESS) ?
                knowWeak(x) :
                know(x, s, e, decompose);

        rewrite(y, d);

        return y;
    }


    private boolean rewrite(Event X, Deriver d) {
        return alias(X, Image.imageNormalize(d.unify.retransform(X.id)));
    }

    private boolean alias(Event X, Term y) {
        Term x = X.id;
        if (y.CONDABLE() && !x.equals(y)) {
            simultaneous(X,
                    //know(y)
                    knowWeak(y)
            );
            return true;
        }
        return false;
    }


    /**
     * called after solution.add
     */
    @Override
    protected boolean solution(Event solution) {
        return --ttl > 0;
    }


    @Override
    protected boolean validSolution(Event s) {
        if (!super.validSolution(s))
            return false;

        int v = s.id.volume();
        return v <= patternVolumeMax && v >= patternVolumeMin;
//        return s instanceof Relative ||
//                (!taskEvent.equals(s) &&
//                        (beliefEvent == null || beliefEvent == taskEvent || !beliefEvent.equals(s)));
    }

    @Override
    protected int occToDT(long x) {
        return Tense.dither(super.occToDT(x), ditherDT);
    }

    void knowPremise(Term x, Term t, Term b, boolean taskOrBelief, @Nullable MutableTruthInterval c, Deriver d) {

        boolean tEqualsB = t.equals(b);

        boolean dt = t.TEMPORALABLE() &&
            !(t instanceof Compound && x.equals(t)); //CONJ/IMPL elisions
        boolean db = b.TEMPORALABLE() &&
            !((tEqualsB || (b instanceof Compound && x.equals(b)))); //CONJ/IMPL elisions

        if (taskOrBelief) {
            knowPremise(d, tEqualsB, c, dt, db, t, b);
        } else {
            knowPremise(d, tEqualsB, c, db, dt, b, t);
        }

        //know(x);
        //rewrite(know(x), d);

    }


    private void knowPremise(Deriver d, boolean gEqualsH, @Nullable MutableTruthInterval c, boolean dg, boolean dh, Term g, Term h) {

        boolean occ = c!=null;
        long cs = occ ? c.s : TIMELESS, ce = occ ? c.e : TIMELESS;
        ce = Math.max(cs, ce); //HACK in case 'c' is being modified in another thread

        if (!dh && !gEqualsH) {
            //b first to add the stop-event preventing its decomposition in case t contains it
            know(h, TIMELESS, TIMELESS, dh, d);
            know(g, cs, ce, dg, d);
        } else {
            know(g, cs, ce, dg, d);
            if (!gEqualsH) // && !(g instanceof Compound && ((Compound) g).containsRecursively(h)))
                know(h, TIMELESS, TIMELESS, dh, d);
        }
    }

    @Nullable public Pair<Term, long[]> solvePremise(Term x, Term t, Term b, boolean taskOrBelief, MutableTruthInterval c, Deriver d, boolean solveOcc) {
        knowPremise(x, t, b, taskOrBelief, c, d);
        return time(x, solveOcc);
    }
}