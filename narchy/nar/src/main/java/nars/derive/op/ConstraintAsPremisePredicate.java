package nars.derive.op;

import nars.$;
import nars.Term;
import nars.derive.Deriver;
import nars.premise.Premise;
import nars.unify.UnifyConstraint;
import nars.unify.constraint.ConstraintAsPredicate;
import nars.unify.constraint.RelationConstraint;
import nars.unify.constraint.UnaryConstraint;

import java.util.function.BiFunction;

import static nars.$.shortSubs;

public enum ConstraintAsPremisePredicate { ;

    private static final BiFunction<Term, Term, Term> TASK = (t, b) -> t;
    private static final BiFunction<Term, Term, Term> BELIEF = (t, b) -> b;

    /** TODO refine */
    private static final float pathCost = 0.25f;

    /**
     * TODO generify a version of this allowing: U extends Unify
     */
    public static ConstraintAsPredicate the(UnifyConstraint m, byte[] xInTask, byte[] xInBelief, byte[] yInTask, byte[] yInBelief) {

        int pathLen;

        BiFunction<Term, Term, Term> extractX;
        Term extractXterm;
        if (xInTask != null) { // && (xInBelief == null || xInTask.length < xInBelief.length)) {
            assert(xInBelief==null);
            int xtl = xInTask.length;
            extractX = xtl == 0 ? TASK : (t, b) -> t.sub(xInTask);
            extractXterm = xtl == 0 ? Premise.Task : $.func(Premise.Task, shortSubs(xInTask));
            pathLen = 1 + xtl;
        } else {
            int xbl = xInBelief.length;
            extractX = xbl == 0 ? BELIEF : (t, b) -> b.sub(xInBelief);
            extractXterm = xbl == 0 ? Premise.Belief : $.func(Premise.Belief, shortSubs(xInBelief));
            pathLen = 1 + xbl;
        }


        float cost = m.cost();
        Term M = m.ref.replace(m.x, extractXterm);
        if (m instanceof final RelationConstraint r) {
            BiFunction<Term, Term, Term> extractY;
            Term extractYterm;
            int ytl = yInTask != null ? yInTask.length : 0;
            int ybl = yInBelief != null ? yInBelief.length : 0;
            if (yInTask != null && (yInBelief == null || ytl < ybl)) {
                extractY = ytl == 0 ? TASK : (t, b) -> t.sub(yInTask);
                extractYterm = ytl == 0 ? Premise.Task : $.func(Premise.Task, shortSubs(yInTask));
                pathLen += 1 + ytl;
            } else {
                extractY = ybl == 0 ? BELIEF : (t, b) -> b.sub(yInBelief);
                extractYterm = ybl == 0 ? Premise.Belief : $.func(Premise.Belief, shortSubs(yInBelief));
                pathLen += 1 + ybl;
            }
            Term MM = M.replace(r.y, extractYterm);
            return new RelationConstraintAsPremisePredicate(MM,
                r, extractX, extractY, cost + pathLen * pathCost);
        } else {
            return new UnaryConstraintAsPremisePredicate(M,
                (UnaryConstraint) m, extractX, cost + pathLen * pathCost);
        }
    }

    public static final class UnaryConstraintAsPremisePredicate extends ConstraintAsPredicate<Deriver, UnaryConstraint<Deriver.PremiseUnify>> {

        UnaryConstraintAsPremisePredicate(Term id, UnaryConstraint<Deriver.PremiseUnify> m, BiFunction<Term, Term, Term> extractX, float cost) {
            super(id, m, extractX, null, cost);
        }

        @Override
        public boolean test(Deriver d) {
            Premise p = d.premise;
            Term x = extractX.apply(p.from(), p.to());
            return
                //x == null ||
                constraint.match(x);
        }

    }

    public static final class RelationConstraintAsPremisePredicate extends ConstraintAsPredicate<Deriver, RelationConstraint<Deriver.PremiseUnify>> {

        RelationConstraintAsPremisePredicate(Term id, RelationConstraint<Deriver.PremiseUnify> m, BiFunction<Term, Term, Term> extractX, BiFunction<Term, Term, Term> extractY, float cost) {
            super(id, m, extractX, extractY, cost);
        }

        public final boolean symmetric() {
            return this.constraint.symmetric();
        }

        @Override
        public boolean test(Deriver d) {
            Premise p = d.premise;
            Term T = p.from(), B = p.to();
            Term x = extractX.apply(T, B);
            if (x == null) {
                //throw new NullPointerException();
                return false;
            }
            Term y = extractY.apply(T, B);
            if (y == null) {
                //throw new NullPointerException();
                return false;
            }
            return !constraint.invalid(x, y, d.unify);
        }

        public boolean isMirror(ConstraintAsPremisePredicate.RelationConstraintAsPremisePredicate r) {
            return
                    volume() == r.volume() && !constraint.x.equals(r.constraint.x) && r.constraint.equals(constraint.mirror()) //safe exhaustive
                    //constraint.x.equals(r.constraint.y) && constraint.y.equals(r.constraint.x))
            ;
        }
    }


}