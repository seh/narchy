package nars.derive.op;

import jcog.Util;
import nars.Term;
import nars.derive.Deriver;
import nars.task.util.ValidIndepBalance;
import nars.term.util.transform.VariableTransform;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.function.Function;


public final class Taskify implements Function<Deriver, Term> {
    private final Term pattern;
    private final Function<Deriver, Term> conc;

    public Taskify(Term pattern, @Nullable Function<Deriver, Term> conc) {
        this.pattern = pattern;
        this.conc = conc!=null ? conc : this;
    }


    public static Term questionSalvage(Term x) {
        //convert orphaned indep vars to query/dep variables
        return ValidIndepBalance.valid(x, true) ?
                VariableTransform.indepToQueryVar.apply(x) : x;
    }

    //    private static boolean same(Term derived, byte punc, Truth truth, long start, long end, int ditherDT, NALTask parent) {
//
//        if (parent.punc() == punc) {
//
//            if (parent.isDeleted()) return false; //at worst it will replace or reactivate it, so allow
//
//            if (derived.equals(parent.term())) { //TODO test for dtDiff
//                //if (Util.equals(parent.start(), start, ditherDT) && Util.equals(parent.end(), end, ditherDT)) {
//                if (parent.ETERNAL() || (start!=ETERNAL && parent.contains(start-ditherDT/2, end+ditherDT/2))) {
//
//                    return (punc == QUESTION || punc == QUEST) || (
//                        Util.equals(parent.freq(), truth.freq(), /* freqRes? */ NAL.truth.TRUTH_EPSILON) &&
//                            parent.conf() >= truth.conf()
//                                //- NAL.truth.TRUTH_EPSILON / 2  // avoids creeping confidence increase?
//                    );
//                }
//            }
//        }
//
//        return false;
//    }



    public Term pattern() {
        return pattern;
    }

    public Function<Deriver, Term> conc() {
        return conc;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Taskify) obj;
        return Objects.equals(this.pattern, that.pattern) &&
                Objects.equals(this.conc, that.conc);
    }

    @Override
    public int hashCode() {
        //return Objects.hash(pattern, conc);
        return Util.hashCombine(pattern, conc);
    }

    @Override
    public String toString() {
        return "Taskify[" +
                "pattern=" + pattern + ", " +
                "conc=" + conc + ']';
    }

    /** the default conclusion transformation implementation */
    @Override public final Term apply(Deriver d) {
        return d.unify.transformDerived.apply(pattern);
    }
}