package nars.derive.impl;

import jcog.data.list.Lst;
import jcog.pri.Prioritizable;
import jcog.pri.bag.Bag;
import jcog.pri.bag.Sampler;
import jcog.pri.bag.impl.ArrayBag;
import jcog.pri.bag.impl.PriArrayBag;
import jcog.pri.op.PriMerge;
import jcog.signal.FloatRange;
import jcog.signal.IntRange;
import nars.NAR;
import nars.derive.Deriver;
import nars.derive.reaction.ReactionModel;
import nars.derive.reaction.Reactions;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.link.MutableTaskLink;
import nars.link.TaskLink;
import nars.premise.Premise;

import java.util.function.Consumer;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static jcog.Str.n4;

public class MixDeriver extends Deriver {

    static final int batches = 16;
    static final int iterPerBatch = 8;
    static final float activeCapacityIters = 1;
    /** needs tuned, probably to the effective premise product tree depth */
    public static float _mix =
        //1/6f;
        1/5f;
        //1/4f;
        //1/7f;
        //1/8f;
        //1/10f;
        //1/2f;
        //1/3f;
        //1/4.5f;
        //0.2f;
        //0.12f;
        //0.1f;
        //0.25f;
        //0.35f;
        //1/11f;
        //1/15f;


    /** <= 1, decay rate */
    float runDecay =
        //1;
        0.5f;
        //0;
        //0.1f;


    public final IntRange iter = new IntRange(batches * iterPerBatch, 1, 2048);
    public final FloatRange mix = new FloatRange(_mix, 0, 1);

//    public final FloatRange forgetRate = new FloatRange(0.9f, 0, 1);

    private final PriArrayBag<Premise> active = new PriArrayBag<>(
            //PriMerge.max
            PriMerge.plus
            ,
        new ArrayBag.PlainListArrayBagModel<>()
    )
//    {
//        @Override
//        protected float merge(Premise existing, Premise incoming, float incomingPri) {
//            //update budget of the referenced premises
//            PriMerge.
//                plus
//                //mean
//                //max
//                .apply(existing.get(), incoming.get().priElseZero());
//
//            return super.merge(existing, incoming, incomingPri);
//        }
//    }
    ;
//    {
//        active.sharp = 2;
//    }


//    active = new PriHijackBag<>(
//            PriMerge.max, 3) {
//
//        @Override
//        public Premise key(NLink<Premise> value) {
//            return value.id;
//        }
//    };


    private final Lst<Premise> next = new Lst<>();
    private final Consumer<Premise> nextRunner = this::_run;

    public MixDeriver(Reactions model, NAR nar) {
        this(model.compile(nar), nar);
    }

    public MixDeriver(ReactionModel model, NAR nar) {
        super(model, nar);
    }

    @Override
    protected void start(Focus f) {
        super.start(f);
        active.clear();
    }

//    /** intercept added tasklinks into buffer */
//    @Override
//    protected void _link(MutableTaskLink l) {
//        taskLinkBuffer.add(l);
//    }

    int premiseCount, linkCount, taskCount;

    private void resetCounts() {
        premiseCount = 0; linkCount = 0;  taskCount = 0;
    }

    @Override
    public void acceptLink(MutableTaskLink l) {
        linkCount++;
        super.acceptLink(l);
    }

    @Override
    protected void acceptPremise(Premise p) {
        premiseCount++;
        active.put(p);
    }

//    private int _premiseCount, _linkCount, _taskCount;
//
//    @Override
//    protected void runBefore(Premise p) {
//        _premiseCount = premiseCount;
//        _linkCount = linkCount;
//        _taskCount = taskCount;
//    }
//    @Override
//    protected void runAfter(Premise p) {
//        int dp = premiseCount - _premiseCount;
//        int dl = linkCount - _linkCount;
//        int dt = taskCount - _taskCount;
//        if (dp == 0 && dl == 0 && dt == 0) {
//            /* inconsequential (except for any side-effects not measured) */
//            if (p instanceof NALPremise) {
//                //Util.nop();
//                System.out.println(p.toString());
//            } else {
//                if (!(p instanceof SubPremise)) //HACK
//                    System.out.println(p.term());
//            }
//        }
//
//    }

    @Override
    protected void next() {

        Focus f = this.focus;
        Bag<Premise, Premise> focusBag = ((BagFocus) f).bag;
        if (focusBag.isEmpty())
            return;

        resetCounts();

        int iter = this.iter.intValue();
        active.capacity(Math.round(activeCapacityIters * iter));

        int batchSize = (int) max(1, Math.ceil(((float)iter) / batches));

        for (int i = iter; i >= 0; ) {
            int ran = nextIter(min(batchSize, i), focusBag);

            if (ran == 0) break; //focus is empty

            i -= ran;
        }

//        System.out.println(stats());
//        active.forEach(z -> System.out.println(z));
//        System.out.println();
//        System.out.println();


        //TODO if (autoClear) {
        //active.clear();
        //active.clearSoft(); //warning: doesnt affect inner premise pri

        tryClear();

    }
    private String stats() {
        return active.size() + "/" + active.capacity() +
            " (" + n4(active.priMin()) + ".." + n4(active.priMax()) + ')';
    }

    private int capacity() {
        return ((BagFocus) focus).capacity.intValue();
    }

    static private final float activeClear = 0.25f;

    /** periodically clears the active list to clear any proxy'd tasks, or other stale refs */
    private void tryClear() {
        if (rng.nextBoolean(activeClear/capacity()))
            active.clear();
    }

    private Bag<?, ? extends Prioritizable> nextSrc(Bag<Premise, Premise> focusBag, float mix) {
        return active.isEmpty() || rng.nextBooleanFast8(mix) ?
                focusBag : active;
    }

    private int nextIter(int batchSize, Bag<Premise, Premise> focusBag) {
        if (!active.isEmpty())
            active.commit();

        Lst<Premise> next = this.next; //assert(next.isEmpty());

        int _focusBatch = rng.floor(/*1 +*/ mix.floatValue() * batchSize);
        int focusBatch =
            _focusBatch;
            //Math.max((batchSize - activeSize), _focusBatch);
            //Math.max((batchSize - activeSize)/2 /* half-fill */, _focusBatch);
            //Math.max(1, _focusBatch);
        load(focusBag, focusBatch, next);

        int activeBatch = batchSize - next.size();
        load(active, activeBatch, next);

        return next.clear(nextRunner);
    }

    private void load(Sampler<? extends Premise> src, int batchSize, Lst<Premise> tgt) {
        if (batchSize <= 0) return;

        tgt.ensureCapacityForAdditional(batchSize, true);

        var s = src.sampleUnique(rng.rng);
        while (s.hasNext()) {
            tgt.addFast(s.next());

            if (--batchSize <= 0)
                break;
        }
    }


    private void _run(Premise x) {
//        Premise y;
//        if (x instanceof TaskLink) {
//            //tasklinks
//            y = ((TaskLink) x);
//        } else {
//            //internal
//            y = (Premise) x;
//        }

        run(x);

        if (!(x instanceof TaskLink))
            x.priMul(runDecay);
    }
}