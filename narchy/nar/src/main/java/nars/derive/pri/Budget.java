package nars.derive.pri;

import jcog.Is;
import jcog.Util;
import jcog.pri.op.PriMerge;
import nars.Term;
import nars.derive.Deriver;
import nars.focus.Focus;
import nars.link.MutableTaskLink;
import nars.link.TaskLink;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

import static jcog.Util.min;
import static jcog.Util.unitizeSafe;

/**
 * attention management strategy
 * http://sol.gfxile.net/interpolation/
 */
@Is({"Occam's_razor","Occam's_superglue","Economy","Attention"})
public abstract class Budget implements Consumer<Focus> {

    public static final PriMerge premiseMerge =
        PriMerge.max;
		//PriMerge.plus;
		//PriMerge.and;
		//PriMerge.mean;
		//PriMerge.or;

    public static final PriMerge tasklinkMerge =
		PriMerge.max;

    public static double confidentParents(@Nullable Truth x, @Nullable Truth y) {
        if (x == null && y == null)
            return 0;
        if (x == null)
            return y.evi();
        else if (y == null)
            return x.evi();
        else {
            return
                min( //relaxed
                //Fuzzy.mean(
                //Util.max( //stricter
                //plus //most strict
                    x.evi(), y.evi()
                );
        }
    }

    /**
     * meta-ockham
     * 'plurality should be posited with as much as is preferred.'
     * @param v volume of the item
*    * @param vMax max volume allowed
     * @param vTgtPct target volume, in  [0..1] proportion to vMax
     * @param intensity parameter affecting degree of specificity to the target
     */
    public static double simpleIdeal(float v, float vTgtPct, float vMax, float intensity) {
        return 1 / (1 + intensity * unitizeSafe(Math.abs(v - ((1 - vTgtPct) * vMax))/vMax) );
    }

    public static double simple(float vol, int volMax, float power) {
        //return simpleStep(vol, volMax, power);
        return simplePow(vol, volMax, power);
    }

    /** smoothstep */
    private static double simpleStep(float vol, int volMax, float power) {
        return Util.smootherstep(1-(power*vol/(volMax+1)));
    }
    /**
     * ockham's razor: 'plurality should not be posited without necessity.'
     */
    private static double simplePow(float vol, int volMax, float power) {
        float POWER =
            2;
            //1;
        return Math.pow(1 - unitizeSafe(vol / (volMax + 1)), power * POWER);
        //return Math.pow(1 - (clampSafe(vol, 0, volMax) / (volMax + 1)), power);
        //return 1 - Math.pow(unitizeSafe(vol / ((float) volMax + 1)), (1/power));

//        float strength =
//                1; /* linear */
//                //2; /* quadratic */
//                //4;
//                //16;
//        return 1 - Math.pow(unitize(vol / (volMax+1)), 1/strength) * (1 - power);
//        //return 1 - unitize(vol / (volMax+1)) * (1 - power); //linear
    }
    //PriMerge.plus;

    /** priority of derived NALTask */
	public final double priDerived(NALTask t, Deriver d) {
		Premise p = d.premise;

//		if (p instanceof TaskifyPremise)
//            p = NALPremise.nalPremise(p);

		return priDerived(t, p.task(), t.BELIEF_OR_GOAL() ? p.belief() : null, d);
	}

	public abstract double priDerived(NALTask xy, NALTask x, NALTask y, Deriver d);

	/** priority of derived premise (feedback) */
	public abstract double priPremise(Premise p, Deriver d);

	/** derived link
	 * @param link may be modified.  source is not (unless source==link)
	 * @param inOrOut whether growing inward (decomposing) or outward
	 */
	public abstract void linkDerived(MutableTaskLink link, TaskLink parent, Object d, boolean inOrOut);

	@Override
	public void accept(Focus f) {

	}

    public abstract float tasklinkPri(Term x, byte punc, Focus f);

}