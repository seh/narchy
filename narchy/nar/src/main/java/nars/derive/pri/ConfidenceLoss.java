package nars.derive.pri;

import static nars.truth.func.TruthFunctions.e2c;

/** confidence loss functions.  returns a multiplier proportional to
 *  the amount of evidence retained from a parent in a derivation. */
public enum ConfidenceLoss {
    EviLinear {
        @Override
        public double loss(double eParent, double eDerived) {
            return eDerived / eParent;
        }
    },
    ConfLinear {
        @Override
        public double loss(double eParent, double eDerived) {
            return e2c(eDerived) / e2c(eParent);
        }
    },

    /** more lenient than linear */
    EviLog {
        @Override
        public double loss(double eParent, double eDerived) {
            double y = Math.log(1 + eDerived) / Math.log(1 + eParent);
            return y;
        }
    };

    abstract public double loss(double eParent, double eDerived);
}