package nars.derive.reaction;

import jcog.TODO;
import jcog.WTF;
import jcog.data.map.UnifriedMap;
import nars.*;
import nars.action.Action;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.op.*;
import nars.derive.util.PuncMap;
import nars.derive.util.Unifiable;
import nars.premise.NALPremise;
import nars.subterm.Subterms;
import nars.subterm.util.SubtermCondition;
import nars.term.*;
import nars.term.atom.Atom;
import nars.term.atom.Atomic;
import nars.term.util.TermException;
import nars.term.util.transform.RecursiveTermTransform;
import nars.term.util.transform.VariableNormalization;
import nars.truth.func.NALTruth;
import nars.truth.func.TruthFunction;
import nars.truth.func.TruthModel;
import nars.unify.UnifyConstraint;
import nars.unify.constraint.*;
import org.eclipse.collections.api.block.function.primitive.ByteToByteFunction;
import org.eclipse.collections.api.block.predicate.primitive.BytePredicate;
import org.eclipse.collections.api.tuple.primitive.ObjectBooleanPair;
import org.eclipse.collections.impl.map.mutable.primitive.ObjectBooleanHashMap;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static nars.Op.*;
import static nars.premise.Premise.BeliefInline;
import static nars.premise.Premise.TaskInline;
import static nars.subterm.util.SubtermCondition.*;
import static nars.term.atom.Bool.Null;
import static nars.term.util.TermPaths.pathExact;
import static nars.term.util.transform.Retemporalize.patternify;
import static nars.unify.constraint.TermMatch.Is.VARIABLE;

/**
 * A rule which matches a Premise and produces a Task
 * contains: preconditions, predicates, postconditions, post-evaluations and metainfo
 */
public class PatternReaction extends MutableReaction {


    private static final Pattern ruleImpl = Pattern.compile("\\|-");
    //private static final Atomic eteConj = $.the("eteConj");
    private static final Map<Term, Truthify> truthifies = new ConcurrentHashMap<>(512);
    @Deprecated
    private static final Term IDENTITY = Atomic.atom("Identity");
    private static final int NEW_OP = 0, REVERSED = 1, NEGATED = 2;
    private static final int REVERSE_COST = 1, NEGATE_COST = 1;
    private final TruthModel truthModel;
    /**
     * derivation conclusion pattern
     */
    @Deprecated
    protected ByteToByteFunction concPunc;
    private Term id;
    private Term idCondition;
    public Term idConclusion;

    private Term beliefTruth;
    private Term goalTruth;
    /**
     * TODO just use one punc->punc transfer function, and its return of 0 to deny
     */
    private BytePredicate taskPunc;
    private Truthify truthify;

    public Term pattern;
    @Deprecated private transient Term _pattern;

    public Taskify taskify;
    public Termify termify;
    public OccurrenceSolver time;

    private PatternReaction(TruthModel truthModel) {
        this.truthModel = truthModel;
    }

    /**
     * default time mode
     */
    public static OccurrenceSolver time(BytePredicate punc) {
//        if (punc.accept(BELIEF) && !punc.accept(GOAL) && !punc.accept(QUESTION) && !punc.accept(QUEST)) {
//            //HACK
//            return OccurrenceSolver.Mid;
//        } else {
        return OccurrenceSolver.Task;
//        }
    }

    @Deprecated
    public static PatternReaction parse(String ruleSrc) throws Narsese.NarseseException {
        return parse(ruleSrc, NALTruth.the, null);
    }

    private static PatternReaction parse(String ruleSrc, TruthModel truthModel, @Nullable String tag) throws Narsese.NarseseException {
        PatternReaction r = new PatternReaction(truthModel);
        r._parse(ruleSrc);
        r.tag(tag);
        return r;
    }

    private static Truthify intern(Truthify x) {
        Truthify y = truthifies.putIfAbsent(x.term(), x);
        return y != null ? y : x;
    }

    @Deprecated
    public static Stream<Reaction> parse(String... rules) {
        return parse(Stream.of(rules), NALTruth.the);
    }

    public static Stream<Reaction> parse(Stream<String> lines, TruthModel truthModel) {
        String[] currentTag = {null};
        return lines
//            .parallel()
                .map(String::trim)
                .filter(x -> !x.isEmpty())
                .map(line -> {
                    if (!line.contains("|-")) {
                        int ll = line.length();
                        if (line.charAt(ll - 1) == '{') {
                            //start tag
                            assert (currentTag[0] == null);
                            currentTag[0] = line.substring(0, ll - 1).trim();
                            assert (!currentTag[0].isEmpty());
                            return null;
                        } else if (line.charAt(ll - 1) == '}') {
                            //close tag
                            if (currentTag[0] == null)
                                throw new TermException("unclosed }: " + line);
                            currentTag[0] = null;
                            return null;
                        }
                    }
                    return line;
                })
                .filter(Objects::nonNull)
                .map(line -> {
                    try {
                        PatternReaction x = parse(line, truthModel, currentTag[0]);
                        x.compile();
                        return x;
                    } catch (Throwable e) {
                        e.printStackTrace();
                        throw new RuntimeException(e.getMessage() + ": " + line);
                        //return null;
                    }
                });//.distinct();

    }

    private static Term parseRuleComponents(String src) throws Narsese.NarseseException {
        String[] ab = ruleImpl.split(src);
        if (ab.length != 2)
            throw new Narsese.NarseseException("Rule component must have arity=2, separated by \"|-\": " + src);

        return $.pFast(
                $.$$$('(' + ab[0].trim() + ')'),
                    $.$$$('(' + ab[1].trim() + ')'));
    }

//    private static Term restoreEteConj(Term c, List<Term> subbedConj, ArrayHashSet<Term> savedConj) {
//        for (int i = 0, subbedConjSize = subbedConj.size(); i < subbedConjSize; i++) {
//            c = c.replace(subbedConj.get(i), savedConj.get(i));
//        }
//        return c;
//    }

//    @Override public String toString() { return source!=null ? source : super.toString(); }
//
//    /**
//     * HACK preserve any && occurring in --> by substituting them then replacing them
//     */
//    @Deprecated
//    private static Term saveEteConj(Term c, Collection<Term> subbedConj, ArrayHashSet<Term> savedConj) {
//
//        //TODO use a TermTransform
//        c.ANDrecurse(x -> x.hasAll(INH.bit | CONJ.bit), t -> {
//            if (t.INH()) {
//                Term s = t.sub(0);
//                Term su = s.unneg();
//                if (su.CONJ() && su.dt() == DTERNAL)
//                    savedConj.add(patternify(s, false));
//                Term p = t.sub(1);
//                Term pu = p.unneg();
//                if (pu.CONJ() && pu.dt() == DTERNAL)
//                    savedConj.add(patternify(p, false));
//            }
//        });
//        if (!savedConj.isEmpty()) {
//            int i = 0;
//            for (Term x : savedConj) {
//                Term y = $.p(eteConj, $.the(i));
//                subbedConj.add(y);
//                c = c.replace(x, y);
//                i++;
//            }
//        }
//        return c;
//    }

    private static boolean found(Term x, Term e) {
        return e.equals(x) ||
                (e instanceof Compound && ((Compound) e).containsRecursively(x));
    }

    private static Function<Term, Term> extractSubterm(byte[] p) {
        if (p.length == 0)
            return x -> x; //as-is
        else if (p.length == 1) {
            byte p0 = p[0];
            return x -> x.sub(p0);
        } else
            return x -> x.sub(p);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o instanceof PatternReaction && id.equals(((PatternReaction)o).id);
    }

    @Override
    public String toString() {
        return id.toString();
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    /**
     * adds a condition opcode
     * interpreted from the Term according to Meta NAL semantics
     */
    public void cond(Term a) {

        boolean negated = a instanceof Neg;
        if (negated)
            a = a.unneg();

        Atomic name = Functor.func(a);
        if (name == null)
            throw new TermException("invalid " + getClass().getSimpleName() + " precondition", a);

        String pred = name.toString();
        Subterms args = Functor.args(a);
        int an = args.subs();
        Term X = an > 0 ? args.sub(0) : null;
        Term Y = an > 1 ? args.sub(1) : null;

        Variable XX = X instanceof Variable ? (Variable) X : null;
        Variable YY = Y instanceof Variable ? (Variable) Y : null;

        boolean[] negAppl = new boolean[1];

        cond(negated, negAppl, pred, X, Y, XX, YY);

        if (negAppl[0] != negated) throw new RuntimeException("unhandled negation: " + a);

        switch (pred) {
            case "task" -> {
                String XString = X.toString();
                assert (XString.equals("\"+\"") || XString.equals("\"-\"") || taskPunc == null);
                switch (XString) {
                    case "\"+\"" -> taskPolarity(true);
                    case "\"-\"" -> taskPolarity(false);
                    case "\"?\"" -> taskPunc = t -> t == QUESTION;
                    case "\"@\"" -> taskPunc = t -> t == QUEST;
                    case "\".\"" -> taskPunc = t -> t == BELIEF;
                    case "\"!\"" -> taskPunc = t -> t == GOAL;
                    case "\".?\"" -> taskPunc = t -> t == BELIEF || t == QUESTION;
                    case "\"?@\"" -> {
                        assert (concPunc == null);
                        taskPunc = t -> t == QUESTION || t == QUEST;
                        concPunc = c -> c;
                    }
                    case "\".!\"" -> {
                        assert (concPunc == null);
                        taskPunc = t -> t == BELIEF || t == GOAL;
                        concPunc = c -> c;
                    }
                    case "all" -> taskPunc = t -> true;
                    default -> throw new RuntimeException("Unknown task punctuation type: " + XString);
                }
            }
        }

    }

    private void cond(boolean neg, boolean[] _negationApplied, String pred, Term x, Term y, Variable Xv, Variable Yv) {

        Term xs, ys;
        if (y == null) {
            xs = x;
            ys = null;
        } else {
            if (x.compareTo(y) <= 0) {
                xs = x;
                ys = y;
            } else {
                xs = y;
                ys = x;
            }
        }
        Variable Xvs = xs instanceof Variable ? (Variable) xs : null;
        Variable Yvs = ys instanceof Variable ? (Variable) ys : null;

        @Deprecated boolean negd = _negationApplied[0]; //safety check to make sure semantic of negation was applied by the handler

        switch (pred) {
            case "taskEternal":
                taskEternal(!neg);
                if (neg) negd = true;
                break;

            case "neq":
                if (Yvs != null)
                    neq(Xvs, Yvs);
                else
                    neq(Xv, y); //???
                break;

            case "deq":
                deq(xs, ys, !neg);
                if (neg) negd = true;
                break;
            case "ceq":
                ceq(xs, ys, !neg);
                if (neg) negd = true;
                break;
            case "ceqdeq":
                ceqdeq(x, true, y, false, !neg);
                if (neg) negd = true;
                break;
            case "ceqPN":
                ceqPN(Xvs, Yvs, neg);
                if (neg) negd = true;
                break;

            case "eqPN":
                eqPN(Xvs, Yvs, !neg);
                if (neg) negd = true;
                break;

            case "eqNeg":
                //TODO special predicate: either (but not both) is Neg
                if (!neg)
                    neq(Xvs, Yvs);
                constrain((UnifyConstraint<Deriver.PremiseUnify>) new EqualNeg(Xvs, Yvs).negIf(neg));
                if (neg) negd = true;
                break;


            case "neqRCom":
                neqRCom(Xvs, Yvs);
                break;

            //TODO rename: condIntersect
            case "eventIntersect":
            case "eventIntersectContainer":
                eventCommon(Xvs, Yvs, 1, !neg);
                if (neg) negd = true;
                if (!neg && pred.endsWith("Container")) {
                    eventContainer(Xvs);
                    eventContainer(Yvs);
                }
                break;
            case "eventIntersectPN":
                eventCommon(Xvs, Yvs, 0, !neg);
                if (neg) negd = true;
                break;

//            case "mutexable":
//                neq(Xv, Yv);
//                is(x, CONJ);
//                is(y, CONJ);
//                constrain(new CommonSubEventConstraint.EventCoNeg(Xv, Yv));
//                break;

            case "opEq":
                constrain(new RelationConstraint.OpEqual(Xvs, Yvs));
                break;

            case "setsIntersect":
                constrain(new RelationConstraint.SetsIntersect(Xvs, Yvs));
//				constraints.add(new RelationConstraint.StructureIntersect(Xvs, Yvs));
                break;

            case "notSetsOrDifferentSets": {
                constrain(new RelationConstraint.NotSetsOrDifferentSets(Xvs, Yvs));
                break;
            }

            case "sub":
            case "subPN": {

                SubtermCondition mode;

//                    if (pred.startsWith("sub"))

                if (y instanceof Neg) {
                    Yv = (Variable) (y = y.unneg());
                    mode = SubtermNeg;
                } else
                    mode = Subterm;
//                    else {
//
//                        if (Y instanceof Neg) {
//                            YY = (Variable) (Y = Y.unneg());
//                            mode = SubsectNeg;
//                        } else
//                            mode = Subsect;
//
//                        if (!negated)
//                            is(XX, Op.CONJ);
//                        else
//                            throw new TODO();
//
//                    }

                if (!neg) {
                    bigger(Xv, false, Yv, mode == SubtermNeg);
                }

                int polarity;
                if (pred.endsWith("PN")) {
                    polarity = 0;
                    assert (!(y instanceof Neg));
                } else {
                    polarity = 1; //negation already handled
                }

                if (y instanceof Variable) {

                    constrain((UnifyConstraint) new SubConstraint(Xv, (Variable) y, mode, polarity).negIf(neg));
                } else {
                    if (polarity == 0)
                        throw new TODO(); //TODO contains(Y) || contains(--Y)
                    iff(Xv, new TermMatch.Contains(y), !neg);
                }

                if (neg) negd = true;
                break;
            }


            case "in":
                in(Xv, y, !(negd = neg));
                break;

            case "hasBelief":
                hasBeliefTask(!(negd = neg));
                break;

            case "conjParallelTaskBelief":
                condition(TaskBeliefEqualRoot.neg());
                condition(conjParallelTaskBelief);
                break;

//            case "par":
//                if (!neg) eventContainer(Xv);
//                iff(x, TermMatch.SEQUENCE, negd = neg); //<- needs separate PARALLEL condition?
//                break;
            case "seq":
                if (!neg) is(x, CONJ);
                iff(x, TermMatch.SEQUENCE, !(negd = neg));
                break;


            case "cond":
            case "condPN": {
                condOf(Cond, Xv, y, pred.endsWith("PN"), !neg);
                if (neg) negd = true;
                break;
            }

            case "condFirst":
            case "condLast":
            case "condFirstPN":
            case "condLastPN":  {
                condOf(condFirstOrLast(pred),
                        Xv, y, pred.contains("PN"), !neg);
                if (neg) negd = true;
                break;
            }

            case "subsMin":
                iff(x, new TermMatch.SubsMin((short) $.intValue(y)));
                break;

            case "is": {
                isAny(x, structOR(y), neg);
                if (neg) negd = true;
                break;
            }
            case "var": {
                iffThese(x, VARIABLE, !neg);
                if (neg) negd = true;
                break;
            }
            case "hasEvents": {
                iff(x, HasEvents, !neg);
                if (neg) negd = true;
                break;
            }
            case "hasVar": {
                _hasAnyAll(x, Variables, !neg, true);
                if (!neg)
                    iff(x, new TermMatch.VolMin(2), true);
                if (neg) negd = true;
                break;
            }
            case "hasVars": {
                //HACK hasVars(X,Y) == hasVar(X)||hasVar(Y)
                constrain(new HasVars(Xvs, Yvs));
                break;
            }

            case "isUnneg": {
                isUnneg(x, op($.unquote(y)).bit, !neg);
                if (neg) negd = true;
                break;
            }


            case "has": {
                hasAll(x, structOR(y), !neg);
                if (neg) negd = true;
                break;
            }

            case "hasAny": {
                hasAny(x, structOR(y), !neg);
                if (neg) negd = true;
                break;
            }


            case "task":
                //HACK ignore; handled in elsewhere
                break;

            default:
                throw new RuntimeException("unhandled condition: " + pred + " in:\n\t" + source);

        }

        _negationApplied[0] = negd;
    }

    private static SubtermCondition condFirstOrLast(String pred) {
        return pred.contains("First") ? CondFirst : CondLast;
    }

    private void ceqPN(Variable x, Variable y, boolean neg) {
        constrain(new CeqPNConstraint(x, y), neg);

        if (!neg)
            constrain(new RelationConstraint.StructureCoContained(x, y, NEG.bit));
    }

    public void eventCommon(Variable x, Variable y, int polarity, boolean isTrue) {
        if (polarity < 0) throw new UnsupportedOperationException();

//        if (!isTrue) {
//            if (polarity==1) {
//                neq(x,y);
//            } else if (polarity==0) {
//                eqPN(x,y,false);
//            }
//        }

        constrain((UnifyConstraint<Deriver.PremiseUnify>)
                new CommonSubEventConstraint(x, y, polarity).negIf(!isTrue));

        if (isTrue) {
            if (polarity == 1)
                constrain(new RelationConstraint.StructureIntersect(x, y)); //TODO structure intersect, with except for polarity!=1
            //constrain(new RelationConstraint.StructureCoContained(x, y, polarity!=1 ? Op.NEG.bit : 0));
        }
    }

    private PatternReaction concPattern(Term c) {
        Term cu = c.unneg();
        if (!(cu instanceof Variable || cu.TASKABLE()))//concTerm instanceof Bool)
            throw new TermException("conclusion pattern is bool", c);

        _pattern = c;
        return this;
    }

    public final PatternReaction time(String taskEvent) {
        time = OccurrenceSolver.solvers.get(Atomic.atom(taskEvent));
        return this;
    }

    private void _parse(String ruleSrc) throws Narsese.NarseseException {
        this.source = ruleSrc;
        Term id = new FunctorLinkingVariableNormalization().apply(
                new UppercaseAtomsToPatternVariables().apply(
                        parseRuleComponents(ruleSrc)
                )
        );
        if (id == Null)
            throw new TermException("pattern error:\n" + ruleSrc);

        Subterms precon = id.sub(0).subterms();

        taskPattern(precon.sub(0));
        beliefPattern(precon.sub(1));

        Subterms postcon = id.sub(1).subterms();

        concPattern(postcon.sub(0));

        double pp = precon.subs();
        for (int i = 2; i < pp; i++)
            cond(precon.sub(i));

        time = null;

        ((Compound) postcon.sub(1)).forEach(this::addModifier);
    }

    private void addModifier(Term m) {

        if (!m.INH()) throw new TermException("Unknown postcondition format", m);

        Term which = m.sub(0), type = m.sub(1);

        switch (type.toString()) {

            case "Punctuation":
                /** belief -> question, goal -> quest */
                /** belief -> question, goal -> quest */
                /** re-ask a new question/quest in response to question/quest */
                /** belief,question -> question, goal,quest -> quest */
                switch (which.toString()) {
                    case "Belief" -> concPunc = x -> BELIEF;
                    case "Goal" -> concPunc = x -> GOAL;
                    case "Question" -> concPunc = x -> QUESTION;
                    case "Quest" -> concPunc = x -> QUEST;

                    case "Answer" -> {
                        assertTaskPuncAndConcPuncNull();
                        assert (beliefTruth != null && goalTruth != null);
                        taskPunc = i -> i == QUESTION || i == QUEST;
                        concPunc = o -> switch (o) {
                            case QUESTION -> BELIEF;
                            case QUEST -> GOAL;
                            default -> (byte) 0;
                        };
                    }
                    case "Ask" -> {
                        assertTaskPuncAndConcPuncNull();
                        taskPunc = i -> i == BELIEF || i == GOAL;
                        concPunc = o -> switch (o) {
                            case BELIEF -> QUESTION;
                            case GOAL -> QUEST;
                            default -> (byte) 0;
                        };
                    }
                    case "AskAsk" -> {
                        assertTaskPuncAndConcPuncNull();
                        taskPunc = i -> i == QUESTION || i == QUEST;
                        concPunc = o -> switch (o) {
                            case QUESTION -> QUESTION;
                            case QUEST -> QUEST;
                            default -> (byte) 0;
                        };
                    }

                    case "AskHowAsk" -> {
                        /* like AskAsk, but also adds Goal->Quest (how) */
                        assertTaskPuncAndConcPuncNull();
                        taskPunc = i -> i == QUESTION || i == QUEST || i == GOAL;
                        concPunc = o -> switch (o) {
                            case QUESTION -> QUESTION;
                            case QUEST, GOAL -> QUEST;
                            default -> (byte) 0;
                        };
                    }
                    case "AskAll" -> {
                        assertTaskPuncAndConcPuncNull();
                        taskPunc = i -> true;
                        concPunc = o -> switch (o) {
                            case BELIEF, QUESTION -> QUESTION;
                            case GOAL, QUEST -> QUEST;
                            default -> throw new UnsupportedOperationException();
                        };
                    }

                    case "Identity" -> {
                        assert (beliefTruth == null && goalTruth == null);
                        taskPunc = i -> true;
                        concPunc = o -> o;
                        beliefTruth = goalTruth = IDENTITY;
                    }

                    default -> throw new RuntimeException("unknown punctuation: " + which);
                }
                break;

            case "Time":
                time = OccurrenceSolver.solvers.get(which);
                if (time == null)
                    throw new RuntimeException("unknown Time modifier:" + which);
                break;


            case "Belief":
                beliefTruth = which;
                break;

            case "Goal":
                goalTruth = which;
                break;


            default:
                throw new RuntimeException("Unhandled postcondition: " + type + ':' + which);
        }
    }

    private void assertTaskPuncAndConcPuncNull() {
        assert (taskPunc == null && concPunc == null);
    }

    /**
     * @param c assumed to be unneg
     */
    private Term conclusion(Term c) {

        Term cu = c.unneg();

        //verify that all pattern variables in c are present in either taskTerm or beliefTerm
        assertConclusionVariablesPresent(cu);

        if (cu instanceof Compound) {

//            Term c00;
//            Pair<List<Term>, ArrayHashSet<Term>> conjs = null;
//            if (c.hasAll(INH.bit | CONJ.bit)) {
//                List<Term> subbedConj = new Lst<>(0);
//                ArrayHashSet<Term> savedConj = new ArrayHashSet<>(0);
//                c00 = saveEteConj(c, subbedConj, savedConj);
//                if (!subbedConj.isEmpty())
//                    conjs = Tuples.pair(subbedConj, savedConj);
//            } else {
//                c00 = c;
//            }

            Term c1 = patternify(cu);

            Term c2 = concOptInline(c1); //auto-add taskInline,beliefInline - but may elide constraint

            c2.ANDrecurse(x -> x.hasAny(INH.bit | SIM.bit), x -> {
                conclusionInhSim(x);
                return true;
            });

            //return conjs != null ? restoreEteConj(c2, conjs.getOne(), conjs.getTwo()) : c2;
            return c2.negIf(c instanceof Neg);
        } else {
            assert (cu instanceof Variable);
            isNotAny(cu, ~Taskables); //must be taskable

//            if (!c.equals(taskPattern) && taskPattern instanceof Compound && (((Compound)taskPattern).containsRecursively(c) || ((Compound)beliefPattern).containsRecursively(c))) {
//
//                //isNotAny(c, s); ///any other ops?
//                iff(c, TermMatcher.Is.is(Op.Variables | Op.IMG.bit), false, false); //only in precondition, not constraint
//            }

            return c;
        }

    }

    private void assertConclusionVariablesPresent(Term c) {
        boolean tb = taskPattern.equals(beliefPattern);
        c.ANDrecurse(Termlike::hasVarPattern, z -> {
            if (z.VAR_PATTERN()) {
                if (!(taskPattern.equals(z) || (taskPattern instanceof Compound && ((Compound) taskPattern).containsRecursively(z))) &&
                        (tb || !(beliefPattern.equals(z) || (beliefPattern instanceof Compound && ((Compound) beliefPattern).containsRecursively(z)))))
                    throw new TermException("conclusion has pattern variable not contained in task or belief pattern", z);
            }
        });
    }

    private void conclusionInhSim(Term x) {
        if (x.isAny(INH.bit | SIM.bit)) {//auto-constraints for construction of a valid inh/sim
            Subterms xx = x.subtermsDirect();
            Term xa = xx.sub(0), xb = xx.sub(1);
            if (xa instanceof Variable && xb instanceof Variable) {

                if (knownValid(x))
                    return;
                if (knownValid((x.INH() ? SIM : INH).the(xx))) //try the opposite, for which also holds
                    return;
//                //try the reverse inh
//                if (knownValid(INH.the(xx.reversed())))
//                    return;

                neqRCom((Variable) xa, (Variable) xb);
            }
        }

    }

    /**
     * tests if an example of the INH has already been successfully constructed so automatically valid
     */
    private boolean knownValid(Term x) {
        return found(x, taskPattern)
                ||
                (taskPattern !=/*!.equals(*/beliefPattern/*)*/ && found(x, beliefPattern));
    }

    private Term concOptInline(Term x) {

        if (x.dt() == XTERNAL)
            return x; //dont modify, because the result could be different but same root

        //decide when inline "paste" (macro substitution) of the premise task or belief terms is allowed.
        boolean tInline = !(taskPattern instanceof Variable) && taskPattern.dt() != XTERNAL;
        boolean bInline = !(beliefPattern instanceof Variable) && !taskPattern.equals(beliefPattern) && beliefPattern.dt() != XTERNAL;

        Term T, B;
        if ((/* one is missing or b < t */ !tInline || !bInline) || beliefPattern.volume() <= taskPattern.volume()) {
            //subst task first
            T = tInline ? x.replace(taskPattern, TaskInline) : x;
            B = bInline ? T.replace(beliefPattern, BeliefInline) : T;
            return B;
        } else {
            //reverse order for max coverage
            B = x.replace(beliefPattern, BeliefInline);
            T = B.replace(taskPattern, TaskInline);
            return T;
        }


    }

    @Override
    protected void commit() {

        TruthFunction beliefTruthOp = truthModel.get(beliefTruth);
        if (beliefTruth != null && beliefTruthOp == null)
            throw new RuntimeException("unknown BeliefFunction: " + beliefTruth);

        TruthFunction goalTruthOp = truthModel.get(goalTruth);
        if (goalTruth != null && goalTruthOp == null)
            throw new RuntimeException("unknown GoalFunction: " + goalTruth);

        /* default arity: force one or the other */
        if (arity == 0)
            arity = (beliefTruthOp != null && !beliefTruthOp.single()) || (goalTruthOp != null && !goalTruthOp.single()) ? 2 : 1;



        /* infer missing conclusion punctuation */
        if (concPunc == null) {
            if (beliefTruth != null && goalTruth != null) {
                if (taskPunc == null) taskPunc = x -> x == BELIEF || x == GOAL;
                concPunc = x -> x;
            } else if (beliefTruth != null) {
                if (taskPunc == null) taskPunc = x -> x == BELIEF;
                concPunc = x -> BELIEF;
            } else if (goalTruth != null) {
                if (taskPunc == null) taskPunc = x -> x == GOAL;
                concPunc = x -> GOAL;
            }

        }

        if (concPunc == null)
            throw new UnsupportedOperationException("no concPunc specified");

        /* infer necessary task punctuation */
        if (taskPunc == null) {
            //auto
            if (beliefTruth != null && goalTruth != null) {
                taskPunc = t -> t == BELIEF || t == GOAL; //accept belief and goal and map to those
            } else if (beliefTruth != null) {
                taskPunc = t -> t == BELIEF; //accept only belief -> belief
            } else if (goalTruth != null) {
                taskPunc = t -> t == GOAL; //accept only goal -> goal
            } else
                throw new UnsupportedOperationException();
        }

        PuncMap p = new PuncMap(
                PuncMap.p(taskPunc, concPunc, BELIEF),
                PuncMap.p(taskPunc, concPunc, GOAL),
                PuncMap.p(taskPunc, concPunc, QUESTION),
                PuncMap.p(taskPunc, concPunc, QUEST),
                (byte) 0 //COMMAND
        );
        taskPunc(p);

        if (time == null) time = time(taskPunc);

        this.truthify = intern(new Truthify(
                p,
                beliefTruthOp, goalTruthOp));
        if (!p.can(QUESTION) && !p.can(QUEST) &&
                (truthify.beliefMode == -1 || (truthify.beliefMode == 0 && !truthify.beliefOverlap)) &&
                (truthify.goalMode == -1 || (truthify.goalMode == 0 && !truthify.goalOverlap))
        ) {
            //TODO decide if all cases are covered by this
            noOverlap();
        }

        this.pattern = _pattern.transformPN(this::conclusion);

        if (pattern.hasAll(FuncBits))
            pattern.ANDrecurse(x -> x.hasAll(FuncBits),
                    Unifiable.eventFuncConstraints(this));

        super.commit();

        this.taskify = taskify(pattern);
    }

    @Override
    public void compile() {
        if (termify == null) {
            synchronized (this) { //HACK some sharing might be occurring
                if (this.termify == null) {
                    conditions(); //compute FIRST

                    termify = new Termify(taskPattern, beliefPattern, constraints);
//                    this.rule = new CompiledPatternReaction(
//                            truthify, time, taskify, tag
//                    );

                    this.idConclusion = $.p(
                            termify.ref,
                            pattern,
                            truthify, time.term
                    );

                    this.idCondition = SETe.the(condition);

                    this.id = $.p(idCondition, idConclusion);

                }
            }
        }
    }

    @Override
    public final Term term() {
//        final Reaction r = this.rule;
//        if (r!=null)
        //return $.identity(this);
        return id;
//        else
//            ..
    }

    @Override
    public Action action(Cause<Reaction> why) {
        return new TruthifyAction(why);
    }

    private Taskify taskify(Term p) {

        //1. direct
        Function<Deriver, Term> c = null;

        //noinspection ConstantConditions
        if (c == null && (p == TaskInline || p.equals(taskPattern)))
            c = concludePremise(true);
        if (c == null && (p.equalsNeg(TaskInline) || p.equalsNeg(taskPattern)))
            c = neg(concludePremise(true));

        if (c == null && (p == BeliefInline || p.equals(beliefPattern)))
            c = concludePremise(false);
        if (c == null && (p.equalsNeg(BeliefInline) || p.equalsNeg(beliefPattern)))
            c = neg(concludePremise(false));

        if (c == null)
            c = conclusionExtractPremiseSubterm(p);

//        if (c == null) {
//            c = neg(conclusionExtractPremiseSubterm(p.neg()));
//            if (c!=null)
//                Util.nop(); //TEMPORARY
//        }

        if (c == null)
            c = conclusionExtractPremiseSubtermTransformed(p); //TODO constraints
//        if (c == null) {
//            c = neg(conclusionExtractPremiseSubtermTransformed(p.neg()));
//            if (c!=null)
//                Util.nop(); //TEMPORARY
//        }

        return new Taskify(p, c);
    }

    @Nullable private static Function<Deriver, Term> neg(@Nullable Function<Deriver, Term> f) {
        if (f == null) return null;
        return f.andThen(Term::neg);
    }

    @Nullable
    private Function<Deriver, Term> concludePremise(boolean taskOrBelief) {
        return constrain(extractTaskOrBelief(taskOrBelief), taskOrBelief ? taskPattern : beliefPattern);
    }

    @Nullable private Function<Deriver, Term> conclusionExtractPremiseSubtermTransformed(Term pattern) {

        if (pattern instanceof Compound && !pattern.CONJ()) { //TODO prefilter by check structure intrsecton of pattern with task/belief

            Subterms patternSubs = pattern.subtermsDirect();

            int N = patternSubs.subs();
            int pv = patternSubs.volume();
            int ps = patternSubs.structureSubs();

            Subterms patternSubsReversed = N > 1 ? patternSubs.reversed() : null;

            boolean patternCommute = N > 1 && pattern.op().commutative;
            int negCount = patternSubs.count(t -> t instanceof Neg);
            Subterms patternSubsNegated = negCount == 0 /*|| negCount == patternSubs.subs()*/ ? //HACK
                    patternSubs.negated() : null;

            byte[] bestPath = null;
            boolean bestRoot = false;
            int bestMode = -1;

            ObjectBooleanHashMap<Term> exacts = new ObjectBooleanHashMap<>(0);
            ObjectBooleanHashMap<Term> reversed = patternSubsReversed != null ? new ObjectBooleanHashMap<>(0) : null;
            ObjectBooleanHashMap<Term> negated = patternSubsNegated != null ? new ObjectBooleanHashMap<>(0) : null;

            Predicate<Compound> possibleContainer =
                    x -> x.volume() >= pv && Op.hasAll(x.structureSubs(), ps);

            boolean[] sources = taskPattern == beliefPattern ? new boolean[]{true} : new boolean[]{true, false};
            for (boolean root : sources) {

                Consumer<Term> processContent = z -> {
                    if (z instanceof Atomic)
                        return;

                    Subterms zz = z.subterms();
                    int zs = zz.subs();
                    if (zs != N) return;

                    boolean zCommute = z.COMMUTATIVE();
                    if (!patternCommute && zCommute)
                        return; //avoid sourcing from commutive terms which may be unified in alternate permutations

                    boolean r = root;
                    if (patternSubs.equals(zz))
                        exacts.put(z, r);
                    else if (patternSubsReversed != null && !(patternCommute && zCommute) && patternSubsReversed.equals(zz))
                        reversed.put(z, r);
                    else if (patternSubsNegated != null && patternSubsNegated.equals(zz))
                        negated.put(z, r);
                    //TODO add reversed's, negated, etc
                };
                (root ? taskPattern : beliefPattern).ANDrecurse(possibleContainer, processContent);
            }


            for (ObjectBooleanPair<Term> m : exacts.keyValuesView()) {
                boolean root = m.getTwo();
                byte[] p = pathExact(root ? taskPattern : beliefPattern, m.getOne());
                if (p != null && (bestPath == null || bestPath.length > p.length)) {
                    bestRoot = root;
                    bestPath = p;
                    bestMode = NEW_OP;
                }
            }

            if (reversed != null) {
                for (ObjectBooleanPair<Term> m : reversed.keyValuesView()) {
                    boolean root = m.getTwo();
                    byte[] p = pathExact(root ? taskPattern : beliefPattern, m.getOne());
                    if (p != null && (bestPath == null || bestPath.length > p.length + REVERSE_COST)) {
                        bestRoot = root;
                        bestPath = p;
                        bestMode = REVERSED;
                    }
                }
            }


            if (negated != null) {
                for (ObjectBooleanPair<Term> m : negated.keyValuesView()) {
                    boolean root = m.getTwo();
                    byte[] p = pathExact(root ? taskPattern : beliefPattern, m.getOne());
                    if (p != null && (bestPath == null || bestPath.length > p.length + NEGATE_COST)) {
                        bestRoot = root;
                        bestPath = p;
                        bestMode = NEGATED;
                    }
                }
            }


            if (bestMode != -1) {
                int patternDT = pattern.dt();
                Op o = pattern.op();
                return extractTaskOrBelief(bestRoot)
                        .andThen(extractSubterm(bestPath))
                        .andThen(Term::subtermsDirect)
                        .andThen(switch (bestMode) {
                            case 0 -> s -> s;
                            case 1 -> Subterms::reversed;
                            case 2 -> Subterms::negated;
                            default -> throw new TODO();
                        }).andThen(s -> o.the(patternDT, s));
            }
        }

        return null;

    }

    private Function<Deriver, Term> conclusionExtractPremiseSubterm(Term p) {
        byte[] concInTask = pathExact(taskPattern, p);
        byte[] concInBelief = (beliefPattern == taskPattern) ? null : pathExact(beliefPattern, p);
        boolean ct = concInTask != null;
        boolean cb = concInBelief != null;
        if (ct && cb) {
            if (concInTask.length > concInBelief.length) {
                concInTask = null;
                ct = false;
            } else
                cb = false;
        }

        if (!ct && !cb) return null;

        Function<Term, Term> sub = extractSubterm(ct ? concInTask : concInBelief);
        Term patternSub = ct ? sub.apply(taskPattern) : sub.apply(beliefPattern);

        return constrain(extractTaskOrBelief(ct).andThen(sub), patternSub);
    }

    /**
     * wraps resolver in a guard that tests for constraints
     */
    @Nullable
    private Function<Deriver, Term> constrain(@Nullable Function<Deriver, Term> resolver, Term patternComponent) {
        if (resolver == null)
            return null;

        if (constraints.length == 0) return resolver;

        var pcEq = patternComponent.equals();
        UnifyConstraint<Deriver.PremiseUnify>[] constraints = Stream.of(this.constraints)
                .filter(c -> pcEq.test(c.x) ||
                        (patternComponent instanceof Compound && ((Compound) patternComponent).containsRecursively(c.x)))
                .toArray(UnifyConstraint[]::new);

        if (constraints.length == 0)
            return resolver;
        else {
            return d -> {
                Term y = resolver.apply(d);
                if (y != null && y != Null) {
                    d.unify.clear(constraints);
                    if (!d.unify.unify(patternComponent, y))
                        return null;
//                    for (UnifyConstraint u : constraints) {
//                        if (u.invalid(y, null))
//                            return null; //constraint violated
//                    }
                }
                return y;
            };
        }
    }

    private Function<Deriver, Term> extractTaskOrBelief(boolean taskOrBelief) {
        return taskOrBelief ?
                (d -> d.premise.from()) :
                (d -> d.premise.to());
    }

    @Override
    public Class<? extends MutableReaction> type() {
        return PatternReaction.class;
    }

    private void taskify(NALPremise i, byte punc, TruthFunction f, Deriver d, short cause) {
        i.refreshTimeIfSerial();

        var t = time.apply(i, punc, d);

        if (f != null && !d.projectBeliefOrGoal(f, t)) {
            d.nar.emotion.deriveFailTruthUnderflow.increment();
            return;
        }

        if (NAL.derive.TASKIFY_INLINE) {
            termify.run(new TaskifyPremise(PatternReaction.this, punc, t, cause), d);
        } else {
            d.add(new TaskifyPremise(PatternReaction.this, i, punc, t, cause));
        }
    }

    private static class UppercaseAtomsToPatternVariables extends RecursiveTermTransform {

        static final java.util.Set<Atomic> reservedMetaInfoCategories = java.util.Set.of(
                Atomic.the("Belief"),
                Atomic.the("Goal"),
                Atomic.the("Punctuation"),
                Atomic.the("Time")
        );
        final UnifriedMap<String, Term> map = new UnifriedMap<>(8);

        @Override
        public Term applyAtomic(Atomic atomic) {
            if (atomic instanceof Atom && !reservedMetaInfoCategories.contains(atomic)) {
                String name = atomic.toString();
                if (name.length() == 1 && Character.isUpperCase(name.charAt(0)))
                    return map.computeIfAbsent(name, (n) -> $.varPattern(1 + map.size()));
            }
            return atomic;
        }

    }

    private static class FunctorLinkingVariableNormalization extends VariableNormalization {
        @Override
        public Term applyAtomic(Atomic x) {
            if (x instanceof Atom) {
                Functor f = Builtin.functor(x);
                return f != null ? f : x;
            } else
                return super.applyAtomic(x);
        }

    }

//    public static class EqualSub extends UnifyConstraint<Deriver.PremiseUnify> {
//        private final Function<Deriver, Term> y;
//        private final byte[] path;
//
//        static public EqualSub the(byte[] ct, byte[] cb, Variable c) {
//            boolean taskOrBelief = cb == null || ct != null && ct.length <= cb.length;
//            byte[] path = taskOrBelief ? ct : cb;
//            return new EqualSub(c, taskOrBelief, path);
//        }
//
//        private EqualSub(Variable c, boolean taskOrBelief, byte[] path) {
//            super(c, "equalSub", taskOrBelief ? Atomic.atom("task") : Atomic.atom("belief"), $.p(path));
//            y = extractTaskOrBelief(taskOrBelief);
//            this.path = path;
//        }
//
//        @Override
//        public float cost() {
//            return 0.1f;
//        }
//
//        @Override
//        public boolean invalid(Term x, Deriver.PremiseUnify f) {
//            Term Y = y.apply(f.deriver());
//            return !x.equals(Y.sub(path));
//        }
//    }

    private static class HasVars extends RelationConstraint<Deriver.PremiseUnify> {

        HasVars(Variable x, Variable y) {
            super(HasVars.class, x, y);
        }

        @Override
        public boolean invalid(Term x, Term y, Deriver.PremiseUnify u) {
            return !x.hasVars() && !y.hasVars();
        }

        @Override
        protected HasVars newMirror(Variable newX, Variable newY) {
            return new HasVars(newX, newY);
        }

        @Override
        public float cost() {
            return 0.05f;
        }
    }

    public static final TermMatch HasEvents = new TermMatch() {

        @Override
        public boolean test(Term term) {
            return term.CONDS();
        }

        @Override
        public float cost() {
            return 0.01f;
        }
    };

    /**
     * TODO static and/or move to Truthify.java
     */
    private final class TruthifyAction extends Action {

        TruthifyAction(Cause<Reaction> why) {
            super(why);
        }

        @Override
        public boolean test(Deriver d) {

            NALPremise p = (NALPremise) d.premise;

            byte punc = punc(p);

            TruthFunction f;
            if (punc == BELIEF || punc == GOAL) f = truthFn(p, punc);
            else if (punc == QUEST || punc == QUESTION) f = null;
            else throw new Op.InvalidPunctuationException(punc);

            taskify(p, punc, f, d, cause());

            return true;
        }


        private byte punc(NALPremise p) {
            return truthify.punc.get(p.task().punc());
        }

        private TruthFunction truthFn(NALPremise p, byte punc) {
            if (NAL.DEBUG && !valid(p, punc)) throw new WTF(); //return null;

            return punc == BELIEF ? truthify.belief : truthify.goal;
        }

        /**
         * WITH CORRECT PREDICATES, THESE CONDITIONS WILL HAVE BEEN ELIMINATED BEFORE REACHING HERE
         */
        private boolean valid(NALPremise p, byte punc) {

            return
                    //single <=> no-belief (strict)
                    p.single() == (p.belief() == null)

                            &&

                            //this condition is somewhat (but not completely?) prevented by NoOverlap predicate
                            (!p.overlapDouble() || (punc == BELIEF ? truthify.beliefOverlap : truthify.goalOverlap));
        }


    }

    private static int structOR(Term y) {
        if (!y.SETe()) {
            return bit(y);
        } else {
            int struct = 0;
            for (Term yy : y.subterms())
                struct |= bit(yy);
            return struct;
        }
    }

    private static int bit(Term yy) {
        Op o = op($.unquote(yy));
        if (o == null)
            throw new UnsupportedOperationException("not an operator: " + yy);
        return o.bit;
    }
}