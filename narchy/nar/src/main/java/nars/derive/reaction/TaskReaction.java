package nars.derive.reaction;

import jcog.exe.flow.Feedback;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.task.NALTask;
import org.jetbrains.annotations.Nullable;

/**
 * responds with zero or more resolved premises in response to input task
 *
 * ex: replies answers to question tasks
 * TODO  'AnswerQuestionsFromProlog' and 'AnswerQuestionsFromExhaustiveMemorySearch'
 */
public abstract class TaskReaction extends NativeReaction {

	protected TaskReaction() {
		this(true,true,true,true);
	}

	protected TaskReaction(boolean b, boolean g, boolean q, boolean Q) {
		single(b, g, q, Q);
	}

//	/** how to route the generated task; either as a continuation premise, or as a task to be stored. */
//	protected boolean store = true;

	protected final void react(@Nullable NALTask y, Deriver d, Cause<Reaction> why) {
		if (y != null) {
			//assume that y's why has been set already, just need to append this transform why
			//Premise p = d.premise;

//			//TODO Budget.priTransform
//			DefaultBudget b = (DefaultBudget) d.focus.budget;
//			double f = b.simple(y.volume(), d);
//			y.priMul((float)f);

			if (d.add(y)) {
//				if (log) logger.info("{} => {} ({})", p, y, why.id);
				return;
			}
		}

		Feedback.is("derive.NALTask.invalid");
	}


//	private static final Logger logger = Log.log(TaskReaction.class);
//	private boolean log = false;
//	public final TaskReaction log(boolean b) {
//		this.log = b;
//		return this;
//	}

}