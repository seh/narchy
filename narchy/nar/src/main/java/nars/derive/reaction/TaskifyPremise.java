package nars.derive.reaction;

import jcog.Util;
import jcog.exe.flow.Feedback;
import nars.Emotion;
import nars.NAL;
import nars.Op;
import nars.Term;
import nars.action.pattern.SubPremise;
import nars.control.Why;
import nars.derive.Deriver;
import nars.derive.op.Taskify;
import nars.derive.util.DerivationFailure;
import nars.derive.util.DerivedTask;
import nars.premise.NALPremise;
import nars.premise.Premise;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.term.Neg;
import nars.term.atom.Bool;
import nars.term.atom.Int;
import nars.truth.MutableTruthInterval;
import org.eclipse.collections.api.tuple.Pair;
import org.jetbrains.annotations.Nullable;

import static nars.Op.ETERNAL;
import static nars.Op.TIMELESS;

public final class TaskifyPremise extends SubPremise<PatternReaction> {

    public final MutableTruthInterval t;
    public final byte puncOut;

    private final short cause;

    @Deprecated
    TaskifyPremise(PatternReaction id, byte puncOut, MutableTruthInterval t, short cause) {
        super(id);
        this.cause = cause;
        this.puncOut = puncOut;
        this.t = t;
    }

    TaskifyPremise(PatternReaction rule, NALPremise parent, byte puncOut, MutableTruthInterval t, short cause) {
        super(rule, parent, null);
        this.cause = cause;
        this.puncOut = puncOut;
        this.t = t;
        this.parent = parent;
    }

    @Nullable
    private static Term taskTerm(Term x, byte punc) {
        boolean neg = x instanceof Neg;
        if (neg) x = x.unneg();

        Term y = NALTask.taskTerm(x, punc);

        boolean q = Op.QUESTION_OR_QUEST(punc);

        if (NAL.derive.QUESTION_SALVAGE && y == null && q)
            y = questionSalvage(x, punc);

        if (y == null) return null;

        Term z = y.negIf(neg && !q);

        return Util.maybeEqual(z, x);
    }

    @Nullable private static Term questionSalvage(Term x, byte punc) {
        return NALTask.taskTerm(Taskify.questionSalvage(x), punc, false, true);
    }

    private static void assertOccValid(MutableTruthInterval o, Deriver d) {
        assertOccValid(o.s, o.e, d);
    }

    private static void assertOccValid(long s, long e, Deriver d) {

        NALTask task = d.premise.task();
        NALTask belief = d.premise.belief();
        boolean single = belief == null;

        if (!((e != TIMELESS) &&
                (s == ETERNAL) == (e == ETERNAL) &&
                (e >= s)) ||
                ((s == ETERNAL) &&
                        (single ? !task.ETERNAL() : (!task.ETERNAL() || !belief.ETERNAL())))) {
            throw new RuntimeException("invalid occurrence result: " + s + ".." + e);
        }
        if (NAL.test.DEBUG_EXTRA /*&& solver!=Now*/) {
            if (s != ETERNAL && !(task instanceof SerialTask) && !(belief instanceof SerialTask)) {
                long taskRange = task.ETERNAL() ? 0 : task.range();
                long beliefRange = single ? 0 : (belief.ETERNAL() ? 0 : belief.range());
                long concRange = e - s;
                if (concRange - 2L * d.ditherDT() > Math.max(taskRange, beliefRange)) {
                    throw new RuntimeException("excessive derived task occurrence: task=" + taskRange + ", belief=" + beliefRange + " conc=" + concRange);
                }
            }
        }

    }

    @Override
    public final Reaction reaction() {
        return id;
    }

    @Override
    public int cause() {
        return cause;
    }

    @Override
    public void act(Deriver d) {
        id.termify.run(this, d);
    }

    public void taskify(@Nullable Term y, Deriver d) {
        if (y == null) return;

        Emotion e = d.nar.emotion;

        if (!y.unneg().TASKABLE()) {
            Feedback.is("derive.NALTask.failTaskTerm", e.deriveFailTaskTerm);
            return;
        }

        var p = (NALPremise) d.premise;//parent;

        Pair<Term, MutableTruthInterval> yy;
        try (var __1 = e.time_derive_occurrify.time()) {
            yy = time(d, y, p, this.t);
            if (yy == null) {
                Feedback.is("derive.NALTask.failTemporal", e.deriveFailTemporal);
                return;
            }
        }

        try (var __ = e.time_derive_taskify.time()) {

            byte punc = puncOut;

            MutableTruthInterval t = yy.getTwo();
            if (Op.BELIEF_OR_GOAL(punc)) {
                if (!t.ditherTruth(d.nar, d.eviMin)) {
                    Feedback.is("derive.NALTask.failTruthDither", e.deriveFailTaskTerm);
                    return;
                }
            }

            Term z = taskTerm(yy.getOne(), punc);
            if (z == null) {
                Feedback.is("derive.NALTask.failTaskTerm", e.deriveFailTaskTerm);
                return;
            }


            d.add(task(p, punc, z, t, d), this);
        }


    }

    @Nullable
    private Pair<Term, MutableTruthInterval> time(Deriver d, Term y, NALPremise p, MutableTruthInterval c) {
        Pair<Term, MutableTruthInterval> o = id.time.solve(p, y, c, id, d);

        if (NAL.DEBUG && o!=null) assertOccValid(o.getTwo(), d);

        return o;
    }


    @Nullable
    public Term termify(Deriver d) {
        Term y = id.taskify.conc().apply(d);

        if (y instanceof Bool) {
            DerivationFailure.Null.record(d);
            return null;
        }
        if (d.invalidVol(y.unneg())) {
            DerivationFailure.VolMax.record(d);
            return null;
        }
        return y;
    }

    public static NALTask task(NALPremise p, byte punc, Term x, MutableTruthInterval out, Deriver d) {


        NALTask y = NALTask.task(x, punc,
                Op.BELIEF_OR_GOAL(punc) ? out : null,
                out, p.stamp(punc));

        return NAL.DEBUG ? new DerivedTask(y, p) : y;
    }

    @Override
    protected void mergeWhy(Premise incoming) {
        this.why = this.why == null ? ((TaskifyPremise) incoming).why : Why.why(this, incoming);
    }

    @Override
    public Term why() {
        final int c = NAL.causeCapacity.intValue();
        return c > 0 ? Why.why2(c, Int.the(cause), parent.why()) : null;
    }

}