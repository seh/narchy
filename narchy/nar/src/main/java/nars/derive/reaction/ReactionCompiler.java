package nars.derive.reaction;

import jcog.WTF;
import jcog.data.list.Lst;
import jcog.data.map.UnifriedMap;
import jcog.data.set.ArrayHashSet;
import jcog.tree.perfect.TrieNode;
import nars.NAL;
import nars.NAR;
import nars.Term;
import nars.action.Action;
import nars.control.Cause;
import nars.derive.Deriver;
import nars.derive.op.ConstraintAsPremisePredicate;
import nars.derive.util.Actions;
import nars.derive.util.memoize.MemoizedPredicate;
import nars.term.control.AND;
import nars.term.control.FORK;
import nars.term.control.NegPredicate;
import nars.term.control.PREDICATE;
import nars.term.util.map.TermPerfectTrie;
import nars.unify.constraint.UnaryConstraint;
import org.eclipse.collections.api.RichIterable;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.api.tuple.primitive.ObjectIntPair;
import org.eclipse.collections.impl.map.mutable.primitive.ObjectFloatHashMap;
import org.eclipse.collections.impl.multimap.list.FastListMultimap;
import org.roaringbitmap.RoaringBitmap;

import java.util.*;
import java.util.function.BiFunction;

import static nars.$.$$;
import static nars.term.control.PREDICATE.andCompile;
import static org.eclipse.collections.impl.tuple.primitive.PrimitiveTuples.pair;

/**
 * high-level interface for compiling premise deriver rules
 */
class ReactionCompiler {

    /**
     * repeatValue compensates for method invocation overhead costs.
     * repeatValue>1: value repetitions more than if flat */
    private static final float repeatValue = 2;

    /** maybe faster one way or another, TODO benchmark */

    private static PREDICATE _unneg(PREDICATE a) {
        return a instanceof NegPredicate ? __unneg(a) : a;
    }

    private static PREDICATE __unneg(PREDICATE x) {
        return ((NegPredicate)x).p;
    }

    private static PREDICATE<Deriver> compile(TrieNode<List<PREDICATE<Deriver>>, Action> node) {
        int fanout = node.childCount();
        if (fanout == 0) {
            List<PREDICATE<Deriver>> cond = node.seq();
            return cond!=null ? AND.the(cond) : null;
        }

        ArrayHashSet<PREDICATE<Deriver>> bb = new ArrayHashSet<>(fanout);

        for (TrieNode<List<PREDICATE<Deriver>>, Action> n : node) {
            PREDICATE<Deriver> branch = compileSeq(n);
            if (branch != null) bb.add(branch);
        }

        //return compileSwitch(bb, 2);
        return ForkCompiler.forkCompile(bb.list);
    }

    private static PREDICATE<Deriver> compileSeq(TrieNode<List<PREDICATE<Deriver>>, Action> n) {

        PREDICATE<Deriver> conseq = n.hasChildren() ? compile(n) : null;

        List<PREDICATE<Deriver>> seq = n.seq().subList(n.start(), n.end());

        List<PREDICATE<Deriver>> b =
                conseq!=null ? new Lst(seq, 1) : seq;

        if (conseq!=null) b.add(conseq);

        return AND.the(andCompile(b));
    }

    public static ReactionModel compile(ArrayHashSet<Reaction> reactions, NAR n) {

        ObjectFloatHashMap<PREDICATE> freq = new ObjectFloatHashMap<>(512);

        for (Reaction R : reactions) {
            for (PREDICATE<Deriver> x : R.condition)
                freq.addToValue(_unneg(x), freqIncrement(x.cost()));
        }

//        freq.keyValuesView().toSortedList(
//            Comparator.comparingDouble(p -> -p.getTwo())).forEach(System.out::println);


        Comparator<PREDICATE> sort = (a, b) -> {
            if (a==b) return 0;
            float ac = freq.get(_unneg(a));
            float bc = freq.get(_unneg(b));

            int uc = Float.compare(bc, ac); //decreasing
            if (uc!=0) return uc;

//            float aa = a.cost(), bb = b.cost();
//            int ab = Float.compare(aa, bb); //increasing
//            if (ab!=0)
//                return ab;

            return a.compareTo(b);
        };

        int s = reactions.size(); assert(s > 0);
        Action[] hows = new Action[s];
        TermPerfectTrie<PREDICATE<Deriver>, Action> paths = new TermPerfectTrie<>(s);

        ArrayDeque<Cause<Reaction>> causes = n.control.newCauses(reactions.list.stream());

        short i = 0;
        for (Reaction r : reactions.list) {

            int cs = r.condition.size();

            Lst<PREDICATE<Deriver>> pre = new Lst<>(0, new PREDICATE[cs + 1]);

            pre.addAll(r.condition);

            pre.sort(sort);

            //tag==null ? cause(n, ref) : tags.computeIfAbsent(tag, t -> cause(n, Atomic.the(t))) //GROUPED
            Action a = r.action(causes.removeFirst());

            pre.addFast((hows[i++] = a).deferred());

            Action added = paths.put( pre,  a );
            if (added != null)
                throw new WTF("duplicate reaction:\n\t" + pre + "\n\t:=" + a.why.name + "\n\t =" + added.why.name);
        }

        if (paths.isEmpty())
            throw new WTF("no reactions added");

        PREDICATE<Deriver> c0 = compile(paths.root);
        if (c0 == null)
            return new ReactionModel(TRUE, hows); //empty

        PREDICATE<Deriver> c1 = MemoizedPredicate.compileMemoize(c0);
        PREDICATE<Deriver> c2 = compilePost(c1);

        return new ReactionModel(c2, hows);

    }

    private static float freqIncrement(float cost) {
        if (!Float.isFinite(cost))
            return 0;
//        return 1;
        //return 1/(1+cost);

        return 1/(1+cost/repeatValue);
    }

    private static PREDICATE<Deriver> compilePost(PREDICATE<Deriver> t) {
        return t.transformPredicate(x -> {
            if (x instanceof AND)
                return compilePostAND((AND<Deriver>) x);
            else
                return x;
        });
    }

    private static PREDICATE<Deriver> compilePostAND(AND<Deriver> x) {
        //TODO group TermMatch, and other predicates with common preludes
        Lst<PREDICATE<Deriver>> xx = (Lst<PREDICATE<Deriver>>) x.conditions();
        //factorPrelude(xx); //TODO
        if(xx.size()==1)
            return xx.getFirst();

        var y = sortByCost(xx, x);

        //experimental:
        if (NAL.derive.COMPILE_TO_LAMBDA && y.subs()==2)
            return ((AND) y).compileLambda();
        else
            return y;
    }

    private static final PREDICATE<Deriver> TRUE = new PREDICATE<>($$("(true)")) {
        @Override
        public boolean test(Deriver o) {
            return true;
        }
    };

    /** TODO finish */
    private static void factorPrelude(Lst<PREDICATE<Deriver>> xx) {
        FastListMultimap<BiFunction<Term,Term,Term>, ObjectIntPair<UnaryConstraint>> u = null;
        for (int i = 0, xxSize = xx.size(); i < xxSize; i++) {
            PREDICATE<Deriver> c = xx.get(i);
            if (c instanceof ConstraintAsPremisePredicate.UnaryConstraintAsPremisePredicate cu) {
                if (u == null) u = new FastListMultimap<>();
                u.put(cu.extractX, pair(cu.constraint, i));
            }
            //TODO RelationConstraint
        }
        if (u!=null) {
            RichIterable<Pair<BiFunction<Term, Term, Term>, RichIterable<ObjectIntPair<UnaryConstraint>>>> uu
                    = u.keyMultiValuePairsView().reject(p -> p.getTwo().size() < 2);
            //if (!uu.isEmpty()) {
                //TODO group
                //Util.nop();
            //}
        }


    }

    private static <X> PREDICATE<X> sortByCost(Lst<PREDICATE<X>> c, PREDICATE<X> x) {
        var s = c.clone();
        s.sort(PREDICATE.CostIncreasing);
        return c.equalsIdentity(s) ? x : AND.the(s); //order changed?
    }

    private enum ForkCompiler { ;
        private static class SubCond {

            private static final Comparator<SubCond> subCondComparator = Comparator.comparingDouble(SubCond::costIfBranch);

            final PREDICATE p;
            final RoaringBitmap branches = new RoaringBitmap();
            private final float pCost;

            private SubCond(PREDICATE p) {
                this.p = p;
                this.pCost = p.cost();
            }

            static void bumpCond(PREDICATE p, int branch, Map<PREDICATE, SubCond> conds) {
                if (p instanceof AND) {
                    for (Term pp : p.subterms())
                        add((PREDICATE) pp, branch, conds); //first layer only
                } else {
                    if (Float.isFinite(p.cost()))
                        add(p, branch, conds);
                }
            }

            private static void add(PREDICATE p, int branch, Map<PREDICATE, SubCond> conds) {
                conds.compute(p, (xx, e) -> {
                    if (e == null)
                        e = new SubCond(xx);
                    e.bump(branch);
                    return e;
                });
            }

            void bump(int branch) {
                branches.add(branch);
            }

            @Override
            public String toString() {
                return p + " x " + branches;
            }

            int size() {
                return branches.getCardinality();
            }

            float costIfBranch() {
                return size() / (1 + pCost);
            }
        }


        private static PREDICATE<Deriver> forkCompile(List<PREDICATE<Deriver>> x) {
            if (x == null)
                return null;

            Map<PREDICATE, SubCond> conds = null;
            do {
                int n = x.size();
                if (n > 1) {

                    if (conds == null) conds = new UnifriedMap<>(n);
                    else conds.clear();

                    for (int b = 0; b < n; b++)
                        SubCond.bumpCond(x.get(b), b, conds);

                    if (!conds.isEmpty()) {
                        Collection<SubCond> cv = conds.values();
                        if (cv.removeIf(z -> z.size() < 2)) {

                            SubCond best = null;

                            for (SubCond subCond : cv) {
                                if ((best == null || SubCond.subCondComparator.compare(subCond, best) > 0))
                                    best = subCond;
                            }
                            //if (best == null)  break; //??

                            if (best != null) {
                                factor(x, best);
                                n = x.size();
                                if (n > 1)
                                    conds.remove(best.p); //continue
                                //else fall-through:
                            }
                        }
                    }
                }

                if (n == 0) return null;
                if (n == 1) return x.get(0);

            } while (!conds.isEmpty());


            //HACK force merge - Deriver-specific post-processing
            if (x.size() > 1) {
                SortedSet<Action> aa = new TreeSet<>();
                x.removeIf(zz -> {
                    if (zz instanceof Actions) {
                        Collections.addAll(aa, ((Actions) zz).actions);
                        return true;
                    } else if (zz instanceof Action)
                        aa.add((Action)zz);
                    return false;
                });
                if (!aa.isEmpty()) {
                    x.add(FORK.fork(aa));
                }
            }

            return FORK.fork(x);
        }

        private static void factor(Collection<PREDICATE<Deriver>> x, SubCond best) {
            PREDICATE<Deriver> bestPred = best.p;
            SortedSet<PREDICATE<Deriver>> bundle = null;
            int i = 0;
            Iterator<PREDICATE<Deriver>> xx = x.iterator();
            while (xx.hasNext()) {
                PREDICATE<Deriver> px = xx.next();
                if (best.branches.contains(i)) {
                    xx.remove();
                    if (px instanceof AND) {
                        List<PREDICATE<Deriver>> pxSub = ((AND<Deriver>) px).conditions(); //TODO use conditions()
                        if (pxSub.remove(bestPred))
                            px = AND.the(pxSub);


                        if (bundle == null) bundle = new TreeSet<>();
                        bundle.add(px);
                    }
                }
                i++;
            }
            if (bundle != null)
                x.add(AND.the(andCompile(new Lst<PREDICATE<Deriver>>().adding(bestPred, forkCompile(new Lst<>(bundle))))));

        }
    }


}