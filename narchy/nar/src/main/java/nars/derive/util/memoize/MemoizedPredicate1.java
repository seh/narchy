package nars.derive.util.memoize;

import nars.derive.Deriver;
import nars.term.control.PREDICATE;

final class MemoizedPredicate1 extends PREDICATE<Deriver> {

    public final int slot;
    public final PREDICATE<Deriver> test;

    MemoizedPredicate1(PREDICATE<Deriver> test, int slot) {
        super(test.ref);
        this.test = test;
        this.slot = slot;
    }

    @Override
    public boolean test(Deriver d) {
        return d.predicateMemoizations.memoize(slot, d, test);
    }


    @Override public float cost() {
        return test.cost();
    }

}