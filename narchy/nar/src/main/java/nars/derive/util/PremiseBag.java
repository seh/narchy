package nars.derive.util;

import jcog.pri.bag.impl.ArrayBag;
import jcog.pri.op.PriMerge;
import jcog.util.PriReturn;
import nars.premise.Premise;

import java.util.concurrent.atomic.AtomicBoolean;

public class PremiseBag extends ArrayBag<Premise, Premise> {

    static final float forgetRate = 1f;

    long nextUpdate = Long.MIN_VALUE;


    public PremiseBag() {
        super(PriMerge.max);
    }

    @Override
    public Premise key(Premise value) {
        return value;
    }

    @Override
    protected float merge(Premise existing, Premise incoming, float incomingPri) {
        return existing.merge(incoming, merge, PriReturn.Delta);
    }

    private final AtomicBoolean busy = new AtomicBoolean();

    public void commit(int capacity, long now, int dur) {
        if ((now < nextUpdate) || !busy.compareAndSet(false, true))
            return;
        try {
            nextUpdate = now + Math.max(1, dur); //TODO dur?
            commit(forget(forgetRate));
            capacity(capacity);
        } finally {
            busy.set(false);
        }
    }


}