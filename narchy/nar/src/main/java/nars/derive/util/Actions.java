package nars.derive.util;

import nars.$;
import nars.action.Action;
import nars.derive.Deriver;
import nars.term.atom.Atomic;
import nars.term.control.PREDICATE;

/**
 * estimates the priority of a continued procedure, and attempts to add into the deriver
 */
public final class Actions extends PREDICATE<Deriver> {

    public final Action[] actions;

    private static final Atomic F = Atomic.the("could");

    private Actions(Action[] a) {
        super($.func(F, a));
        assert(a.length > 1);
        this.actions = a;
    }

    @Override
    public boolean test(/*Pre*/Deriver d) {
        for (Action h : actions)
            h.test(d);
        return true;
    }
}