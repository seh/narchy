package nars.premise;

import jcog.pri.op.PriMerge;
import jcog.util.PriReturn;
import nars.Term;
import nars.control.Why;
import nars.term.Termed;

public abstract class AbstractPremise extends HashedPremise {

    protected transient Termed why;

    protected AbstractPremise(int hash, Termed why) {
        super(hash);
        this.why = why;
    }

    @Override
    public float merge(Premise incoming, PriMerge merge, PriReturn r) {
        float y = merge.apply(this, incoming.pri(), r);
        mergeWhy(incoming);
        return y;
    }

    protected void mergeWhy(Premise incoming) {
        why = Why.whyMerge(this.why, incoming.why());
    }

    @Override
    public Term why() {
        Termed w = this.why;
        return w != null ? (w instanceof Term ? ((Term) w) : w.term()) : null;
    }
}