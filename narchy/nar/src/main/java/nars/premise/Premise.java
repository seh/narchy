package nars.premise;

import jcog.exe.flow.Feedback;
import jcog.pri.op.PriMerge;
import jcog.util.PriReturn;
import nars.Term;
import nars.derive.Deriver;
import nars.derive.reaction.MutableReaction;
import nars.derive.reaction.Reaction;
import nars.task.CommandTask;
import nars.task.NALTask;
import nars.term.atom.Atom;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;

import static nars.term.atom.Atomic.atom;

/**
 * Defines the conditions used in an instance of a derivation
 * Contains the information necessary for generating derivation Tasks via reasoning rules.
 * <p>
 * It is meant to be disposable and should not be kept referenced longer than necessary
 * to avoid GC loops, so it may need to be weakly referenced.
 * <p>
 * note: Comparable as implemented here is not 100% consistent with Task.equals and Term.equals.  it is
 * sloppily consistent for its purpose in collating Premises in optimal sorts during hypothesizing
 */
public abstract class Premise extends CommandTask {

	//	@Deprecated
//	public transient /*volatile*/ Set<Premise> sub = null;
	@Deprecated
	public transient Premise parent;

	public Premise() {
		setPlain(Float.NaN);
	}




	public final void run(Deriver d) {
		boolean f = Feedback.start(why());

		pre(d);
		act(d);

		if (f) Feedback.end();
	}

	/**
	 * called by extension premises
	 */
	@Deprecated
	public void pre(Deriver d) {

	}

	protected void act(Deriver d) {
		d.rules.run(d);
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object that);

	public static final Premise[] EmptyPremiseArray = new Premise[0];
	public static final Atom Task = atom("task");
	public static final Atom Belief = atom("belief");
	public static final Atom TaskInline = atom("taskTerm");
	public static final Atom BeliefInline = atom("beliefTerm");


	/**
	 * hashcode optimized impl
	 */
	@Nullable
	public Term other(Term x, int xh, boolean reverse) {
		return x.equals(reverse ? to() : from()) ? (reverse ? from() : to()) : null;
	}

	/**
	 * a single / self / structural premise
	 */
	public boolean self() {
		return from().equals(to());
	}

	public @Nullable Term equalReverse(Predicate<Term> fromEquals, int fromHash, Predicate<Term> toEquals, int toHash) {
		return toEquals.test(this.to()) &&
				!fromEquals.test(this.from()) ? this.from() : null;
	}

	public final void merge(Premise incoming, PriMerge merge) {
		merge(incoming, merge, PriReturn.Void);
	}

	public abstract float merge(Premise incoming, PriMerge merge, PriReturn delta);

	@Deprecated
	@Nullable
	public NALTask task() {
		throw new UnsupportedOperationException();
	}

	@Deprecated
	@Nullable
	public NALTask belief() {
		throw new UnsupportedOperationException();
	}

	/**
	 * taskTerm
	 */
	@Deprecated
	public abstract Term from();

	/**
	 * beliefTerm
	 */
	@Deprecated
	public abstract Term to();

//	public int hashFromTo() { return Term.hashShort(from(), to()); }
//
//	public final boolean sub(Premise p/*, int capacity, long now, int dur*/) {
//		Set<Premise> sub = this.sub;
//
//		if (sub==null) this.sub = sub = Sets.newConcurrentHashSet(); //TODO atomic
//		//else sub.removeIf(q -> q.isDeleted() /* && p==q?  */); //clean existing
//
//		return sub.add(p);
//
////		sub.commit(capacity, now, dur);
////		sub.put(p);
//	}

	/**
	 * reaction which generated this premise
	 */
    @Nullable
    public abstract Reaction reaction();

	@Nullable
	public final Class<? extends MutableReaction> reactionType() {
		@Nullable Reaction r = reaction();
		return r != null ? r.type() : null;
	}

	public int cause() {
		return -1; //TODO
	}

//	public Premise root() {
//		Premise p = this, q;
//		while ((q = p.parent)!=null)
//			p = q;
//		return p;
//	}

}