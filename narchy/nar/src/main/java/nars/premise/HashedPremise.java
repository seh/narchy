package nars.premise;

public abstract class HashedPremise extends Premise {

    public final int hash;

    protected HashedPremise(int hash) {
        this.hash = hash;
    }

    @Override
    public final int hashCode() {
        return hash;
    }

}