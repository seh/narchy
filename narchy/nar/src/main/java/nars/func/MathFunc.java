package nars.func;

import nars.$;
import nars.Op;
import nars.Term;
import nars.eval.Evaluation;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.Functor;
import nars.term.Variable;
import nars.term.atom.Bool;
import nars.term.atom.Int;
import nars.term.functor.InlineBinaryFunctor;
import nars.term.functor.InlineCommutiveBinaryFunctor;
import org.jetbrains.annotations.Nullable;

import static nars.term.atom.Bool.*;

public enum MathFunc {
    ;

    public static Term add(Term... x) {
        return add.theCommutive(x);
    }

    public static Term add(Term x, Term y) {
        if (x instanceof Int && y instanceof Int)
            return Int.the(Int.the(x) + Int.the(y));
        return add.theCommutive(x, y);
    }

    public static Term mul(Term x, Term y) {
        if (x instanceof Int && y instanceof Int)
            return Int.the(Int.the(x) * Int.the(y));
        return mul.theCommutive(x, y);
    }

    public static Term mul(Term x, int y) {
        if (y == 1) return x;

        if (x instanceof Int)
            return Int.the(Int.the(x) * y);

        return mul.theCommutive(x, Int.the(y));
    }

    public static Term negate(Term v) {
        return mul(v, Int.NEG_ONE);
    }


    public static final ArithmeticCommutiveFunctor add = new ArithmeticCommutiveFunctor("add") {

        @Override
        public Term equality(Evaluation e, Compound x, Term y) {
            Subterms xa = Functor.args(x);
            if (xa.subs() == 2) {
                Term xa0 = xa.sub(0), xa1 = xa.sub(1);
                if (xa0 instanceof Variable && y instanceof Int && xa1 instanceof Int)
                    return e.is(xa0, Int.the(Int.the(y) - Int.the(xa1))) ? True : False; //"equal(add(#x,a),y)"

                if (xa1 instanceof Variable && y.equals(xa0))
                    return e.is(xa1, Int.ZERO) ? True : False; //equal(add(#x,#y),#x) |- is(#y, 0)
                if (xa0 instanceof Variable && y.equals(xa1))
                    return e.is(xa0, Int.ZERO) ? True : False;  //equal(add(#y,#x),#x) |- is(#y, 0) //can this happen?
            } //TODO 3-ary

            //includes: (#x,add(#x,#x)) |- is(#x, 0)
            return null;
        }

    };
    public static final ArithmeticCommutiveFunctor mul = new ArithmeticCommutiveFunctor("mul") {
        @Override
        public @Nullable Term equality(Evaluation e, Compound x, Term y) {
            if (y instanceof Int) {
                Subterms xa = Functor.args(x, 2);
                if (xa != null) {
                    Term xa0 = xa.sub(0), xa1 = xa.sub(1);
                    if (xa0 instanceof Variable && xa1 instanceof Int)
                        return division(e, (Int) y, (Variable) xa0, (Int) xa1);
                    if (xa1 instanceof Variable && xa0 instanceof Int)
                        return division(e, (Int) y, (Variable) xa1, (Int) xa0);
                }
                //TODO 3-ary
            } else if (y instanceof Variable) {
                Subterms xa = Functor.args(x, 2);
                if (xa != null) {
                    Term xa0 = xa.sub(0), xa1 = xa.sub(1);
                    if (xa0 instanceof Variable && xa1 instanceof Variable) {
                        if (y.equals(xa0))
                            return e.is(xa1, Int.ONE) ? True : False; //equal(#y,mul(#x,#y)) |- is(#x, 1)
                        if (y.equals(xa1))
                            return e.is(xa0, Int.ONE) ? True : False; //equal(#y,mul(#y,#x)) |- is(#x, 1)
                    }
                }
                //TODO 3-ary
            }
            return null;

        }

        private static Term division(Evaluation e, Int y, Variable x, Int a) {
            int numerator = Int.the(y);
            int denominator = Int.the(a);
            Term z;
            if (numerator == denominator)
                z = numerator == 0 ? Op.NaN : Int.ONE;
            else if (denominator == 0)
                z = numerator > 0 ? Op.InfinityPos : Op.InfinityNeg;
            else
                z = $.the(((double) numerator) / denominator);

            return e.is(x, z) ? True : False; //"equal(mul(#x,a),y)"
        }


    };
    public static final Functor pow = new InlineBinaryFunctor("pow") {

        @Override
        protected Term compute(Evaluation e, Term x, Term y) {
            if (x instanceof Int && y instanceof Int)
                return Int.the((int) Math.pow(Int.the(x), Int.the(y)));
            else
                return null;
        }

    };


    /**
     * TODO define better, ex: xor({a,b})?
     * TODO abstract CommutiveBooleanBidiFunctor
     *
     */
    public static final InlineCommutiveBinaryFunctor xor = new InlineCommutiveBinaryFunctor("xor") {

        @Override
        protected Term compute(Evaluation e, Term x, Term y) {
            if (x instanceof Bool && y instanceof Bool && x != Null && y != Null) {
                return x == y ? False : True;
            }
            return null;
        }
    };

    public static void addTo(Subterms x, Term[] y) {
        int s = y.length;
        for (int i = 0; i < s; i++) {
            y[i] = add(x.sub(i), y[i]);
        }
    }


}