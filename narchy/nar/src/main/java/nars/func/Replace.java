package nars.func;

import nars.Term;
import nars.eval.Evaluation;
import nars.subterm.Subterms;
import nars.term.Functor;
import nars.term.atom.Atom;
import nars.term.atom.Atomic;
import nars.term.util.transform.InlineFunctor;
import org.jetbrains.annotations.Nullable;

import static nars.term.atom.Bool.Null;


/**
 * if STRICT is 4th argument, then there will only be a valid result
 * if the input has changed (not if nothing changed, and not if the attempted change had no effect)
 */
public class Replace extends Functor implements InlineFunctor<Evaluation> {

    public static final Replace replace = new Replace("replace");

    protected Replace(String id) {
        this((Atom)Atomic.the(id));
    }

    protected Replace(Atom id) {
        super(id);
    }

    @Override
    public @Nullable Term apply(Evaluation e, Subterms xx) {

        Term input = xx.sub(0);

        Term x = xx.sub(1);

        Term y = xx.sub(2);

        return apply(xx, input, x, y);
    }

    protected static Term apply(Subterms xx, Term input, Term x, Term y) {
        boolean novel = xx.subs() > 3 && xx.subEquals(3, UniSubst.NOVEL);

        Term z = input.replace(x, y);

        return (z == Null) || (novel && input.equals(z)) ? Null : z;
    }






}