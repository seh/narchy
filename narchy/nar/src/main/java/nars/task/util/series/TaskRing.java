package nars.task.util.series;

import jcog.data.list.FastAtomicIntegerArray;
import jcog.data.list.MetalRing;
import jcog.signal.tensor.AtomicArrayTensor;
import nars.Term;
import nars.task.NALTask;
import nars.truth.DiscreteTruth;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.atomic.AtomicLongArray;

/** TODO this is unfinished */
public class TaskRing extends MetalRing<NALTask> {

    final int len;

    final Term term;
    final byte punc;
    long[] stamp;

    final AtomicLongArray when;
    final FastAtomicIntegerArray truth;
    final AtomicArrayTensor pri;

    public TaskRing(Term term, byte punc, long[] stamp, int len) {
        this.term = term;
        this.punc = punc;
        this.len = len;
        this.stamp = stamp;
        when = new AtomicLongArray(len * 2); //TODO long start + int range
        truth = new FastAtomicIntegerArray(len);
        pri = new AtomicArrayTensor(len);
    }

    float pri(int i) {
        return pri.getAt(i);
    }

    Truth truth(int index) {
        return DiscreteTruth.the(truth.get(index));
    }

    @Override
    public int length() {
        return len;
    }

    @Override
    public NALTask get(int i) {
        return new TaskView().set(i);
    }

    @Override
    protected void set(int i, NALTask t) {
        when.set(i*2, t.start());
        when.set(i*2+1, t.end());
        pri.setAt(i, t.pri());
        truth.set(i, DiscreteTruth.hash(t.truth()));
    }

    @Override
    protected NALTask getAndSet(int i, NALTask nalTask) {
        throw new UnsupportedOperationException();
    }

    private final class TaskView extends NALTask {
        int i;

        public TaskView set(int i) {
            this.i = i;
            pri(TaskRing.this.pri(i));
            return this;
        }

        @Override
        public long start() {
            return when.get(i * 2);
        }

        @Override
        public long end() {
            return when.get(i * 2 + 1);
        }

        @Override
        public byte punc() {
            return punc;
        }

        @Override
        public @Nullable Truth truth() {
            return TaskRing.this.truth(i);
        }

        @Override
        public Term term() {
            return term;
        }

        @Override
        public long[] stamp() {
            return stamp;
        }
    }

}