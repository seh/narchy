package nars.task.util;

import jcog.Util;
import jcog.pri.bag.impl.PriArrayBag;
import jcog.signal.FloatRange;
import jcog.util.PriReturn;
import nars.NAL;
import nars.action.memory.RememberAll;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.task.NALTask;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static nars.task.NALTask.i;

public abstract class TaskBuffer implements Consumer<NALTask> {
    protected Focus f;

    /**
     * called on cycle start, or if focus changes
     */
    public void start(Focus f) {
        this.f = f;
    }

    /**
     * flush, called on cycle end or anytime before that
     */
    public void commit() {

    }

    public static class Direct extends TaskBuffer {

        @Override
        public void accept(NALTask t) {
            f.rememberNow(t);
        }
    }

    /** TODO */
    public static class MapBuffered extends Direct {
        final MapBuffer<NALTask> map = new MapBuffer<>(new ConcurrentHashMap<>()) {
            @Override
            protected void merge(NALTask p, NALTask n) {
                NALTask.merge(p, n);
            }
        };

        private final AtomicBoolean draining = new AtomicBoolean(false);

        /** lower values are lower latency but potentially less throughput and deduplication */
        public final FloatRange flushCapacities;

        int overflowThresh = Integer.MAX_VALUE;

        public MapBuffered(float capacityProportion) {
            this.flushCapacities = new FloatRange(capacityProportion, 0, 4);
        }

        @Override
        public void accept(NALTask t) {
            map.put(t);
            if (map.size() > overflowThresh)
                drain();
        }

        @Override
        public void commit() {
            overflowThresh = (int) Math.max(Math.ceil(
                flushCapacities.asFloat() * ((BagFocus) f).bag.capacity()
            ), 1);
            if (!map.isEmpty())
                drain();
        }

        private void drain() {
            if (!draining.compareAndSet(false,true))
                return;

            try {
                f.rememberAll(map.map);
            } finally {
                draining.set(false);
            }
        }

        @Override
        public void start(Focus f) {
            super.start(f);
            clear();
        }

        public void clear() {map.clear();}


    }

    public static final class BagBufferedByPunc extends Direct {

        public final BagBuffered[] b;

        public BagBufferedByPunc() {
             b = Util.arrayOf(i -> new BagBuffered(), new BagBuffered[4]);
        }

        @Override
        public final void accept(NALTask t) {
            b[i(t.punc())].accept(t);
        }

        @Override
        public void start(Focus f) {
            super.start(f);
            for (BagBuffered bb : b) bb.start(f);
        }

        @Override
        public void commit() {
            for (BagBuffered bb : b) bb.commit();
        }

        public final BagBufferedByPunc rate(float v) {
            for (BagBuffered bb : b) bb.rate.set(v);
            return this;
        }
    }

    public static final class BagBuffered extends Direct {

        //final Random rng = new XoRoShiRo128PlusRandom();
        private final RememberAll all = new RememberAll();

        /** units: capacities per duration */
        public final FloatRange rate = new FloatRange(0.5f, 0, 1.0f);

        /** units: max tasks per tasklink */
        public final FloatRange capacityFactor = new FloatRange(1, 0, 4);

        final PriArrayBag<NALTask> bag = new PriArrayBag<>(NAL.taskPriMerge) {
            @Override
            protected float merge(NALTask existing, NALTask incoming, float incomingPri) {
                return NALTask.merge(existing, incoming, NAL.taskPriMerge, PriReturn.Delta);
            }

            @Override
            protected int histogramBins(int s) {
                return 0;
            }

//            @Override
//            public void onReject(NALTask t) {
//                BagBuffered.this.onReject(t);
//            }
        };

        public BagBuffered() {

        }

        @Override
        public void start(Focus f) {
            Focus fPrev = this.f;
            super.start(f);

            if (fPrev!=f)
                clear();

            bag.capacity(_capacity((BagFocus) f));
        }

        private int _capacity(BagFocus f) {
            return f.bag.capacity() * capacityFactor.intValue();
        }

        public void clear() {
            bag.clear();
        }


        @Override
        public void accept(NALTask t) {
            bag.put(t);
        }

        @Override public void commit() {
//            try (var __ = nar.emotion.derive_time_Input_Derivation.time()) {

            if (!bag.isEmpty()) {
                float freq =
                    1;
                    //f.freq();

                /* capacities/dur */
                float rate = freq * this.rate.floatValue();

                /* in durs */
                float dt = f.updateDT();

                int cap = bag.capacity();
                int n = Util.clampSafe(Math.round(dt * rate * cap), 1, cap);

//                try {
                    f.rememberAll(bag, n, all);
//                } catch (Throwable t) {
//                    //HACK
//                    bag.clear(); //reset bag state
//                    throw t;
//                }

            }

        }

//        private void onReject(Task t) {
////            nar.emotion.derivedTaskDrop.increment();
//        }

        public BagBuffered rate(float v) {
            rate.set(v);
            return this;
        }
    }


}