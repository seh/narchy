package nars.task.util;

import jcog.signal.FloatRange;
import nars.Term;
import nars.task.NALTask;
import org.eclipse.collections.api.block.function.primitive.FloatFunction;

public enum Eternalization { ;

    public static final FloatFunction<NALTask> None = x -> 0;

    public static class Flat implements FloatFunction<NALTask> {

        public final FloatRange ete = new FloatRange(0, 0, 1);

        @Override
        public float floatValueOf(NALTask t) {
            return ete.floatValue();
        }

        public FloatFunction<NALTask> set(float ete) {
            this.ete.set(ete);
            return this;
        }
    }

    public static class TemporalsAndVariables extends Eternalization.Flat {
        @Override
        public float floatValueOf(NALTask t) {
            return eternalizable(t.term()) ? ete.floatValue() : 0;
        }

        protected boolean eternalizable(Term t) {
            return
                    t.hasVars()||
                            t.IMPL() ||
                            t.CONDS()
                    ;
        }
    }

}