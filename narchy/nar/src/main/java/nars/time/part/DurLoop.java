package nars.time.part;

import jcog.signal.FloatRange;
import nars.$;
import nars.NAR;
import nars.NARPart;
import nars.Term;
import nars.time.ScheduledTask;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static jcog.Str.n2;

/**
 * a part that executes a given procedure once every N durations (approximate)
 * N is an adjustable duration factor (float, so fractional values are ok)
 * the part is retriggered asynchronously so it is not guaranteed consistent
 * timing although some adjustment is applied to help correct for expectable
 * amounts of lag, jitter, latency, etc.
 *
 * these are scheduled by the NAR's Time which holds a priority queue of
 * temporal events.  at any given time it will contain zero or one of this
 * Dur's immutable and re-usable AtDur event.
 */
public abstract class DurLoop extends NARPart implements Consumer<NAR> {

    /**
     * ideal duration multiple to be called, since time after implementation's procedure finished last
     */
    public final FloatRange durations = new FloatRange(1, 0.5f, 32.0f);

    private final WhenDur at = new WhenDur();

    @Override
    public final WhenDur event() {
        return at;
    }
    
    protected DurLoop(Term id) {
        super(id);
    }
    protected DurLoop() {
        super();
    }


    /**
     * set period (in durations)
     */
    public DurLoop durs(float durations) {
        this.durations.set(durations);
        return this;
    }

    @Override protected void starting(NAR nar) {
        //intial trigger
        at.next = nar.time();
        nar.runLater(at);
    }

    public int durCyclesInt(NAR nar) {
        return Math.max(1, Math.round(durCycles(nar)));
    }

    public float durCycles(NAR nar) {
        return (float) (durations.doubleValue() * nar.dur());
    }

    public static final class DurRunnable extends DurLoop {
        private final Runnable r;

        public DurRunnable(Runnable r) {
            super($.identity(r));
            this.r = r;
        }

        @Override
        public void accept(NAR n) {
            r.run();
        }

    }

//    public volatile long deltaCycles;

    public class WhenDur extends ScheduledTask {

        /**
         * when the last cycle ended
         */
        private long started = Long.MIN_VALUE, startedPrev = Long.MIN_VALUE;

        private final AtomicBoolean busy = new AtomicBoolean(false);
        static final private boolean trace = false;

        @Override
        public void accept(NAR nar) {

            //cancelled between previous iteration and this iteration?
            if (!isOn() || !busy.weakCompareAndSetAcquire(false, true))
                return;

            pre();
            try {
                DurLoop.this.accept(nar);
            } finally {
                post();
            }

        }

        private void pre() {
            this.startedPrev = this.started;
            this.started = nar.time();
//            deltaCycles = startedPrev == Long.MIN_VALUE ? 0 : started - startedPrev;
        }

        private void post() {
            NAR nar = DurLoop.this.nar; if (nar == null) return; /* stopped */

            this.next = next(nar);

            busy.setRelease(false);

            nar.runAt(this);
        }


        private long next(NAR nar) {
            int dur = durCyclesInt(nar);


            long now = nar.time();
            long elapsed = now - started;
            long slow = elapsed - dur;
            if (slow > 0) {
                if (trace) {
                    float slowDurs = ((float) slow) / dur;
                    logger.info("{} slow {}%", DurLoop.this, n2(100 * slowDurs));
                }
            }


            long startedIdeal = startedPrev + dur;

            long lag = started - startedIdeal;
            nar.emotion.durLoop(DurLoop.this, Math.max(lag, 0), Math.max(slow, 0), dur);

            return nextTime0(dur, startedIdeal);
            //return nextTime1(dur);
        }

        /** fixed delay */
        private long nextTime1(int dur) {
            return nar.time() + dur;
        }

        /** fixed rate, with correctional shift period, so that it attempts to maintain a steady rhythm and re-synch even if a frame is lagged*/
        private long nextTime0(int dur, long startedIdeal) {
            long nextIdeal = startedIdeal + dur;

            //TODO refine
            return (nextIdeal < started) ?
                started + dur //restart
                :
                nextIdeal; //ok soon enough to catch up
        }


        @Override
        public final Term term() {
            return DurLoop.this.term();
        }
    }
}