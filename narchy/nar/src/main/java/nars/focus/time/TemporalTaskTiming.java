package nars.focus.time;

import nars.derive.Deriver;
import nars.task.NALTask;

public class TemporalTaskTiming extends TaskTiming {
    @Override
    public long[] whenRelative(NALTask task, Deriver d) {
        return task.ETERNAL() ? whenAbsolute(d) : super.whenRelative(task, d);
    }
}
