package nars.focus.time;

import jcog.decide.Roulette;
import jcog.math.LongInterval;
import jcog.signal.FloatRange;
import nars.derive.Deriver;
import nars.task.NALTask;
import org.jetbrains.annotations.Nullable;

import static nars.Op.ETERNAL;

/** delegates to the temporal focus of the deriver's Focus */
public class FocusTiming implements TaskWhen {

    public final FloatRange focusProbability = FloatRange.unit(
            1
    );

    /** in relative mode only */
    public final FloatRange taskProbability = FloatRange.unit(
            1/16f
    );

    public final FloatRange wheneverProbability = FloatRange.unit(
            1/32f
    );
//    public final FloatRange presentProbability = FloatRange.unit(
//            1/4f
//    );

    private static long[] focus(Deriver d) {
        return d.focus.whenDithered();
    }

    @Override
    public long[] whenAbsolute(Deriver d) {
        return focus(d);
        //return when(null, d);
    }

    @Override
    public long[] whenRelative(NALTask task, Deriver d) {
        return when(task, d);
    }

    private long[] when(NALTask task, Deriver d) {
        int r = Roulette.selectRoulette(new float[]{
                task !=null ? taskProbability.floatValue() : 0,
                //presentProbability.floatValue(),
                focusProbability.floatValue(),
                wheneverProbability.floatValue()},
        d.random::nextFloat);
        return when(task, d, r);
    }

    private long[] when(@Nullable NALTask task, Deriver d, int r) {
        return switch (r) {
            case 0 -> task.startEndArray();
            case 1 -> LongInterval.range(d.now(), d.dur() /*task.range()*/);
            case 2 -> focus(d);
            case 3 -> new long[]{ETERNAL, ETERNAL};
            default -> throw new UnsupportedOperationException();
        };
    }

//    /** surround the task's occurrence with the focus duration */
//    private static long[] whenTask(NALTask t, float minRange) {
//        long tRange = t.range()-1;
//        return tRange >= minRange ?
//                t.startEndArray() :  //task range wider
//                LongInterval.range(t.mid(), minRange); //focus duration wider
//
//    }

}