package nars.focus.time;

import nars.derive.Deriver;
import nars.task.NALTask;

public class ProxyTiming implements TaskWhen {
    final TaskWhen delegate;

    public ProxyTiming(TaskWhen delegate) {
        this.delegate = delegate;
    }

    @Override
    public long[] whenRelative(NALTask task, Deriver d) {
        return delegate.whenRelative(task, d);
    }

    @Override
    public long[] whenAbsolute(Deriver d) {
        return delegate.whenAbsolute(d);
    }
}