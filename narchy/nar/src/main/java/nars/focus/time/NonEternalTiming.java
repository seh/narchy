package nars.focus.time;

import jcog.math.LongInterval;
import jcog.signal.FloatRange;
import nars.derive.Deriver;
import nars.task.NALTask;

public class NonEternalTiming extends PresentTiming {

    public final FloatRange focusDur = new FloatRange(1.0f, 0, 32);

    public NonEternalTiming() {
    }

    @Override
    public long[] whenRelative(NALTask task, Deriver d) {
        return task.ETERNAL() ?
            LongInterval.range(d.now(), d.dur() * focusDur.floatValue()) :
            task.startEndArray();
    }

}