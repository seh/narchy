package nars.focus.time;

import nars.derive.Deriver;
import nars.task.NALTask;

public class PresentTiming implements TaskWhen {

    @Override
    public long[] whenAbsolute(Deriver d) {
        return d.focus.whenDithered();
    }

    @Override
    public long[] whenRelative(NALTask task, Deriver d) {
        return whenAbsolute(d);
    }

}