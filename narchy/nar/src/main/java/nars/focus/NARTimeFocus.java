package nars.focus;

public class NARTimeFocus extends TimeFocus {
    private float _dur = 1;

    @Override
    public float dur() {
        return _dur;
    }

    @Override
    public long[] when(Focus f) {
        return when(f.now(), (int) Math.ceil(
            this._dur = f.nar.dur()
        ));
    }

    @Override
    public void dur(float dur) {
        //ignore
        //throw new UnsupportedOperationException();
    }
}