package nars.focus;

import jcog.pri.Prioritizable;
import jcog.pri.bag.Bag;
import jcog.signal.IntRange;
import nars.Term;
import nars.concept.Concept;
import nars.link.TaskLinkArrayBag;
import nars.premise.Premise;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.Objects;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static nars.derive.pri.Budget.tasklinkMerge;

public class BagFocus extends Focus {
    /**
     * attention focus
     * TODO replace with an abstract FocusModel which can be implemented by a Bag, or anything else
     */
    public final Bag<Premise, Premise> bag;



    /**
     * capacity of the links bag
     */
    public final IntRange capacity;


    public Function<Bag,Consumer<Prioritizable>> updater;

    public BagFocus(Term id, int capacity) {
        this(id, new TaskLinkArrayBag(capacity, tasklinkMerge));
    }

    public BagFocus(Term id, Bag<Premise, Premise> bag) {
        super(id);
        this.bag = bag;
        capacity = new IntRange(bag.capacity(), 0, 4096) {
            @Override
            @Deprecated
            protected void changed() {
                bag.setCapacity(intValue());
            }
        };
        updater =
            new BagSustain();
            //new BagSustainByVariables();
    }


    @Override
    protected void commit() {
        bag.commit(updater.apply(bag));
    }

    @Override
    public final boolean link(Premise x) {
        return bag.put(x) !=null;
    }

    @Override
    public final boolean isEmpty() {
        return bag.isEmpty();
    }

    @Override
    public void clear() {
        bag.clear();
        //bag.clearSoft();
    }

    @Override
    public Stream<Concept> concepts() {
        return terms(/*Term::CONCEPTUALIZABLE*/s -> true)
                .map(nar::concept)
                .filter(Objects::nonNull)
                ;
    }

    @Override
    public final Stream<Term> terms(Predicate<Term> filter) {
        return bag.stream()
                .unordered()
                .flatMap(x -> Stream.of(x.from(), x.to()))
                .distinct()
                .filter(filter);
    }

    @Override
    public final Iterator<Premise> iterator() {
        return bag.iterator();
    }

    @Override
    public final void sample(Random rng, Function<? super Premise, SampleReaction> each) {
        bag.sample(rng, each);
    }

    @Override
    public @Nullable Premise sample(Random rng) {
        return bag.sample(rng);
    }

    @Override
    public Iterator<Premise> sampleUnique(Random rng) {
        return bag.sampleUnique(rng);
    }


//    public static class BagSustainByVariables extends AbstractBagSustain  {
//
//        /** max vars to saturate scale at */
//        final static int varLimit = 3;
//
//        @Override
//        public void accept(Premise x) {
//            float vars = (x.from().vars() + x.to().vars()) / 2.0f;
//            x.priMul(Util.unitizeSafe(
//            lerpSafe(_factor * vars/varLimit, multiplier, sqrt(multiplier))
//            ));
//            //x.priMul(strength * 1 / (1 + _factor * (vars - 1)));
//        }
//    }
//    public static class BagSustainByVolume extends AbstractBagSustain  {
//
//        @Override
//        public void accept(Premise x) {
//            x.priMul(multiplier * 1 / (1 + _factor * ((x.from().volume() + x.to().volume()) / 2.0f - 1)));
//        }
//    }

//    /**
//     * TODO needs tested in how the components combine with the base factor
//     */
//    public static class BagSustainByOp extends BagSustain {
//
//        public final FloatRange belief = new FloatRange(0.0f, 0, 1);
//        public final FloatRange goal = new FloatRange(0.0f, 0, 1);
//        public final FloatRange question = new FloatRange(0.0f, 0, 1);
//        public final FloatRange quest = new FloatRange(0.0f, 0, 1);
//
//
//        public final FloatRange atom = new FloatRange(0, 0, 1);
//        public final FloatRange inh = new FloatRange(0, 0, 1);
//        public final FloatRange sim = new FloatRange(0, 0, 1);
//        public final FloatRange impl = new FloatRange(0, 0, 1);
//        public final FloatRange conj = new FloatRange(0, 0, 1);
//        public final FloatRange prod = new FloatRange(0, 0, 1); //and others
//
//        @Override
//        public Consumer<Prioritizable> apply(Bag b) {
//            return Forgetting.forget(b, 1 - sustain.floatValue(), MyPriForget::new);
//        }
//
//        private final class MyPriForget implements Consumer<Premise> {
//
//            public final float b, g, q, Q;
//
//            MyPriForget(float sustain) {
//                b = lerpSafe(belief.floatValue(), sustain, 1);
//                q = lerpSafe(question.floatValue(), sustain, 1);
//                g = lerpSafe(goal.floatValue(), sustain, 1);
//                Q = lerpSafe(quest.floatValue(), sustain, 1);
//            }
//
//            @Override
//            public void accept(Premise x) {
//                Term f = x.from();
//                float r = remember(f);
//                Term t = x.to();
//                if (f != t)
//                    r =
//                            Util.mean(
//                                    //Util.or(
//                                    //Util.and(
//                                    r, remember(t)
//                            );
//
//                ((MutableTaskLink) x).priMul(
//                        lerpSafe(r, b, 1),
//                        lerpSafe(r, q, 1),
//                        lerpSafe(r, g, 1),
//                        lerpSafe(r, Q, 1)
//                );
//            }
//
//            /**
//             * 0  = normal forget, 1 = no forget
//             */
//            float remember(Term xx) {
//                return switch (xx.op()) {
//                    case ATOM -> atom.floatValue();
//                    case INH -> inh.floatValue();
//                    case SIM -> sim.floatValue();
//                    case IMPL -> impl.floatValue();
//                    case CONJ -> conj.floatValue();
//                    default -> prod.floatValue();
//                };
//            }
//        }
//    }
}