package nars.focus;

import jcog.TODO;
import jcog.event.ByteTopic;
import jcog.event.Off;
import jcog.event.Topic;
import jcog.pri.PLink;
import jcog.pri.UnitPri;
import jcog.pri.bag.Sampler;
import jcog.pri.bag.impl.PriArrayBag;
import jcog.signal.FloatRange;
import jcog.util.ConsumerX;
import nars.*;
import nars.action.memory.Remember;
import nars.action.memory.RememberAll;
import nars.concept.Concept;
import nars.concept.TaskConcept;
import nars.derive.pri.Budget;
import nars.derive.pri.DefaultBudget;
import nars.eval.Evaluator;
import nars.eval.TaskEvaluation;
import nars.link.MutableTaskLink;
import nars.premise.Premise;
import nars.task.CommandTask;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.task.util.TaskBuffer;
import nars.time.Tense;
import org.jetbrains.annotations.Nullable;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static nars.Op.TIMELESS;
import static nars.term.Functor.isFunc;


public abstract class Focus extends NARPart implements Sampler<Premise>, Iterable<Premise>, Externalizable, ConsumerX<Task> {

    public final ByteTopic<Task> eventTask = new ByteTopic<>(Op.Punctuation);
    public final PriSource pri = new PriSource(this.id, 1);
    public final UnitPri freq = new UnitPri(0.5f);
    private final FloatRange commitDurs = new FloatRange(1, 0.5f, 16);

    /**
     * collects outgoing/result tasks
     */
    private final TaskBuffer out =
        new TaskBuffer.MapBuffered(1/3f);
        //new TaskBuffer.Direct();
        //new TaskBuffer.BagBuffered().rate(0.5f);
        //new TaskBuffer.BagBufferedByPunc().rate(0.2f);

    /**
     * ensure at most only one commit is in progress at any time
     */
    private final AtomicBoolean busy = new AtomicBoolean(false);
    /**
     * local metadata
     */
    private final Map meta = new ConcurrentHashMap<>();
    /**
     * where threads can store threadlocal
     */
    private final ThreadLocal<Map> threadLocal = ThreadLocal.withInitial(HashMap::new);
    public TimeFocus time = new BasicTimeFocus();
    //public final IntRange volMax = new IntRange(Inte, );
    public int volMax = Integer.MAX_VALUE;
    public Budget budget = new DefaultBudget();
    /**
     * redirect output for derived tasks
     */
    public Focus output = this;
    public float durBase = 1;

    /** for update period timing: */
    public transient volatile long prev;
    private transient volatile long now;

    private volatile long updateNext, novelBefore;

    public Focus(Term id) {
        super(id);
    }

    protected abstract void commit();

    /**
     * link (direct insert)
     *
     * @return whether the link was inserted or merged.  false if rejected
     */
    public abstract boolean link(Premise link);


    /**
     * activates tasklink for a task
     */
    public final boolean link(Term x, byte punc, float pri) {
        float P = budget.tasklinkPri(x, punc, this) * pri;
        return link(MutableTaskLink.link(x).priPunc(punc, P));
    }

    public abstract void clear();

    public abstract Stream<Concept> concepts();

    public abstract Stream<Term> terms(Predicate<Term> filter);

    @Override
    public abstract Iterator<Premise> iterator();

    @Override
    public abstract void sample(Random rng, Function<? super Premise, SampleReaction> each);

    @Override
    public abstract @Nullable Premise sample(Random rng);

    @Override
    public abstract Iterator<Premise> sampleUnique(Random rng);

    @Override
    public final void writeExternal(ObjectOutput objectOutput) {
        throw new TODO();
    }

    @Override
    public final void readExternal(ObjectInput objectInput) {
        throw new TODO();
    }

    @Override
    protected void starting(NAR nar) {

        durBase = nar.dur();

        if (time.dur() < 1) //HACK
            time.dur(durBase);

        updateNext = nar.time() - 1; //force start ASAP
        super.starting(nar);

        nar.pri.add(pri);

        out.start(this);
    }

    @Override
    protected void stopping(NAR nar) {
        nar.pri.remove(pri);
        super.stopping(nar);
    }

    /**
     * perceptual duration (cycles)
     */
    public final float dur() {
        return time.dur();
    }

    @Deprecated
    public final Premise sample() {
        return sample(random());
    }

    private void log(Task t) {
        eventTask.emit(t, t.punc());
    }

    @Deprecated
    public final Random random() {
        return nar.random();
    }

    /**
     * warning: possibly asynchronous and does not guarantee processing and storage
     */
    public final void remember(NALTask x) {
        out.accept(x);
    }

    public final void rememberAll(Map<?, NALTask> mapToBeDrained) {
        _remember(new RememberAll().drain(mapToBeDrained));
    }

    public final void rememberAll(PriArrayBag<NALTask> toDrain, int n) {
        RememberAll r = new RememberAll();
        rememberAll(toDrain, n, r);
    }

    public void rememberAll(PriArrayBag<NALTask> toDrain, int n, RememberAll r) {
        _remember(r.drain(toDrain, n));
    }

    public final void rememberAll(Stream<NALTask> t) {
        _remember(new RememberAll(t));
    }

    /**
     * remember immediately
     */
    public final void rememberNow(NALTask x) {
//        if (x instanceof SerialTask) { rememberNow((SerialTask)x); return; }

        try (Remember r = new Remember(this)) {
            r.input(x);
        }
    }

    /**
     * streamlined activation path for serial tasks
     */
    public final void link(SerialTask x, @Nullable TaskConcept c) {
        new Remember(this).link(x, c);
    }

    private void _remember(RememberAll r) {
        r.commit(this).close();
    }

    private void run(CommandTask t) {
        if (isFunc(t.term()))
            new CommandEvaluation(t);
        else
            throw new UnsupportedOperationException();
    }


    public final Off onTask(Consumer<Task> listener, byte... punctuations) {
        return eventTask.on(listener, punctuations);
    }

    public final boolean tryCommit(long now) {
        if (busy.weakCompareAndSet(false, true)) {
            try {
                if (now >= this.updateNext) {
                    _commit(now);
                    return true;
                }
            } finally {
                busy.set(false);
            }
        }
        return false;
    }

    private void _commit(long now) {
        commitTime(now);

        commitNovelty(now);

        budget.accept(this);

        this.updateNext = now + Math.max(1, Math.round(
                (this.durBase = nar.dur()) * commitDurs.floatValue())
        );

        commit();

        out.commit();

    }

    private void commitNovelty(long now) {
        float novelDur =
                durBase;
        //dur();
        long novelCycles = Tense.occToDT(Math.max(1, Math.round(novelDur * NAL.belief.NOVEL_DURS)));
        novelBefore = now - novelCycles;
    }

    private void commitTime(long now) {
        this.prev = this.now;
        this.now = now;
    }

    public Off log() {
        return log((Predicate) null);
    }

    public Off log(@Nullable Predicate<?> pred) {
        return logTo(System.out, pred);
    }

    public Off logTo(Appendable out, @Nullable Predicate<?> includeValue) {
        return logTo(out, null, includeValue);
    }

    private Off logTo(Appendable out, @Nullable Predicate<String> includeKey, @Nullable Predicate includeValue) {
        return eventTask.on(new TaskLogger(includeValue, out, includeKey));
    }

    //@Override
    public final float pri() {
        return pri.pri();
    }

    public final float freq() {
        return freq./*pri*/asFloat();
    }

    public final Focus pri(float freq, float pri) {
        this.freq.pri(freq);
        this.pri.pri(pri);
        return this;
    }

    public final Focus freq(float f) {
        freq.pri(f);
        return this;
    }

//    public final void freqAndPri(float x) {
//        freq.pri(x);
//        pri.pri(x);
//    }

    @Nullable
    public <X> X focusLocal(Object key) {
        return (X) meta.get(key);
    }

//    @Nullable public <X> Stream<Map.Entry<?,X>> metas(Class<? super X> key) {
//        return meta.entrySet().stream().filter(e -> key.isAssignableFrom(((Map.Entry)e).getValue().getClass()));
//    }

    public <K, X> X focusLocal(K key, Function<K, ? extends X> build) {
        return (X) meta.computeIfAbsent(key, build);
    }

    public final void acceptAll(Task... t) {
        for (Task x : t) accept(x);
    }

    public final void acceptAll(Stream<? extends Task> t) {
        t.forEach(this);
    }

    /**
     * warning: this is direct input.  otherwise, use remember(x)
     */
    @Override
    public final void accept(Task x) {
        if (x instanceof NALTask)
            rememberNow((NALTask) x);
        else if (x instanceof CommandTask)
            run((CommandTask) x);
        else
            throw new UnsupportedOperationException();
    }

    public final <X> X threadLocal(Object key) {
        return (X) threadLocal.get().get(key);
    }

    public final <X> X threadLocal(Object key, Function<Focus, X> ifMissing) {
        //return (X) threadLocal.get().computeIfAbsent(key, k -> ifMissing.apply(this));

        var m = threadLocal.get();
        Object v = m.get(key);
        if (v == null)
            m.put(key, v = ifMissing.apply(this));
        return (X) v;
    }

    public abstract boolean isEmpty();

    public final Focus dur(float dur) {
        time.dur(dur);
        return this;
    }

    public final long[] when() {
        return time.when(this);
    }

    public final long[] whenDithered() {
        return Tense.dither(when(), nar.dtDither());
    }


    /**
     * novelty filter for: activation, logging, etc.
     * determines whether a previously-stored task is novel, and can be re-activated now
     *
     * @return TIMELESS if it should not be reactivated, or the current time to set its
     * re-creation time as if it should be.
     */
    private boolean novelTime(NALTask x) {
        long creation = x.creation();
        if (creation == TIMELESS || novel(creation)) {
            x.setCreation(now); //initialize or re-activate
            return true;
        }

        return NAL.LINK_TASK_ALWAYS;
    }

    private boolean novel(long creation) {
        return creation <= novelBefore;
    }

    public int volMax() {
        return Math.min(volMax, nar.volMax());
    }

    /**
     * called after successful remembering, or explicitly by sensors
     */
    public void link(Remember r) {
        NALTask s = r.stored;

        if (novelTime(s)) {
            //emit the result, but using the input pri
            link(r.target(), s.punc(), r.pri());

            log(s);
        }

        nar.emotion.busyVol.add(s.term().volume());

//        Term why = t.why();
//        if (why!=null)
//            n.control.learn(MetaGoal.Perceive.ordinal(), why, ((float) vol) / NAL.term.COMPOUND_VOLUME_MAX, n);
    }

    /** time since last update, in durs */
    public final float updateDT() {
        return (now - prev)/durBase;
    }

    /** TODO switch between realtime and buffered time */
    public final long now() {
        return nar.time(); //more accurate
        //return now;
    }


    /** current attention priority of the focus within the running system */
    public final float focusPri() {
        PLink<Focus> f = nar.focus.get(id);
        return f!=null ? f.pri() : Float.NaN;
    }


    /**
     * TODO histogram priority to adaptively determine ANSI color by percentile
     */
    static class TaskLogger implements Consumer<Task> {

        private final @Nullable Predicate includeValue;
        private final Appendable out;
        String previous;

        TaskLogger(@Nullable Predicate includeValue, Appendable out, @Nullable Predicate<String> includeKey) {
            this.includeValue = includeValue;
            this.out = out;
            previous = null;
            Topic.all(this, (k, v) -> {
                if (includeValue == null || includeValue.test(v)) {
                    outputEvent(out, previous, k, v);
                    previous = k;
                }
            }, includeKey);
        }

        private static void outputEvent(Appendable out, String previou, String chan, Object v) {

            try {

                if (!chan.equals(previou)) {
                    out.append(chan).append(": ");

                } else {

                    int n = chan.length() + 2;
                    for (int i = 0; i < n; i++)
                        out.append(' ');
                }

                if (v instanceof Object[]) {
                    v = Arrays.toString((Object[]) v);
                } else if (v instanceof Task tv) {
                    v = tv.toString(true);
//                    float tvp = tv.priElseZero();
//                    v = ansi()
//                            .a(tvp >= 0.25f ?
//                                    Ansi.Attribute.INTENSITY_BOLD :
//                                    Ansi.Attribute.INTENSITY_FAINT)
//                            .a(tvp > 0.75f ? Ansi.Attribute.NEGATIVE_ON : Ansi.Attribute.NEGATIVE_OFF)
//                            .fg(budgetSummaryColor(tv))
//                            .a(
//                                    v
//                            )
//                            .reset()
//                            .toString();
                }

                out.append(v.toString()).append('\n');
            } catch (IOException e) {
                logger.error("outputEvent", e);
            }

        }

//        static Ansi.Color budgetSummaryColor(Prioritized tv) {
//            return switch ((int) (tv.priElseZero() * 5)) {
//                default -> Ansi.Color.DEFAULT;
//                case 1 -> Ansi.Color.MAGENTA;
//                case 2 -> Ansi.Color.GREEN;
//                case 3 -> Ansi.Color.YELLOW;
//                case 4 -> Ansi.Color.RED;
//            };
//        }

        @Override
        public void accept(Task v) {
            if (includeValue == null || includeValue.test(v)) {
                outputEvent(out, previous, "task", v);
                previous = "task";
            }
        }


    }

    private class CommandEvaluation extends TaskEvaluation<Task> {

        private final CommandTask task;

        CommandEvaluation(CommandTask t) {
            super(new Evaluator(nar::axioms));
            this.task = t;
            eval(t.term());
        }

        @Override
        public CommandTask task() {
            return task;
        }

        @Override
        public NAR nar() {
            return nar;
        }

        @Override
        public void accept(Task x) {
            Focus.this.accept(x);
        }

        @Override
        public boolean test(Term term) {
            return false; //first only
        }
    }

}