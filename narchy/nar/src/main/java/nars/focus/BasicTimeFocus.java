package nars.focus;

import jcog.signal.FloatRange;
import nars.NAL;

public class BasicTimeFocus extends TimeFocus {

    /**
     * duration of present-moment perception, in global clock cycles,
     * specific to this What, and freely adjustable
     * TODO DurRange - with .fps() method
     */
    public final FloatRange dur = new FloatRange(1, 1, 16 * 1024);

    /**
     * past/present/future shift, in system durations
     */
    public final FloatRange shift = new FloatRange(0, -16*1024, +16*1024);

    public final float dur() {
        return dur.floatValue();
    }

    public void dur(float dur) {
        this.dur.set(dur);
    }

    public BasicTimeFocus shiftDurs(float durs) {
        shift.set(durs);
        return this;
    }

    public BasicTimeFocus shiftCycles(int cycles, NAL n) {
        return shiftDurs(cycles / n.dur());
    }

    @Override
    public long[] when(Focus f) {

        float dur = dur();

        long shift = Math.round(f.nar.dur() * this.shift.doubleValue());

        return when(f.now(), shift, dur);
    }

}