package nars.focus;

import jcog.pri.Forgetting;
import jcog.pri.Prioritizable;
import jcog.pri.bag.Bag;
import jcog.pri.op.PriAdd;
import jcog.signal.FloatRange;
import org.eclipse.collections.api.block.function.primitive.FloatToObjectFunction;

import java.util.function.Consumer;
import java.util.function.Function;

import static jcog.pri.Prioritized.EPSILON;

/** 'sustain' Bag forgetting */
public class BagSustain implements Function<Bag, Consumer<Prioritizable>>, FloatToObjectFunction<Consumer<Prioritizable>> {

    public final FloatRange sustain = new FloatRange(0, -1, +1);

    @Override
    public Consumer<Prioritizable> apply(Bag b) {
        float s = sustain.floatValue();
        PriAdd base = Forgetting.forget(b, Math.min(1, 1 - s), z -> new PriAdd(z));
        if (base == null) base = new PriAdd(0);
        if (s < 0)
            base.x += s * b.mass()/b.size();
        if (Math.abs(base.x) < EPSILON)
            return null;
        return base;
        //return s < 0 ? drain(s, base instanceof PriMult ? (PriMult) base : null) : base;
    }

//    /**
//     * forced draining; vacuum
//     */
//    @Deprecated
//    private Consumer<Prioritizable> drain(float s, PriMult base) {
//        if (base == null) base = new PriMult(1);
//        int exponent = 4;
//        base.x = (float) Util.lerp(Math.pow(-s, exponent), base.x, 0);
//        return base;
//    }

    @Override
    public Consumer<Prioritizable> valueOf(float v) {
        return new PriAdd<>(v);
    }
}