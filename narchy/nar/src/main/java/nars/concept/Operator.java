package nars.concept;

import jcog.util.ArrayUtil;
import nars.$;
import nars.NAR;
import nars.Task;
import nars.Term;
import nars.eval.Evaluation;
import nars.eval.TaskEvaluation;
import nars.subterm.Subterms;
import nars.task.NALTask;
import nars.term.Functor;
import nars.term.atom.Atom;
import nars.term.atom.Atomic;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import static nars.Op.*;

/**
 * Operator interface specifically for goal and command punctuation
 * Allows dynamic handling of argument list like a functor,
 * but at the task level

 * TODO implement generic Task reaction interface
 */
@Deprecated public final class Operator extends Functor {

    private static final String LOG = String.valueOf(Character.valueOf((char) 8594));

    public final BiFunction<Task, NAR, Task> model;

    private Operator(Atom name, BiFunction<Task, NAR, Task> model) {
        super(name);
        this.model = model;
    }


    public static Functor simple(String name, BiConsumer<Task, NAR> exe) {
        return simple(name, (x,y)->{
            exe.accept(x, y);
            return null;
        });
    }

    public static Functor simple(String name, BiFunction<Task, NAR, Task> exe) {
        return simple(Atomic.atom(name), exe);
    }

    public static Functor simple(Atom name, BiFunction<Task, NAR, Task> exe) {
         return new Operator(name, new SimpleOperatorModel(exe));
    }

    public static Task error(Task x, Throwable error, long when) {
        
        
        return Operator.command("error", when, $.quote(x.toString()),
                
                error!=null ? $.quote(error.getMessage()!=null ? error.getMessage() : error.toString()) : $.the("?") 
        );
    }


    public static NALTask command(Term content, long when) {
        return NALTask.taskUnsafe(content, COMMAND, null, when, when, ArrayUtil.EMPTY_LONG_ARRAY);
    }

    public static NALTask log(long when, Object content) {
        return Operator.command(LOG, when, $.the(content));
    }

    private static NALTask command(String func, long now,  Term... args) {
        return Operator.command($.func(func, args), now);
    }

    public static Term arg(Term operation, int sub) {
        return operation.sub(0).subterms().sub(sub);
    }

    @Override
    public final Term apply(Evaluation E, Subterms args) {
        if (E instanceof TaskEvaluation e) {
            Task t = e.task();
            byte punc = t.punc();
            if (punc == GOAL || punc == COMMAND) {
                Task y = model.apply(t, e.nar());
                if (y != null && y != t)
                    e.accept(y);
            }
        }
        return null;
    }


    private static class SimpleOperatorModel implements BiFunction<Task, NAR, Task> {

        private final BiFunction<Task, NAR, Task> exe;

        SimpleOperatorModel(BiFunction<Task, NAR, Task> exe) {
            this.exe = exe;
        }

        @Override
        public Task apply(Task task, NAR nar) {
            //default handler
            if (task.COMMAND())
                return exe.apply(task, nar);
            else {
                NALTask t = (NALTask)task;
                assert(t.GOAL());
                if (executeGoal(t)) {
                    long s = t.start();
                    if (s == ETERNAL)
                        return exe.apply(task, nar);

                    long now = nar.time();
                    float dur = nar.dur();
                    if (s >= now - dur / 2) {
                        if (s > now + dur / 2) {
                            //delayed
                            nar.runAt(s, () -> nar.input(exe.apply(task, nar)));
                        } else {
                            return exe.apply(task, nar);
                        }
                    }
                }

                return null;
            }
        }

        static boolean executeGoal(NALTask goal) {
            return goal.expectation() > 0.5f;
        }
    }

}