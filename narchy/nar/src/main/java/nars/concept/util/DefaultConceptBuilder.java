package nars.concept.util;

import nars.Term;
import nars.concept.Concept;
import nars.concept.NodeConcept;
import nars.table.BeliefTable;
import nars.table.LazyBeliefTable;
import nars.table.LazyQuestionTable;
import nars.table.eternal.EternalTable;
import nars.table.question.HijackQuestionTable;
import nars.table.question.QuestionTable;
import nars.table.temporal.NavigableMapBeliefTable;

import java.util.function.Consumer;

public class DefaultConceptBuilder extends ConceptBuilder {

    private final Consumer<Concept> alloc;

    public DefaultConceptBuilder(Consumer<Concept> allocator) {
        this.alloc = allocator;
    }

    @Override protected Concept nodeConcept(Term t) {
        return new NodeConcept(t);
    }

    @Override public BeliefTable eternalTable(Term t) {
        //return new EternalTable(0);
        return new LazyBeliefTable(EternalTable::new);
    }

    @Override
    public BeliefTable temporalTable(Term t, boolean beliefOrGoal) {
        return new LazyBeliefTable(NavigableMapBeliefTable::new);
        //return new NavigableMapBeliefTable();
        //return new RTreeBeliefTable();
    }

    @Override public QuestionTable questionTable() {
        return new LazyQuestionTable(HijackQuestionTable::new);
        //return new HijackQuestionTable();
    }

    @Override
    public void start(Concept c) {
        super.start(c);
        alloc.accept(c);
    }


}