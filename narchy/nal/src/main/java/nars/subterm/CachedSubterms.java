package nars.subterm;

import jcog.Hashed;
import jcog.The;
import nars.Term;
import nars.subterm.util.SubtermMetadataCollector;
import nars.subterm.util.TermMetadata;

import java.util.function.Predicate;

import static nars.Op.CONJ;

/**
 * what differentiates TermVector from TermContainer is that
 * a TermVector specifically for subterms.  while both
 * can be
 */
public abstract class CachedSubterms extends TermMetadata implements Subterms, The, Hashed {

    transient boolean normalizedKnown, normalized;
    transient boolean internableKnown, internables;
    
//    @Nullable transient byte[] bytes = null;

    CachedSubterms(SubtermMetadataCollector intrinMetadata) {
        super(intrinMetadata);
    }

    private static final Predicate<Term> these = Term::internable;

    protected CachedSubterms(Term... terms) {
        super(terms);
    }

    @Override
    public boolean equals(/*@NotNull*/ Object obj) {
        return obj instanceof Subterms && equalTerms((Subterms)obj);
    }

    @Override
    public int indexOf(Term t, int after) {
        return impossibleSubTerm(t) ? -1 : Subterms.super.indexOf(t, after);
    }

    @Override
    public final int hashCodeSubterms() {
        //assert(hash == Subterms.super.hashCodeSubterms());
        return hash;
    }

    @Override
    public final boolean internables() {
        if (!internableKnown) {
            internables = AND(these);
            internableKnown = true;
        }

        return internables;
    }

    @Override
    public boolean hasSeq() {
        return hasAny(CONJ.bit) && Subterms.super.hasSeq();
    }

    @Override
    public final int seqDur(boolean xternalSensitive) {
        return TEMPORALABLE() ? Subterms.super.seqDur(xternalSensitive) : 0;
    }

    @Override
    @Deprecated public void setNormalized() {
        normalized = normalizedKnown = true;
    }

    @Override
    public boolean NORMALIZED() {
        if (!normalizedKnown) {
            normalized = Subterms.super.NORMALIZED();
            normalizedKnown = true;
        }
        return normalized;
    }


    @Override
    public abstract Term sub(int i);

    @Override
    public String toString() {
        return Subterms.toString(this);
    }



//    protected transient byte[] bytes = null;

//    @Override
//    public void appendTo(ByteArrayDataOutput out) {
//        byte[] b = this.bytes;
//        if (b ==null) {
//            Subterms.super.appendTo(out);
//        } else {
//            out.write(b);
//        }
//    }

//    @Override
//    public void acceptBytes(DynBytes constructedWith) {
//        if (bytes == null)
//            bytes = constructedWith.arrayCopy(1 /* skip op byte */);
//    }


}