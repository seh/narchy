package nars.subterm;

import jcog.Hashed;
import jcog.util.ArrayUtil;
import nars.Term;

import java.util.Arrays;

public final class ReversedSubterms extends RemappedSubterms<Subterms> implements Hashed {


    private final int hash;

    /**
     * make sure to calculate hash code in implementation's constructor
     */
    ReversedSubterms(Subterms base) {
        super(base);
        this.hash = super.hashCode();
    }

    public static Subterms reverse(Subterms x) {
        int n = x.subs();
        if (n <= 1)
            return x;

        if (x instanceof ReversedSubterms) {
            //un-reverse
            return ((ReversedSubterms)x).ref;
        }

        if (x instanceof ArrayRemappedSubterms mx) {
            //TODO test if the array is already perfectly reversed without cloning then just undo
            byte[] q = mx.map, r;

            //palindrome or repeats?
            if ((q.length==2 && q[0]==q[1]) || (q.length==3 && q[0]==q[2]) || (q.length==4 && q[0] == q[3] && q[1]==q[2]) /* ... */) {
                //obvious palindrome/repeats
                return x;
            } else {
                r = q.clone();
                ArrayUtil.reverse(r);
                if (Arrays.equals(q,r))
                    return x; //palindrome
            }
            return new ArrayRemappedSubterms(mx.ref, r);
        }
        //else {
//            byte[] m = new byte[n];
//            for (byte k = 0, i = (byte) (m.length - 1); i >= 0; i--, k++)
//                m[k] = (byte) (i + 1);
//            return new ArrayMappedSubterms(x, m);
//        }
        return new ReversedSubterms(x);
    }


    @Override
    public Subterms reversed() {
        return ref;
    }

    @Override
    public Term sub(int i) {
        return ref.sub(subs -1-i);
    }


    @Override public int hashCode() { return hash; }

}