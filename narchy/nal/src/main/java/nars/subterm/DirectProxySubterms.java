package nars.subterm;

import jcog.Hashed;
import nars.Term;

import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Predicate;

/** maximally proxies metadata access methods to the referant */
public abstract class DirectProxySubterms extends ProxySubterms<Subterms> implements Hashed {

    public DirectProxySubterms(Subterms ref) {
        super(ref);
    }

    @Override
    public boolean NORMALIZED() {
        return ref.NORMALIZED();
    }

    @Override
    public void setNormalized() {
        ref.setNormalized();
    }

    @Override
    public boolean internables() {
        return ref.internables();
    }

    @Override
    public boolean equalTerms(Subterms b) {
        return ref.equalTerms(b);
    }

    @Override
    public boolean subEquals(int i, Term x) {
        return ref.subEquals(i, x);
    }

    @Override
    public void forEach(Consumer<? super Term> action) {
        ref.forEach(action);
    }

    @Override
    public Iterator<Term> iterator() {
        return ref.iterator();
    }

    @Override
    public final boolean equals(Object obj) {
        return this == obj || ref.equalTerms((Subterms)obj);
    }

    @Override
    public final int hashCode() {
        return ref.hashCode();
    }

    @Override
    public int volume() {
        return ref.volume();
    }

    @Override
    public int height() {
        return ref.height();
    }

    @Override
    public int structure() {
        return ref.structure();
    }

    @Override
    public int structureSubs() {
        return ref.structureSubs();
    }

    @Override
    public int structureSurface() {
        return ref.structureSurface();
    }

    @Override
    public boolean impossibleSubStructure(int structure) {
        return ref.impossibleSubStructure(structure);
    }

    @Override
    public boolean impossibleSubVolume(int otherTermVolume) {
        return ref.impossibleSubVolume(otherTermVolume);
    }

    @Override
    public boolean BOOL(Predicate<? super Term> t, boolean andOrOr) {
        return ref.BOOL(t, andOrOr);
    }
//
//    @Override
//    public @Nullable Term subSub(byte[] path) {
//        return ref.subSub(path);
//    }

    @Override
    public Term subUnneg(int i) {
        return ref.subUnneg(i);
    }


    @Override
    public final int seqDur(boolean xternalSensitive) {
        return ref.seqDur(xternalSensitive);
    }

    @Override
    public Term[] arrayShared() {
        return ref.arrayShared();
    }

    //TODO others
}