package nars.subterm;

import com.google.common.io.ByteArrayDataOutput;
import jcog.util.ArrayUtil;
import nars.Op;

import java.util.Arrays;

public final class ArrayRemappedSubterms extends RemappedPNSubterms {
    /** TODO even more compact 2-bit, 3-bit etc representations */
    public final byte[] map;

    private final byte negs;

    private ArrayRemappedSubterms(byte[] map, Subterms base) {
        super(base);
        assert(base.subs()==map.length);
        assert(!(base instanceof IntrinSubterms)): "IntrinSubterms can negate on its own";
        this.map = ArrayUtil.intern(map);
        this.negs = (byte) super.negs();
    }

    ArrayRemappedSubterms(Subterms base, byte[] map) {
        this(map, base);
        this.hash = hashExhaustive();
    }

    public ArrayRemappedSubterms(Subterms base, byte[] map, int hash) {
        this(map, base);
        this.hash = hash;
    }

    @Override
    public void write(ByteArrayDataOutput o) {
        byte[] xx = this.map;
        o.writeByte(xx.length);
        for (byte x : xx)
            writeSubterm(x, o);
    }

    private void writeSubterm(byte x, ByteArrayDataOutput o) {
        if (x < 0) {
            o.writeByte(Op.NEG.id);
            x = (byte) -x;
        }
        mapTerm(x).write(o);
    }

    @Override
    protected final boolean wrapsNeg() {
        return negs>0;
    }

    @Override
    protected final int negs() {
        return negs;
    }




    @Override
    public final int subMap(int i) {
        return map[i];
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof ArrayRemappedSubterms m) {
            return hash == m.hash && Arrays.equals(map, m.map) && ref.equals(m.ref);
        } else {
            return obj instanceof Subterms &&
                   hash == ((Subterms)obj).hashCodeSubterms() &&
                   equalTerms(((Subterms) obj));
        }
    }


}