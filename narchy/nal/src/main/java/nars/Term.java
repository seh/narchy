/*
 * Term.java
 *
 * Copyright (C) 2008  Pei Wang
 *
 * This file is part of Open-NARS.
 *
 * Open-NARS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Open-NARS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-NARS.  If not, see <http:
 */
package nars;


import jcog.Is;
import jcog.The;
import jcog.Util;
import jcog.WTF;
import jcog.data.list.Lst;
import nars.io.TermAppendable;
import nars.subterm.Subterms;
import nars.term.*;
import nars.term.anon.IntrinAtomic;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.atom.Int;
import nars.term.util.TermEquality;
import nars.term.util.TermException;
import nars.term.util.Terms;
import nars.term.util.builder.InterningTermBuilder;
import nars.term.util.builder.TermBuilder;
import nars.term.util.transform.Replace;
import nars.unify.Unify;
import org.eclipse.collections.api.list.primitive.ByteList;
import org.eclipse.collections.impl.list.mutable.primitive.ByteArrayList;
import org.jetbrains.annotations.Nullable;

import java.io.PrintStream;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static nars.Op.*;


/**
 * The meaning "word or phrase used in a limited or precise sense"
 * is first recorded late 14c..
 * from Medieval Latin use of terminus to render Greek horos "boundary,"
 * employed in mathematics and logic.
 * Hence in terms of "in the language or phraseology peculiar to."
 * https://www.etymonline.com/word/term
 */
@Is({"Rewriting", "Regulated_rewriting"})
public abstract class Term implements Termlike, Termed, Comparable<Term> {

    private static boolean pathsTo(Term that, ByteArrayList p, Predicate<Compound> descendIf, Predicate<Term> subterm, BiPredicate<ByteList, Term> receiver) {
        if (!(that instanceof Compound c))
            return false;

        if (!descendIf.test(c)) return true;

        Subterms ss = c.subtermsDirect();

        int n = ss.subs();
        if (n == 0) return true;

        int ppp = p.size();

        boolean kontinue = true;

        for (int i = 0; kontinue && i < n; i++) {
            if (i == 0)
                p.add((byte) 0);
            else
                p.set(ppp, (byte) i);

            Term s = ss.sub(i);

            kontinue = !(subterm.test(s) && !receiver.test(p, s))
                    &&
                    (!(s instanceof Compound) || pathsTo(s, p, descendIf, subterm, receiver));
        }
        p.removeAtIndex(ppp);

        return true;
    }

    public static Term nullIfNull(@Nullable Term maybeNull) {
        return (maybeNull == null) ? Bool.Null : maybeNull;
    }

    public static boolean commonEventStructure(Term x, Term y) {
        return x == y || hasCommon(x.unneg().structure() & (~(NEG.bit | CONJ.bit)), y.unneg().structure() & (~(NEG.bit | CONJ.bit)));
    }

    public static int hashShort(Term x, Term y) {
        int X = x.hashCodeShort();
        int Y = x == y ? X : y.hashCodeShort();
        return (Y << 16) | X;
    }

    public boolean ATOM() {
        return false;
    }

    public boolean VAR_DEP() {
        return false;
    }

    public boolean VAR_INDEP() {
        return false;
    }

    public boolean VAR_QUERY() {
        return false;
    }

    public boolean VAR_PATTERN() {
        return false;
    }

    public boolean INT() {
        return false;
    }

    public boolean INH() {
        return false;
    }

    public boolean SIM() {
        return false;
    }

    public boolean IMPL() {
        return false;
    }

    public boolean CONJ() { return false; }

    public boolean PROD() { return false; }

    public boolean EQ() {
        return false;
    }

    public boolean SETe() {
        return false;
    }

    public boolean SETi() {
        return false;
    }

    public boolean SET() {
        return false;
    }

    public boolean DELTA() {
        return false;
    }

    public boolean STATEMENT() {
        return false;
    }

    public boolean COMMUTATIVE() {
        return false;
    }

    public boolean CONCEPTUALIZABLE() {
        return (opBit() & Conceptualizables) != 0;
    }

    public boolean CONDABLE() {
        return (opBit() & Condables) != 0;
    }

    /**
     * whether this contains events (aka conditions)
     */
    public boolean CONDS() {
        return false;
    }

    public boolean TASKABLE() {
        return (opBit() & Taskables) != 0;
    }

    public boolean TEMPORAL() {
        if (this instanceof Compound) {
            int o = opID();
            return o == CONJ.id || o == IMPL.id;
        }
        return false;
    }

    /**
     * whether a conjunction is a sequence (includes check for factored inner sequence)
     */
    public boolean SEQ() {
        return false;
    }

    @Override
    public final Term term() {
        return this;
    }

    public final Op op() {
        return the(opID());
    }

    public final int opBit() {
        return 1 << opID();
    }

    public abstract int opID();

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract int hashCode();

    public int hashCodeShort() {
        int h = hashCode();
        return ((h & 0xffff) ^ (h >>> 16));
    }

    /**
     * returns the hashcode as if this term were negated
     */
    public int hashCodeNeg() {
        return Compound.hash1(NEG.id, this);
    }

    /**
     * rewraps a possible negation if a transform function results in a change to the unneg value
     */
    public final Term transformPN(Function<Term, Term> f) {
        return this instanceof Neg ?
            transformN(f) :
            f.apply(this);
    }

    private Term transformN(Function<Term, Term> f) {
        Term x = unneg();
        Term y = f.apply(x);
        return x == y ? this : y.neg();
    }


    public final boolean ANDrecurse(Predicate<Compound> superCompoundMust, Consumer<Term> each) {
        return ANDrecurse(superCompoundMust, (sub, sup) -> {
            each.accept(sub);
            return true;
        });
    }

    public final boolean ANDrecurse(Predicate<Compound> superCompoundMust, BiPredicate<Term, Compound> each) {
        return boolRecurse(superCompoundMust, each, null, true);
    }

    public final boolean ANDrecurse(Predicate<Compound> superCompoundMust, Predicate<Term> each) {
        return ANDrecurse(superCompoundMust, (s, p) -> each.test(s));
    }

    public @Nullable Term replaceAt(ByteList path, Term replacement) {
        return replaceAt(path, 0, replacement);
    }

    public @Nullable Term replaceAt(ByteList path, int depth, Term replacement) {
        int ps = path.size();
        if (ps == depth)
            return replacement;
        if (ps < depth)
            throw pathException("path overflow", path, depth, replacement);

        Compound src = (Compound) this;
        Subterms css = src.subtermsDirect();

        int s = css.subs();

        byte which = path.get(depth);
        if (which >= s)
            throw pathException("path subterm out-of-bounds", path, depth, replacement);

//        assert (which < s);

        Term x = css.sub(which);
        Term y = x.replaceAt(path, depth + 1, replacement);
        if (y == x) {
            return src; //unchanged
        } else {
//            if (css.sub(which)==y)
//                return src; //unchanged
            Term[] target = css.arrayClone();
            if (target[which] == y)
                return src; //unchanged, can this happen?
            target[which] = y;
            return src.op().the(src.dt(), target);
        }
    }

    private TermException pathException(String msg, ByteList path, int depth, Term replacement) {
        return new TermException(msg + ": " + path + " " + depth + " -> " + replacement, this);
    }

    //    public boolean pathsTo(Predicate<Term> selector, Predicate<Term> descendIf, BiPredicate<ByteList, Term> receiver) {
//        return pathsTo((UnaryOperator<Term>) x -> selector.test(x) ? x : null, descendIf, receiver);
//    }
    public boolean pathsTo(Predicate<Term> selector, Predicate<Compound> descendIf, BiPredicate<ByteList, Term> receiver) {
        if (selector.test(this) && !receiver.test(Util.EmptyByteList, this))
            return false;

        return !(this instanceof Compound)
                ||
                pathsTo(this, new ByteArrayList(0), descendIf, selector, receiver);
    }

    public Term commonParent(Lst<ByteList> subpaths) {
        int n = subpaths.size(); //assert (subpathsSize > 1);

        int shortest = n > 1 ? (int) subpaths.minValue(ByteList::size) : subpaths.getFirst().size();
        int i;
        done:
        for (i = 0; i < shortest; i++) {
            byte needs = 0;
            for (int j = 0; j < n; j++) {
                byte pi = subpaths.get(j).get(i);
                if (j == 0)
                    needs = pi;
                else if (needs != pi)
                    break done;
            }
        }
        return i == 0 ? this : sub(subpaths.getFirst(), 0, i);
    }

//    public <X> boolean pathsTo(Function<Term, X> target, Predicate<Term> descendIf, BiPredicate<ByteList, X> receiver) {
//        X ss = target.apply(this);
//        if (ss != null && !receiver.test(Util.EmptyByteList, ss))
//            return false;
//
//        return this.subs() <= 0 ||
//                pathsTo(this, new ByteArrayList(0), descendIf, target, receiver);
//    }

    public final @Nullable Term sub(ByteList path) {
        int p = path.size();
        return p > 0 ? sub(path, 0, p) : this;
    }

    /**
     * extracts a subterm provided by the address tuple
     * returns null if specified subterm does not exist
     */
    public final @Nullable Term sub(byte... path) {
        int p = path.length;
        return p > 0 ? sub(0, p, path) : this;
    }

    public final @Nullable Term subSafe(byte... path) {
        int p = path.length;
        return p > 0 ? subSafe(0, p, path) : this;
    }

    private Term sub(int start, int end, byte... path) {
        Term ptr = this;
        for (int i = start; i < end; i++)
            ptr = ptr.sub(path[i]);
        return ptr;

    }

    private @Nullable Term subSafe(int start, int end, byte... path) {
        Term ptr = this;
        for (int i = start; i < end; i++) {
            if ((ptr = ptr.sub(path[i], null)) == null)
                return null;
        }
        return ptr;
    }

    private Term sub(ByteList path, int start, int end) {
        Term ptr = this;
        for (int i = start; i < end; i++)
            ptr = ptr.sub(path.get(i));
        return ptr;
    }

//    public @Nullable Term subSafe(ByteList path, int start, int end) {
//        Term ptr = this;
//        for (int i = start; i < end; i++) {
//            if ((ptr = ptr.subSafe(path.get(i))) == Bool.Null)
//                return null;
//        }
//        return ptr;
//    }

    /**
     * true if the operator bit is included in the enabld bits of the provided vector
     */
    public boolean isAny(int bitsetOfOperators) {
        return (opBit() & bitsetOfOperators) != 0;
    }

    public StringBuilder appendTo(StringBuilder s) {
        return (StringBuilder) new TermAppendable(s).appendSafe(this);
    }


    public boolean pathsTo(Term target, Predicate<Compound> superTermFilter, BiPredicate<ByteList, Term> receiver) {
        return pathsTo(
                target.equals(),
                superTermFilter, //.and(x -> !x.impossibleSubTerm(target)),
                receiver);
    }

    public int compareTo(Term t) {
        if (this == t) return 0;

        boolean atomic = this instanceof Atomic, batomic = t instanceof Atomic;
        if (atomic && !batomic) {
            return +1;
        } else if (batomic && !atomic) {
            return -1;
        } else if (!atomic) {
            //volume decreasing
            int vc = Integer.compare(t.volume(), this.volume());
            if (vc != 0)
                return vc;
        }

        //opID icnreasing
        int oa = this.opID();
        int ob = Integer.compare(oa, t.opID());
        if (ob != 0)
            return ob;

        return atomic ? compareToAtomic(t) : compareToCompound(t, oa);
    }

    private int compareToCompound(Term t, int op) {
        int c = Subterms.compare(
                this.subtermsDirect(),
                t.subtermsDirect()
        );

        return c != 0 ? c : ((((1 << op) & Temporals) != 0) ? Integer.compare(dt(), t.dt()) : 0);
    }

    private int compareToAtomic(Term t) {
        if (this instanceof Int /*&& t instanceof Int*/)
            return Integer.compare(Int.the(this), Int.the(t));

        if (this instanceof IntrinAtomic && t instanceof IntrinAtomic)
            return Integer.compare(hashCode(), t.hashCode()); //same op, same hashcode

        //return Arrays.compare(((Atomic) this).bytes(), ((Atomic) t).bytes());
        return Util.compare(
                ((Atomic) this).bytes(),
                ((Atomic) t).bytes()
        );
    }

    public abstract Subterms subterms();

    /**
     * for direct access to subterms only; implementations may return this instance if SameSubtermsCompound
     */
    public Subterms subtermsDirect() {
        return subterms();
    }


    public void printRecursive() {
        printRecursive(System.out);
    }

    public void printRecursive(PrintStream out) {
        Terms.printRecursive(out, this);
    }

    /**
     * returns this target in a form which can identify a concept, or Null if it can't
     * generally this is equivalent to root() but for compound it includes
     * unnegation and normalization steps. this is why conceptual() and root() are
     * different.  usually 'this'
     */
    public abstract Term concept();
    /**
     * the skeleton of a target, without any temporal or other meta-assumptions
     */
    public abstract Term root();

    /**
     * TODO make Compound only
     */
    public int dt() {
        return DTERNAL;
    }

    public Term normalize(byte offset) {
        return this;
    }

    public final Term normalize() {
        return normalize((byte) 0);
    }


    public final Term replace(Map<? extends Term, Term> m) {
        return replace(m, terms);
    }

    public final Term replace(Term from, Term to) {
        return replace(from, to, terms);
    }

    public final Term replace(Map<? extends Term, Term> m, TermBuilder B) {
        return Replace.replace(this, m, B);
    }

    /**
     * replaces the 'from' target with 'to', recursively
     */
    public final Term replace(Term from, Term to, TermBuilder B) {
        return Replace.replace(this, from, to, B);
    }

    /**
     * unwraps negation - only changes if this term is NEG
     */
    public final Term unneg() {
        return this instanceof Neg || this == Bool.False ? neg() : this;
    }

    public final Term unnegIf(boolean b) {
        return b ? unneg() : this;
    }

    public Term neg() {
        return terms.neg(this);
    }

    public final Term negIf(boolean negate) {
        return negate ? neg() : this;
    }


    public boolean internable() {
        return this instanceof The && (
            !(this instanceof Compound)
            ||
            ((InterningTermBuilder.internTemporal || !dtSpecial(dt())) && ((Compound) this).internables())
        );
    }

    public abstract boolean equalsRoot(Term x);

    public boolean equalsPN(Term t) {
        return this == t || unneg().equals(t.unneg());
    }

    public boolean equalsNeg(Term t) {
        if (this instanceof Neg)
            return !(t instanceof Neg) && unneg().equals(t);
        else if (t instanceof Neg)
            return /*!(this instanceof Neg) &&*/ equals(t.unneg());
        else
            return this instanceof Bool && t instanceof Bool && neg().equals(t);
    }

    public abstract boolean equalConcept(Term x);

    public Predicate<Term> equals() {
        return new TermEquality(this);
    }
    public Predicate<Term> equalsPN() {
        return new TermEqualityPN(this);
    }

    /** whether this is an event of provided event containers */
    public Predicate<Term> condIn() {
        return y -> y instanceof Compound && ((Compound) y).condOf(this);
    }

    public Predicate<Term> equalsRoot() {
        return TEMPORALABLE() ? new TermRootEquality() : equals();
    }
    public final Predicate<Term> equalsOp() {
        return t -> t.opID()==opID();
    }

    /**
     * @param polarity +1 (positive, as-is)   0: pos or negative,    -1: negative
     */
    public Predicate<Term> equals(int polarity) {
        return switch (polarity) {
            case +1 -> equals();
            case -1 -> neg().equals(); //TODO optimize
            case 0 -> equalsPN();
            default -> throw new UnsupportedOperationException();
        };
    }

    public Predicate<Term> unifiable(int vars, int dur) {
        if (this instanceof Compound) {
            int qOp = opID();
            return x -> x instanceof Compound &&
                    ((Compound) x).opID == qOp &&
                    //structured(x.structure()) &&
                    //!invalidTargets.containsKey(x) &&
                    Unify.isPossible(this, x, vars, dur);
        } else if (this instanceof Variable) {
            if (Op.hasAny(vars, opBit()))
                return x -> true;
        }

        return equals();
    }

    @Deprecated public final int DT() {
        int x = dt();
        if (x == XTERNAL)
            throw new WTF();
        return x == DTERNAL ? 0 : x;
    }

    public boolean VAR() {
        return false;
    }

    public final Predicate<Term> equalsOrInCond() {
        int vx = volume();
        return y -> {
            int vy;
            if (!(y instanceof Compound) || vx==(vy=y.volume()))
                return equals(y);
            else if (vx < vy)
                return ((Compound)y).condOf(this);
            else
                return false;
        };
        //return equals().or(condIn());
    }

    /**
     * @return 0=not equal, -1=equalsNeg, +1=equals
     */
    public final int equalsPolarity(Term y) {
        Term x = this;
        boolean invert = false;
        if (x instanceof Neg) {
            if (!(y instanceof Neg)) {
                x = x.unneg();
                invert = true;
            } else {
                x = x.unneg();
                y = y.unneg(); //unwrap both
            }
        } else {
            if (y instanceof Neg) {
                y = y.unneg();
                invert = true;
            }
        }

        return x.equals(y) ? (invert ? -1 : +1) : 0;
    }


    private static class TermEqualityPN extends TermEquality {

        TermEqualityPN(Term x) {
            super(x.unneg());
        }

        @Override
        public boolean test(Term y) {
            return super.test(y.unneg());
        }
    }

    private final class TermRootEquality implements Predicate<Term> {
        @Nullable TermEquality t;

        @Override
        public boolean test(Term x) {
            if (x == Term.this) //fast test
                return true;
            if (x.opID()!=opID())
                return false;

            if (t == null) t = new TermEquality(root());

            return t.test(x.root());
        }
    }


//    enum TermWalk {
//        Left, //prev subterm
//        Right, //next subterm
//        Down, //descend, recurse, or equivalent to Right if atomic
//        Stop //CUT
//    }

    /**
     * absolute time duration (in cycles) spanned by a sequence conjunction
     * @param xternalSensitive
     */
    public int seqDur(boolean xternalSensitive) {
        return 0;
    }

    public final int seqDur() {
        return seqDur(false);
    }


}