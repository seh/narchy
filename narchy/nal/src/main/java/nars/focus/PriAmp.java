package nars.focus;

import jcog.pri.op.PriMerge;
import jcog.signal.FloatRange;

import static jcog.pri.op.PriMerge.plus;

/** PriNode + Post-Processor (ex: Amplifier) */
public class PriAmp extends PriNode {

	private PriMerge mode =
		PriMerge.and;
		//plus;

	private final FloatRange amp = new FloatRange(mode==plus ? 0 : /*and*/ 1, 0, 1f);

	public PriAmp(Object id) {
		super(id);
	}
	public PriAmp(Object id, float initialAmp) {
		this(id);
		amp(initialAmp);
	}

	@Override
	protected float in(double p) {
        return mode.valueOf(amp.floatValue(), (float) p);
    }

	public PriAmp amp(float a) {
		amp.set(a);
		return this;
	}


}