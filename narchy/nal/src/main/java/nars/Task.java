package nars;

import jcog.Util;
import jcog.math.LongInterval;
import jcog.pri.UnitPri;
import nars.control.Caused;
import nars.control.Why;
import nars.task.NALTask;
import nars.task.SerialTask;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Termed;
import nars.term.atom.Atom;
import nars.term.atom.Atomic;
import nars.term.util.transform.RecursiveTermTransform;
import nars.term.var.NormalizedVariable;
import nars.truth.Truth;
import org.eclipse.collections.impl.map.mutable.primitive.ShortByteHashMap;
import org.jetbrains.annotations.Nullable;

import static nars.Op.*;

/**
 * NAL Task to be processed, consists of a Sentence, stamp, time, and budget.
 */
public abstract class Task extends UnitPri implements Caused, Termed {

//    public static final Task[] EmptyTaskArray = new Task[0];


    public static final Atom BeliefAtom = $.quote(String.valueOf((char)BELIEF));
    public static final Atom GoalAtom =  $.quote(String.valueOf((char)GOAL));
    public static final Atom QuestionAtom =  $.quote(String.valueOf((char)QUESTION));
    public static final Atom QuestAtom =  $.quote(String.valueOf((char)QUEST));
    public static final Atom CommandAtom =  $.quote(String.valueOf((char)COMMAND));


//    /**
//     * fast, imprecise sort.  for cache locality and concurrency purposes
//     */
//    public static final Comparator<? extends Task> sloppySorter = Comparator
//            .comparingInt((Task x) ->
//                    x.term().concept().hashCode())
//            .thenComparing(x -> -x.priElseZero());

//    public static void fund(Task y, Task x, float factor, boolean copyOrMove) {
//        if (!copyOrMove)
//            throw new TODO();
//
////        Task.fund(y, new Task[] { x }, copyOrMove);
////        y.priMult(factor);
//        Task.merge(y, new Task[] { x }, factor * x.priElseZero());
//
//
////        //discount pri by increase in target complexity
////        float xc = xx.voluplexity(), yc = yy.voluplexity();
////        float priSharePct =
////                1f - (yc / (xc + yc));
////        yy.pri(0);
////        yy.take(xx, priSharePct * factor, false, copyOrMove);
//    }

    public final int volume() {
        return term().volume();
    }

    public final Op op() {
        return the(opID());
    }

    public final int opID() { return term().opID(); }

    @Override
    public boolean equals(Object that) {
        return this == that
            ||
            (that instanceof Task
                && !(that instanceof SerialTask) //HACK
                && hashCode() == that.hashCode()
                && equal(this, (Task) that));
    }

    /**
     * see equals()
     */
    public static int hash(Term term, Truth truth, byte punc, long start, long end, long[] stamp) {
        int h = Util.hashCombine(term.hashCode(), punc);

        //if (stamp.length > 1) {

        if (truth != null)
            h = Util.hashCombine(h, truth.hashCode());

        if (start != LongInterval.ETERNAL) {
            h = Util.hashCombine(h, start);
            if (end != start)
                h = Util.hashCombine(h, end);
        }
        //}

        if (stamp.length > 0)
            h = Util.hashCombine(h, stamp);

        //keep 0 as a special non-hashed indicator value
        return h != 0 ? h : 1;
    }

    /**
     * assumes identity and hash have been tested already.
     * <p>
     * if evidence is of length 1 (such as input or signal tasks,), the system
     * assumes that its ID is unique (or most likely unique)
     * and this becomes the only identity condition.
     * (start/stop and truth are not considered for equality)
     * this allows these values to mutate dynamically
     * while the system runs without causing hash or equality
     * inconsistency.  see hash()
     */
    private static boolean equal(Task a, Task b) {
        byte p = a.punc();
        if (p != b.punc())
            return false;

        return (p==COMMAND || ((NALTask)a).equalNAL((NALTask)b))
               &&
               a.term().equals(b.term());
    }


//    static StableBloomFilter<Task> newBloomFilter(int cap, Random rng) {
//        return new StableBloomFilter<>(
//                cap, 1, 0.0005f, rng,
//                new BytesHashProvider<>(IO::taskToBytes));
//    }

    public void proof(int indent, StringBuilder sb) {
        appendTo(sb.append("  ".repeat(Math.max(0, indent))), true).append("\n  ");
    }

    public static void priWhy(NALTask y, double pri, Task... x) {
        y.pri((float) pri);
        y.why(Why.why(x));
        //assert(!y.isDeleted());
    }


    public boolean QUESTION() {
        return (punc() == QUESTION);
    }

    public boolean BELIEF() {
        return (punc() == BELIEF);
    }

    public boolean GOAL() {
        return (punc() == GOAL);
    }

    public boolean QUEST() {
        return (punc() == QUEST);
    }

    public boolean COMMAND() {
        return (punc() == COMMAND);
    }

    @Nullable
    public Appendable toString(boolean showStamp) {
        return appendTo(new StringBuilder(128), showStamp);
    }

    public boolean QUESTION_OR_QUEST() {
        return Op.QUESTION_OR_QUEST(punc());
    }

    public boolean BELIEF_OR_GOAL() {
        return Op.BELIEF_OR_GOAL(punc());
    }


    public StringBuilder appendTo(@Nullable StringBuilder sb) {
        return appendTo(sb, false);
    }

    public String toStringWithoutBudget() {
        return appendTo(new StringBuilder(32)).toString();
    }


    @Deprecated
    public StringBuilder appendTo(StringBuilder buffer, boolean showStamp) {
        return term().appendTo(buffer);
    }


    public Task budget(float factor, NAL nar) {
        priMax(factor * nar.priDefault(punc()));
        return this;
    }


    public abstract byte punc();


    @Deprecated public Task pri(NAL defaultPrioritizer) {
        pri(defaultPrioritizer.priDefault(punc()));
        return this;
    }


    public static final class VariableInverter extends RecursiveTermTransform {
        private final ShortByteHashMap counts;

        public VariableInverter(ShortByteHashMap counts) {
            this.counts = counts; //assert(!counts.isEmpty());
        }

        @Override
        public Term applyCompound(Compound c) {
            if (c instanceof Neg) {
                Term d = c.unneg();
                if (invert(d))
                    return d;
            }

            return c.hasVars() ? super.applyCompound(c) :c;
        }

        @Override
        public Term applyAtomic(Atomic a) {
            return invert(a) ? a.neg() : a;
        }

        boolean invert(Term d) {
            return d instanceof NormalizedVariable &&
                counts.containsKey(((NormalizedVariable) d).intrin());
        }
    }
}