package nars.truth.func;

import jcog.Fuzzy;
import jcog.Is;
import jcog.util.Reflect;
import nars.$;
import nars.Term;
import nars.truth.Truth;
import nars.truth.func.annotation.AllowOverlap;
import nars.truth.func.annotation.SinglePremise;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;

import static nars.truth.func.TruthFunctions.*;
import static nars.truth.func.TruthFunctions2.*;

/**
 * NAL Truth Model
 * derivative of the original set of NAL truth functions, preserved as much as possible
 */
public enum NALTruth implements TruthFunction {

    Deduction() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return deduction(T, B, true, confMin);
        }

//        @Override
//        public boolean apply(MutableTruth y, @Nullable Truth task, @Nullable Truth belief, float confMin) {
//            throw new TODO();
//            deduction(y, task, belief, true, confMin);
//            return y.is();
//        }

    },
    DeductionSym() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return deductionSym(T,B,true,confMin);
        }
    },
    DeductionSymWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return deductionSym(T,B,false, confMin);
        }
    },

    Conduct() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return conduct(T, B, confMin);
        }
    },

    ConductWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(Conduct.apply(T, B, confMin), confMin);
        }
    },

    /** HACK for occurrence time calculation */
    @Deprecated NegConductWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return neg(ConductWeak.apply(T, B, confMin));
        }
    },

    SemiConduct() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return semiconduct(T, B, confMin);
        }
    },
    SemiConductWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(SemiConduct.apply(T, B, confMin), confMin);
        }
    },

    /** TODO reverse args TODO rename */
    @Deprecated Biduct() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return SemiConduct.apply(B, T, confMin);
        }
    },

    /** TODO reverse args TODO rename */
    @Deprecated BiductWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return SemiConductWeak.apply(B, T, confMin);
        }
    },

    /** TODO reverse args TODO rename */
    @Deprecated BiductWeaker() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {

            return weak(BiductWeak.apply(T, B, confMin), confMin);
        }
    },

    DeductionWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return deduction(T, B, false, confMin);
        }

    },

//    @AllowOverlap DeductionRecursive() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return deduction(T, B, true, confMin);
//        }
//
//    },
//    @AllowOverlap DeductionWeakRecursive() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return TruthFunctions.deduction(T, B, false, confMin);
//        }
//
//        @Override
//        public boolean taskTruthValid(@Nullable Truth t) {
//            return nonZeroFreq(t);
//        }
//    },

    /** this is like sqrt(truth) .... try actual: f = sqrt(f0) */
    @AllowOverlap @SinglePremise StructuralDeduction() {
        @Override
        public Truth apply(Truth T, Truth Bignored, float confMin) {
            return Deduction.apply(T, $.tt(1, T.conf()), confMin);
//            final double c = T!=null ? confReduce(T) : 0;
//            return c > confMin ? Deduction.apply(T, $.t(1, c), confMin) : null;
        }

        @Override
        public boolean beliefTruthSignificant() {
            return false;
        }
    },

    /**
     * similar to structural deduction but keeps the same input frequency, only reducing confidence
     * TODO check if this is equivalent to a 'structural induction' if so rename it
     */
    @SinglePremise StructuralReduction() {
        @Override
        public Truth apply(Truth T, Truth Bignored, float confMin) {
            double c = confReduce(T);
            return c < confMin ? null : $.tt(T.freq(), c);
        }

        @Override
        public boolean beliefTruthSignificant() {
            return false;
        }
    },
//    @SinglePremise SymmetricReduction() {
//        @Override
//        public Truth apply(Truth T, Truth Bignored, float confMin) {
//            double c = confReduce(T);
//            return c >= confMin ? $.tt((float) Math.sqrt(T.freq()), c) : null;
//        }
//
//        @Override
//        public boolean beliefTruthSignificant() {
//            return false;
//        }
//    },

    Induction() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return induction(T, B, confMin);
        }
        //TODO belief freq  non-zero
    },


    @Is("Sherlock_Holmes#Holmesian_deduction") Abduction() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return induction(B, T, confMin);
        }

    },


    /**
     * polarizes according to an implication belief.
     * here if the belief is negated, then both task and belief truths are
     * applied to the truth function negated.  but the resulting truth
     * is unaffected as it derives the subject of the implication.
     */
    AbductionSym() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            if (symNegates(T, B)) {
                T = T.neg();
                B = B.neg();
            }
            return Abduction.apply(T, B, confMin);
        }
    },
    InductionSym() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            if (symNegates(T, B)) {
                T = T.neg();
                B = B.neg();
            }
            return Induction.apply(T, B, confMin);
        }
    },


//    AbductionXOR() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return B.NEGATIVE() ? Abduction.apply(T, B.neg(), confMin) : Abduction.apply(T.neg(), B, confMin);
//        }
//    },

    Comparison() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return comparison(T, B, confMin);
        }
    },
//    ComparisonSymmetric() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return TruthFunctions2.comparisonSymmetric(T, B, confMin);
//        }
//    },

    Conversion() {
        @Override
        public boolean taskTruthSignificant() {
            return false;
        }

        @Override
        public Truth apply(@Nullable Truth T, Truth B, float confMin) {
            return conversion(B, confMin);
        }
    },

    Resemblance() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return resemblance(T, B, confMin);
            //return TruthFunctions2.resemblance(T, B, confMin);
        }
    },

    Contraposition() {
        @Override
        public boolean taskTruthSignificant() {
            return false;
        }

        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return contraposition(B, confMin);
            //return TruthFunctions2.contraposition2(B, confMin);
        }
    },

    Intersection() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return intersect(T, B, confMin);
        }
    },

    Union() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return neg(intersect(T, true, B, true, confMin));
        }
    },

    Similarity() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return similarity(T, B, confMin);
        }
    },
    Polarduct() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return polarduct(T, B, confMin);
        }
    },
    PolarductWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(Polarduct.apply(T, B, confMin), confMin);
        }
    },
    //    IntersectionSym() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return B.NEGATIVE() ?
//                    Intersection.apply(T.neg(), B.neg(), confMin)
//                    :
//                    Intersection.apply(T, B, confMin);
//        }
//    },
    Pre() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return pre(T, B, true, confMin);
        }

    },
    PreWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return pre(T, B, false, confMin);
        }
    },
    @AllowOverlap PreRecursive() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return pre(T, B, true, confMin);
        }

    },

    Post() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return post(T, B, true, true, confMin);
        }
    },

    Need() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return post(T, B, false, true, confMin);
        }
    },

    PostWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return post(T, B, true, false, confMin);
        }
    },

//    Want() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return pre(T, B, false, true, confMin);
//        }
//
//    },
//    WantWeak() {
//        @Override
//        public @Nullable Truth apply(@Nullable Truth task, @Nullable Truth belief, float confMin) {
//            return weak(Want.apply(task, belief, confMin), confMin);
//        }
//    },

//    Need() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return post(T, B, true, true, confMin);
//        }
//    },
//    NeedWeak() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return weak(Need.apply(T,B,confMin), confMin);
//        }
//    },
//    NeedPolar() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            boolean neg;
//            if (T.POSITIVE() && !B.POSITIVE()) {
//                neg = true;
//                B = B.neg();
//            } else if (!T.POSITIVE() && B.POSITIVE()) {
//                neg = true;
//                T = T.neg();
//            } else
//                neg = false;
//            return negIf(TruthFunctions2.post(T, B, true, true, confMin), neg);
//        }
//    },

    Mix() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return mix2(T, B, confMin);
        }
    },
//    MixDisj() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return TruthFunctions2.mix(T, B, false, confMin);
//        }
//    },

//    IntersectionPB() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return B.isNegative() ?
//                    negIfNonNull(Intersection.apply(T.neg(), B.neg(), confMin))
//                    :
//                    Intersection.apply(T, B, confMin);
//        }
//    },
//    UnionPB() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return B.isNegative() ?
//                    negIfNonNull(Union.apply(T.neg(), B.neg(), confMin))
//                    :
//                    Union.apply(T, B, confMin);
//        }
//    },


//    Difference() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return Intersection.apply(T, B.neg(), confMin);
//        }
//    },
//
//    DifferenceReverse() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return NALTruth.Difference.apply(B, T, confMin);
//        }
//    },

    Analogy() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return analogy(T, B.freq(), B.conf(), confMin);
            //return weak(TruthFunctions2.analogyBipolar(T, B, confMin), confMin);
        }
    },

//    ReduceConjunction() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return reduceConj(T, B, confMin);
//        }
//    },

//
//    AnonymousAnalogy() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return TruthFunctions.anonymousAnalogy(T, B, confMin);
//        }
//    },


//    BixemplificationStrong() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            boolean tNeg = T.NEGATIVE();
//            return negIf(TruthFunctions2.exemplification(T.negIf(tNeg), B, confMin), tNeg);
//        }
//
//    },

//    /** weak exemplification */
//    Could() {
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return weak(Conduct.apply(T, B, confMin), confMin);
//        }
//
//    },

    /** TODO rename to 'Exemplification' */
    ExemplificationStrong() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return exemplification(T, B, confMin);
        }
    },
    /** TODO rename to 'ExemplificationWeak' */
    Exemplification() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(ExemplificationStrong.apply(T, B, confMin), confMin);
        }
    },
    /** TODO rename to 'ExemplificationWeaker' */
    ExemplificationWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(Exemplification.apply(T, B, confMin), confMin);
        }
    },


    /** TODO rename to 'Suppose' */
    SupposeStrong() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return suppose(T, B, confMin);
        }
    },
    /** TODO rename to 'SupposeWeak' */
    Suppose() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(SupposeStrong.apply(T, B, confMin), confMin);
        }
    },
//    DecomposeDiff() {
//        @Override
//        public Truth apply(final Truth T, final Truth B, float minConf, NAL n) {
//            return TruthFunctions2.decomposeDiff(T, B, minConf);
//        }
//    },

    Divide() {
        @Override
        public Truth apply(Truth XY, Truth X, float confMin) {
            return divide(XY, X, confMin);
        }
    },

    DivideWeak() {
        @Override
        public Truth apply(Truth XY, Truth X, float confMin) {
            return weak(Divide.apply(XY, X, confMin), confMin);
        }
    },

//    Undivide() {
//        @Override
//        public Truth apply(Truth XY, Truth X, float confMin) {
//            return TruthFunctions2.undivide(XY, X, confMin);
//        }
//    },

    @SinglePremise
    Identity() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return TruthFunction.identity(T, confMin);
        }

    },


    BeliefIdentity() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return TruthFunction.identity(B, confMin);
        }

        @Override
        public boolean taskTruthSignificant() {
            return false;
        }

    },


    /**
     * maintains input frequency but reduces confidence
     */
    BeliefStructuralReduction() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return StructuralReduction.apply(B, null, confMin);
        }
        @Override
        public boolean taskTruthSignificant() {
            return false;
        }
    },
//    BeliefSymmetricReduction() {
//        @Override
//        public boolean taskTruthSignificant() {
//            return false;
//        }
//
//        @Override
//        public Truth apply(Truth T, Truth B, float confMin) {
//            return B == null ? null : SymmetricReduction.apply(B, null, confMin);
//        }
//    },

    BeliefStructuralDeduction() {
        @Override
        public boolean taskTruthSignificant() {
            return false;
        }


        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return B == null ? null : StructuralDeduction.apply(B, null, confMin);
        }
    },

    Xor() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            double c = confCompose(T,B); if (c < confMin) return null;

            float tf = T.freq(), bf = B.freq();
            //                f,c
            //tf+ & bf+ => bf 1,0
            //tf+ & bf- => bf 1,1
            //tf- & bf+ => bf 0,1
            //tf- & bf- => bf 0,0

            double diff =
                Fuzzy.xor(tf, bf);
                //1 - Fuzzy.xnrNorm(tf, bf);

            c *= diff;
            if (c < confMin) return null;

            double f = tf;
                //1;
                //Util.lerpSafe(diff, 0.5, tf);
                //Util.lerpSafe(polarity(tf)*polarity(bf), 0.5, tf);
                //Util.lerpSafe(polarity(bf), 0.5, tf);

            return $.tt((float) f, c);
        }
    },
    XorWeak() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(Xor.apply(T, B, confMin), confMin);
        }
    },

    XorWeaker() {
        @Override
        public Truth apply(Truth T, Truth B, float confMin) {
            return weak(XorWeak.apply(T, B, confMin), confMin);
        }
    }

    ;

    private static boolean symNegates(Truth T, Truth B) {
        return symNegates(T.freq(), B.freq());
    }

    private static boolean symNegates(float tf, float bf) {
        return tf*bf < (1-tf)*(1-bf);
    }


    public static final TruthModel the = new TruthModel(values());
    private final boolean single;
    private final boolean overlap;
    private final Term term;


    NALTruth() {
        Field f = Reflect.on(NALTruth.class).field(name()).get();
        this.single = f.isAnnotationPresent(SinglePremise.class);
        this.overlap = f.isAnnotationPresent(AllowOverlap.class);
        this.term = TruthFunction.super.term();
    }

    public static NALTruth implStrong(boolean fwd, boolean beliefOrGoal) {
        if (beliefOrGoal) {
            return fwd ? Pre : PostWeak;
        } else {
            return fwd ? PreWeak : Post;
        }
    }

    @Override
    public final Term term() {
        return term;
    }

    @Override
    public final boolean single() {
        return single;
    }

    @Override
    public final boolean allowOverlap() {
        return overlap;
    }

}

//    IntersectionSym() {
//        @Override
//        public Truth apply(final Truth T, final Truth B, NAR m, float minConf) {
//            return TruthFunctions2.intersectionSym(T,B,minConf);
//        }
//    },
//
//    UnionSym() {
//        @Override
//        public @Nullable Truth apply(@Nullable Truth T, @Nullable Truth B, NAR m, float minConf) {
//            return TruthFunctions2.unionSym(T,B,minConf);
//
////            if (T.isPositive() && B.isPositive()) {
////                return Union.apply(T, B, m, minConf);
////            } else if (T.isNegative() && B.isNegative()) {
////                Truth C = Union.apply(T.neg(), B.neg(), m, minConf);
////                return C != null ? C.neg() : null;
////            } else {
////                return null;
////            }
//        }
//    },