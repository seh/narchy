package nars.truth.func;

import jcog.Fuzzy;
import jcog.Is;
import nars.NAL;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import static jcog.Fuzzy.polarity;
import static jcog.Util.lerpSafe;
import static nars.NAL.truth.TRUTH_EPSILON;
import static nars.truth.func.TruthFunctions.*;

public enum TruthFunctions2 {
    ;

//    public static @Nullable Truth conduct(Truth goal, Truth belief, boolean uni,  float confMin) {
//
//        double c = confCompose(belief, goal); if (c < confMin) return null;
//
//        float gF = goal.freq(), bF = belief.freq();
//
//        c *= (uni ? gF : 1) * bF; if (c < confMin) return null;
//
//        float f = uni ? 1 : gF;
//                  //gf;
//
//        return tt(f, c);
//    }


    /**
     *   X, (  X ==> Y) |- Y
     * --X, (--X ==> Y) |- Y
     * frequency determined by the XimplY
     * TODO weak -> !strong
     */
    public static Truth pre(Truth X, Truth XimplY, boolean strong, float confMin) {
        double _c = confCompose(X, XimplY);
        if (_c < confMin) return null;

        double x = X.freq();
        double xy = XimplY.freq();

        double f, c;

        f = xy; c = _c * x; //direct

        //f = lerpSafe(x, 0.5, xy); c = _c * x; //fade to maybe + doubt

        //f = lerpSafe(x, 0.5, xy); c = _c; //fade to maybe

//            f = lerpSafe(x, 1-xy, xy); c = _c; //crossover

//            f = lerpSafe(x, 1-xy, xy); c = _c * x; //crossover, with doubt

//            f = lerpSafe(x, 0.5, xy); c = _c;


//            f = lerpSafe(x, 1-xy, xy); c = _c * polarity(x); //crossover


        if (c < confMin) return null;

        if (!strong) {
            c = weak(c);
            if (c < confMin) return null;
        }

        return tt(f, c);
    }


    /**
     *   Y. , (X ==>   Y). |- X.
     * --Y. , (X ==> --Y). |- X.
     */
    public static Truth post(Truth Y, Truth XimplY, boolean beliefOrGoal, boolean strong, float confMin) {
        double c = confCompose(Y, XimplY);
        if (c < confMin) return null;

        float y = Y.freq();
        float xy = XimplY.freq();

        double f;
        double alignment = alignment(y, xy);

        if (beliefOrGoal) {

            //f = Util.lerp(alignment, 0.5, 1); c *= alignment; //half-wave: smooth bipolar with doubt

            //f = alignment; c *= alignment; //bipolar with doubt

            f = 1; c *= alignment; //full-wave


        } else {
            f = alignment; c *= 1; //direct (freq only)

            //f = alignment; c *= polarity(xy);

            //f = alignment; c *= min(polarity(y), polarity(xy));
            //f = alignment; c *= max(polarity(y), polarity(xy));
            //f = alignment; c *= polarity(y) * polarity(xy);

            //f = 1; c *= alignment; //full-wave
            //f = alignment; c *= polarity(alignment); //absolute; neutral alignment -> inaction
            //f = alignment; c *= 0.5 + polarity(alignment)/2; //absolute; neutral alignment -> inaction
        }




        //f = alignment; c *= lerpSafe(polarity(alignment), Fuzzy.mean(polarity(y), polarity(xy)), 1f); //conf decay for promoting decisiveness //TODO parameters for hi/low range

        //f = alignment; //bipolar

//            c *= polarity(alignment);

//            c *= alignment; //benefit of the doubt: the opposite case could also be unaligned

//            f = Fuzzy.pow(alignment, 0.5);
////            c *= (1 + polarity(alignment))/2;
//
//            c *= polarity(alignment);
//            c *= polarity(Math.max(alignment, 1-alignment));

        if (c < confMin) return null;

        if (!strong) {
            c = weak(c);
            if (c < confMin) return null;
        }

        return tt(f, c);
    }

    private static double alignment(float x, float y) {
        return
            Fuzzy.equals(x, y);
            //Fuzzy.eqNorm(y, xy);
            //Fuzzy.xnr(y, xy);
            //Fuzzy.alignment(y, xy, NAL.truth.TRUTH_EPSILON);
            //Fuzzy.xnrNorm(y, xy);
            //Fuzzy.equalsPolar(y, xy, 4);
    }

//    private static double freqExpandTanh(double f, float contrast, float margin) {
//        return Fuzzy.unpolarize(
//                Math.tanh(Fuzzy.polarize(f) * contrast) * (1.0 + margin)
//        );
//    }

//    private static double freqExpandSigmoid(double f, float contrast) {
//        return Util.unitizeSafe(Util.sigmoid(Fuzzy.polarize(f) * contrast));
//    }


    public static @Nullable Truth divide(Truth XY, Truth X, float confMin) {
        return divide(XY, X,
                //NAL.truth.DIVIDE_DOUBT, false,
                NAL.truth.DIVIDE_DOUBT, NAL.truth.DIVIDE_DOUBT,
                confMin);
    }

    public static @Nullable Truth divide(Truth XY, Truth X, boolean doubtOverflow, boolean doubtUnderflow, float confMin) {
        double x = X.freq();
        if (x >= TRUTH_EPSILON) {
            double c = confCompose(XY, X);
            if (c < confMin) return null;

            float xy = XY.freq();

            double y = Fuzzy.divide(xy, x);
            if (y > 1) {
                if (doubtOverflow) {
                    c *= xy;
                    //c /= y; //c *= x/xy
                    if (c < confMin) return null;
                }
                y = 1;
            } else if (y < TRUTH_EPSILON/2) {
                if (doubtUnderflow) {
                    c *= x;
                    if (c < confMin) return null;
                }
                y = 0;
            }
            return tt(y, c);
        }
        return null;
    }

//    /**
//     * EXPERIMENTAL
//     * variation of the original union truth function that
//     * decreases confidence in proportion to the information lost
//     * by one component's higher frequency masking the other
//     * component's lower frequency.  if the components
//     * have equal frequencies then no loss is involved.
//     */
//    public static Truth unionFair(Truth t, Truth b, float confMin) {
//        double c = confCompose(t, b); if (c < confMin) return null;
//
//        float tf = t.freq(), bf = b.freq();
//        float f = Fuzzy.or(tf, bf);
//        if (f < TRUTH_EPSILON)
//            return null;
//
//        float loss = abs((f - tf) - (f - bf));
//        float lossFraction = loss / f;
//        c *= 1 - lossFraction;
//        return c < confMin ? null : tt(f, c);
//    }


    /**
     * commutative
     * {<M --> S>, <P --> M>} |- <S --> P>
     * {<M ==> S>, <P ==> M>} |- <S ==> P>
     */
    static Truth exemplification(Truth a, Truth b, float confMin) {
        float af = a.freq();
        if (af < TRUTH_EPSILON) return null;
        float bf = b.freq();
        if (bf < TRUTH_EPSILON) return null;

        double cc = confCompose(a, b);
        if (cc < confMin) return null;

        double c = cc * af * bf;
        return c < confMin ? null : tt(1, c);
    }

    /**
     * commutative
     * a 'half-way' exemplification that 'bottoms-out' at freq=0.5
     */
    static Truth suppose(Truth a, Truth b, float confMin) {

        float af = Fuzzy.polarize(a.freq());
        if (af < TRUTH_EPSILON) return null;
        float bf = Fuzzy.polarize(b.freq());
        if (bf < TRUTH_EPSILON) return null;

        double cc = confCompose(a, b);
        if (cc < confMin) return null;

        double c = cc * af * bf;
        return c < confMin ? null : tt(1, c);
    }
    /**
     * like mixing two frequencies of light or sound to produce a 3rd that is a compromise of both
     */
    @Nullable
    public static Truth mix2(Truth t, Truth b, float confMin) {
        float tf = t.freq(), bf = b.freq();

        //double tc = t.conf(), bc = b.conf();

        double c = confCompose(t, b) *
                //eviReviseToConf(te, be) *
                //Fuzzy.xnr(tf, bf);
                //Fuzzy.alignment(tf, bf, NAL.truth.TRUTH_EPSILON/2)
                //Fuzzy.equals(tf, bf);
                Fuzzy.eqNorm(tf, bf);

        if (c < confMin) return null;


        double te = t.evi(), be = b.evi();
        double tb = te / (te + be);
        double f = lerpSafe(tb, bf, tf); //linear interpolate weighed by conf

        return tt(f, c);
    }

    /** propagates frequency, attenuates conf */
    public static Truth conduct(Truth dir, Truth mag, float confMin) {
        float mf = mag.freq();
        double c = confCompose(mag, dir) * mf;
        return c < confMin ? null :
                tt(dir.freq(), c);
    }

    /**
     * soft conduct/semi-conduct
     * non-inverting analogy / bi-polarized deduction
     */
    public static Truth semiconduct(Truth dir, Truth mag, float confMin) {
        float mf = mag.freq(), df = dir.freq();
        double c = confCompose(mag, dir) * mf;
        return c < confMin ? null :
                tt(lerpSafe(mf, 0.5, df), c);
    }

    @Is("Ising_model") public static Truth polarduct(Truth dir, Truth mag, float confMin) {
        double df = dir.freq(), mf = mag.freq();
        double c = confCompose(mag, dir) * polarity(mf);
        if (c < confMin) return null;

        double f =
            lerpSafe(mf, 1 - df, df) //soft
            //mf >= 0.5 ? df : 1 - df //hard
        ;

        return tt(f, c);
    }


//    /**
//     * new bipolar analogy
//     */
//    @Nullable
//    public static Truth analogyBipolar(Truth x, Truth a, float confMin) {
//        double af = a.freq();
//
//        double c = confCompose(x, a) * polarity(af);
//        if (c < confMin) return null;
//
//        double xf = x.freq();
//        double F = lerpSafe(af, 1 - xf, xf);
//        return tt(F, c);
//    }

    public static double confReduce(Truth t) {
        return confCompose(t, NAL.truth.GULLIBILITY);
        //return confCompose(t, (float) t.conf()); //too strict
    }


    @Nullable
    public static Truth xConduct(Truth g, Truth c, float confMin) {
        double cf = c.freq();
        double cc = confCompose(g, c) * polarity(cf);
        if (cc < confMin) return null;

        double gf = g.freq();
        double f = lerpSafe(cf, 1 - gf, gf); //inversion
        return tt(f, cc);
    }


    @Nullable
    public static Truth deductionSym(Truth T, Truth B, boolean strong, float confMin) {
        double tf = T.freq(), bf = B.freq();
        boolean neg = (1 - tf) * (1 - bf) > tf * bf;
        if (neg) {
            T = T.neg();
            B = B.neg();
        }
        return negIf(deduction(T, B, strong, confMin), neg);
    }

    public static Truth similarity(Truth a, Truth b, double minConf) {
        double c = confCompose(a, b);
        if (c < minConf) return null;
        double sim = Fuzzy.equals(
                //Fuzzy.eqNorm(
                a.freq(), b.freq());

//        //optional:
//        c *= Fuzzy.polarity(sim); if (c < minConf) return null;

        return tt(sim, c);
    }
}

//    /**
//     * freq symmetric intersection
//     * to the degree the freq is the same, the evidence is additive
//     * to the degree the freq is different, the evidence is multiplicative
//     * resulting freq is weighted combination of inputs
//     */
//    public static Truth intersectionX(Truth a, Truth b, float confMin) {
//        float diff = Math.abs(a.freq() - b.freq());
//        float ac = a.conf(), bc = b.conf();
//        float conf = Util.lerp(diff, w2cSafe(c2wSafe(ac) + c2wSafe(bc)), (ac * bc));
//        float freq = ((a.freq() * ac) + (b.freq() * bc)) / (ac + bc);
//        return conf >= confMin ? $.tt(freq, conf) : null;
//    }

//    /**
//     * freq symmetric difference
//     * to the degree the freq differs or is similar, the evidence is additive
//     * to the degree the freq is not different nor similar, the evidence is multiplicative
//     * resulting freq is weighted difference of inputs
//     */
//    public static Truth differenceX(Truth a, Truth b, float confMin) {
//        float extreme = 2f * Math.abs(0.5f - Math.abs(a.freq() - b.freq()));
//        float ac = a.conf(), bc = b.conf();
//        float conf = Util.lerp(extreme, (ac * bc), w2cSafe(c2wSafe(ac) + c2wSafe(bc)));
//
//        float freq = a.freq() * (1f - b.freq());
//        return conf >= confMin ? $.tt(freq, conf) : null;
//    }

//    public static Truth unionX(Truth a, Truth b, float confMin) {
//        Truth z = intersectionX(a.neg(), b.neg(), confMin);
//        return z != null ? z.neg() : null;
//    }

//    @Nullable
//    public static Truth deduction(Truth a, float bF, float bC, float confMin) {
//
//
//        float f = and(a.freq(), bF);
//
//
//        float aC = a.conf();
//
//        float c = Util.lerp(bC/(bC + aC), 0 ,aC);
//
//        return c >= confMin ? t(f, c) : null;
//    }

//    @Nullable
//    public static Truth deduction(Truth a, float bF, float bC, float confMin) {
//
//        float f;
//        float aF = a.freq();
////        if (bF >= 0.5f) {
////            f = Util.lerp(bF, 0.5f, aF);
////        } else {
////            f = Util.lerp(bF, 0.5f, 1- aF);
////        }
//        f = Util.lerp(bF, 1-aF, aF);
//
//        float p = Math.abs(f - 0.5f)*2f; //polarization
//
//        float c = //and(/*f,*/ /*p,*/ a.conf(), bC);
//                    TruthFunctions.confCompose(a.conf(), bC);
//
//        return c >= confMin ? t(f, c) : null;
//    }
//
//    /**
//     * {<S ==> M>, <P ==> M>} |- <S ==> P>
//     *
//     * @param a Truth value of the first premise
//     * @param b Truth value of the second premise
//     * @return Truth value of the conclusion, or null if either truth is analytic already
//     */
//    public static Truth induction(Truth a, Truth b, float confMin) {
//        float c = w2cSafe(a.conf() * b.freqTimesConf());
//        return c >= confMin ? $.tt(a.freq(), c) : null;
//    }

//
//    /**
//     * frequency determined entirely by the desire component.
//     */
//    @Nullable
//    public static Truth desireNew(Truth goal, Truth belief, float confMin, boolean strong) {
//
//        float c = and(goal.conf(), belief.conf(), belief.freq());
//
//        if (!strong) {
//            //c *= TruthFunctions.w2cSafe(1.0f);
//            c = w2cSafe(c);
//        }
//
//        if (c >= confMin) {
//
//
//            float f = goal.freq();
//
//            return $.tt(f, c);
//
//        } else {
//            return null;
//        }
//    }

//    /**
//     * experimental
//     */
//    public static @Nullable Truth educt(Truth goal, Truth belief, boolean strong, float confMin) {
//
//        double cc = confCompose(belief, goal);
//        if (cc < confMin) return null;
//
//
//        double bF = belief.freq();
//        float gF = goal.freq();
//
//        cc *= (bF);
//        if (cc < confMin) return null;
//        if (!strong) {
//            cc = weak(cc);
//            if (cc < confMin) return null;
//        }
//
//        double f =
//                lerpSafe(bF, 1 - gF, gF);
////                lerpSafe(bF, 0.5, gF);
//
//        return tt((float) f, cc);
//
//    }

//    public static @Nullable Truth demand(Truth goal, Truth condition, float confMin, boolean strong) {
//
//        double cc = confCompose(goal, condition);
//        if (cc < confMin) return null;
//        if (!strong) {
//            cc = weak(cc);
//            if (cc < confMin) return null;
//        }
//
//        double cF = condition.freq();
//        float gF = goal.freq();
//
//        float f =
//                gF;
////                cF >= 0.5f ? gF : 1-gF;
//        //gF >= 0.5f ?  +1 : 0;
//        //(float) cF;
//        //(float) a;
//        //1;
//
//        double a =
////                polarity(cF) * gF;
//                cF;
//        //cF * gF;
//        //cF * polarity(gF);
//        //gF;
//        //Util.lerpSafe((float) (cF * gF), 0.5f, 1);
//        //Math.sqrt(cF * gF);
//        //1;
//
//        cc *= a;
//        if (cc < confMin) return null;
//
//
//        return tt(f, cc);
//    }