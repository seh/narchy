package nars.truth.proj;

import com.google.common.collect.Iterables;
import jcog.Is;
import jcog.Research;
import jcog.Util;
import jcog.WTF;
import jcog.data.array.IntComparator;
import jcog.data.bit.MetalBitSet;
import jcog.data.graph.BitMatrixGraph;
import jcog.data.list.Lst;
import jcog.math.LongInterval;
import jcog.sort.QuickSort;
import jcog.util.ArrayUtil;
import nars.NAL;
import nars.Term;
import nars.subterm.TermList;
import nars.task.NALTask;
import nars.task.ProxyTask;
import nars.task.Tasked;
import nars.task.proxy.SpecialOccurrenceTask;
import nars.task.proxy.SpecialPuncTermAndTruthTask;
import nars.task.proxy.SpecialTermTask;
import nars.task.util.StampOverlapping;
import nars.task.util.TruthComputer;
import nars.term.Compound;
import nars.term.compound.LightCompound;
import nars.term.util.DTDiffer;
import nars.term.util.Intermpolate;
import nars.term.util.conj.ConjBundle;
import nars.time.Moment;
import nars.truth.Truth;
import nars.truth.evi.EviInterval;
import nars.truth.util.TaskEviList;
import org.eclipse.collections.api.block.procedure.primitive.IntIntProcedure;
import org.jetbrains.annotations.Nullable;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static java.lang.System.arraycopy;
import static jcog.Util.fma;
import static jcog.Util.sqr;
import static nars.NAL.derive.OVERLAP_ALLOW;
import static nars.Op.CONJ;

/**
 * Truth Interpolation and Extrapolation of Temporal Beliefs/Goals
 * see:
 * https:
 * https:
 */
@Is({"Interpolation", "Extrapolation"})
@Research
public abstract class MutableTruthProjection extends TaskEviList implements TruthProjection, TruthComputer, Consumer<NALTask>, IntComparator {

    private final EviInterval at;

    public MutableTruthProjection freqRes(float f) {
        freqRes = f;
        return this;
    }

    public double eviMin() {
        return eviMin;
    }

    public enum EviMerge {
        Plus,
        Mean,
        Max,
        Min
    }

    /**
     * content target, either equal in all the tasks, or the result is
     * intermpolated (and evidence reduction applied as necessary)
     */
    @Nullable public Term term;

    double eviMin = NAL.truth.EVI_MIN;

    public EviMerge eviMerge = EviMerge.Plus;

    /** whether to ignore overlap */
    public boolean overlapIgnore = OVERLAP_ALLOW;

    /**
     * used in final calculation of to start/end time intervals
     */
    private int ditherDT = 1;
    private float freqRes;

    private int sizeMin = 1, sizeMax = Integer.MAX_VALUE;

    private boolean timeAuto;

    MutableTruthProjection(int capacity) {
        this(TIMELESS, TIMELESS, capacity);
    }

    MutableTruthProjection(long start, long end) {
        this(start, end, 0);
    }

    MutableTruthProjection(long start, long end, int capacity) {
        super(capacity);
        at = new EviInterval(start, end);
        if (start == TIMELESS)
            timeAuto = true;
    }

    @Override
    public final long[] startEndArray() {
        return AND(NALTask::ETERNAL) ? new long[]{ETERNAL, ETERNAL} : super.startEndArray();
    }

    protected final int sizeMax() {
        return sizeMax;
    }

    @Override
    public final Moment when() { return at; }

    public final TruthProjection with(NALTask[] t, int numTasks) {
        this.items = t;
        this.size = numTasks;
        return this;
    }

    public final MutableTruthProjection dtDither(int ditherDT) {
        this.ditherDT = Math.max(1, ditherDT);
        at.dither(ditherDT);
        return this;
    }

    @Override
    public final void accept(NALTask t) {
        add(t);
    }

    @Override
    public final @Nullable Truth truth() {
        return commit() ? computeTruth() : null;
    }

    /**
     * removes the weakest components sharing overlapping evidence with stronger ones.
     * should be called after all entries are added
     */
    private boolean commit() {
        if (evi!=null)
            throw new UnsupportedOperationException("already commited");

        int sizeMin = this.sizeMin;
        if (size < sizeMin)
            return false;

        focus(timeAuto);
        if (size < sizeMin)
            return false;

        if (NAL.truth.TRUTH_PROJECTION_FILTER_WEAK_PRE && size > 1)
            filterWeak();

        if (size == 1) {
            term(term(0));
            return true;
        } else {
            return commitN();
        }
    }

    private void filterWeak() {
         if (size > 1)
             filterWeak(weakPct());
    }

    private boolean commitN() {

        if (!overlapIgnore)
            filterOverlap();

        if (size < this.sizeMin) return false;

        if (!intermpolate())
            return false;

        if (timeAuto)
            focusFinal(true /* TODO *//*size!=sizeBefore*/);

        removeExcess();

        if (NAL.truth.TRUTH_PROJECTION_FILTER_WEAK_POST)
            filterWeak();

        return true;
    }

    private float weakPct() {
        /* TODO allow larger margin if all the frequency are similar, etc ?  */
        //return NAL.truth.TRUTH_EPSILON;
        return Util.max(freqRes, NAL.truth.TRUTH_EPSILON);
    }

    private void focusFinal(boolean refocus) {
        if (refocus)
            focus(true);

        //can't fine-tune if intermpolation involved because it would invalidate the calculated intermpolation
        if (size > 1 && concentrate())
            tryConcentrate();
    }

    private void removeExcess() {
        int s = this.size;
        int excess = s - sizeMax();
        if (excess > 0) {
            for (int i = 0; i < excess; i++)
                nullify(--s);
            removeNulls(); //HACK
            //assert(this.size==s);
        }

    }

    /** filter weak contributors to avoid including them in the stamp */
    private boolean filterWeak(double pctThresh) {
        if (pctThresh < Double.MIN_NORMAL)
            return false;

        int sPrev = size;
        int sMin = sizeMin;
        if (sPrev <= sMin) return false;

        double[] e = this.evi;

        double eSum = Util.sum(e, 0, sPrev);
        assert(eSum >= Double.MIN_NORMAL): "uninitialized";

        NALTask[] t = this.items;
        double eThresh = eSum * pctThresh; //Math.max(ee * pctThresh, Double.MIN_NORMAL);
        int removeMax = sPrev - sMin;
        boolean removedSomething = false;
        for (int i = sPrev - 1; i >= 1; i--) {
            double ei = e[i];
            if (ei < eThresh) {
                e[i] = 0;
                t[i] = null;
                removedSomething = true;
                if (--removeMax <= 0)
                    break;

                if (ei > Double.MIN_NORMAL) {
                    //lower thresh
                    eSum -= ei; eThresh = eSum * pctThresh;
                }
            }
        }

        if (removedSomething) {
            removeNulls();
            return true;
        } else
            return false;
    }

    @Is("Center_of_mass") private void tryConcentrate() {
        long s = this.start();
        if (s == ETERNAL)
            return;

        long[] newTime = concentrate(s, this.end(), concentrationStrength());
        if (newTime == null)
            return;

        EviInterval f = at.cloneIfChanged(newTime[0], newTime[1], ditherDT);
        if (f == null)
            return;

        //final int remain = update(); //assert(remain >= minComponents);
        int n = size;
        double[] cEvi/*n Key*/ = new double[n];
        computeComponents(items, 0, n, f, cEvi);

//                if (Util.count(MutableTruthProjection::eviValid, cEvi) >= sizeMin &&
//                    Util.sum(cEvi) >= Util.sum(this.evi, 0, n)) {
        time(f.s, f.e, ditherDT);
        applyComponents(cEvi, n);
//                } else {
//                        throw new TODO("undo?");
//                }

    }

    /** strength proportional to the difference in (freq,?) conf, occ among the projected tasks
     *  TODO refine
     */
    private float concentrationStrength() {
//        if (size == 2) {
//            final NALTask a = items[0], b = items[1];
//            //double df = Math.abs(a.freq() - b.freq());
//            double dc = Math.abs(a.conf() - b.conf());
//            //return (float) Util.unitizeSafe(df + dc);
//            //TODO occ?
//            return (float)dc;
//        } else
//            return 1; //HACK TODO n>2: variance, etc..

        double cMin = Double.POSITIVE_INFINITY, cMax = Double.NEGATIVE_INFINITY;
        int n = 0;
        for (NALTask t : this) {
            if (t.start()==ETERNAL) continue;
            double tc =
                    //t.conf() * t.range();
                    t.evi() * t.range();
                    //t.conf();
                    //TODO maybe range of T intersecting with with qs,qe
            if (tc < cMin) cMin = tc;
            if (tc > cMax) cMax = tc;
            n++;
        }
        if (n <= 1) return 0;
        float s = (float) ((cMax-cMin) / cMax);
        return Math.min(1, s/(n-1));
    }

    /** computes an ideal (weighted) inner temporal range for the contained tasks according to a ranking function.
     *  items are ignored when the rank function returns NaN
     *  returns null if unchanged
     *
     * @param strength (anti-momentum, between 0 and 1) 0 = qs..qe unchanged, 1 = fully concentrated
     */
    @Nullable private long[] concentrate(long qs, long qe, float strength) {
        if (strength < Float.MIN_NORMAL)
            return null;

        assert(qs!=TIMELESS && qs!=ETERNAL); //if (qs == TIMELESS) throw new TODO("compute union here");

        NALTask[] tasks = this.items;
        int size = this.size;
        double[] evi = this.evi;

        /* interpolated time range; stored as offset from qs and qe (respectively) for maximal precision */
        double ss = 0, ee = 0, rSum = 0;

        for (int i = 0; i < size; i++) {
            NALTask t = tasks[i];
            long ts = t.start(), te;
            if (ts == ETERNAL || !LongInterval.containsRaw(qs, qe, ts, te = t.end())) {
                ts = qs; te = qe;
            }

            double ei = evi[i];//  if (r!=r || r < Double.MIN_NORMAL) continue;
            rSum += ei;
            ss  = fma(ei, ts - qs, ss);
            ee  = fma(ei, te - qe, ee);
        }

        if (rSum <= Double.MIN_NORMAL)
            return null; //??

        if (strength < 1) {
            float weak = 1 - strength;
            double eWeak = rSum * weak;
            rSum += eWeak; //TODO fma ^
        }

        long cs = (long) /*Math.floor*/(qs + ss/rSum);
        long ce = (long) Math.ceil (qe + ee/rSum);

        return new long[] {cs, ce};
    }

    protected boolean concentrate() {
        return NAL.truth.CONCENTRATE_PROJECTION;
    }


    /**
     * evidence value being computed:
     * <p>
     * if the query is ETERNAL (ie. all components are eternal): evi[] stores the average evidence,
     * otherwise it would be infinite.
     * <p>
     * else if the query is TEMPORAL (one or more components are non-eternal): evi[] stores the evidence integral (area under curve) during the query time range
     */
    private void update() {
        int n = size;
        if (n <= 0) return;

        double[] e = this.evi;
        if (e == null || e.length < n)
            e = new double[n];

        computeComponents(items, 0, n, at, e);

        size = applyComponents(e, n);
    }

    private int applyComponents(double[] evi, int n) {
        int count = 0;
        NALTask[] tasks = items;
        boolean sort = false; //if sort required
        double eiPrev = Double.POSITIVE_INFINITY;

        for (int i = 0; i < n; i++) {
            double ei = evi[i];
            if (eviValid(ei)) {
                if (!sort) sort = (ei > eiPrev);
                eiPrev = ei;
                count++;
            } else {
                tasks[i] = null;
                evi[i] = 0;
            }
        }

        return _commitComponents(evi, n, count, sort);
    }

    private int _commitComponents(double[] evi, int n, int count, boolean sort) {
        if (count >= sizeMin) {

            this.evi = evi;

            if (count != n)
                removeNulls();

            if (sort && count > 1) sort();

            return count;
        } else {
            clear();
            return 0;
        }
    }

    protected abstract boolean computeComponents(NALTask[] tasks, int from, int to, EviInterval at, double[] evi);

    protected void sort() {
        sort(this);
    }

    protected void sort(IntComparator sortComparator) {
        QuickSort.quickSort(0, size, sortComparator, this::swap); //descending
        if (size > 2)
            shuffleTiered();
    }

    private void shuffleTiered() {
        ArrayUtil.shuffleTiered(i -> evi[i], this::swap, size, ThreadLocalRandom::current);
    }

//    /**
//     * simple case
//     * warning: does not un-overlap like filterN
//     */
//    private int filter2() {
//        NALTask[] items = this.items;
//        if (items[0] == items[1] || items[0].stampOverlapping(items[1])) {
//            if (minComponents > 1) return 0;
//            nullify(1);
//            removeNulls();
//            return 1;
//        }
//        return 2;
//    }

    /**
     * removes weakest tasks having overlapping evidence with stronger ones
     */
    private int filterOverlap() {

        //assert(minComponents >= 1);

        final int s = size;

        double[] evi = this.evi;
        NALTask[] tasks = this.items;

        BitMatrixGraph g = null;
        int conflicts = 0;
        boolean trims = false;
        try (StampOverlapping o = new StampOverlapping()) {
            for (int r = 0; r < s-1; r++) { //strongest first
                o.set(tasks[r]);

                for (int c = s - 1; c >= r+1; c--) {  //weakest first
                    if (o.test(tasks[c])) {
                        if (separate(r, c)) {
                            trims = true;
                        } else {
                            if (g == null) g =
                                new BitMatrixGraph.BitSetMatrixGraph(s, false);
                                //new BitMatrixGraph.BitSetRowsMatrixGraph(s, false);
                            g.set(r, c, true);
                            conflicts++;
                        }
                    }
                }
            }
            //TODO if there is only one conflict for a given pair of edges,
            //and the overlap time is relatively minimal compared to their ranges,
            //trim the weaker one to a range where no overlap occurrs and remove
            //the conflict
        }

        int sAfter = s;

        if (conflicts == 1) {
            //simple case; find it and remove the weaker item
            if (--sAfter < sizeMin)
                return 0; //failed

            for (int r = 0; r < s; r++) {
                int c = g.lastColumn(r, true);
                if (c >= 0) {
                    nullifyNode(c, g);
                    break;
                }
            }
//            if (NAL.DEBUG) {
//                if (!g.isEmpty()) //TEMPORARY
//                    throw new WTF();
//            }

        } else if (conflicts > 1) {
            //Dominator Elimination
            int[] rowOrder = sortDominators(g);

            for (int ro = 0; ro < s; ro++) {
                int r = rowOrder[ro];
                double thisValue = evi[r];
                if (thisValue <= 0) continue;

                double otherValue = 0;
                int rN = 0;
                for (int c = s-1; c >= 0; c--) {
                    if (r!=c && g.isEdge(r,c) /* ? uncontended: && g.rowCardinality(c)<=1 */) {
                        otherValue += evi[c];
                        rN++;
                    }
                }
                if (rN == 0)
                    continue;



                if (thisValue > otherValue && (sAfter - rN >= sizeMin)) {
                    //sacrifice the others
                    sAfter -= rN;
                    for (int c = s-1; c >= 0; c--) {
                        if (r != c && g.isEdge(r,c)) {
                            nullifyNode(c, g);
                            if (--rN <= 0)
                                break; //exit early
                        }
                    }
                } else if (--sAfter >= sizeMin){
                    //sacrifice for the others
                    nullifyNode(r, g);
                } else {
                    //sacrifice impossible; fail
                    clear();
                    return size = 0;
                }

                if (g.isEmpty())
                    break; //done
            }


        }
        assert(sAfter >= sizeMin);

        if (sAfter != s)
            removeNulls();

        assert(size > 0);

        if (trims)
            sort();

        return this.size;
    }

    private static int[] sortDominators(BitMatrixGraph g) {
        //TODO if minComponents > 1, pre-test to determine if too many conflicts for any possible resolution
        return new DominatorSorter(g).rowOrder;
    }

    private void nullifyNode(int r, BitMatrixGraph g) {
        nullify(r);
        g.removeNode(r);
    }

    /** trim y (weaker) to not overlap temporally with x (stronger)
     *  @return whether trim was successful
     *
     *        4 cases:
     *           1  x contains y: impossible
     *           2  y contains x: longer of either prefix or suffix
     *           3     y leads x: prefix
     *           4     x leads y: suffix
     *
     * TODO when range 2: somehow use both prefix and suffix instead of just either
     *
     * */
    private boolean separate(int x, int y) {
        NALTask[] tasks = this.items;
        NALTask X = tasks[x];
        long xs = X.start(); if (xs == ETERNAL) return false;
        NALTask Y = tasks[y];
        long ys = Y.start(); if (ys == ETERNAL) return false;
        long xe = X.end(), ye = Y.end();
        if (LongInterval.containsRaw(xs, xe, ys, ye)) return false;

        long prefixLen, suffixLen;
        if (LongInterval.containsRaw(ys, ye, xs, xe)) {
            prefixLen = xs - ys;  suffixLen = ye - xe;
        } else {
            if (ys < xs) { prefixLen = xs - ys; suffixLen = 0; }
            else if (ye > xe) { suffixLen = ye - xe; prefixLen = 0; }
            else throw new WTF();
        }

        assert(prefixLen > 0 || suffixLen > 0);

//        int yr = Y.term().seqDur();
        long s, e;
        if (prefixLen >= suffixLen) {
            s = ys;   e = xs-1;
        } else {
            s = xe+1; e = ye;
        }

//        s = Tense.dither(s, ditherDT);
//        e = Tense.dither(e, ditherDT);

        if (e < s)
            return false; //impossible

        return separated(y, tasks, s, e);
    }

    private boolean separated(int y, NALTask[] tasks, long s, long e) {
        double[] evi = this.evi;

        NALTask y0 = tasks[y];
        double e0 = evi[y];

        NALTask y1 = new MySpecialOccurrenceTask(y0, s, e);
//        if (y1 != y0) {
            tasks[y] = y1.copyMeta(tasks[y]);
            computeComponents(tasks, y, y + 1, at, evi);

            if (!eviValid(evi[y])) {
                //fail; undo
                tasks[y] = y0;
                evi[y] = e0;
                return false;
            }
//        }

        return true;
    }


    /**
     * warning: time focus is not cleared
     */
    @Override
    public void clear() {
        super.clear();
        evi = null;
        term = null;
        //time(TIMELESS, TIMELESS); //HACK
        //dur(0);
    }


    /**
     * compacts both the items[] and evi[] cache, so that they remain in synch
     */
    @Override
    public final boolean removeNulls() {
        int sizeBefore = size;
        if (sizeBefore == 0)
            return false;

        double[] evi = this.evi;
        if (evi == null)
            return super.removeNulls(); //just remove inner nulls from the items[]

        NALTask[] items = this.items;
        int sizeAfter = sizeBefore;

        for (int k = 0; k < sizeBefore; k++) {
            if (items[k] == null || !eviValid(evi[k])) {
                sizeAfter--;
                evi[k] = 0;
                items[k] = null;
            }
        }
        if (sizeBefore == sizeAfter)
            return false; //no change
        else {
            _removeNulls(evi, items, sizeBefore, sizeAfter);
            this.size = sizeAfter;
            return true;
        }
    }

    private static void _removeNulls(double[] evi, NALTask[] items, int sizeBefore, int sizeAfter) {
        int sizeCurrent = sizeBefore;
        for (int i = 0; i < sizeCurrent - 1; ) {
            if (evi[i] == 0) {
                int span = (--sizeCurrent) - i;
                arraycopy(evi, i + 1, evi, i, span);
                arraycopy(items, i + 1, items, i, span);
            } else
                i++;
        }
        Arrays.fill(evi, sizeAfter, sizeBefore, 0);
        Arrays.fill(items, sizeAfter, sizeBefore, null);
    }


//    /**
//     * @noinspection RedundantCast - jdk14 compiler assertion error
//     */
//    private int firstValidOrNonNullIndex(int after) {
//        return indexOf(after,
//                evi != null ?
//                        (IntPredicate) this::valid :
//                        (IntPredicate) (i -> items[i] != null) /* first non-null */);
//    }

    public final TaskEviList add(NALTask... tasks) {
        return add(tasks.length, tasks);
    }

    public final <T extends Tasked> MutableTruthProjection add(int firstN, T[] tasks) {
        ensureCapacity(size() + firstN);
        for (int i = 0; i < firstN; i++)
            addFast(tasks[i]);
        return this;
    }

    private void addFast(Tasked t) {
        addFast(t.task());
    }

    public final TruthProjection add(Collection<? extends Tasked> tasks) {
        ensureCapacityForAdditional(tasks.size(), true);
        for (Tasked task : tasks)
            addFast(task);

        return this;
    }

    /** comparator by decreasing evidence */
    @Override public final int compare(int a, int b) {
        return a == b ? 0 : Double.compare(evi[b], evi[a]);
    }

    @Override
    public void swap(int a, int b) {
        if (a != b) {
            ArrayUtil.swap(items, a, b);
            ArrayUtil.swapDouble(evi, a, b);
        }
    }

    public MutableTruthProjection sizeMin(int n) {
        this.sizeMin = n;
        return this;
    }

    public MutableTruthProjection sizeMax(int n) {
        assert(n >= sizeMin);
        this.sizeMax = n;
        return this;
    }

    /**
     * task evidence leak dur
     */
    public final MutableTruthProjection dur(float dur) {
        at.dur(dur);
        return this;
    }

    private boolean intermpolate() {
        if (termsEqual()) {
            this.term(term(0));
            return true;
        }

        NALTask[] items = this.items;

        final int s = size; //assert(s >= 2);
        int remain = s;

        if (!preIntermpolate())
            return false;

        double[] evi = this.evi;

        int j = 0; //TODO try different J because J=0 may actually be the most incompatible
        Compound termCommon = (Compound) items[0].term();

        boolean nulled = false;
        for (int k = 1; k < s; k++) {

            Term _ab = Intermpolate.intermpolate(
                    termCommon, (Compound) items[k].term(),
                    (float) (evi[j] / (evi[j] + evi[k])), ditherDT);
            if (_ab.TASKABLE()) {
                Term ab = NALTask.taskTerm(_ab, punc(), false, true);

                if (ab!=null && termCommon.opID == ab.opID()) {
//                if (NAL.DEBUG) {
//                     && termCommon.volume()==ab.volume()
//                    if (!((ac == null ? (ac = termCommon.concept()) : ac).equals(ab.concept())))
//                        throw new TermTransformException("fault in intermpolation in TruthProjection", ac, ab.concept());
//                }
                    termCommon = (Compound) ab; //settle for hybrid result of first compatible pair
                    break;
                }
            }

            if (--remain < sizeMin) return false; //failure
            nullify(k); //remove incompatible
            nulled = true;
        }

        boolean eviChanged = false;
        if (NAL.truth.INTERMPOLATE_DISCOUNT_EVIDENCE) {

            if (remain > 1) {

                //discount evidence in proportion to dtDiff

                DTDiffer diffA = null;
                for (int i = 0; i < s; i++) {

                    NALTask task = items[i];
                    if (task == null) continue;

                    Compound term = (Compound) task.term();
                    if (term.equals(termCommon)) continue;

                    float discount = ((diffA == null) ? diffA = DTDiffer.the(termCommon) : diffA).factor(term);
                    if (discount < 1) {
                        Truth truthDiscounted = (discount > NAL.truth.EVI_MIN) ?
                                task.truth().eviMult(discount, NAL.truth.EVI_MIN) : null;

                        if (truthDiscounted == null) {
                            nullify(i);
                            if (--remain < sizeMin) return false;
                            nulled = true;
                        } else {
                            set(i, new MySpecialPuncTermAndTruthTask(termCommon, truthDiscounted, task));
                            eviChanged |= computeComponent(items, i, evi);
                        }
                    }
                }

            }
        }

        if (nulled)
            removeNulls();

        if (eviChanged && remain > 1)
            sort();

        term(termCommon);

        return size >= sizeMin;
    }

    public MutableTruthProjection term(@Nullable Term t) {
        this.term = t;
        return this;
    }


    /** check common ID's and convert bundled INH to conj proxy tasks */
    private boolean preIntermpolate() {
        int conj = 0;
        int s = size;
        MetalBitSet bundled = null;
        NALTask[] items = this.items;
        for (int i = 0; i < s; i++) {
            Term ii = items[i].term();
            int o = ii.opID();
            if (ConjBundle.bundled(ii)) {
                if (bundled==null) bundled = MetalBitSet.bits(s);
                bundled.set(i);
            } else if (o == CONJ.id)
                conj++;
        }

        if (conj > 0 && bundled != null) {
            //convert all bundledInh to proxy tasks
            for (int i = 0; i < s; i++) {
                if (bundled.test(i)) {
                    NALTask ii = items[i];
                    items[i] = new MySpecialTermTask(ii, unbundle(ii.term()));
                }
            }
        }

        //TODO false if not all same "op"
        return true;
    }

    private static Term unbundle(Term i) {
        TermList events = new TermList();
        ConjBundle.events(i, events::add);
        return new LightCompound(CONJ.id, events);
    }

    private boolean computeComponent(NALTask[] items, int i, double[] evi) {
        return computeComponent(items, i, at, evi);
    }

    private boolean computeComponent(NALTask[] items, int i, EviInterval at, double[] evi) {
        return computeComponents(items, i, i+1, at, evi);
    }

    @Override
    public final Lst<NALTask> sortThis() {
        sort();
        return this;
    }

    @Override
    public final void sort(Comparator<? super NALTask> comparator) {
        throw new UnsupportedOperationException("use sort()");
    }

    private boolean termsEqual() {
        int n = size;
        if (n < 2) return true;
        NALTask[] items = this.items;
        Predicate<Term> xEquals = items[0].term().equals();
        for (int i = 1; i < n; i++) {
            if (!xEquals.test(items[i].term()))
                return false;
        }
        return true;
    }

//	private void intermpolateRemove(IEntry ii) {
//		PeekableIntIterator ic = ii.id.getIntIterator();
//		while (ic.hasNext())
//			nullify(ic.next());
//	}

//	/**
//	 * if necessary, intermpolate a result term and reduce evidence by the dtDiff to each.
//	 * if intermpolation is impossible for any result, nullify that result.
//	 * return false if the projection becomes invalid
//	 */
//	private boolean intermpolate0(NAL nar, int minComponents) {
//
//		int thisSize;
//		main:
//		while ((thisSize = size()) >= 1) {
//			final int root = firstValidIndex();
//			Task rootComponent = get(root);
//			Term first = rootComponent.term();
//			this.term = first;
//			if (thisSize == 1 || !first.TEMPORALABLE()) {
//				//assumes that all the terms are from the same concept.  so if the first target has no temporal components the rest should not either.
//				return true;
//			}
//
//
//			MetalBitSet matchesFirst = MetalBitSet.bits(thisSize);
//			matchesFirst.set(root);
//			for (int i = firstValidIndex() + 1; i < thisSize; i++) {
//				Task t = this.get(i);
//				if (valid(i) && first.equals(t.term()))
//					matchesFirst.set(i);
//			}
//			int mc = matchesFirst.cardinality();
//			if (mc > 1) {
//				if (mc < thisSize) {
//					//HACK this is too greedy
//					//exact matches are present.  remove those which are not
//					for (int i = firstValidIndex() + 1; i < thisSize; i++)
//						if (!matchesFirst.get(i))
//							remove(i);
//				}
//				return true;
//			} else {
//
//
//				for (int next = root + 1; next < size; next++) {
//					int tryB = firstValidIndex(next);
//					if (tryB != -1) {
//						Task B = get(tryB);
//						Term a = first;
//						Term b = B.term();
//						final double e2Evi = evi[tryB];
//
//						float dtDiff;
//						//HACK this chooses the first available 2+-ary match, there may be better
//						if ((Float.isFinite(dtDiff = dtDiff(a, b)))) {
//
//							final double e1Evi = evi[root];
//
//							Term ab;
//							try {
//								//if there isnt more evidence for the primarily sought target, then just use those components
//								ab = Intermpolate.intermpolate((Compound) a, (Compound) b, (float) (e1Evi / (e1Evi + e2Evi)), nar);
//							} catch (TermTransformException e) {
//								//HACK TODO avoid needing to throw exception
//								if (NAL.DEBUG) {
//									throw new RuntimeException(e);
//								} else {
//									//ignore.  it may be a contradictory combination of events.
//									ab = Null;
//								}
//							}
//
//							if (Task.validTaskTerm(ab)) {
//
//								this.term = ab;
//								for (int i = 0; i < size; i++)
//									if (i != root && i != tryB) {
//										if (get(i) != null && !get(i).term().equals(ab))
//											remove(i);
//									}
//
//								//return 1 - dtDiff * 0.5f; //half discounted
//								//return 1 - dtDiff;
//								return true; //no discount for difference
//							}
//
//
//						}
//					}
//				}
//				//removeNulls(); //HACK
//				if (eviSum(i -> i != root) >= evi[root]) {
//					// if value of remainder > value(0):
//					remove(root);  //abdicate current root and continue remaining
//					continue main;
//				} else {
//					size = 1;
//					return true;
//				}
//
////            //last option: remove all except the first
////            removeNulls();
////
////            this.term = first;
////            return 1;
//
//
//			}
//		}
//		return true; //?
//	}

//	@Override
//	public Task remove(int index) {
//		throw new UnsupportedOperationException("use nullify");
//		//nullify(index);
//		//return null;
//	}

    @Override
    public void nullify(int index) {
        super.nullify(index);
        evi[index] = 0;
    }

    public final byte punc() {
        //if (isEmpty()) throw new RuntimeException();
        return items[0].punc();
    }


    public void print() {
        print(System.out);
    }


    public void print(PrintStream o) {
        forEachWith((t, oo) -> oo.println(t.proof()), o);
    }

    @Override
    public final long start() {
        return at.s;
    }

    @Override
    public final long end() {
        return at.e;
    }


    /**
     * aka "trimwrap", or "trim". use after filtering cyclic.
     * adjust start/end to better fit the (remaining) task components and minimize temporalizing truth dilution.
     * if the start/end has changed, then evidence for each will need recalculated
     * returns the number of active tasks
     *
     * @param all - true if applying to the entire set of tasks; false if applying only to those remaining active
     */
    private void focus(boolean trim) {

        int s = size; //assert (s > 0);

//		if (start!=ETERNAL && start!=TIMELESS && trim && anySatisfy(x->x.isEternal()))
//			trim = false; //disable triming any further

        boolean changed = false;
        long qsPrev = trim ? TIMELESS : this.start();
        if (trim || (qsPrev == ETERNAL || qsPrev == TIMELESS)) {

            long u0, u1;

            if (s > 1) {
                long[] union = this.unionInterval();
                u0 = union[0];
                u1 = union[1];
            } else {
                NALTask only = items[0];//evi != null ? items[0] : items[firstValidOrNonNullIndex(0)];
                u0 = only.start();
                u1 = only.end();
            }

            if (u0 == ETERNAL) {
                //eternal result
                changed = time(ETERNAL, ETERNAL);
            } else {
                //temporal result
                if (qsPrev == TIMELESS || qsPrev == ETERNAL) {
                    //override eternal range with the union
                    changed = time(u0, u1, ditherDT);
                } else {
                    //trim
                    long qePrev = this.end();
                    if ((qsPrev <= u0 && u1 <= qePrev) && (qsPrev != u0 && qePrev != u1))
                        changed = time(u0, u1, ditherDT);
                }
            }
        }

        if (changed || (evi == null))
            update();

    }

//	private boolean focusDur() {
//		float d = dur();
//		if (d < Prioritizable.EPSILON || start() == ETERNAL)
//			return false;
//
//		float nextDur = Math.min(d, durIdeal());
//		if (!Util.equals(nextDur, d, 0.25f)) {
//			dur(nextDur);
//			return true;
//		}
//		return false;
//
//	}

//	protected float durIdeal() {
//		long[] range = unionInterval();
//		return (float) ((range[1] - range[0]) * 1);
//
////		return (float)E.rangeIfNotEternalElse(0);
//
////		long minRange = minValue(x -> x.rangeIfNotEternalElse(TIMELESS));
////		return minRange!=TIMELESS ? minRange : Float.POSITIVE_INFINITY;
//	}

    public final float dur() {
        return at.dur;
    }

    private long[] unionInterval() {
        //Tense.union equivalent
//				long[] union = Tense.union(IntStream.range(0, s)
//					.filter(evi != null ? this::valid : this::nonNull)
//					.mapToObj(x -> (TaskRegion) items[x]).iterator());
//				u0 = union[0];
//				u1 = union[1];

        long u0 = Long.MAX_VALUE, u1 = Long.MIN_VALUE;
        NALTask[] items = this.items;
        boolean hasEvi = evi != null;
        int s = size();
        for (int i = 0; i < s; i++) {
            if (hasEvi ? valid(i) : nonNull(i)) {
                NALTask t = items[i];
                long ts = t.start();
                if (ts != ETERNAL) {
                    u0 = Math.min(u0, ts);
                    u1 = Math.max(u1, t.end());
                }
            }
        }

        if (u0 == Long.MAX_VALUE)
            u0 = u1 = ETERNAL; //all eternal

        return new long[] { u0, u1 };
    }


//    /**
//     * expects start=index 0, end = index 1
//     */
//    private boolean time(long[] se) {
//        return time(se[0], se[1]);
//    }



    /**
     * Truth Coherency Metric
     * inversely proportional to the statistical variance of the contained truth's frequency components
     * <p>
     * TODO refine, maybe weight by evi
     */
    @Research
    public double coherency() {
        int s = size;
        if (s == 0) return Double.NaN;


        NALTask[] items = this.items;
        int n = 0;
        double mean = 0;
        for (int i = 0; i < s; i++) {
            if (valid(i)) {
                mean += items[i].freq();
                n++;
            } else
                n--;
        }

        if (n <= 1)
            return 1;

        mean /= n;

        double variance = 0.0;
        for (int i = 0; i < s; i++) {
            if (valid(i))
                variance += sqr(items[i].freq() - mean);

        }
        variance /= n;

        return Math.max(0, 1 - variance);
    }


    /**
     * if not triming, the projection's current range is used raw.  otherwise it will be trimmed to fit the contained tasks.
     * does not force project a singular result.
     */
    @Nullable @Override public final NALTask task() {
        if (!commit())
            return null;

        int size = this.size; //  assert(size>0);
        if (size == 0)
            return null; //HACK shouldnt happen

        NALTask y;
        if (size == 1 && timeAuto)
            y = get(0);
        else
            y = task(term, this, this);

//        if (y!=null && size == 1) {
//            //same as only input?
//            NALTask first = get(0);
//            if (y!=first && first.equals(y))
//                return first;
//        }

        if (y!=null) {
            Term t = y.term();
            if (t instanceof LightCompound) {
                //create durable immutable representation
                Term u = t.op().the(t.dt(), t.subtermsDirect());
                assert(u.TASKABLE());
                y = SpecialTermTask.proxy(y, u).copyMeta(y);
            }
        }

        return y;
    }



//    public final @Nullable NALTask task(boolean trim) {
//        return task(trim, null);
//    }


    /**
     * sets the evidence min cut-off (for results, not components)
     */
    public final MutableTruthProjection eviMin(double eviMin) {
        this.eviMin = eviMin;
        return this;
    }


    public void addAll(Iterable<NALTask> i) {
        i.forEach(this::add);
    }

    public void addFirst(int n, Iterable<NALTask> i) {
        for (NALTask nalTask : i) {
            add(nalTask);
            if (--n <= 0) break;
        }
    }


    public Iterable<NALTask> sources() {
        //assertNoNulls();
        return Iterables.transform(this, MutableTruthProjection::unwrap);
    }

    /** unwraps to the source task */
    public static NALTask unwrap(NALTask x) {
        while (x instanceof TruthProjectionWrapper) {
            //noinspection CastConflictsWithInstanceof
            x = ((ProxyTask) x).task;
        }
        return x;
    }


    /** marker interface for wrapped task */
    private interface TruthProjectionWrapper { }

    private static final class MySpecialOccurrenceTask extends SpecialOccurrenceTask implements TruthProjectionWrapper{
        MySpecialOccurrenceTask(NALTask task, long start, long end) {
            super(task, start, end);
            copyMeta(task);
        }
    }

    private static final class MySpecialTermTask extends SpecialTermTask implements TruthProjectionWrapper{
        MySpecialTermTask(NALTask task, Term t) {
            super(task, t);
            copyMeta(task);
        }
    }

    private static final class MySpecialPuncTermAndTruthTask extends SpecialPuncTermAndTruthTask implements TruthProjectionWrapper {
        MySpecialPuncTermAndTruthTask(Compound a, @Nullable Truth truth, NALTask task) {
            super(a, truth, task);
            copyMeta(task);
        }
    }

    private final static class DominatorSorter implements IntComparator, IntIntProcedure {
        final int[] rowConflict, rowOrder;

        DominatorSorter(BitMatrixGraph g) {
            int s = g.size();
            rowConflict = new int[s];
            rowOrder = new int[s];
            for (int r = 0; r < s; r++) {
                rowConflict[r] = g.rowCardinality(r);
                rowOrder[r] = r;
            }
            QuickSort.quickSort(0, s, this, this);
        }

        @Override
        public int compare(int a, int b) {
            int ab = Integer.compare(rowConflict[b], rowConflict[a]); //descending
            if (ab != 0)
                return ab;
            else
                return Integer.compare(b, a); //descending
        }

        /** swapper */
        @Override public void value(int a, int b) {
            int o = rowOrder[b];
            rowOrder[b] = rowOrder[a];
            rowOrder[a] = o;

            int c = rowConflict[b];
            rowConflict[b] = rowConflict[a];
            rowConflict[a] = c;
        }
    }
}