package nars.truth.proj;

import com.google.common.collect.Iterators;
import nars.NAL;
import nars.task.NALTask;
import nars.time.Moment;
import nars.time.Tense;
import nars.time.When;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;

import static nars.truth.PreciseTruth.byEvi;

/**
 * lightweight wrapper around a single task result
 */
public final class SingletonTruthProjection extends When<NALTask> implements TruthProjection {

    private double eviMin = NAL.truth.EVI_MIN;

    public static SingletonTruthProjection the(NALTask x, long qs, long qe, int ditherDT, float dur) {
        if (qs == TIMELESS || qs == ETERNAL) {
            qs = x.start(); qe = x.end(); //AUTO
        }
        if (ditherDT > 1) {
            qs = Tense.dither(qs, ditherDT);
            qe = Tense.dither(qe, ditherDT);
        }
        return new SingletonTruthProjection(x, qs, qe, dur);
    }

    private SingletonTruthProjection(NALTask x, long qs, long qe, float dur) {
        super(x, qs, qe, dur);
    }

    public final SingletonTruthProjection eviMin(double eviMin) {
        this.eviMin = eviMin;
        return this;
    }

    @Override public void clear() {
        the(null);
    }

    @Override
    public Moment when() {
        return this;
    }


    @Override @Nullable public Truth truth() {
        if (s == ETERNAL) {
            assert(x.ETERNAL());
            return x.truth(); //TODO eviMin?
        } else {
            double evi = x.eviMean(this, 0);
            return evi < eviMin ?
                    null :
                    byEvi(x.freq(), evi);
        }
    }

    @Override
    public @Nullable NALTask task() {
        return (s == x.start() && e == x.end()) ?
                x :
                NALTask.projectAbsolute(x, s, e, dur, 0, eviMin);
    }

    @Override
    public Iterator<NALTask> iterator() {
        return Iterators.singletonIterator(x);
    }

    @Override
    public void delete() {
        /** nothing */
    }
}