package nars.truth;

import jcog.Util;
import nars.$;
import nars.NAL;
import nars.truth.func.TruthFunctions;


/**
 * truth rounded to a fixed size precision
 * to support hashing and equality testing
 * internally stores representation as one 32-bit integer
 */
public class DiscreteTruth extends Truth {

	/**
	 * truth component resolution corresponding to Param.TRUTH_EPSILON
	 */
	private final int hash;

	DiscreteTruth(float f, double c) {
		this(hash(f, c));
	}

	DiscreteTruth(int hash) {
		this.hash = hash;
	}

	public static DiscreteTruth the(int hash8) {
		if (shared==null)
			return _the(hash8);

		int index = hash8 % shared.length;
		DiscreteTruth t = shared[index];
		return t == null || t.hash != hash8 ?
		 	shared[index] = _the(hash8)
			:
			t;
	}

	private static DiscreteTruth _the(int hash8) {
		return new DiscreteTruth(hash8);
	}

	/**
	 * The hash code of a TruthValue, perfectly condensed,
	 * into the two 16-bit words of a 32-bit integer.
	 * <p>
	 * Since the same epsilon used in other truth
	 * resolution here (Truth components do not necessarily utilize the full
	 * resolution of a floating point value, and also do not necessarily
	 * need the full resolution of a 16bit number when discretized)
	 * the hash value can be used for equality comparisons
	 * as well as non-naturally ordered / non-lexicographic
	 * but deterministic compareTo() ordering.
	 * correct behavior of this requires epsilon
	 * large enough such that: 0 <= h < 2^15:
	 */
	private static int truthToInt(float freq, short freqDisc, double conf, short confDisc) {

		if (!Float.isFinite(freq) || freq < 0 || freq > 1)
			throw new TruthException("invalid freq", freq);

		if (!Double.isFinite(conf) || conf < 0)
			throw new TruthException("invalid conf", conf);

		if (conf > NAL.truth.CONF_MAX) {
			if (NAL.DEBUG)
				throw new TruthException("over-confident", conf);
			conf = NAL.truth.CONF_MAX; //max-out
		}

		int freqHash = Util.toInt(freq, freqDisc);
		int confHash = Util.toInt(conf, confDisc);
		return Util.shortToInt(freqHash, confHash);
	}

	private static int hash(float freq, double conf) {
		if (!Float.isFinite(freq) || freq < 0 || freq > 1)
			throw new TruthException("invalid freq", freq);

		int freqHash = Util.toInt(freq, hashDiscreteness);
		int confHash = Util.toInt(hashConf(conf), hashDiscreteness);
		return freqHash << 16 | confHash;
	}

	private static double hashConf(double conf) {

		if (!Double.isFinite(conf) || conf < 0)
			throw new TruthException("invalid conf", conf);

		if (conf > NAL.truth.CONF_MAX) {
			if (NAL.DEBUG)
				throw new TruthException("over-confident", conf);
			else
				return NAL.truth.CONF_MAX; //max-out
		}
		return conf;
	}

	public static int hash(Truthed truth) {
		return (truth instanceof DiscreteTruth ?
				(DiscreteTruth) truth :
				$.t(truth.freq(), truth.conf()))
			.hash;
	}

	@Override
	public float freq() {
		return Util.toFloat(Util.shortUpper(hash) /* & 0xffff*/, hashDiscreteness);
	}

	@Override
	public double conf() {
		return Util.toDouble(Util.shortLower(hash), hashDiscreteness);
	}

	@Override
	public double evi() {
		return TruthFunctions.c2e(conf());
	}

	@Override
	public String toString() {
		return _toString();
	}

	@Override
	public final boolean equals(Object that) {
		return
			this == that
				||
			(that instanceof DiscreteTruth ?
					hash == ((DiscreteTruth) that).hash :
				equals((Truth) that, NAL.truth.TRUTH_EPSILON));
	}

	@Override public final int hashCode() {
		return hash;
	}

	private static final int _hashDiscreteness =
			(int) Math.round(1.0 / NAL.truth.TRUTH_EPSILON);
	static { assert(_hashDiscreteness <= Short.MAX_VALUE); }
	public static final short hashDiscreteness = (short)_hashDiscreteness;

	private static final DiscreteTruth[] shared = hashDiscreteness <= 256 ?
		new DiscreteTruth[Util.sqr(hashDiscreteness)] : null;

}