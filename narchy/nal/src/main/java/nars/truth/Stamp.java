/*
 * Stamp.java
 *
 * Copyright (C) 2008  Pei Wang
 *
 * This file is part of Open-NARS.
 *
 * Open-NARS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Open-NARS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Pbulic License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Open-NARS.  If not, see <http:
 */
package nars.truth;

import jcog.Is;
import jcog.TODO;
import jcog.Util;
import jcog.WTF;
import jcog.data.bit.MetalBitSet;
import jcog.data.set.MetalLongSet;
import jcog.math.LongInterval;
import jcog.random.Rand;
import jcog.random.SplitMix64Random;
import jcog.util.ArrayUtil;
import nars.NAL;
import nars.Term;
import nars.task.NALTask;
import nars.truth.func.TruthFunctions;
import org.eclipse.collections.api.iterator.LongIterator;
import org.eclipse.collections.impl.list.mutable.primitive.LongArrayList;

import java.util.Arrays;
import java.util.function.IntFunction;
import java.util.function.IntSupplier;

public interface Stamp {


    /**
     * GATHER only
     */
    static boolean overlap(NALTask x, NALTask y) {
//        return x == y ||
//                (x.intersects((LongInterval) y) && overlapsAny(x.stamp(), y.stamp()));
        return x == y || x.stampOverlapping(y);
    }


    @Deprecated static long[] zip(Stamp A, Stamp B) {
        return zip(A.stamp(), B.stamp(), A, B);
    }

    @Deprecated static long[] zip(long[] a, long[] b, Object A, Object B) {
        return zip(a, b, ()->Util.hashCombine(A.hashCode(), B.hashCode()));
    }

    static long[] zip(long[] a, long[] b, IntSupplier sampleHash) {
//        float al = aToB * a.length;
//        float bl = (1 - aToB) * b.length;
//        float aToB_adjusted = (al / (al + bl));
        return zip(a, b, sampleHash, NAL.STAMP_CAPACITY);
    }

    /**
     * applies a fair, random-removal merge of input stamps
     */
    static long[] zip(long[] a, long[] b, IntSupplier sampleHash, int capacity) {
        int aa = a.length, bb = b.length;
        if (bb == 0 || (aa == bb && ArrayUtil.equals(a, b))) {
            if (aa > capacity) throw new TODO();
            return a;
        }
        if (aa == 0) {
            if (bb > capacity) throw new TODO();
            return b;
        }

        //count how much overlap; if the total unique is <= capacity, use mergeDuplicates
        int o = overlapCount(a, b);
        if (o > 0 && aa == o) return b;//A is contained entirely by B
        if (o > 0 && bb == o) return a;//B is contained entirely by A

        int abLen = aa + bb;
        abLen -= o;
        if (abLen <= capacity) {
            long[] y = o > 0 ? zipFlat(a, b, abLen) : zipDirect(a, b);
            return Util.maybeEqual(Util.maybeEqual(y, b), a);
        } else {
            return zipSample(capacity, i -> i == 0 ? a : b, sampleHash, 2, abLen);
        }
    }

//    private static long[] mergeShuffle(long[] a, long[] b, float aToB, int capacity, int abLength) {
//
//        byte[] aa = ArrayUtil.rangeBytes(a.length);
//        byte[] bb = ArrayUtil.rangeBytes(b.length);
//
//        int A = aa.length;
//        int B = bb.length;
//
//        RandomBits rng = new RandomBits(new XoRoShiRo128PlusRandom(Util.hashCombine(Arrays.hashCode(a), Arrays.hashCode(b)) /* TODO maybe expensive */));
//
//        //TODO test if b[x] is in a, and skip adding it to the bb[].  if A.size + B.size < abLength, just add to new long[] and return sorted
//        //long[] aa = a.clone();
//        if (A > 1) ArrayUtil.shuffle(aa, rng);
//
//        //long[] bb = b.clone();
//        if (B > 1) ArrayUtil.shuffle(bb, rng);
//
//        MetalLongSet AB = new MetalLongSet(abLength);
//        int s = 0;
//        do {
//            boolean which;
//            if (A == 0) which = false;
//            else if (B == 0) which = true;
//            else which = rng.nextBoolean(aToB);
//
//            int xs = which ? A : B;
//            if (AB.add((which ? a : b)[(which ? aa : bb)[--xs]]))
//                s++;
//            if (s == capacity)
//                break;
//            if (xs <= 0) {
//                if (which) {
//                    if (B == 0) break; //add remaining from b?
//                } else {
//                    if (A == 0) break; //add remaining from a?
//                }
//            }
//            if (which) A--; else B--;
//        } while (true);
//        return AB.toSortedArray();
//    }

    /**
     * TODO if inputs are ordered, this can be done without the binary search. just use original zip method and exclude duplicates
     */
    private static long[] zipFlat(long[] a, long[] b, int abLen) {
        LongArrayList l = new DisposeableLongArrayList(abLen);
        for (int i = 0; i < 2; i++) {
            for (long s : i == 0 ? a : b) {
                int x = l.binarySearch(s);
                if (x < 0)
                    l.addAtIndex(-x - 1, s);
            }
        }
        return l.toArray(); //already sorted
    }

    private static long[] zipDirect(long[] a, long[] b) {
        //simple case: direct sequential merge with no contention
        int aa = a.length, bb = b.length, abLength = aa + bb;
        long[] ab = new long[abLength];
        int ia = 0, ib = 0;
        for (int i = 0; i < abLength; i++) {
            long an = ia < aa ? a[ia] : Long.MAX_VALUE;
            long bn = ib < bb ? b[ib] : Long.MAX_VALUE;
            long abn;
            if (an < bn) {
                abn = an;
                ia++;
            } else {
                abn = bn;
                ib++;
            }
            ab[i] = abn;
        }
        return ab;
    }


    /***
     * zips two evidentialBase arrays into a new one
     * assumes a and b are already sorted in increasing order
     * the later-created task should be in 'b'
     */
    static long[] zip(long[] a, long[] b, float aToB, int maxLen, boolean newToOld) {
        if (Arrays.equals(a, b))
            return a;

        int aLen = a.length, bLen = b.length;

        int aMin = 0, bMin = 0;
        if (aLen + bLen > maxLen) {
            if (!newToOld)
                throw new UnsupportedOperationException("reverse weighted not yet unimplemented");

            if (!Float.isFinite(aToB)) {
                if (NAL.DEBUG)
                    throw new WTF();
                aToB = 0.5f; //HACK
            }

            if (aToB <= 0.5f) {
                int usedA = Math.max(1, (int) (aToB * (aLen + bLen)));
                if (usedA < aLen) {
                    if (bLen + usedA < maxLen)
                        usedA += maxLen - usedA - bLen;
                    aMin = Math.max(0, aLen - usedA);
                }
            } else /* aToB > 0.5f */ {
                int usedB = Math.max(1, (int) ((1f - aToB) * (aLen + bLen)));
                if (usedB < bLen) {
                    if (aLen + usedB < maxLen)
                        usedB += maxLen - usedB - aLen;
                    bMin = Math.max(0, bLen - usedB);
                }
            }

        }

        int baseLength = Math.min(aLen + bLen, maxLen);
        long[] c = new long[baseLength];
        if (newToOld) {

            int ib = bLen - 1, ia = aLen - 1;
            for (int i = baseLength - 1; i >= 0; ) {
                boolean ha = (ia >= aMin), hb = (ib >= bMin);


                long next;
                if (ha && hb) {
                    next = (i & 1) > 0 ? a[ia--] : b[ib--];
                } else if (ha) {
                    next = a[ia--];
                } else if (hb) {
                    next = b[ib--];
                } else {
                    throw new RuntimeException("stamp fault");
                }

                c[i--] = next;
            }
        } else {

            int ib = 0, ia = 0;
            for (int i = 0; i < baseLength; ) {

                boolean ha = ia < (aLen - aMin), hb = ib < (bLen - bMin);
                c[i++] = ((ha && hb) ?
                        ((i & 1) > 0) : ha) ?
                        a[ia++] : b[ib++];
            }
        }

        return toSetArray(c, maxLen);
    }

    static boolean validStamp(long[] stamp) {
        if (stamp.length > 1) {
            if (stamp.length > NAL.STAMP_CAPACITY)
                throw new WTF();
            for (int i = 1, stampLength = stamp.length; i < stampLength; i++) {
                if (stamp[i - 1] >= stamp[i])
                    return false; //out of order or duplicate
            }
        }
        return true;
    }

    /*
        @return
             Integer.MIN_VALUE no match
             0 a == b
            +1 a contains b
            -1 b contains a
     */
    static int equalsOrContains(long[] a, long[] b) {
        if (a == b) return 0;
        if (a.length == b.length) {
            return Arrays.equals(a, b) ? 0 : Integer.MIN_VALUE;
        } else {
            boolean swap = a.length < b.length;
            if (swap) {
                //swap
                long[] c = a;
                a = b;
                b = c;
            }
            nextB:
            for (long bb : b) {
                for (long aa : a) {
                    if (aa == bb)
                        break nextB; //found, try next B
                    if (aa > bb)
                        return Integer.MIN_VALUE; //exceeds value so bb not present in aa
                }
            }
            return swap ? -1 : +1;
        }
    }


    static long[] toSetArray(long[] x) {
        return toSetArray(x, x.length);
    }

    static long[] toSetArray(long[] x, int outputLen) {
        int l = x.length;
        return (l < 2) ? x : _toSetArray(outputLen, Arrays.copyOf(x, l));
    }

    static long[] _toSetArray(int outputLen, long[] sorted) {

        Arrays.sort(sorted);

        long lastValue = -1;
        int uniques = 0;

        for (long v : sorted) {
            if (lastValue != v)
                uniques++;
            lastValue = v;
        }

        if ((uniques == outputLen) && (sorted.length == outputLen))
            return sorted;

        int outSize = Math.min(uniques, outputLen);
        long[] dedupAndTrimmed = new long[outSize];
        int uniques2 = 0;
        long lastValue2 = -1;
        for (long v : sorted) {
            if (lastValue2 != v)
                dedupAndTrimmed[uniques2++] = v;
            if (uniques2 == outSize)
                break;
            lastValue2 = v;
        }
        return dedupAndTrimmed;
    }

    /**
     * true if there are any common elements;
     * assumes the arrays are sorted and contain no duplicates
     *
     * @param a evidence stamp in sorted order
     * @param b evidence stamp in sorted order
     * @noinspection ArrayEquality
     */
    static boolean overlapsAny(long[] a, long[] b) {

        int A = a.length;
        if (A == 0) return false;

        if (a == b) return true; //now test for identity after 0-length

        int B = b.length;
        if (B == 0) return false;

        return A == 1 && B == 1 ? a[0] == b[0] :
                LongInterval.intersectsRaw(a[0], a[A - 1], b[0], b[B - 1]) //intervals are completely disjoint
                        &&
                        overlapsExhaustive(a, b, A, B);
    }

    private static boolean overlapsExhaustive(long[] a, long[] b, int a2, int b2) {
        if (a2 > b2) {
            //swap to get the larger stamp in the inner loop
            long[] _a = a;
            a = b;
            b = _a;
        }

        /* TODO there may be additional ways to exit early from this loop */

        for (long x : a) {
            for (long y : b) {
                if (y > x)
                    break;
                else if (x == y)
                    return true;
            }
        }
        return false;
    }


    static long[] zip(int capacity, IntFunction<long[]> t, IntSupplier sampleHash, int n) {

        switch (n) {
            case 0:
                throw new NullPointerException();
            case 1:
                return t.apply(0);
            case 2:
                return zip(t.apply(0), t.apply(1), sampleHash, capacity);
            default:
                //max possible size estimate for the long hashset because it can be expensive
                int m = 0;
                for (int i1 = 0; i1 < n; i1++) m += t.apply(i1).length;

                return m > capacity ?
                        zipSample(capacity, t, sampleHash, n, m) :
                        maybeEqual(zipFlat(t, n, m), t, n);
        }


    }

    /**
     * try to find an existing equivalent stamp component for the output, and re-use it
     */
    private static long[] maybeEqual(long[] next, IntFunction<long[]> prevs, int n) {
        for (int i = 0; i < n; i++) {
            long[] prev = prevs.apply(i);
            if (ArrayUtil.equals(next, prev))
                return prev;
        }
        return next;
    }

    private static long[] zipFlat(IntFunction<long[]> t, int n, int len) {
        LongArrayList l = new DisposeableLongArrayList(len);
        for (int i = 0; i < n; i++) {
            for (long s : t.apply(i)) {
                int a = l.binarySearch(s);
                if (a < 0)
                    l.addAtIndex(-a - 1, s);
            }
        }
        return l.toArray(); //already sorted
    }

    private static long[] zipSample(int capacity, IntFunction<long[]> t, IntSupplier sampleHash, int n, int maxPossibleSize) {
        MetalLongSet evi = new MetalLongSet(maxPossibleSize);
        for (int i = 0; i < n; i++) evi.addAll(t.apply(i));
        //e.compact();

        int nab = evi.size();
        if (nab <= capacity)
            return maybeEqual(evi.toSortedArray(), t, n);
        else {
            return zipSample(capacity, sampleHash.getAsInt(), n, evi, nab);
        }
    }

    @Is({"Block_chain", "Cryptocurrency"}) private static long[] zipSample(int capacity, int sampleHash, int n, MetalLongSet evi, int nab) {
        int toRemove = nab - capacity;
        assert(toRemove>0);

        //RandomBits rng = new RandomBits(new XoRoShiRo128PlusRandom(sampleHash));
        Rand rng = new SplitMix64Random(sampleHash);

        //TODO special case: toRemove < nab/2, invert selection
        MetalBitSet skip = MetalBitSet.bits(nab);
        for (int i = 0; i < toRemove; i++) {
            int r = rng.nextInt(nab);
            while (skip.test(r)) {
                r++;
                if (r == nab) r = 0;
            }
            skip.set(n);
        }
        return zipSampleCollect(capacity, skip, evi.longIterator());
    }

    private static long[] zipSampleCollect(int capacity, MetalBitSet skip, LongIterator ee) {
        int j = 0, k = 0;
        long[] aa = new long[capacity];
        while (ee.hasNext()) {
            long l = ee.next();
            if (!skip.test(j++)) {
                aa[k++] = l;
                if (k == capacity) break;
            }
        }
        //assert(k==capacity);
        if (aa.length > 1)
            Arrays.sort(aa);
        return aa;
    }

    /**
     * the fraction of components in common divided by the total amount of unique components.
     * <p>
     * how much two stamps overlap can be used to estimate
     * the potential for information gain vs. redundancy.
     * <p>
     * == 0 if nothing in common, completely independent
     * >0 if there is at least one common component;
     * 1.0 if they are equal, or if one is entirely contained within the other
     * < 1.0 if they have some component in common
     * <p>
     * assumes the arrays are sorted and contain no duplicates
     */
    static float overlapFraction(long[] a, long[] b) {

        int al = a.length;
        int bl = b.length;

        if (al == 1 && bl == 1) {
            return (a[0] == b[0]) ? 1 : 0;
        }

        if (al > bl) {

            long[] ab = a;
            a = b;
            b = ab;
        }

        int common = overlapCount(a, b);
        if (common == 0)
            return 0f;

        int denom = Math.min(al, bl);
        assert (denom != 0);

        return Util.unitizeSafe(((float) common) / denom);
    }

    /**
     * assumes 'a' and 'b' are sorted
     */
    static int overlapCount(long[] a, long[] b) {
        if (a.length < b.length) {
            long[] c = a;
            a = b;
            b = c;
        }

        long aMin = a[0];
        long aMax = a[a.length - 1];
        //a is equal or longer
        int count = 0;
        for (long bb : b) {
            if (bb < aMin || bb > aMax) continue;

            //if (ArrayUtil.indexOf(a, bb)!=-1)
            if (Arrays.binarySearch(a, bb) >= 0)
                count++;
        }
        return count;
    }

    @Deprecated static long endOverlap(NALTask n) {
        return n.end();
//        final long e = n.end();
//        return e == ETERNAL ? ETERNAL : endOverlap(n.term(), e);
    }

    static long endOverlap(Term n, long e) {
        return e + n.seqDur();
    }






    /**
     * deduplicated and sorted (increasing) version of the evidentialBase.
     * this can always be calculated deterministically from the evidentialBAse
     * since it is the deduplicated and sorted form of it.
     */
    long[] stamp();

    /**
     * originality monotonically decreases with evidence length increase.
     * it must always be < 1 (never equal to one) due to its use in the or(conf, originality) ranking
     */
    default float originality() {
        return TruthFunctions.originality(stampLength());
    }

    default int stampLength() {
        return stamp().length;
    }


    final class DisposeableLongArrayList extends LongArrayList {
        DisposeableLongArrayList(int len) {
            super(len);
        }

        @Override
        public long[] toArray() {
            return size == items.length ?
                    items :
                    super.toArray();
        }
    }
}