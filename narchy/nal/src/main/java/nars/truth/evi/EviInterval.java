package nars.truth.evi;

import jcog.math.LongInterval;
import jcog.sort.FloatRank;
import nars.NAL;
import nars.task.NALTask;
import nars.time.Moment;
import nars.time.Tense;
import org.jetbrains.annotations.Nullable;

import static jcog.Util.lerpSafe;

/**
 * an Evidence Duration - describes a time range in which
 * multiple task's/truths can be reduced to onoe truth value
 *
 * computes evidence projections across time, and range integrals
 *
 * TODO extend When<?>
 */
public class EviInterval extends Moment implements FloatRank<NALTask> {

    public EviInterval(long s, long e) {
        when(s,e);
    }

    public EviInterval(long s, long e, float dur) {
        this(s, e);
        dur(dur);
    }

    @Deprecated protected EviInterval() {

    }

    /** integrated evidence using this Evidence envelope on provided Task */
    public final double eviInteg(NALTask t) {
        return eviInteg(t, 0, false);
    }

    private double eviInteg(NALTask t, float ete, boolean mean) {
        return factor(s, e, dur, t, ete, mean, NAL.truth.curve) * t.evi();
    }

    /** integrated evidence using this Evidence envelope on provided Task */
    public static double factor(long qs, long qe, float dur, LongInterval t, float ete, boolean meanOrSum, EviCurve curve) {
        double f = factor(qs, qe, dur, t, curve);
        long range = 1 + qe - qs;
        double sum = ete > 0 ? lerpSafe(ete, f, range) : f;
        return meanOrSum ? sum / range : sum;
    }

    /** computes the integral factor,
     *  result < 0..1 x (e-s+1)
     *  for a factor to evi() */
    private static double factor(long qs, long qe, float dur, LongInterval t, EviCurve curve) {
        long ts = t.start();
        return qs == qe ?
            factorPoint(qs, dur, t, curve, ts) :
            factorRange(qs, qe, dur, t, curve, ts);
    }

    private static double factorRange(long qs, long qe, float dur, LongInterval t, EviCurve curve, long ts) {
        long te;
        return (ts == ETERNAL || (qe == (te = t.end()) && qs == ts)) ?
            qe - qs + 1 //shortcut, assumes 1.0 internally
            :
            curve.integ(qs, qe, ts, te, dur);
    }

    private static double factorPoint(long q, float dur, LongInterval t, EviCurve curve, long ts) {
        long dt;
        if (ts == ETERNAL || q == ETERNAL ||
            (dt = LongInterval.minTimeToRaw(q, ts, t.end())) <= 0)
                return 1;
        else
            return dur <= 0 ? 0 : curve.project(dt, dur);
    }

    @Nullable public EviInterval cloneIfChanged(long s, long e, int ditherDT) {
        if (s != TIMELESS && s != ETERNAL && ditherDT > 1) {
            s = Tense.dither(s, ditherDT/*, -1*/);
            e = Tense.dither(e, ditherDT/*, +1*/);
        }
        return this.s != s || this.e != e ? new EviInterval(s, e, dur) : null;
    }


    public long range() {
        if (s == ETERNAL)
            throw new UnsupportedOperationException("eternal range()");
        return 1 + (e - s);
    }

    @Override
    public final float rank(NALTask x, float min) {
        double v = eviInteg(x);
        return v < NAL.truth.EVI_MIN ?
                Float.NaN :
                (float) v;
    }

    public final double eviInteg(NALTask t, float ete) {
        return eviInteg(t, ete, false);
    }

    public final double eviMean(NALTask t, float ete) {
        return eviInteg(t, ete, true);
    }
}

//    /** not necessarily any faster than exact.  of questionable value */
//    public double integrateTrapezoid(long ts, long te) {
//
//        if (max(qs, ts) > min(qe, te)) {
//            //DISJOINT - entirely before, or after //!LongInterval.intersectsRaw(ts, te, qs, qe)) {
//            return integrateTrapezoid2(qs, qe);
//        } else if (ts <= qs && te >= qe) {
//            //task equals or contains question
//            return integrateTrapezoid2(qs, qe);
//        } else if (qs >= ts /*&& qe > te*/) {
//            //question starts during and ends after task
//            //return integrateN(qs, te, Math.min(te + 1, qe), (te + qe) / 2, qe);
//            //return integrateN(qs, te, (te + qe) / 2, qe);
//            return integrateTrapezoid4(qs, te, min(te +1, qe), qe);
//        } else if (/*qs < ts &&*/ qe <= te) {
//            //question starts before task and ends during task
//            return integrateTrapezoid4(qs, max(qs, ts -1), ts, qe);
//        } else {
////			assert(qs <= ts && qe >= te);
//
//            //question surrounds task
//            return integrateTrapezoidN(
//                qs, max(qs, ts - 1),
//                ts, te,
//                min(te + 1, qe), qe);
//        }
//    }
//
//    public double integrateTrapezoid2(long a, long b) {
//        double aa = value(a);
//        return a == b ?
//            aa :
//            (((b - a) + 1) * (aa + value(b))) / 2;
//    }
//
//    public double integrate3(long a, long b, long c) {
//        if (a == b)
//            return integrateTrapezoid2(a, c);
//        else if (b == c)
//            return integrateTrapezoid2(a, b);
//
//        double aa = value(a), bb = value(b), cc = value(c);
//        double ab = (aa+bb), bc = (bb + cc);
//        return ((((b-a)+1) * ab) + (((c-b)+1) * bc))/2;
//    }
//
//    public double integrateTrapezoid4(long a, long b, long c, long d) {
//        if (a == b)
//            return integrate3(a, c, d);
//        if (b == c)
//            return integrate3(a, b, d);
//        if (c == d)
//            return integrate3(a, b, c);
//        double aa = value(a), bb = value(b), cc = value(c), dd = value(d);
//        double ab = (aa+bb), bc = (bb + cc), cd = (cc+dd);
//        return ((((b-a)+1) * ab) + (((c-b)+1) * bc) + (((d-c)+1) * cd))/2;
//    }
//
//    public double integrateTrapezoidN(long... x) {
//        double sum = Double.NaN;
//        long xPrev = x[0];
//        double yPrev = value(xPrev);
//        for (int i = 1, xLength = x.length; i < xLength; i++) {
//            long xNext = x[i];
//            if (xPrev != xNext) {
//                assert(xNext > xPrev);
//                double yNext = value(xNext);
//                sum = sampleTrapezoidally(xPrev, yPrev, xNext, yNext, sum);
//                yPrev = yNext;
//                xPrev = xNext;
//            }
//        }
//		//zero or 1 points
//		return sum == sum ? sum : yPrev;
//    }
//
//    private static double sampleTrapezoidally(long xPrev, double yPrev, long xNext, double yNext, double sum) {
//        Util.assertFinite(yNext);
//
//        if (yPrev==yPrev) {
//            //non-first value
//            if (xNext == xPrev)
//                return sum; //no effect, same point. assume same y-value
//            else {
////            if (xNext < xPrev)
////                throw new WTF("x must be monotonically increasing");
//
//                //accumulate sum
//                if (sum != sum)
//                    sum = 0; //initialize first summation
//                long dt = xNext - xPrev;
//                sum += (yNext + yPrev) / 2.0 * (dt + 1);
//            }
//        }
//
//        return sum;
//    }