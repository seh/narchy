package nars.truth;

import jcog.Util;
import nars.NAL;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.VarHandle;

import static nars.truth.func.TruthFunctions.c2e;

public class MutableTruth extends Truth {

	private static final VarHandle FREQ = Util.VAR(MutableTruth.class, "freq", float.class);
	@SuppressWarnings({"FieldMayBeFinal", "FieldCanBeLocal"})
	private volatile float freq;

	private static final VarHandle EVI = Util.VAR(MutableTruth.class, "evi", double.class);
	@SuppressWarnings({"FieldMayBeFinal", "FieldCanBeLocal"})
	private volatile double evi;

	public MutableTruth(float f, double evi) {
		evi(evi).freq(f);
	}

	public MutableTruth() {
		this(0.5f, 0);
	}

	public MutableTruth(Truthed t) {
		this(t.freq(), t.evi());
	}

	@Override
	public final float freq() {
		return (float)FREQ.getOpaque(this);
	}

	@Override
	public final double evi() {
		return (double)EVI.getOpaque(this);
	}

	public final MutableTruth freq(double f) {
		return freq((float)f);
	}
	
	public final MutableTruth freq(float f) {
		Util.assertUnitized(f);
		FREQ.setOpaque(this, f);
		return this;
	}
	public final MutableTruth evi(double e) {
		Util.assertFinite(e);
		if (e > NAL.truth.EVI_MAX)
			throw new TruthException("evi overflow", e);
		_evi(e);
		return this;
	}

	private void _evi(double e) {
		EVI.setOpaque(this, e);
	}

	public MutableTruth conf(double c) {
		evi(c2e(c));
		return this;
	}

	/** modifies this instance */
	public final MutableTruth negThis() {
		//assertIs();
		freq(freqNeg());
		return this;
	}

	private void assertIs() {
		if (!is())
			throw new RuntimeException("null truth");
	}

//	@Override
//	public final MutableTruth neg() {
//		//assertIs();
//		return new MutableTruth( freqNeg(), evi());
//	}

	public final MutableTruth set(@Nullable Truthed x) {
		if (this == x)
			return this;
		else
			return x != null ? evi(x.evi()).freq(x.freq()) : clear();
	}

	/*
	@Override
	public boolean equals(Object obj) {
		//throw new UnsupportedOperationException();
		return this == obj || (obj instanceof Truth && equalTruth((Truth)obj, NAL.truth.TRUTH_EPSILON));
	}
	*/

//	@Override
//	public final int hashCode() {
//		//throw new UnsupportedOperationException();
//		return System.identityHashCode(this);
//	}
//
//	@Override
//	public final boolean equals(Object x) {
//		//throw new UnsupportedOperationException();
//		return this == x; //no other equality allowed
////		return this == x || (
////			Util.equals(freq, ((Truth)x).freq(), NAL.truth.TRUTH_EPSILON)
////			&&
////			Util.equals(conf(), ((Truth)x).conf(), NAL.truth.TRUTH_EPSILON)
////		);
//	}

	public @Nullable Truth clone() {
		return new MutableTruth(freq(), evi());
	}

	public @Nullable Truth immutable() {
		return is() ? immutableUnsafe() : null;
	}

	public PreciseTruth immutableUnsafe() {
		return PreciseTruth.byEvi(freq(), evi());
	}


	/** sets to an invalid state */
	public MutableTruth clear() {
		_evi(0);
		return this;
	}

	/** whether this instance's state is set to a specific value (true), or clear (false) */
	public final boolean is() {
		//return evi() > NAL.truth.EVI_MIN;
		return evi() > Double.MIN_NORMAL;
	}

	@Override
	public String toString() {
		return _toString();
	}


	@Nullable public final Truth ifIs() {
		return is() ? this : null;
	}

	public boolean ditherTruth(NAL n, double eviMin) {

		double nextEvi = c2e(Truth.conf(conf(), n.confResolution.doubleValue()));
//		if (NAL.truth.EVI_STRICT) {
			if (nextEvi < eviMin) {
				clear();
				return false;
			}
//		} else {
//			nextEvi = Math.max(nextEvi, eviMin);
//		}

		evi(nextEvi);

		freq(Truth.freq(freq(), n.freqResolution.floatValue()));

		return true;
	}

	public MutableTruth setLerp(Truth x, float momentum) {
		if (this == x) return this;
		if (momentum <= 0 || !is())
			set(x);
		else {
			freqLerp(x.freq(), momentum);
			confLerp(x.conf(), momentum);
		}
		return this;
	}

	private void freqLerp(float freq, float momentum) {
		freq(Util.lerp(momentum, freq(), freq));
	}
	private void confLerp(double conf, float momentum) {
		conf(Util.lerp(momentum, conf(), conf));
	}
}