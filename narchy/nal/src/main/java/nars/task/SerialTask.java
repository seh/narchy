package nars.task;

import nars.Term;
import nars.task.proxy.SpecialOccurrenceTask;
import nars.truth.MutableTruth;
import nars.truth.Truth;

/**
 * overrides equality and hashcode impl because these are generated with (mostly-)unique serial stamp by a sensor or other internal process #
 */
public class SerialTask extends AbstractNALTask {

	private volatile long start, end;

	/**
	 * @param stamp typically a 1-element stamp
	 */
	public SerialTask(Term term, byte punc, Truth value, long start, long end, long[] stamp) {
		super(term, punc, value, start, end, stamp);

		//assert(stamp.length == 1): SerialTask.class + " requires stamp of length 1 so it can be considered an Input Task and thus have consistent hashing even while its occurrrence time is stretched";

		this.start = start;
		this.end = end;
	}

	@Override
	public NALTask why(Term otherWhy) {
		if (this.why == null)
			this.why = otherWhy; //accept an initial cause only
		return this;
	}

	@Override public final boolean equals(Object x) { return x==this; }

	@Override
	protected int hashCalculate(long start, long end, long[] stamp) {
		//return Util.hashCombine(term.hashCode(), stamp[0]);
		//return Long.hashCode(stamp[0]);
		return System.identityHashCode(this);
	}

	public final SerialTask occ(long s, long e) {
		if (e < s)
			throw new UnsupportedOperationException();

		//special handling to avoid reverse ranged in between the two set's
		long s0 = start, e0 = end;
		long sMin = Math.min(s0, s);
		long eMax = Math.max(e0, e);

		//expand
		start = sMin; end = eMax;

		//fence?

		//then shrink, if necessary
		start = s; end = e;

		return this;
	}

	public final void setStart(long s) {
		if (end < s) throw new UnsupportedOperationException();
		this.start = s;
	}

	public final void setEnd(long e) {
		if (start > e) throw new UnsupportedOperationException();
		this.end = e;
	}

	@Override
	public final long start() {
		return start;
	}

	@Override
	public final long end() {
		return end;
	}

	/** caution */
	public final SerialTask set(long start, long end, float f, double evi, float pri) {
		//x.delete();
		setUncreated(); //triggers novelty refresh
		occ(start, end);
		((MutableTruth)truth()).freq(f).evi(evi);
		pri(pri);
		return this;
	}

	/** returns an immutable snapshot of this task to prevent the effects of this potentially being stretched */
	public NALTask freeze() {
		return new SpecialOccurrenceTask(this, start, end).copyMeta(this);
	}
}