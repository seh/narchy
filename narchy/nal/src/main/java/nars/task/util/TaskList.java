package nars.task.util;

import jcog.TODO;
import jcog.Util;
import jcog.data.list.Lst;
import jcog.math.LongInterval;
import jcog.math.LongIntervalArray;
import nars.Task;
import nars.Term;
import nars.task.NALTask;
import nars.truth.Stamp;
import nars.truth.Truth;
import org.eclipse.collections.api.block.function.primitive.IntToFloatFunction;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Supplier;

import static nars.NAL.STAMP_CAPACITY;

/**
 * A List of Task's which can be used for various purposes, including dynamic truth and evidence calculations (as utility methods)
 */
public class TaskList extends Lst<NALTask> implements TaskRegion {


    public TaskList() {
        super(0);
    }

    public TaskList(int initialCap) {
        super(0, initialCap > 0 ? new NALTask[initialCap] : NALTask.EmptyNALTaskArray);
    }

    public TaskList(NALTask[] t, int n) {
        super(n, t);
    }

    public TaskList(Collection<NALTask> t) {
        super(0, new NALTask[t.size()]);
        for (NALTask task : t)
            addFast(task);
    }

//    public TaskList(Iterable<NALTask> t, int sizeEstimate) {
//        super(0, new NALTask[sizeEstimate]);
//        for (NALTask task : t)
//            add(task);
//    }

    protected class EviPrioritizer implements IntToFloatFunction {
        private double eviSum = Double.NaN;

        public long creation() {
            int n = size();
            long t = Util.max(nalTask -> {
                long w = nalTask.creation();
                return w == TIMELESS ? Long.MIN_VALUE : w; //HACK
            }, items, n);

            if (t == Long.MIN_VALUE)
                t = TIMELESS; //HACK

            return t;
        }

        @Override public float valueOf(int i) {
            double eviSum = this.eviSum;
            if (eviSum!=eviSum) this.eviSum = eviSum = eviSum();
            return eviSum < Double.MIN_NORMAL ? 0 :
                (float) (pri(i) * evi(i) / eviSum);
        }

        public float pri(int i) { return items[i].priElseZero(); }
        public double evi(int i) {
            return items[i].evi();
        }

        private double eviSum() {
            int n = size();
            double s = 0;
            for (int i = 0; i < n; i++)
                s += evi(i);
            return s;
        }
    }

    public final NALTask task(Term x, LongIntervalArray occ, TruthComputer t) {
        NALTask y = NALTask.task(x, items[0].punc(), occ, t, this.stampZip(STAMP_CAPACITY));
        if (y != null && y.uncreated())
            fund(y, new EviPrioritizer());
        return y;
    }

    /** weighted average of priorities by the contributed evidence */
    public void fund(NALTask y, IntToFloatFunction pri) {
        double p = 0;
        NALTask[] ii = arrayCommit();
        for (int i = 0, iiLength = ii.length; i < iiLength; i++)
            p += pri.valueOf(i);

        Task.priWhy(y, p, ii);
        if (pri instanceof EviPrioritizer ePri)
            y.setCreation(ePri.creation());
    }

    private NALTask[] arrayCommit() {
        int s = size;
        if (s == 0)
            return NALTask.EmptyNALTaskArray;

        NALTask[] items = this.items;
        if (s == items.length)
            return items;
        else
            return this.items = Arrays.copyOf(items, s);
    }

    @Override
    public long start() {

        long start = longify((m, t) ->{
            long s = t.start();
            return s != ETERNAL && s < m ? s : m;
        }, TIMELESS);

		return start == TIMELESS ? ETERNAL : start;
    }

    @Override
    public long end() {
        return maxValue(LongInterval::end);
    }

//    @Override
//    @Nullable
//    public short[] why() {
//        return CauseMerge.AppendUnique.merge(NAL.causeCapacity.intValue(),
//                Util.map(0, size(), short[][]::new, x -> get(x).why()));
//    }

    @Override
    public float freqMin() {
        throw new TODO();
    }

    @Override
    public float freqMax() {
        throw new TODO();
    }

    @Override
    public float confMin() {
        throw new TODO();
    }

    @Override
    public float confMax() {
        throw new TODO();
    }

    @Override
    public @Nullable NALTask task() {
        throw new UnsupportedOperationException();
    }

    @Override
    public final boolean addAll(Collection<? extends NALTask> source) {
        for (NALTask task : source)
            add(task);
        return true;
    }

    private Supplier<long[]> stampZip(int capacity) {
        int ss = size;
        return ss == 1 ?
            this::stampFirst
            :
            ()->stampZipN(capacity);
    }

    private long[] stampZipN(int capacity) {
        return Stamp.zip(capacity, this::stamp, this::hashCode, size); //calculate stamp after filtering and after intermpolation filtering
    }

    @Nullable private long[] stampFirst() {
        return stamp(0);
    }

    @Nullable public final long[] stamp(int component) {
        NALTask t = items[component];
        return t!=null ? t.stamp() : null;
    }

    public final Term term(int i) {
        return items[i].term();
    }

    @Nullable public Truth taskTruth(int i) {
        return items[i].truth();
    }

    /** this impl returns mean evi of a task */
    public double eviMean(int i) {
        return items[i].evi();
    }


    /**
     * test for whether an amount of evidence is valid
     */
    protected static boolean eviValid(double e) {
        return e > Double.MIN_NORMAL;
    }



    /** calculate the minimum range (ie. intersection of the ranges) */
    public long rangeIntersection() {
        long l = minValue(t -> t.rangeElse(Long.MAX_VALUE));
        return l == Long.MAX_VALUE ?
                0 :
                l - 1;
    }
    /**
     * latest start time, excluding ETERNALs.  returns ETERNAL if all ETERNAL
     */
    public long latestStart() {
        return maxValue(LongInterval::start);
    }

    public long latestEnd() {
        return maxValue(LongInterval::end);
    }
//    /** occurrence end + term's sequence range */
//    public long latestEndWithRange() {
//        return maxValue((NALTask z) ->
//            z.end() + (z.term().seqDur()));
//    }
    public long latestStartPlusRange() {
        return maxValue((NALTask z) ->
                z.start() + z.term().seqDur());
    }


//    public double eviSum(@Nullable IntPredicate each) {
//        double e = 0;
//        int n = size;
//        for (int i = 0; i < n; i++) {
//            if (each == null || each.test(i)) {
//                double ce = evi(i);
//                if (eviValid(ce))
//                    e += ce;
//            }
//        }
//        return e;
//    }

    //    protected float pri(long start, long end) {
//
//        //TODO maybe instead of just range use evi integration
//
//
//        if (start == ETERNAL) {
//            //TODO if any sub-tasks are non-eternal, maybe combine in proportion to their relative range / evidence
//            return reapply(DynTruth::pri, Param.DerivationPri);
//        } else {
//
//
//            double range = (end - start) + 1;
//
//            return reapply(sub -> {
//                float subPri = DynTruth.pri(sub);
//                long ss = sub.start();
//                double pct = ss!=ETERNAL ? (1.0 + Longerval.intersectLength(ss, sub.end(), start, end))/range : 1;
//                return (float) (subPri * pct);
//            }, Param.DerivationPri);
//
//        }
//    }
}