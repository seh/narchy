package nars.task;

import nars.Task;
import nars.Term;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

/** implementations are immutable but will usually have a different hash and
  * equality than the origin task. hashcode here is calculated lazily.
 * the only mutable components are the hashcode and the cyclic status which is optionally inherited from the source.
 * */
public class ProxyTask extends NALTask {

    public final NALTask task;

    private int hash;

    protected ProxyTask(NALTask task) {


//        if (task instanceof ProxyTask) {
//            //System.out.println(task.getClass() + " may be unwrapped for " + getClass());
//            throw new TaskException(task.getClass() + " -> double proxy -> " + getClass(), task);
//        }
        this.task = task;
        this.why = task.why();

//        float p = task.pri();
//        if (p!=p) {
//            if (NAL.DELETE_PROXY_TASK_TO_DELETED_TASK)
//                pri.delete();
//            else
//                pri.pri(Prioritizable.EPSILON);
//        } else
//            pri.pri(p);

    }

//    protected boolean preValidated() {
//        return true;
//    }

    @Override
    public float freq(long start, long end) {
        return task.freq(start, end);
    }

    @Override
    public String toString() {
        return appendTo(null).toString();
    }



    @Override
    public Term term() {
        return task.term();
    }


    /** for hash consistency, this assumes that the involved values will not change after the initial calculation.
     * if values will change, call invalidate() to reset hash
     * */
    @Override public final int hashCode() {
        int h = this.hash;
        return h != 0 ? h :
            (this.hash = Task.hash(term(), truth(), punc(), start(), end(), stamp()));
    }
    protected void invalidate() {
        this.hash = 0;
    }

    @Override
    public long start() {
        return task.start();
    }

    @Override
    public long end() {
        return task.end();
    }

    @Override
    public long[] stamp() {
        return task.stamp();
    }

    @Override
    public @Nullable Truth truth() {
        return task.truth();
    }

    @Override
    public byte punc() {
        return task.punc();
    }



}