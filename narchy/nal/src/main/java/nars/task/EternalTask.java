package nars.task;

import jcog.math.LongInterval;
import nars.Term;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

public class EternalTask extends AbstractNALTask {

	EternalTask(Term term, byte punc, @Nullable Truth truth, long[] stamp) {
		super(term, punc, _truth(truth), LongInterval.ETERNAL, LongInterval.ETERNAL, stamp);
	}

    @Override
    public final long start() {
        return LongInterval.ETERNAL;
    }

    @Override
    public final long end() {
        return LongInterval.ETERNAL;
    }

}