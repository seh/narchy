package nars.task;

import jcog.io.BinTxt;
import jcog.math.LongInterval;
import jcog.math.LongIntervalArray;
import jcog.pri.op.PriMerge;
import jcog.tree.rtree.HyperRegion;
import jcog.util.PriReturn;
import nars.NAL;
import nars.Op;
import nars.Task;
import nars.Term;
import nars.control.Why;
import nars.task.proxy.SpecialTruthAndOccurrenceTask;
import nars.task.util.*;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Termed;
import nars.term.Termlike;
import nars.term.util.Image;
import nars.term.util.TermException;
import nars.term.util.TermTransformException;
import nars.time.Tense;
import nars.truth.*;
import nars.truth.evi.EviInterval;
import nars.truth.func.TruthFunctions;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.function.Supplier;

import static nars.Op.*;


/**
 * base class for non-axiomatic logic tasks
 */
public abstract class NALTask extends Task implements Truthed, Stamp, TaskRegion {

    Term why;

    /**
     * although this field is named creation,
     * it is used to store the last re-activation time for novelty filtering.
     * begins TIMELESS to signal an un-initialized state
     */
    private long creation = TIMELESS; //TODO protected or private

    public static NALTask taskUnsafe(Term term, byte punc, @Nullable Truth truth, long start, long end, long[] evidence) {
        if (term instanceof Neg) {
            term = term.unneg();
            if (truth != null) truth = truth.neg();
        }

        return start == ETERNAL ?
            new EternalTask(term, punc, truth, evidence) :
            new TemporalTask(term, punc, truth, start, end, evidence);
    }


    public static NALTask task(Term x, byte punc, @Nullable Truth truth, LongInterval occ, long[] evidence) {
        return task(x, punc, truth, occ.start(), occ.end(), evidence);
    }

    public static NALTask task(Term x, byte punc, @Nullable Truth truth, long start, long end, long[] evidence) {
        Term term = taskTerm(x, punc, true, false);

        taskValidOcc(start, end, term, false);

        taskValidTruth(truth, term, punc, false);

        taskValidStamp(evidence, term);

        return taskUnsafe(term, punc, truth, start, end, evidence);
    }

    /**
     * safe, lazy procedure for potentially-failing task constructions
     */
    @Nullable
    public static NALTask task(Term x, byte punc, LongIntervalArray occ, TruthComputer t, Supplier<long[]> stamp) {
        Term y = taskTerm(x, punc, true, true);
        if (y == null) return null;

        Truth T = t.computeTruth();
        if (!taskValidTruth(T, y, punc, true)) return null;

        long[] se = occ.startEndArray();
        if (se == null) return null;
        long start = se[0], end = se[1];
        if (!taskValidOcc(start, end, y, false)) return null;

        long[] e = stamp.get();
        taskValidStamp(e, y);

        return taskUnsafe(y, punc, T, start, end, e);
    }

    private static boolean taskValidOcc(long start, long end, Term x, boolean safe) {
        boolean startEternal = start == ETERNAL;

        if (!startEternal && end - start > NAL.belief.TASK_RANGE_LIMIT)
            return fail(x, "excessive range: " + (end - start), safe);

        if ((startEternal && end != ETERNAL) ||
                (start > end) ||
                (start == TIMELESS) || (end == TIMELESS))
            return fail(x, "start=" + start + ", end=" + end + " is invalid task occurrence time", safe);

        return true;
    }



    private static boolean taskValidTruth(@Nullable Truth tr, Term x, byte punc, boolean safe) {

        boolean beliefOrGoal = (punc == BELIEF) || (punc == GOAL);
        if (beliefOrGoal == (tr == null))
            return fail(x, "truth/punc mismatch", safe);

//        if (truth!=null && truth.conf() < NAL.truth.TRUTH_EPSILON)
//            throw new Truth.TruthException("evidence underflow: conf=", truth.conf());

        if (beliefOrGoal && tr.evi() < NAL.truth.EVI_MIN_safe)
            return fail(x, "evidence underflow", safe);

        return true;
    }

    private static void taskValidStamp(long[] evidence, Term x) {
        if (NAL.test.DEBUG_EXTRA)
            if (!Stamp.validStamp(evidence))
                throw new TaskException("invalid stamp: " + Arrays.toString(evidence), x);
    }

    public static NALTask taskEternal(Term x, byte punc, @Nullable Truth t, NAL n) {
        return task(x, punc, t, ETERNAL, ETERNAL, n.evidence());
    }


    /** complete clone to immutable instance */
    public static NALTask clone(NALTask x) {
        return clone(x, x.term(), x.truth(), x.punc());
    }

    private static NALTask clone(NALTask x, Term newContent, Truth newTruth, byte punc) {
        return clone(x, newContent, newTruth, punc, x.start(), x.end());
    }

    private static NALTask clone(NALTask x, Term newContent, Truth newTruth, byte punc, long start, long end) {
        return clone(x, newContent, newTruth, punc, start, end, x.stamp(), true);
    }

    public static NALTask clone(NALTask x, Term newContent, Truth newTruth, byte punc, long start, long end, long[] stamp, boolean safe) {

        //Term c = Task.taskValid(newContent, newPunc, newTruth, safe);
        //return c==null ? null :
        NALTask t = safe ?
                task(newContent, punc, newTruth,  /* HACK */ start, end, stamp) :
                taskUnsafe(newContent, punc, newTruth,  /* HACK */ start, end, stamp);

        t.why(x.why());
        t.pri(x.pri());
        return t;

//        if (x.target().equals(y.target()) && x.isCyclic())
//            y.setCyclic(true);
    }

    @Nullable
    public static NALTask eternalize(NALTask x, float eviRate, double eviMin) {
        if (x.ETERNAL())
            return x;

        Truth tt = x.truth();
        double tre = tt.eviEternalized(eviRate);
        return tre < eviMin ? null :
            SpecialTruthAndOccurrenceTask.proxy(x, ETERNAL, ETERNAL,
                PreciseTruth.byEvi(tt.freq(), tre));
    }

//    /**
//     * assumes start!=ETERNAL
//     * TODO boolean eternalize=false
//     */
//    public static @Nullable NALTask projectRelative(NALTask x, long s, long e, float dur, long now, float ete, double eviMin) {
//        return project(x, s, e, (t, S, E) -> t.truthRelative(S, E, dur, now, ete, eviMin));
//    }

    public static @Nullable NALTask projectAbsolute(NALTask x, long s, long e, float dur, float ete, double eviMin) {
        long xs = x.start();
        if (s == xs && e == x.end())
            return x; //same occurrence; no change necessary

        boolean question = !x.BELIEF_OR_GOAL();
        Truth tt;
        if (question) {
            tt = null;
        } else {
            tt = x.truth(s, e, dur, ete, eviMin);
            if (tt == null)
                return null;
        }

        NALTask y = SpecialTruthAndOccurrenceTask.proxy(x, s, e, tt).copyMeta(x);
//        if (!question) {
//            double pri = tt.conf() / x.conf();
////			p = tt.evi() / x.evi();
//            y.priMul((float) pri);
//        }
        return y;
    }

    public static float merge(NALTask e, NALTask i, PriMerge merge, @Nullable PriReturn returning) {
        if (e == i)
            return 0;

        //non-serial task, or proxy to non-serial task
        if (NAL.truth.ABSORB_ON_TASK_MERGE && e.BELIEF_OR_GOAL() && e.truth() instanceof PreciseTruth ep)
            ep.absorb(i.truth());

        mergeWhy(e, i);

        //use the latest creation time
        long iCreation = i.creation();
        if (iCreation != TIMELESS && iCreation > e.creation())
            e.setCreation(iCreation);

        return merge.apply(e, i.pri(), returning);
    }

//	/**
//	 * creates negated proxy of a task
//	 */
//	public static NALTask negated(NALTask x) {
//		return x instanceof SpecialNegatedTask ? ((SpecialNegatedTask) x).task : new SpecialNegatedTask(x).withPri(x);
//	}

    /**
     * with most common defaults
     */
    public static void merge(NALTask pp, NALTask tt) {
        merge(pp, tt, NAL.taskPriMerge);
    }

    private static void merge(NALTask pp, NALTask tt, PriMerge merge) {
        merge(pp, tt, merge, null);
    }

    private static void mergeWhy(NALTask e, NALTask i) {
        Term iw = i.why();
        if (iw != null)
            e.why(iw);
        //else TODO
    }


//	@Nullable public static NALTask negIf(@Nullable NALTask x, boolean negate) {
//		return negate && x!=null ? negated(x) : x;
//	}

    //    /** TODO dither? */
//    @Deprecated private static Truth revise(Truth te, long re, Truth ti, long ri) {
//        double Ee = te.evi() * (1+re), Ei = ti.evi() * (1+ri);
//        double e2i = Ei/(Ei+Ee);
//        double evi = Util.lerpSafe(e2i, Ee, Ei);
//        return PreciseTruth.byEvi(Util.lerpSafe(e2i, te.freq(), ti.freq()), evi);
//    }

    public static Term taskTerm(Term x, byte punc) {
        return taskTerm(x, punc, true, !NAL.test.DEBUG_EXTRA);
    }

    /**
     * validates and prepares a term for use as a task's content
     */
    @Nullable public static Term taskTerm(Term x, byte punc, boolean imageNormalize, boolean safe) {

        boolean negated;
        Term yu;
        if (x instanceof Compound) {
            negated = x instanceof Neg;
            Term xu = negated ? x.unneg() : x;

            if (!validTaskTermPre(xu, safe))
                return null; //basic enough to check before normalize

            yu = (imageNormalize ? Image.imageNormalize(xu) : xu).normalize();

            if (yu instanceof Neg)
                throw new TermTransformException("normalization negation", xu, yu);

        } else {
            yu = x;
            negated = false;
        }

        return TASKS(yu, punc, safe) ? yu.negIf(negated) : null;
    }

    /** validity as a task's term */
    public static boolean TASKS(@Nullable Term t) {
        return TASKS(t, (byte) 0);
    }

    /** validity as a particular punctuation task's term */
    public static boolean TASKS(@Nullable Term t, byte punc) {
        return TASKS(t, punc, true);
    }

    public static boolean TASKS(@Nullable Term t, byte punc, boolean safe) {

        boolean x = validTaskTermPre(t, safe);
        if (!x)
            return false;

        if (punc != COMMAND && (t instanceof Compound && !((Compound) t).NORMALIZED())) {
//
//                @Nullable Term n = t.normalize();
//                if (!n.equals(t))
            return fail(t, "task target not a normalized Compound", safe);
//                else
//                    t = n;
        }

        if (t.hasAny(VAR_PATTERN))
            return fail(t, "pattern variables", safe);

        if (!NAL.ABSTRACT_TASKS_ALLOWED && !t.hasAny(AtomicConstant))
            return fail(t, "excessively abstract", safe);

        if (punc == BELIEF || punc == GOAL) {
            if (t.hasVarQuery())
                return fail(t, "belief/goal query variable", safe);
            if (t.TEMPORAL() && t.TEMPORAL_VAR())
                return fail(t, "belief/goal dt=XTERNAL", safe);
        }

        if ((punc == GOAL || punc == QUEST) && t.IMPL() /*hasAny(IMPL)*/)
            return fail(t, "goal/quest implication", safe);

        return ValidIndepBalance.valid(t, safe);
    }

    private static boolean validTaskTermPre(@Nullable Term t, boolean safe) {
        if (t == null)
            return fail(null, "null", safe);

        if (!t.TASKABLE())
            return fail(t, "taskable", safe);

        return true;
    }

    public static boolean fail(@Nullable Object t, String reason, boolean safe) {
        if (safe)
            return false;
        else
            throw t instanceof Termed ?
                new TaskException(reason, (Termed) t) :
                new TermException(reason, (Termlike) t);
    }

    public static void copyMeta(NALTask to, NALTask from) {
        if (from == to) return;
        if (to instanceof SerialTask)
            return;

        copyPri(to, from);
        copyWhy(to, from);
    }

    private static void copyWhy(NALTask to, NALTask from) {
        to.why(from.why());
    }

    private static void copyPri(NALTask to, NALTask from) {
        to.withPri(from);
    }

    /**
     * misc verification tests which are usually disabled
     */
    public static void verify(NALTask x, NAL nar) {

        if (NAL.test.DEBUG_ENSURE_DITHERED_TRUTH && x.BELIEF_OR_GOAL())
            Truth.assertDithered(x.truth(), nar);

        if (NAL.test.DEBUG_ENSURE_DITHERED_DT || NAL.test.DEBUG_ENSURE_DITHERED_OCCURRENCE) {
            int d = nar.dtDither();
            if (d > 1) {
//                if (!x.isInput()) {
                if (NAL.test.DEBUG_ENSURE_DITHERED_DT)
                    Tense.assertDithered(x.term(), d);
                if (NAL.test.DEBUG_ENSURE_DITHERED_OCCURRENCE)
                    Tense.assertDithered(x, d);
//                }
            }
        }
    }

    /**
     * punc -> index, @see p(punc)
     */
    public static byte i(byte punc) {
        return (byte) switch (punc) {
            case BELIEF -> 0;
            case QUESTION -> 1;
            case GOAL -> 2;
            case QUEST -> 3;
            default -> -1;
        };
    }

    /** index -> punc, @see i(index) */
	public static byte p(int index) {
		return switch (index) {
			case 0 -> BELIEF;
			case 1 -> QUESTION;
			case 2 -> GOAL;
			case 3 -> QUEST;
			default -> (byte)-1;
		};
	}
    public static boolean same(NALTask x, NALTask y, NAL n) {
        return same(x, y.term(), y.punc(), y.start(), y.end(), y.truth(), true, n);
    }


    public static boolean same(NALTask x, Term y, byte yPunc, MutableTruthInterval yTruth, boolean testTruth, NAL n) {
        return same(x, y, yPunc, yTruth.s, yTruth.e, yTruth, testTruth, n);
    }

    /**
     *  @param x existing task
     * @param y new task term
     */
    public static boolean same(NALTask x, Term y, byte yPunc, long ys, long ye, Truth yt, boolean testTruth, NAL n) {
        return x.punc() == yPunc &&
                x.contains(ys, ye) //parent.start() == s && parent.end() == e &&
                && (!testTruth || Op.QUESTION_OR_QUEST(yPunc) || x.truth().equalsStronger(yt, y instanceof Neg, n)) &&
                y.unneg().equals(x.term());
    }


    @Nullable
    public abstract Truth truth();


    /** produce a concrete, non-proxy clone.
     * "metadata" (pri, creation, why) fields are NOT propagated automatically; use copyMeta() to do so explicitly
     * ex:
     * //                .why(why)
     * //                .withPri(pri())
     * //                .withCreation(creation())
     * */
    public NALTask the() {
        if (this instanceof ProxyTask || this instanceof SerialTask) {
            //boolean preValidated =
            ////false;


            return //preValidated ?
                    taskUnsafe(term(), punc(), truth(), start(), end(), stamp())
                            .copyMeta(this);
            //:
            //NALTask.clone(this);
        }
        return this;
    }

    @Override
    public float freq() {
        return truth().freq();
    }

    @Override
    public double evi() {
        return truth().evi();
    }

    /**
     * merges with any existing why
     */
    public NALTask why(Term otherWhy) {
        int cc = NAL.causeCapacity.intValue();
        if (cc > 0) {
            this.why = this.why != null ? Why.why2(cc, why, otherWhy) : otherWhy;
        }
        return this;
    }

    @Override
    public final Term why() {
        return why;
    }

    public final long creation() {
        return creation;
    }

    public final boolean uncreated() {
        return creation==TIMELESS;
    }

    public final NALTask setUncreated() {
        return setCreation(TIMELESS);
    }

    public final NALTask setCreation(long creation) {
        this.creation = creation;
        return this;
    }

    @Override
    public boolean delete() {
        if (super.delete()) {
            why = null;
            return true;
        }
        return false;
    }

    @Override
    public TaskRegion mbr(HyperRegion y) {
        return TaskRegion.mbr((TaskRegion) y, this);
    }

    @Override
    public float freqMean() {
        return freq();
    }

    @Override
    public float freqMin() {
        return freqMean();
    }

    @Override
    public float freqMax() {
        return freqMean();
    }

    @Override
    public float confMean() {
        return (float) conf();
    }

    @Override
    public float confMin() {
        return confMean();
    }

    @Override
    public float confMax() {
        return confMean();
    }

    @Override
    public final NALTask task() {
        return this;
    }


    /**
     * computes an average truth measurement for interval qStart..qEnd
     * @param dur if negative, auto-dur
     */
    @Nullable
    public final Truth truth(long qStart, long qEnd, float dur, float ete, double eviMin) {
        return ETERNAL() ?
            truthEternal(eviMin) :
            truthTemporal(qStart, qEnd, dur, ete, eviMin);
    }

    @Nullable
    private Truth truthEternal(double eviMin) {
        Truth t = truth();
        return t.evi() < eviMin ? null : t;
    }

    @Nullable
    private Truth truthTemporal(long qStart, long qEnd, float dur, float ete, double eviMin) {
        double e = EviInterval.factor(qStart, qEnd, dur, this, ete, true, NAL.truth.curve) * evi();
//        if (factor < Double.MIN_NORMAL)
//            return null;

        return e < eviMin ? null :
            PreciseTruth.byEvi(
                    freq() /* TODO interpolate frequency wave */,
                    e);
    }

    //    public double eviInteg(long qStart, long qEnd) {
//        return eviInteg(qStart, qEnd, 0, 0);
//    }

//    @Nullable
//    public final Truth truthRelative(long ts, long te, float dur, long now, float ete, double eviMin) {
//        final double p = project(now, ts, te, dur, ete);
////		return truth().confMult(p, eviMin);
//        return truth().eviMult(p, eviMin);
//    }

    /**
     * projected evidence component
     */
    public final double evi(long now, long ts, long te, float dur, float ete) {
        return evi() * project(now, ts, te, dur, ete);
    }

//	/** warning: careful not to call eviInteg(s, e) with 2 long arguments */
//	public double eviInteg(long w, float dur) {
//		return eviInteg(w, w, dur);
//	}
//
//	public double eviInteg(long qStart, long qEnd, float dur) {
//		if (dur == 0 && qStart==qEnd) {
//			//quick test
//
//			assert(qStart!=ETERNAL);
//
//			if (containsSafe(qStart))
//				return evi();
//			else
//				return 0;
//		}
//
//		return eviInteg(new Evidence(qStart, qEnd, dur));
//	}

    /**
     * projective evidence factor
     *
     * @param ts target start occurrence
     * @param te target end occurrence
     */
    private double project(long now, long ts, long te, float dur, float ete) {
        return
                TruthFunctions.projectMid
//			TruthFunctions.projectTrapezoidal
        (now, start(), end(), ts, te, dur, ete/*, 3 */ /* 2 */);
    }


    @Deprecated public double eviMean(long qStart, long qEnd, float dur, float ete) {
        long s = start();
        return s == ETERNAL ? evi() :
                new EviInterval(qStart, qEnd, dur).eviMean(this, ete);
    }
    public double eviMean(EviInterval q, float ete) {
        long s = start();
        return s == ETERNAL ? evi() :
                q.eviMean(this, ete);
    }


    public String proof() {
        return proof(new StringBuilder(1024)).toString().trim();
    }


    private StringBuilder proof(StringBuilder temporary) {
        proof(0, temporary);
        return temporary;
    }

    /**
     * computes the average frequency during the given interval
     */
    public float freq(long start, long end) {
        return freq();
    }

    @Override
    public String toStringWithoutBudget() {
        return appendTo(new StringBuilder(128), true, false,
                false,
                false
        ).toString();
    }

    public StringBuilder appendTo(StringBuilder buffer, boolean showStamp) {
        boolean notCommand = punc() != COMMAND;
        return appendTo(buffer, true, showStamp && notCommand,
                notCommand,
                showStamp
        );
    }

    private StringBuilder appendTo(@Nullable StringBuilder buffer, boolean term, boolean showStamp, boolean showBudget, boolean showLog) {

        String contentName = term ? term().toString() : "";

        CharSequence tenseString;


        appendOccurrenceTime(
                (StringBuilder) (tenseString = new StringBuilder()));


        CharSequence stampString = showStamp ? stampAsStringBuilder() : null;

        int stringLength = contentName.length() + tenseString.length() + 1 + 1;

        boolean hasTruth = BELIEF_OR_GOAL();
        if (hasTruth)
            stringLength += 11;

        if (showStamp)
            stringLength += stampString.length() + 1;

        /*if (showBudget)*/

        stringLength += 1 + 6 + 1 + 6 + 1 + 6 + 1 + 1;


        if (buffer == null)
            buffer = new StringBuilder(stringLength);
        else
            buffer.ensureCapacity(stringLength);


        if (showBudget)
            toBudgetStringExternal(buffer).append(' ');


        buffer.append(contentName).append((char) punc());

        if (!tenseString.isEmpty())
            buffer.append(' ').append(tenseString);

        if (hasTruth) {
            buffer.append(' ');
            Truth.appendString(buffer, 2, freq(), (float) conf());
        }

        if (showStamp)
            buffer.append(' ').append(stampString);

        return buffer;
    }

    private void appendOccurrenceTime(StringBuilder sb) {
        long oc = start();

        /*if (oc == Stamp.TIMELESS)
            throw new RuntimeException("invalid occurrence time");*/


        if (oc != ETERNAL) {
            int estTimeLength = 8; /* # digits */
            sb.ensureCapacity(estTimeLength);
            sb.append(oc);

            long end = end();
            if (end != oc) {
                sb.append((char) 0x22c8 /* bowtie, horizontal hourglass */).append(end);
            }


        }

    }

    public NALTask copyMeta(NALTask t) {
        copyMeta(this, t);
        return this;
    }
    public NALTask asLateAs(NALTask x) {
        if (this != x) {
            long s = creation, xs = x.creation;
            if (xs != TIMELESS)
                setCreation(s == TIMELESS ? xs : Math.max(xs, s));
        }
        return this;
    }

    /**
     * Check if a Task is a direct input,
     * or if its origin has been forgotten or never known
     */
    public boolean isInput() {
        return stampLength() <= 1;
    }
//	private boolean stampOverlapsAny(long[] yStamp) {
//		return Stamp.overlapsAny(stamp(), yStamp);
//	}
//	private boolean stampOverlapsAny(Stamp y) {
//		return this==y || stampOverlapsAny(y.stamp());
//	}

    public boolean ETERNAL() {
        return this instanceof EternalTask || start() == ETERNAL;
    }

    //	@Deprecated public final Predicate<NALTask> timeIntersecting() {
//		long xs;
//		if ((xs = start()) == ETERNAL)
//			return t->true;
//		else
//			return new TimeIntersecting(xs, end() + term().eventRange());
//	}


//	private static final class StampOverlapping implements Predicate<NALTask> {
//		long[] xStamp;
//
//		private StampOverlapping(long[] xStamp) {
//			this.xStamp = xStamp;
//		}
//
//		@Override
//		public boolean test(NALTask y) {
//			return Stamp.overlapsAny(xStamp, y.stamp());
//		}
//	}

//	private static class TimeIntersecting implements Predicate<NALTask> {
//		long xs, xe;
//
//		public TimeIntersecting(long xs, long xe) {
//			this.xs = xs;
//			this.xe = xe;
//		}
//
//		@Override
//		public boolean test(NALTask t) {
//			return t.intersects(xs, xe);
//		}
//	}

    public final StampOverlapping stampOverlapping() {
        return new StampOverlapping().set(this);
    }

    public final boolean stampOverlapping(NALTask x) {
        try (StampOverlapping s = new StampOverlapping()) {
            return s.set(this).test(x);
        }
    }

    public boolean equalNAL(NALTask b) {
        return
            equalStamp(this, b)
            &&
            (!BELIEF_OR_GOAL() || this.truth().equals(b.truth()))
            &&
            (this.start() == b.start()) && (this.end() == b.end())
        ;
    }

    private static boolean equalStamp(NALTask a, NALTask b) {
        long[] aa = a.stamp(), bb = b.stamp();
        if (aa == bb)
            return true;
        if (Arrays.equals(aa, bb)) {
            shareStamp(a, b, aa, bb);
            return true;
        }
        return false;
    }

    private static void shareStamp(NALTask a, NALTask b, long[] aa, long[] bb) {
        if (b instanceof AbstractNALTask && System.identityHashCode(aa) < System.identityHashCode(bb))
            ((AbstractNALTask) b).stamp = aa;
        else if (a instanceof AbstractNALTask)
            ((AbstractNALTask) a).stamp = bb;
    }

    private CharSequence stampAsStringBuilder() {

        long[] x = stamp();
        int n = x.length;
        int estimatedInitialSize = 8 + (n * 3); //TODO tune

        StringBuilder y = new StringBuilder(estimatedInitialSize);
        y.append(STAMP_OPENER);

        /*if (creation() == TIMELESS) {
            buffer.append('?');
        } else */
        /*if (!(start() == ETERNAL)) {
            appendTime(buffer);
        } else {*/
        long c = creation();
        if (c != TIMELESS)
            y.append(c);

        y.append(STAMP_STARTER).append(' ');

        for (int i = 0; i < n; i++) {
            BinTxt.append(y, x[i]);
            if (i < n - 1)
                y.append(STAMP_SEPARATOR);
        }

        return y.append(STAMP_CLOSER);
    }


    public NALTask copyMetaAndCreation(NALTask x) {
        return copyMeta(x).setCreation(x.creation());
    }

    public static final NALTask[] EmptyNALTaskArray = new NALTask[0];
}