package nars.task.proxy;

import nars.Term;
import nars.task.NALTask;
import nars.term.Neg;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

public class SpecialPuncTermAndTruthTask extends SpecialTermTask {

    private final Truth truth;
    private final byte punc;

    /** use with caution */
    public SpecialPuncTermAndTruthTask(Term term, Truth truth, NALTask task) {
        this(term, task.punc(), truth, task);
    }

    @Deprecated
    private SpecialPuncTermAndTruthTask(Term term, byte punc, Truth truth, NALTask task) {
        super(task, term);

        this.truth = truth;
        this.punc = punc;
    }

    public static SpecialPuncTermAndTruthTask proxy(Term term, byte punc, Truth truth, NALTask task) {

        Term y = NALTask.taskTerm(term, punc);

        boolean neg = y instanceof Neg;
        if (neg) {
            if (truth!=null) truth = truth.neg();
            y = y.unneg();
        }

        //TODO other equality tests

        return y!=null ? new SpecialPuncTermAndTruthTask(y, punc, truth, task) : null;
    }


    @Override
    public float freq(long start, long end) {
        return truth.freq();
    }

    @Override
    public byte punc() {
        return punc;
    }

    @Override
    public @Nullable Truth truth() {
        return truth;
    }

}