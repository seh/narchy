package nars.task.proxy;

import nars.task.NALTask;
import nars.task.ProxyTask;

public class SpecialOccurrenceTask extends ProxyTask {

    public static NALTask proxy(NALTask t, long s, long e) {
        return SpecialTruthAndOccurrenceTask.proxy(t, s, e, t.truth()); //HACK
    }

    private final long start;
    private final long end;

    public SpecialOccurrenceTask(NALTask task, long start, long end) {
        super(task instanceof SpecialOccurrenceTask ? ((SpecialOccurrenceTask)task).task : task);
        this.start = start;
        this.end = end;
    }

    @Override
    public final long start() {
        return start;
    }

    @Override
    public final long end() {
        return end;
    }


}