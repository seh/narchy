package nars.task.proxy;

import nars.task.NALTask;
import nars.task.util.TaskException;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * accepts replacement truth and occurrence time for a proxied task
 */
public class SpecialTruthAndOccurrenceTask extends SpecialOccurrenceTask {


    public static NALTask proxy(NALTask t, long s, long e, @Nullable Truth tr) {
        if (e < s)
            throw new TaskException("reversed occurrence for proxy: " + s + ".." + e, t);
        if (t.QUESTION_OR_QUEST() != (tr==null))
            throw new TaskException("invalid truth/non-truth for proxy", t);

        boolean oSame = equivalentOcc(t, s, e), tSame = Objects.equals(t.truth(), tr);
        if (oSame && tSame)
            return t;

        if (t instanceof SpecialTruthAndOccurrenceTask) {
            t = ((SpecialTruthAndOccurrenceTask) t).task; //unwrap

            //check if the existing proxy is equivalent
            oSame = equivalentOcc(t, s, e); tSame = Objects.equals(t.truth(), tr);
            if (oSame && tSame)
                return t;
        }

        if (!oSame && !tSame)
            return new SpecialTruthAndOccurrenceTask(t, tr, s, e);
        else if (!oSame) {
            if (t instanceof SpecialOccurrenceTask) {
                t = ((SpecialOccurrenceTask)t).task; //unwrap
                if (equivalentOcc(t, s, e))
                    return t;
            }
            return new SpecialOccurrenceTask(t, s, e);
        } else /*if (!tSame)*/ {
            if (t instanceof SpecialTruthTask) {
                t = ((SpecialTruthTask)t).task; //unwrap
                if (Objects.equals(t.truth(), tr))
                    return t;
            }
            return new SpecialTruthTask(t, tr);
        }
    }

    private static boolean equivalentOcc(NALTask t, long start, long end) {
        return t.start() == start && t.end() == end;
    }


    /**
     * either Truth, Function<Task,Truth>, or null
     */
    private final Truth truth;

    private SpecialTruthAndOccurrenceTask(NALTask task, Truth truth, long start, long end) {
        super(task, start, end);
        this.truth = truth;
    }

    @Override
    public @Nullable Truth truth() {
        return truth;
    }

}