package nars.time;

import jcog.math.LongInterval;
import nars.truth.evi.EviInterval;

/**
 * reference to a temporal context:
 *      --start, end
 *          occurrence
 *      --dur
*           time constant (may be less equal or greater than the occurrence range)
 *
 *
 * represents a mutable perceptual window in time, including a perceptual duration (dur) and
 * reference to invoking context X.
 * TODO Use this in all applicable long,long,int,dur parameter sequences
 * */
public class When<X> extends EviInterval {

    public transient X x;

    @Deprecated public When() {
        super();
    }

    @Deprecated public When(long s, long e) {
        super(s, e);
    }

    public When(X x, LongInterval interval, float dur) {
        this(x, interval.start(), interval.end(), dur);
    }

    public When(X x, long start, long end, float dur) {
        super(start, end, dur);
        this.x = x;
    }

    public final boolean the(X next) {
        X prev = this.x;
        if (prev!=next) {
            this.x = next;
            return true;
        }
        return false;
    }

    /** shift forward or backward in time. returns a new instance unless dt==0 */
    public final When<X> add(long dt) {
        return dt == 0 ? this : new When(x, this.s + dt, this.e + dt, this.dur);
    }

    public final When<X> addDurs(float n) {
        return add(Math.round(n * dur));
    }
}