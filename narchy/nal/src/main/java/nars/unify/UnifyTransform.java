package nars.unify;

import jcog.random.RandomBits;
import nars.NAL;
import nars.Op;
import nars.Term;
import org.jetbrains.annotations.Nullable;

/** unify + transform */
public class UnifyTransform extends Unify {

	@Nullable private Term transformed;
	@Nullable private Term result;

	/** force novel (non-equal) results */
	public boolean novel;

	public UnifyTransform(RandomBits rng) {
		super(Op.Variables, rng, NAL.unify.UNIFICATION_STACK_CAPACITY);
	}

	@Override
	public UnifyTransform clear() {
		this.novel = false;
		this.result = this.transformed = null;
		super.clear();
		return this;
	}


	/** filters acceptable results */
	protected boolean filter(Term y) {
		return true;
	}

	/** may need to call clear first */
	@Nullable public Term unifySubst(Term x, Term y, @Nullable Term transformed) {
		this.transformed = transformed;
		this.result = null;

		unify(x, y);

		return result;
	}


	/**
	 * terminate after the first match
	 */
	@Override
	protected final boolean match() {
		Term x = this.transformed;
		if (x == null)
			return false; //done

		Term y = transform(x);
		if (y!=null) {
			this.result = y;
			return false; //done
		}
		return true; //kontinue
	}

	@Nullable
	public Term transform(Term x) {
		Term y = apply(x);
		return y != null && y.CONCEPTUALIZABLE() && (!novel || !x.equals(y)) && filter(y) ?
				y : null;
	}

	public final UnifyTransform clear(int ttl) {
		setTTL(ttl);
		return clear();
	}

}