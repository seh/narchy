package nars.unify.constraint;

import nars.Term;
import nars.term.control.PREDICATE;
import nars.unify.UnifyConstraint;

import java.util.function.BiFunction;

public abstract class ConstraintAsPredicate<U, C extends UnifyConstraint> extends PREDICATE<U> {

    public final C constraint;

    /** taskterm, beliefterm -> extracted */
    public final BiFunction<Term,Term,Term> extractX;
    protected final BiFunction<Term,Term,Term> extractY;
    private final float cost;

    protected ConstraintAsPredicate(Term id, C m, BiFunction<Term, Term, Term> extractX, BiFunction<Term, Term, Term> extractY, float cost) {
        super(id);
        this.constraint = m;
        this.cost = cost;
        this.extractX = extractX;
        this.extractY = extractY;
    }

//    @Override
//    public boolean reduceIn(List<PREDICATE<U>> p) {
//        return p.removeIf(x -> {
//            if (x!=ConstraintAsPredicate.this && x instanceof ConstraintAsPredicate) {
//                if (((ConstraintAsPredicate)x).constraint.getClass().equals(constraint.getClass()))
//                    Util.nop();
//            }
//            return false;
//        });
//    }

    @Override
    public float cost() {
        return cost;
    }

}