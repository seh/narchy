package nars.term.control;

import jcog.TODO;
import nars.Term;
import nars.unify.constraint.TermMatch;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.function.Function;

import static java.lang.invoke.MethodHandles.insertArguments;


/**
 * decodes a target from a provied context (X)
 * and matches it according to the matcher impl
 */
public final class TermMatching<U> extends PREDICATE<U> {

    private final TermMatch match;
    private final float cost;
    private final Function<U, Term> resolve;

    public TermMatching(TermMatch match, Function<U, Term> resolve, int depth) {
        //super($.p(match.name(resolve), trueOrFalse ? Int.ONE : Int.NEG_ONE));
        super(match.name(resolve));
        this.resolve = resolve;

        this.match = match;
        this.cost = pathCost(depth) + match.cost();
    }

    private static float pathCost(int pathLen) {
        return pathLen * 0.002f;
    }

    @Override
    public float cost() {
        return cost;
    }

    @Override
    public final boolean test(U x) {
        Term y = resolve.apply(x);
        return y!=null && match.test(y);
    }


    @Override
    public MethodHandle method(U u) {

        //TODO test
        MethodHandle c = MethodHandles.filterReturnValue(
                insertArguments(R.bindTo(resolve), 0, u),
                T.bindTo(match)
        );

        throw new TODO();
        //return MethodHandles.guardWithTest(c, constant(boolean.class, trueOrFalse), constant(boolean.class, !trueOrFalse));
    }

    private static final MethodHandle R;
    private static final MethodHandle T;
    static {
        MethodHandles.Lookup l = MethodHandles.lookup();

        try {
            Method rr = Function.class.getMethod("apply", Object.class); rr.trySetAccessible();
            Method tt = PREDICATE.class.getMethod("test", Object.class); tt.trySetAccessible();
            R = l.unreflect(rr);
            T = l.unreflect(tt);
        } catch (IllegalAccessException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}