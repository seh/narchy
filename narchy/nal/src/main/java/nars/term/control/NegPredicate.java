package nars.term.control;

import nars.term.Neg;

import java.lang.invoke.MethodHandle;

import static java.lang.invoke.MethodHandles.guardWithTest;

public final class NegPredicate<X> extends PREDICATE<X> implements Neg {
    public final PREDICATE<X> p;

    NegPredicate(PREDICATE<X> p) {
        super(p.ref.neg());
        this.p = p;
    }

    @Override
    public MethodHandle method(X x) {
        //inverter:
        return guardWithTest(p.method(x), PREDICATE.CONSTANT_FALSE, PREDICATE.CONSTANT_TRUE);
    }

    @Override
    public MethodHandle method() {
        //inverter:
        return guardWithTest(p.method(), PREDICATE.CONSTANT_FALSE, PREDICATE.CONSTANT_TRUE);
    }

    @Override
    public float cost() {
        return p.cost() + 0.001f;
    }

    @Override
    public PREDICATE<X> neg() {
        return p; //unneg
    }



    @Override
    public boolean test(X o) {
        return !p.test(o);
    }
}