package nars.term.control;

import jcog.TODO;
import nars.Term;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * TODO generify key/value
 * default impl:
 *    require keys map to integer states. then switch on the integer (not some equality / map-like comparison)
 */
public abstract class SWITCH<X,D> extends PREDICATE<D> {

    protected final Map<X, PREDICATE<D>> cases;

    @Override
    public final boolean test(D m) {
        X k = branch(m);
        return k == null || cases.get(k).test(m);
    }

    @Override
    public float cost() {
        throw new TODO();
    }

    public SWITCH(Term id, Map<X, PREDICATE<D>> cases) {
        super(id);

        this.cases = cases;
    }

    @Nullable
    public abstract X branch(D m);


}

//        swtch = new PREDICATE[24];
//        for (Map.Entry<X, PREDICATE<D>> entry : cases.entrySet()) {
//            swtch[entry.getKey()] = entry.getValue();
//        }