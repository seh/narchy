package nars.term.functor;

import nars.Term;
import nars.eval.Evaluation;
import nars.term.Compound;
import org.jetbrains.annotations.Nullable;

/** functors can optionally implement this interface to provide backward-solved solutions for missing values */
public interface InverseFunctor {

    /** called when evaluation an equal(x,y) , where 'x' is this functor and 'y' is another value.
     *  the returned value rewrites the containing equal(x,y), while also providing an implementation
     *  to assign values in the evaluation context (e).
     */
    @Nullable Term equality(Evaluation e, Compound x, Term y);
}