package nars.term;

import com.google.common.io.ByteArrayDataOutput;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.atom.Atomic;
import org.eclipse.collections.api.block.function.primitive.IntObjectToIntFunction;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

import static nars.Op.*;

/**
 * something which is like a target but isnt quite,
 * like a subterm container
 * <p>
 * Features exhibited by, and which can classify terms
 * and termlike productions
 */
public interface Termlike {

    /** whether contains any potentially temporal (&&,==>) terms */
    default boolean TEMPORALABLE() {
        return this instanceof Subterms &&
               !(this instanceof CondAtomic) &&
               hasAny(Temporals);
    }

    /**
     * subterm access
     */
    default Term sub(int i) {
        throw new ArrayIndexOutOfBoundsException();
    }

    default Term subUnneg(int i) {
        return sub(i).unneg();
    }



    /* final */

    /**
     * tries to get the ith subterm (if this is a TermContainer),
     * or of is out of bounds or not a container,
     * returns the provided ifOutOfBounds
     */
    default Term sub(int i, Term ifOutOfBounds) {
        return i >= subs() ?
                ifOutOfBounds
                :
                sub(i);
    }


    /**
     * number of subterms. if atomic, size=0
     * TODO move to Subterms
     */
    int subs();

    /**
     * recursion height; atomic=1, compound>1
     */
    int height();

    /**
     * syntactic volume = 1 + total volume of terms = complexity of subterms - # variable instances
     */
    int volume();


    /**
     * syntactic complexity 1 + total complexity number of leaf terms, excluding variables which have a complexity of zero
     */
    default int complexity() {
        return 1 + sum(Term::complexity);
    }

    /**
     * only 1-layer (shallow, non-recursive)
     */
    default int sum(ToIntFunction<Term> value) {
//        int x = 0;
//        int s = subs();
//        for (int i = 0; i < s; i++)
//            x += value.applyAsInt(sub(i));
//
//        return x;
        return intifyShallow(0, (x, t) -> x + value.applyAsInt(t));
    }

    /**
     * only 1-layer (shallow, non-recursive)
     */
    default int max(ToIntFunction<Term> value) {
        return intifyShallow(Integer.MIN_VALUE, (x, t) -> Math.max(value.applyAsInt(t), x));
    }


    /**
     * non-recursive, visits only 1 layer deep, and not the current if compound
     */
    default int intifyShallow(int v, IntObjectToIntFunction<Term> reduce) {
        int n = subs();
        for (int i = 0; i < n; i++)
            v = reduce.intValueOf(v, sub(i));
        return v;
    }

    default int intifyRecurse(int v, IntObjectToIntFunction<Term> reduce) {
        //return intifyShallow(vIn, (vNext, subterm) -> subterm.intifyRecurse(vNext, reduce));
        int n = subs();
        for (int i = 0; i < n; i++)
            v = sub(i).intifyRecurse(v, reduce);
        return v;
    }



    /**
     * structure hash bitvector
     */
    int structure();


    /** only the ops in the immediate subterms of compounds, 0 if atomic */
    int structureSubs();

    /**
     * structure of the first layer (surface) only
     */
    int structureSurface();

    /**
     * average of complexity and volume
     */
    default float voluplexity() {
        return (complexity() + volume()) / 2f;
    }



    /** if the term is or contains variably-temporal subterms  (ie. XTERNAL) */
    boolean TEMPORAL_VAR();

    /**
     * parent compounds must pass the descent filter before ts subterms are visited;
     * but if descent filter isnt passed, it will continue to the next sibling:
     * whileTrue must remain true after vistiing each subterm otherwise the entire
     * iteration terminates
     *
     * implementations are not obligated to visit in any particular order, or to repeat visit a duplicate subterm
     * for that, use recurseTermsOrdered(..)
     */
    boolean boolRecurse(Predicate<Compound> aSuperCompoundMust, BiPredicate<Term, Compound> whileTrue, Compound parent, boolean andOrOr);

    default /* final */ boolean ANDrecurse(Predicate<Compound> inSuper, BiPredicate<Term, Compound> whileTrue, @Nullable Compound parent) {
        return boolRecurse(inSuper, whileTrue, parent, true);
    }
    default /* final */ boolean ORrecurse( Predicate<Compound> inSuper, BiPredicate<Term, Compound> whileTrue, @Nullable Compound parent) {
        return boolRecurse(inSuper, whileTrue, parent, false);
    }


    default boolean impossibleSubTerm(Termlike x) {
        return
            impossibleSubVolume(x.volume()) ||
            impossibleSubStructure(x.structure());
//        return impossibleSubTerm().test(x);
    }

    default /* final */ boolean impossibleSubStructure(int structure) {
        return this instanceof Atomic || !has(structureSubs(), structure, true);
    }

    default boolean impossibleSubVolume(int otherTermVolume) {
        return this instanceof Atomic || otherTermVolume > volume() - subs();
    }

    default boolean hasAll(int struct) {
        return Op.hasAll(structure(), struct);
    }

    default boolean hasAny(int struct) {
        return Op.hasAny(structure(), struct);
    }

    default /* final */ boolean hasAny(/*@NotNull*/ Op op) {
        return hasAny(op.bit);
    }

    default boolean hasVarIndep() {
        return hasAny(VAR_INDEP);
    }

    default boolean hasVarDep() {
        return hasAny(VAR_DEP);
    }

    default boolean hasVarQuery() {
        return hasAny(VAR_QUERY);
    }

    default boolean hasVarPattern() {
        return hasAny(VAR_PATTERN);
    }

    default boolean hasVars() {
        return false;
    }

    int vars();


    default /* final */ boolean isConstant() { return !hasVars(); }

    int varIndep();
    int varQuery();
    int varPattern();
    int varDep();


    boolean recurseTermsOrdered(Predicate<Term> inSuperCompound, Predicate<Term> whileTrue, Compound parent);

    default /* final */ boolean recurseTermsOrdered(Predicate<Term> whileTrue) {
        return recurseTermsOrdered(x -> true, whileTrue, null);
    }

    void write(ByteArrayDataOutput out);

    default int addAllTo(Term[] t, int offset) {
        int s = subs();
        for (int i = 0; i < s; )
            t[offset++] = sub(i++);
        return s;
    }

    default boolean containsRecursively(Term x) {
        return false;
    }

    default Predicate<Termlike> impossibleSubTerm() {
        return this instanceof Atomic ?
            x -> true
            :
            new ImpossibleSubterm(this);
    }

    default Predicate<Termlike> impossibleSubTermOf() {
        return new ImpossibleSubtermOf(this);
    }


    final class ImpossibleSubterm implements Predicate<Termlike> {
        private final int xVolSub;
        private final int xStructureSubs;

        private ImpossibleSubterm(Termlike x) {
            xVolSub = x.volume() - x.subs();
            xStructureSubs = x.structureSubs();
        }

        @Override
        public boolean test(Termlike y) {
            return
                xVolSub < y.volume() ||
                !has(xStructureSubs, y.structure(), true);
        }

    }
    final class ImpossibleSubtermOf implements Predicate<Termlike> {
        private final int yVolSub;
        private final int yStruct;

        private ImpossibleSubtermOf(Termlike y) {
            yVolSub = y.volume() - y.subs();
            yStruct = y.structure();
        }

        @Override
        public boolean test(Termlike x) {
            return
                yVolSub >= x.volume() ||
                !has(x.structureSubs(), yStruct,true);
        }

    }


}