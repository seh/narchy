package nars.term.atom;

import com.google.common.io.ByteArrayDataOutput;
import nars.$;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.CondAtomic;
import nars.term.Variable;
import nars.term.util.TermException;
import org.eclipse.collections.api.block.function.primitive.IntObjectToIntFunction;
import org.jetbrains.annotations.Nullable;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static nars.Op.ATOM;
import static nars.Op.EmptySubterms;
import static nars.term.atom.Bool.Null;


/**
 * Base class for Atomic types.
 */
public abstract class Atomic extends Term implements CondAtomic {

    public final boolean ATOM() {
        return isAny(ATOM.bit);
    }

    @Override
    public final boolean TEMPORAL_VAR() {
        return false;
    }

    @Override
    public final boolean equalsRoot(Term x) {
        return equals(x);
    }

    @Override
    public final void write(ByteArrayDataOutput out) {
        if (this == Null)
            throw new NullPointerException("null output");
        out.write(bytes());
    }

    @Override
    public int varDep() {
        return 0;
    }

    @Override
    public int varIndep() {
        return 0;
    }

    @Override
    public int varQuery() {
        return 0;
    }

    @Override
    public int varPattern() {
        return 0;
    }

    @Override
    public final Subterms subterms() { return EmptySubterms; }

    @Override
    public final boolean equalConcept(Term x) {
        return x instanceof Atomic && equals(x);
    }

    @Override
    public final Term concept() {
        if (!CONCEPTUALIZABLE())
            throw new TermException(Op.UNCONCEPTUALIZABLE, this);

        return this;
        //return CONCEPTUALIZABLE() ? this : Null;
    }

    @Override
    public final Term root() { return this; }



    @Override
    public final int intifyShallow(int v, IntObjectToIntFunction<Term> reduce) {
        return reduce.intValueOf(v, this);
    }

    @Override
    public final int intifyRecurse(int vIn, IntObjectToIntFunction<Term> reduce) {
        return intifyShallow(vIn, reduce);
    }

    public static Atom atom(String id) {
        return (Atom)the(id);
    }

    public static @Nullable Atomic the(char c) {
        return switch (c) {
            case Op.VarAutoSym -> Op.VarAuto;
            case Op.NullSym -> Null;
            case Op.imIntSym -> Op.ImgInt;
            case Op.imExtSym -> Op.ImgExt;
            case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' -> Int.pos[c - '0'];
            case Op.NaNChar -> Op.NaN;
            case Op.InfinityPosChar -> Op.InfinityPos;
            case Op.InfinityNegChar -> Op.InfinityNeg;
            default -> c < CharAtom.chars.length ? CharAtom.chars[c] : Op.terms.atomNew(String.valueOf(c));
        };
    }

    /*@NotNull*/
    public static Atomic the(/*TODO CharSequence */ String id) {
        int l = id.length();
        if (l == 1)
            return the(id.charAt(0));

        if (l > 0) {
            switch (id) {
                case "true":
                    return Bool.True;
                case "false":
                    return Bool.False;
                case "null":
                    return Null;
                default: {
                    if (Atom.quoteable(id, l)) {

    //                    if (l > 1 /* already handled single digit cases in the above switch */ && Character.isDigit(id.charAt(0))) {
    //
    //                        int i = Texts.i(id, MIN_VALUE);
    //                        if (i != MIN_VALUE)
    //                            return Int.the(i);
    //                    }

                        return $.quote(id);
                    } else {
                        Atom.validateAtomID(id);
                        return Op.terms.atomNew(id);
                    }
                }
            }
        }

        throw new RuntimeException("attempted construction of zero-length Atomic id");
    }

    @Override
    public abstract String toString();

    /** byte[] representation */
    public abstract byte[] bytes();

    @Override
    public final boolean boolRecurse(Predicate<Compound> aSuperCompoundMust, BiPredicate<Term, Compound> whileTrue, Compound parent, boolean andOrOr) {
        return whileTrue.test(this, parent);
    }

    @Override
    public final boolean recurseTermsOrdered(Predicate<Term> inSuperCompound, Predicate<Term> whileTrue, Compound parent) {
        return whileTrue.test(this);
    }

    @Override
    public final int subs() {
        return 0;
    }

    @Override public final int vars() { return this instanceof Variable ? 1 : 0; }
    @Override public final int complexity() { return this instanceof Variable ? 0 : 1; }
    @Override public final float voluplexity() {
        return this instanceof Variable ? 0.5f : 1;
    }

    @Override
    public final int volume() {
        return 1;
    }


    @Override
    public final Term sub(int i, Term ifOutOfBounds) {
        return ifOutOfBounds;
    }

    @Override
    public final int structure() {
        return opBit();
    }

    @Override
    public final int structureSubs() {
        return 0;
    }
    @Override
    public final int structureSurface() {
        return 0;
    }

    public final int height() { return 1; }

}