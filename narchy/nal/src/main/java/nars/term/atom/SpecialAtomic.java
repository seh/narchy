package nars.term.atom;

import jcog.The;

/** special atoms which are universally singleton */
public abstract class SpecialAtomic extends AbstractAtomic implements The {
	private final String label;

	SpecialAtomic(String label, byte... bytes) {
		super(bytes);
		this.label = label;
	}


	@Override
	public final boolean equals(Object u) {
		return u == this;
	}

	@Override
	public final String toString() {
		return label;
	}

}