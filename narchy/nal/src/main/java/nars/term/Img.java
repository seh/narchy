package nars.term;

import nars.Op;
import nars.Term;
import nars.term.anon.AbstractIntrinAtomic;

/** the / and \ Image operators */
public final class Img extends AbstractIntrinAtomic {

    private final String str;

    public Img(byte sym) {
        super(Op.IMG, sym);
        this.str = String.valueOf((char) sym);
    }

    @Override
    public Term neg() {
        return this==Op.ImgExt ? Op.ImgExtNeg : Op.ImgIntNeg;
    }

    @Override
    public int opID() {
        return Op.IMG.id;
    }

    @Override
    public final String toString() {
        return str;
    }

}