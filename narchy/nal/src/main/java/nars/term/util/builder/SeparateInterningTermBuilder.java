package nars.term.util.builder;

import jcog.data.bit.IntBitSet;
import jcog.data.bit.MetalBitSet;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TmpTermList;
import nars.term.util.cache.Intermed;

import java.util.function.Function;

import static nars.Op.CONJ;
import static nars.Op.NEG;

/**
 * intern subterms and compounds.
 * the requirements for intern cache admission are configurable.
 **/
@Deprecated public class SeparateInterningTermBuilder extends InterningTermBuilder {

    private final Function<Intermed.InternedTermArray, Subterms> subs;
//    private final Function<InternedTermArray, Subterms> intrinSubs;
    private final Function<Intermed.InternedCompoundByComponents, Term>[] terms = new Function[Op.count];


    /** used for quickly determining if op type is internable */
    private final int termsInterned;

    public SeparateInterningTermBuilder() {
         this(MegaHeapTermBuilder.the /*Op._terms*/);
    }

    public SeparateInterningTermBuilder(TermBuilder builder) {
        this(atomCapacityDefault, compoundCapacityDefault, volMaxDefault, builder);
    }

    public SeparateInterningTermBuilder(int atomCapacity, int compoundCapacity, int compoundVolMax, TermBuilder _builder) {
        super(_builder, atomCapacity, compoundVolMax);



        subs = memoizer("subterms", z -> _subtermsNew(z.subs), compoundCapacity * 2);

//        intrinSubs = memoizer("intrinSubterms", x -> new IntrinSubterms(x.subs), compoundCapacity);

        Function<Intermed.InternedCompoundByComponents, Term> statements = memoizer("statement", x ->
            termBuilder.statementNew(Op.the(x.op), x.dt, x.sub(0), x.sub(1)), compoundCapacity * 3);

        Function<Intermed.InternedCompoundByComponents, Term> compounds = x ->
            super.compoundNew(Op.the(x.op), x.dt, x.terms());

        for (int i = 0; i < Op.count; i++) {
            Op o = Op.the(i);
            if (o.atomic || (/*!internNegs && */o == NEG)) continue;

            //TODO use multiple PROD slices to decrease contention

            Function<Intermed.InternedCompoundByComponents, Term> c;
            if (o == CONJ) {
                c = memoizer("conj", x -> _conjNew(x.dt, new TmpTermList(x.terms())), compoundCapacity);
            } else if (o.statement) {
                c = statements;
            } else {
                c = memoizer(o.str, compounds, compoundCapacity);
            }
            terms[i] = c;
        }

        IntBitSet termsInterned = (IntBitSet) MetalBitSet.bits(terms.length);
        for (int i = 0, termsLength = terms.length; i < termsLength; i++)
            if (terms[i] != null) termsInterned.set(i);
        this.termsInterned = termsInterned.x;
    }

    @Override
    public Term compoundNew(Op o, int dt, Subterms t) {
        boolean internable = internable(o) && internableTerms(t);
        return internable ?
                terms[o.id].apply(new Intermed.InternedCompoundByComponentsArray(o, dt, TermBuilder.array(t) /*o.sortedIfNecessary(dt, t)*/)) :
                termBuilder.compoundNew(o, dt, _subterms(t));
    }

//
//    @Override
//    protected Subterms subtermsInterned(Term[] t) {
//        return subs.apply(new InternedTermArray(t));
//    }

    private boolean internable(Op op) {
        return internable(op.id);
    }

    private boolean internable(int opID) {
        return (termsInterned & (1 << opID)) != 0;
    }

    @Override
    public Term compound1New(Op o, Term x) {
        return termBuilder.compound1New(o, x);
    }

    @Override protected Term conjIntern(int dt, Term[] u) {
        return terms[CONJ.id].apply(new Intermed.InternedCompoundByComponentsArray(CONJ, dt, u));
    }

    @Override protected Term statementInterned(Op o, int dt, Term S, Term P) {
        return terms[o.id].apply(new Intermed.InternedCompoundByComponentsArray(o, dt, S, P));
    }


    @Override
    protected Subterms subtermsInterned(Term[] t) {
        return subs.apply(new Intermed.InternedTermArray(t));
    }
}
//    @Override
//    protected Subterms subterms(Op o, Term[] t, @Nullable DynBytes key) {
//        if (t.length == 0)
//            return EmptySubterms;
//
//        Subterms subs = this.subterms(t);
//
//        if (key != null && cacheSubtermKeyBytes) {
//            if (subs instanceof Subterms.SubtermsBytesCached)
//                ((Subterms.SubtermsBytesCached) subs).acceptBytes(key);
//        }
//
//        return subs;
//    }
//	/**
//	 * allows a Subterms implementation to accept the byte[] key that was used in constructing it,
//	 * allowing it to cache it for fast serialization.  typically it will want to keep:
//	 * <p>
//	 * byte[] cached = builtWith.arrayCopy(1) //skip prefix op byte
//	 */
//	@FunctionalInterface
//	interface SubtermsBytesCached {
//		void acceptBytes(DynBytes constructedWith);
//	}