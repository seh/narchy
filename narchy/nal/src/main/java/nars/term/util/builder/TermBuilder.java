package nars.term.util.builder;

import jcog.Util;
import jcog.WTF;
import jcog.util.ArrayUtil;
import nars.NAL;
import nars.Op;
import nars.Task;
import nars.Term;
import nars.io.IO;
import nars.subterm.ArrayRemappedSubterms;
import nars.subterm.IntrinSubterms;
import nars.subterm.Subterms;
import nars.subterm.TmpTermList;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Termlike;
import nars.term.Variable;
import nars.term.anon.Intrin;
import nars.term.anon.IntrinAtomic;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.util.Statement;
import nars.term.util.TermException;
import nars.term.util.TermTransformException;
import nars.term.util.Terms;
import nars.term.util.conj.ConjPar;
import nars.term.util.conj.ConjSeq;
import nars.term.util.transform.TermTransform;
import nars.term.util.transform.VariableNormalization;
import nars.term.var.NormalizedVariable;
import nars.time.Tense;
import org.eclipse.collections.impl.map.mutable.primitive.ShortByteHashMap;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.function.Predicate;

import static nars.Op.*;
import static nars.term.atom.Bool.*;
import static nars.term.util.Image.imageNormalize;

//import nars.term.var.ellipsis.Ellipsislike;

/**
 * interface for target and subterm builders
 * this call tree eventually ends by either:
 * - instance(..)
 * - reduction to another target or True/False/Null
 */
public abstract class TermBuilder {

    public static Term[] array(Subterms s) {
        return s instanceof TmpTermList ?
                ((TmpTermList) s).arrayTake() :
                s.arrayShared();
    }

    private static Term postNormalize(Term y, Compound x) {
        Term z = CompoundPostNormalizer.normalize((Compound) y);
        if (NAL.NORMALIZE_STRICT && z != y) assertSameOp(x, z);
        return z;
    }

    private static void assertSameOp(Compound x, Term y) {
        if (y.opID() != x.opID) {
            if (!NAL.DEBUG && x.CONDS() && y.CONDS()) return; //HACK allow for now

            throw new TermTransformException("normalization op fault", x, y);
        }
    }


//    public final Term compound(Op o, int dt, Subterms s) {
//        return o.build(this, dt, array(s));
//    }

    public final Term compound(byte[] b) {
        return compound(b, 0, b.length);
    }

    private Term compound(byte[] b, int start, int end) {
        if (start != 0 && end != b.length)
            b = Arrays.copyOfRange(b, start, end);

        return IO.bytesToTerm(b, this);
    }

    public abstract Atomic atomNew(String id);

    protected abstract Term negNew(Term u);

    protected abstract Term compound1New(Op o, Term x);

    public Term compoundNew(Op o, int dt, Term... t) {
        return compoundNew(o, dt, subterms(t));
    }

    public abstract Term compoundNew(Op o, int dt, Subterms subs);

    protected abstract Subterms subtermsNew(Term[] t);

    public Term root(Compound x) {
        return NAL.conceptualization.applyCompound(x);
    }

    public Compound normalize(Compound x, byte varOffset) {
        int nVars = x.vars();
        if (nVars == 0) return x;

        Term y = new VariableNormalization(nVars, varOffset).apply(x);
        if (NAL.NORMALIZE_STRICT)
            assertSameOp(x, y);

        if (NAL.POST_NORMALIZE && y instanceof Compound)
            y = postNormalize(y, x);

        return (Compound) y;
    }

    public final Term compound(Op o, int dt, boolean preCommute, Term[] __t) {
        Term[] _t = pre(__t);
        if (_t == null) return Null;

        Term[] t = preCommute && o.commutative && _t.length > 1 && Tense.parallel(dt) ?
                Terms.commute(_t) : _t;

        int s = t.length;

        if (o.minSubs > s || o.maxSubs < s)
            throw new TermException("subterm overflow/underflow", o, dt, t);

        switch (s) {
            case 0:
                return switch (o) {
                    case PROD -> EmptyProduct;
                    case CONJ -> True;
                    default -> throw new TermException("unsupported 0-length subterms", o, dt, t);
                };
            case 1:
                Term t0 = t[0];
                return o.id == NEG.id ? neg(t0) : compound1New(o, t0);
            default:
                return compoundNew(o, dt, t);
        }
    }

    @Nullable
    private Term[] pre(Term[] _t) {
        Term[] t = _t;
        int s = t.length;
        for (int i = 0; i < s; i++) {
            Term x = t[i];
            if (x instanceof Compound) {
                Term y = imageNormalize((Compound) x, this);
                if (y != x) {
                    if (y == Null)
                        throw new TermTransformException("image normalize failure", x, y); //HACK
                    if (t == _t) t = _t.clone(); //TODO make unnecessary
                    t[i] = y;
                }
            } else {
                if (x == Null) return null; //try to detect earlier
            }
        }
        return t;
    }

    public final Term neg(Term u) {
        if (u instanceof Neg || u instanceof Bool)
            return u.neg();
        else if (u instanceof IntrinAtomic)
            return Intrin.neg(((IntrinAtomic)u).intrin());
        else
            return negNew(u);
    }


    /**
     * individual subterm resolution should be performed here
     *
     * @param t accepts the array and may modify it, as well as use it in the final output.
     *          so in other cases the array may need to be cloned (.arrayClone() not .arrayShared())
     */
    public final Subterms subterms(Term... t) {
        return t.length == 0 ? EmptySubterms : subtermsNew(t);
    }


    public final Term conj(Term... u) {
        return conj(DTERNAL, u);
    }

    public final Term conj(Subterms u) {
        return conj(DTERNAL, u);
    }

    public final Term conj(int dt, Subterms u) {
        return u.subs() == 1 ? u.sub(0) : conj(dt, array(u));
    }

    public final Term conj(int dt, Term... u) {
        TmpTermList uu = conjSubs(dt, u);
        return uu == null ? Null : switch (uu.size()) {
            case 0 -> True;
            case 1 -> uu.getFirst();
            default -> _conj(dt, uu);
        };
    }

    /** HACK */
    private static final TmpTermList FalseTermList = new TmpTermList(False);

    /**
     * prepares subterms list for CONJ args, depending on DT that will connect them
     */
    @Nullable
    private TmpTermList conjSubs(int dt, Term[] x) {

        int trues = 0;
        for (Term t : x) {
            if (t instanceof Bool) {
                if (t == Null)
                    return null;
                if (t == False)
                    return FalseTermList;
                if (t == True)
                    trues++;
            } else if (!NAL.term.IMPL_IN_CONJ && t.IMPL())
                return null;
        }

        if (trues == x.length) return new TmpTermList();

        boolean parallel = Tense.parallel(dt);

        TmpTermList l = new TmpTermList(x.length - (parallel ? trues : 0));
        for (Term t : x) {
            if (parallel) {
                if (t == True) {
                    continue;
                } else if (dt == DTERNAL && t.CONJ() && t.dt() == DTERNAL) {
                    conjSubsInline(l, t);
                    continue;
                }
            }

            l.addImageNormalized(t, this);
        }

        if (parallel)
            l.commuteThis();

        if (dt == XTERNAL && l.size() == 1 && !(l.getFirst() instanceof Bool) && x.length > 1 && x[0].equals(x[1]))
            l.add(l.getFirst()); //repeat collapsed XTERNAL

        return l;
    }

    private void conjSubsInline(TmpTermList l, Term t) {
        Subterms tt = t.subtermsDirect();
        int s = tt.subs();
        l.ensureCapacityForAdditional(s, true);
        for (Term xx : tt)
            l.addImageNormalized(xx, this);
    }

    /**
     * assumes u is already sorted
     */
    protected Term _conj(int dt, TmpTermList u) {
        return switch (dt) {
            case DTERNAL, 0 ->
                ConjPar.parallel(dt, u, false, NAL.term.INH_BUNDLE, this);
            case XTERNAL -> conjXternal(u);
            default -> conjSeq(dt, u);
        };
    }

    private Term conjXternal(TmpTermList u) {
        if (u.size() < 2)
            throw new TermException("xConj n<2 subterms", CONJ, XTERNAL, u);

        return compound(CONJ, XTERNAL, false, u.arrayTake());
    }

    private Term conjSeq(int dt, TmpTermList u) {
        if (u.size() != 2)
            throw new TermException("sequence n!=2 subterms", CONJ, dt, u);

        return ConjSeq.conjAppend(u.get(0), dt, u.get(1), this);
    }

    public final Term inh(Term s, Term p) {
        return statement(INH, DTERNAL, s, p);
    }

    public final Term statement(Op o, int dt, Subterms u) {
        return statement(o, dt, array(u));
    }

    public final Term statement(Op o, int dt, Term... sp) {
        if (sp.length != 2) throw new TermException("requires 2 arguments", o, dt, sp);
        return statement(o, dt, sp[0], sp[1]);
    }

    private Term statement(Op o, int dt, Term S, Term P) {

        boolean impl = o == IMPL;

        if (impl && dt == DTERNAL)
            dt = 0; //HACK temporary normalize

        return statementNew(o, dt, imageNormalize(S), imageNormalize(P));
    }

    Term statementNew(Op o, int dt, Term S, Term P) {
        return Statement.statement(this, o, dt, S, P);
    }

    public final Term equal(Term x, Term y) {
//		return Equal.compute(null, x, y);
        if (x == y) return True;
//		if (x == False)
//			return y.neg();
//		else if (y == False)
//			return x.neg();
//		else if (x == True)
//			return y;
//		else if (y == True)
//			return x;

//		boolean negOneSide = false;
        if (x instanceof Neg && y instanceof Neg) {
            x = x.unneg();
            y = y.unneg();
            if (x == y) return True;
        }
//		} else if (y instanceof Neg) {
//			negOneSide = true;
//			y = y.unneg();
//		} else if (x instanceof Neg) {
//			negOneSide = true;
//			x = x.unneg();
//		} else {
//
//		}


        x = imageNormalize(x);
        y = imageNormalize(y);

        if (x.equals(y)) {
            return True;
        } else {
            //Term xy = Equal.compute(null, x, y);
            if (!x.hasVars() && !y.hasVars()) {
                //TODO better test for inner functors
                if (!x.hasAll(FuncBits) && !y.hasAll(FuncBits)) //TODO in case it can reduce by evaluation
                    return False;
            }
        }


//		int xy = x.compareTo(y);
//		if (xy == 0) return True; //negOneSide ? False : True; //equal
//		if (xy > 0) {
//			Term c = x; x = y; y = c; //swap for commute
//		}
//            @Nullable Term p = Equal.pretest(x, y);
//            return p != null ? p : theCommutive(equal, x, y);

//		boolean negOuter = false;
//		if (negOneSide) {
//			if (x instanceof Int || y instanceof Int) //TODO other constant types?
//				negOuter = true;
//			else {
//				x = x.neg();
//				if (x.compareTo(y) > 0) {
//					Term c = x; x = y; y = c; //swap again
//				}
//			}
//		}

        return compound(EQ, true, x, y);
    }

    public final Term compound(Op o, Term... s) {
        return compound(o, true, s);
    }

    private Term compound(Op o, boolean preCommute, Term... s) {
        return compound(o, DTERNAL, preCommute, s);
    }


    public static final class CompoundPostNormalizer implements Predicate<Term> {

        private final ShortByteHashMap counts;
        private boolean skipNext;
        private transient Subterms xx;

        private CompoundPostNormalizer(int v) {
            counts = new ShortByteHashMap(v);
            skipNext = false;
        }

        private CompoundPostNormalizer(Compound x, int v) {
            this(v);
            scan(x);
        }

        public static Term normalize(Compound x) {
            int s = x.structure();
            if (!hasAny(s, NEG.bit)) return x;

            int v = x.vars();
            if (v == 0) return x;

            //TODO VAR_QUERY and VAR_INDEP, including non-0th variable id
            return new CompoundPostNormalizer(x, v).apply(x);
        }

        private void scan(Compound x) {
            x.recurseTermsOrdered(Termlike::hasVars, this, null);
        }

        @Override
        public boolean test(Term x) {
            if (skipNext) {
                skipNext = false;
                //this is the variable contained inside a Neg that was counted
            } else {
                byte b;
                if (x instanceof Neg) {
                    Term xu = x.unneg();
                    if (!(xu instanceof Variable)) return true;

                    skipNext = true;
                    x = xu;
                    b = -1;
                } else if (x instanceof Variable) {
                    b = +1;
                } else
                    return true;

                counts.addToValue(((NormalizedVariable) x).intrin(), b);
            }
            return true;
        }

        @Nullable
        private Task.VariableInverter invertVariables() {

            counts.keySet().removeIf(cc -> {
                byte c = counts.get(cc);
                if (c > 0)
                    return true; //positive dominant; dont change

                if (c == 0) {
                    //HACK better
                    //determine if the first appearance of the variable is pos or neg. if neg, then keep so it can be inverted
                    Term ccc = Intrin.term(cc);

                    boolean[] firstNeg = {false};
                    xx.ANDrecurse(tt -> tt.containsRecursively(ccc), (tt, zs) -> {
                        //first appearance is neg or non-neg?
                        if (tt == ccc) {
                            firstNeg[0] = zs instanceof Neg;
                            return false;
                        } else
                            return true;
                    }, null);

                    //first is negative, so keep to invert to normal form where neg is first
                    //pos is already first, do nothing
                    return firstNeg[0];
                }

                return false; //negative, keep to be inverted
            }); //keep only entries where more neg than positive. these will be flipped

            return counts.isEmpty() ? null : new Task.VariableInverter(counts);
        }

        private Term apply(Compound x) {
            int cs = counts.size();
            xx = x.subtermsDirect();
            if (cs > 0 && x.IMPL()) {
                //exclude the subject/pred of a statement as this can affect the semantics of the outer compound
                Term implPred = xx.sub(1);
                if (implPred instanceof Variable) {
                    counts.remove(((NormalizedVariable) implPred).intrin());
                    cs = counts.size();
                }
            }

            if (cs > 0) {
                Task.VariableInverter f = invertVariables();
                if (f != null) {
                    Term u = ((TermTransform) f).apply(x);
                    //fail if op changed; may be a negation caused by an implication predicate changing polarity; dont post-normalize
                    return u.opID() == x.opID ? u : x;
                }
            }

            return x;
        }
    }

    /**
     * un-permute terms to canonical subterm sorting for maximizing instance sharing
     *
     * for extra memory savings by wrapping non-canonically sorted and semi-inverted Subterms
     * 
     * @noinspection ArrayEquality*/
    public final Subterms unpermute(Term[] x, boolean dedup) {

        if (x.length == 0) return EmptySubterms;

        if (Intrin.intrin(x))
            return new IntrinSubterms(x);

        Term a = x[0];
        switch (x.length) {
            case 1:
                return subtermsNew(x);

            case 2: {
                Term b = x[1];
                if (!(a instanceof Neg) && !(b instanceof Neg))
                    return unpermute2(x, this, dedup, a, b);

                break;
            }
        }

        Term[] y = x;
        boolean hadNegs = false;
        for (int j = 0; j < y.length; j++) {
            if (y[j] instanceof Neg) {
                if (y==x)
                    y = x.clone();
                y[j] = y[j].unneg();
                hadNegs = true;
            }
        }

        if (dedup)
            y = Terms.commute(y);
        else {
            if (y==x)
                y = Terms.sort(y);
            else
                Arrays.sort(y);
        }

        //already sorted and has no negatives
        //TODO if (xx.length == 1) return RepeatedSubterms.the(xx[0],x.length);
        if (!hadNegs && ArrayUtil.equalsIdentity(x, y))
            return subtermsNew(x);
        else
            return arraySubterms(x, subtermsNew(y));
    }

    private static Subterms unpermute2(Term[] x, TermBuilder B, boolean dedup, Term a, Term b) {
        int i = a.compareTo(b);
        if (dedup && i == 0)
            return B.subtermsNew(new Term[] {a});
        else if (i <= 0)
            return B.subtermsNew(x);
        else
            return B.subterms(b, a).reversed();
    }

    private static ArrayRemappedSubterms arraySubterms(Term[] x, Subterms base) {

        int n = x.length;
        byte[] m = new byte[n];

        int hash = 1;         //int hash = Subterms.hash(target);
        for (int i = 0; i < n; i++) {
            Term xx = x[i];

            hash = Util.hashCombine(hash, xx); //TODO defer to after unwrap, then virtually negate the hashed term after

            boolean neg = (xx instanceof Neg);

            Term xi = neg ? xx.unneg() : xx;

            int mi = base.indexOfInstance(xi)+1;

            if (mi <= 0)
                return missing(base, xi);

            m[i] = (byte) (neg ? -mi : mi);
        }

        return new ArrayRemappedSubterms(base, m, hash);
    }

    private static ArrayRemappedSubterms missing(Subterms base, Term xi) {
        throw new WTF
                //TermException
                (
                        xi + " not found in " + base + ", base.class=" + base.getClass() + " target.xi.class=" + xi.getClass());
    }
}