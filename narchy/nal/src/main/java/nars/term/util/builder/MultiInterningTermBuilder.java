package nars.term.util.builder;

import jcog.WTF;
import jcog.data.map.BucketByteMap8;
import jcog.data.map.LazyMap;
import jcog.memoize.byt.ByteKey;
import jcog.memoize.byt.ByteKeyExternal;
import jcog.signal.meter.SafeAutoCloseable;
import nars.Op;
import nars.Term;
import nars.subterm.ByteCachedSubterms;
import nars.subterm.Subterms;
import nars.subterm.TmpTermList;
import nars.term.Compound;
import nars.term.util.cache.Intermed;

import java.util.function.Function;

import static nars.Op.*;

public class MultiInterningTermBuilder extends InterningTermBuilder {


    private final Function<ByteKeyExternal, InternedArraySubterms> memo;

    public MultiInterningTermBuilder(TermBuilder _builder) {
        this(_builder, atomCapacityDefault, compoundCapacityDefault, volMaxDefault);
    }

    private MultiInterningTermBuilder(TermBuilder _builder, int atomCap, int compoundCap, int compoundVolMax) {
        super(_builder, atomCap, compoundVolMax);

        memo = memoizer(InternedArraySubterms.class.getSimpleName(), j ->
                        new InternedArraySubterms(
                                j instanceof Intermed.InternedTermArray ?
                                        _subtermsNew(((Intermed.InternedTermArray) j).subs) :
                                        //new TmpTermList(((Intermed.InternedTermArray) j).subs) :

//                    subtermBuilder.apply(array(((Intermed.SubtermsKey) j).subs))
                                        ((Intermed.SubtermsKey) j).subs
                                , j)
                , compoundCap);
    }

    @Override
    public Term compound1New(Op o, Term x) {
        if (internable(x)) {
            InternedArraySubterms c = _subtermsInterned(x);
            return c.data.computeIfAbsent((byte) (InternedArraySubterms.COMPOUND_1 + o.id), () ->
                    o.build(termBuilder, c.sub(0))
            );
        } else {
            return termBuilder.compound1New(o, x);
        }
    }

    @Override
    @Deprecated public final Term compoundNew(Op o, int dt, Term... t) {
        return compoundNew(o, dt, new TmpTermList(t));
    }

    @Override
    public Term compoundNew(Op o, int dt, Subterms subs) {

        if (internableTerms(subs)) {

            subs = _subtermsInterned(array(subs));

            if (internable(dt))
                return compoundNInterned(o, dt, (InternedArraySubterms) subs, false /*o!=CONJ*/ /* HACK */);

        } else {
            //TODO allow subtermBuilder to test if it's already in final form, or deduplicate if equal
            subs = _subtermsNew(array(subs));
        }

        if (subs instanceof InternedArraySubterms)
            subs = ((InternedArraySubterms)subs).ref; //HACK unwrap, shouldnt actually happen

        return termBuilder.compoundNew(o, dt, subs); //direct
    }

    @Override
    protected Term statementInterned(Op o, int dt, Term S, Term P) {
        Term[] sp;
        return  internable(dt) && internableTerms(sp = new Term[] { S, P }) ?
            compoundNInterned(o, dt, _subtermsInterned(sp), false) :
            termBuilder.statementNew(o, dt, S, P);
    }

    private Term compoundNInterned(Op o, int dt, InternedArraySubterms target, boolean direct) {
        return target.data.computeIfAbsent((byte) switch (dt) {
            case DTERNAL -> InternedArraySubterms.COMPOUND_N + o.id;
            case XTERNAL -> InternedArraySubterms.COMPOUND_N_XTERNAL + o.id;
            default -> throw new UnsupportedOperationException();
        }, direct ?
            () -> termBuilder.compoundNew(o, dt, target)
            :
            () -> o.build(termBuilder, dt, target)
                  //termBuilder.compound(o, dt, target)
        );
    }

    @Override
    protected Term conjIntern(int dt, Term[] u) {
        InternedArraySubterms s = _subtermsInterned(u);
        return s.data.computeIfAbsent(
                (byte) (((dt == DTERNAL) ? InternedArraySubterms.COMPOUND_N : InternedArraySubterms.COMPOUND_N_XTERNAL) + CONJ.id),
                () -> _conjNew(dt, new TmpTermList(s.ref)))
                ;
    }

    @Override
    public Compound normalize(Compound x, byte varOffset) {

        if (varOffset == 0 && x.dt() == DTERNAL && internable(x))
            return (Compound) subtermsInterned(x.subtermsDirect()).data.computeIfAbsent(
                    (byte) (InternedArraySubterms.NORMALIZE + x.opID), () ->
                            super.normalize(x, (byte) 0)
            );
        else
            return super.normalize(x, varOffset);
    }

    @Override
    public Term root(Compound x) {
        int dt = x.dt();
        return internable(dt) && internable(x) ?
                subtermsInterned(x.subtermsDirect()).data.computeIfAbsent(
                        (byte) (x.opID +
                            (dt==DTERNAL ? InternedArraySubterms.ROOT : InternedArraySubterms.ROOT_XTERNAL)),
                        () -> super.root(x)
                )
                :
                super.root(x);
    }

    private static boolean internable(int dt) {
        return dt == DTERNAL || dt == XTERNAL;
    }

    @Override
    protected Subterms subtermsInterned(Term[] t) {
        return _subtermsInterned(t).ref;
    }

    private InternedArraySubterms _subtermsInterned(Term... t) {
        return memo.apply(new Intermed.InternedTermArray(t));
    }

    private InternedArraySubterms subtermsInterned(Subterms t) {
        return memo.apply(new Intermed.SubtermsKey(t));
    }

    /**
     * TODO make this extends ArrayCachedSubterms
     */
    private static class InternedArraySubterms extends ByteCachedSubterms implements SafeAutoCloseable {

        private static final byte ops = (byte) values().length;

        //new BucketBucketByteMap<>(8, 8, EmptyTermArray);
        //new CompactArrayMap()
        //new SynchroNiceByteMap(4);
        //new CompleteByteMap(new Object[maxOps]);
        private static final byte COMPOUND_N = 0;
        private static final byte COMPOUND_1 = (byte) (ops + COMPOUND_N);
        private static final byte COMPOUND_N_XTERNAL = (byte) (ops + COMPOUND_1);
        private static final byte ROOT = (byte) (ops + COMPOUND_N_XTERNAL);  //DTERNAL ONLY
        private static final byte ROOT_XTERNAL = (byte) (ops + ROOT);  //DTERNAL ONLY
        private static final byte NORMALIZE = (byte) (ops + ROOT_XTERNAL); //DTERNAL ONLY
        private static final int _maxOps = NORMALIZE + ops;

        //        private static final byte maxOps = (byte)_maxOps;
        static {
            //noinspection ConstantConditions
            if (_maxOps > Byte.MAX_VALUE) throw new WTF("problem in " + MultiInterningTermBuilder.class);
        }

        //final Subterms s;
        private final LazyMap<Byte, Term> data = new BucketByteMap8<>();

        private InternedArraySubterms(Subterms s, ByteKey k) {
            super(s, k.array());
        }

        @Override
        public void close() {
            data.clear();
        }


    }

}