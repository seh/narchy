package nars.term.util.builder;

import nars.Op;
import nars.Term;
import nars.subterm.*;
import nars.term.Neg;
import nars.term.anon.Intrin;
import nars.term.compound.CachedCompound;
import nars.term.compound.CachedUnitCompound;

/** megamorphic OCD heap builder, stateless implementation */
public class MegaHeapTermBuilder extends HeapTermBuilder {

    public static final MegaHeapTermBuilder the = new MegaHeapTermBuilder();


    protected MegaHeapTermBuilder() { }

    public final Subterms subtermsNew(Term[] t) {
        if (Intrin.intrin(t))
            return new IntrinSubterms(t);

        Term a = t[0];
        switch (t.length) {
//            case 0: throw new UnsupportedOperationException();

            case 1: return new UnitSubterm(a);

            case 2: {
                Term b = t[1];

                return
                  a instanceof Neg && b instanceof Neg ?
                    new BiSubterm(a.unneg(), b.unneg()).negated() :
                    //subterms(a.unneg(), b.unneg()).negated() :
                    new BiSubterm(t/*a, b*/);
            }

            default: {
//                //TODO Param.SUBTERM_BYTE_KEY_CACHED_BELOW_VOLUME
//                boolean different = false;
//                for (int i = 1; i < t.length; i++) {
//                    if (!t[i].equals(t[i-1])) { //if (t[i] != t[i - 1]) {
//                        different = true;
//                        break;
//                    }
//                }
//
//                return different ?
                return
                    new ArraySubterms(t);
//                  ;  new RemappedSubterms.RepeatedSubterms<>((a), t.length);
            }
        }
    }

    @Override
    public Term compound1New(Op o, Term x) {
        return new CachedUnitCompound(o, x);
    }

    @Override public Term compoundNew(Op o, int dt, Subterms subs) {
        return CachedCompound.the(o, dt, subs);
    }


}