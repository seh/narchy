package nars.term.util.transform;

import jcog.Util;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.Compound;
import nars.term.util.conj.ConjSeq;
import org.jetbrains.annotations.Nullable;

import static nars.Op.*;
import static nars.time.Tense.parallel;

/**
 * defines target -> concept mapping.
 * generally, terms name concepts directly except in temporal cases there is a many to one target to concept mapping
 * effectively using concepts to 'bin' together varieties of related terms
 */
public enum Conceptualization {
    ;

    /**
     * the standard conceptualization strategy.
     * chunks event sequence into super-events sharing no common conegated events
     * TODO rename. 'Hybrid' is no longer applicable.
     */
    public static final Untemporalize Hybrid = new Untemporalize() {

        @Override
        protected Term transformConjSeq(Compound x) {
//          {
//            Subterms xx = x.subtermsDirect();
//            y = CONJ.the(XTERNAL, xx.sub(0).root(), xx.sub(1).root());
//          }

            TermList xx = eventCollector(x, false, false);

            xx.replaceAll(this);

            int s = xx.subs();

            if (s > 2) xx.reverseThis(); //HACK put sequence start outside

            Term y;
            if (/*!flatten() ||*/ s > 2 || ((x.structureSubs() & CONJ.bit) != 0) || (s == 2 && xx.sub(0).equalsPN(xx.sub(1)))) {
                y = ConjSeq.xSeq(xx);
            } else {
                y = CONJ.the(XTERNAL,(Subterms) xx);
            }
            xx.delete();

            if (x!=y)
                y = Util.maybeEqual(y, x);

            return y;
        }


    };
    public abstract static class Untemporalize extends Retemporalize {

        protected abstract Term transformConjSeq(Compound x);

        @Override
        protected final Term applyConj(Compound x) {
            return parallel(x.dt()) ?
                    transformConjPar(x) : transformConjSeq(x);
        }

        private static Term transformConjPar(Compound x) {
            Subterms xx = x.subterms();
            Subterms yy = transformSubs(xx);
//            if (yy == xx) return x;

            int xdt = x.dt();
            int ydt = conjParDT(yy, xdt);

            if (xdt == ydt && xx==yy)
                return x;

            Term y = CONJ.the(ydt, yy);

            if (x!=y && xdt == ydt)
                y = Util.maybeEqual(y, x);

            return y;
        }

        private static int conjParDT(Subterms yy, int xdt) {
            return yy.subs() == 2 && (xdt == XTERNAL || yy.seqDur(true) == 0) ?
                    XTERNAL : DTERNAL;
        }

        @Nullable private static Subterms transformSubs(Subterms xx) {
            if (xx.hasAny(Op.Temporals /*Compounds & ~NEG.bit*/))
                return transformSubs(xx, /*this*/Term::root);
            else
                return xx;
        }

        @Override
        protected Term applyImpl(Compound x) {
            Subterms xx = x.subterms();
            Subterms yy = transformSubs(xx);
            return x.dt() == XTERNAL && xx.equals(yy) ? x :
                    IMPL.the(XTERNAL, yy);
        }

    }

    static TermList eventCollector(Compound x, boolean dE, boolean dX) {
        var y = new TermList(3);
        x.conds(z->y.add(z.root()), dE /* HACK */, dX);
        return y;
    }

}