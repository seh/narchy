package nars.term.util;

import jcog.Hashed;
import nars.Term;
import nars.term.Neg;

import java.util.function.Predicate;

/**
 * TODO subclass for Neg
 */
public class TermEquality implements Predicate<Term> {

    private final Term x;
    private final int opUnneg;
    private final boolean neg;

    /** lazily computed */
    private int xHash;
    //        private final Class<? extends Term> c;

    public TermEquality(Term x) {
        this.x = x;
        this.neg = x instanceof Neg;
        this.opUnneg = xUnneg().opID();
    }

    private Term xUnneg() {
        return neg ? x.unneg() : x;
    }

    private int hashX() {
        int h = this.xHash;
        return h == 0 ? (this.xHash = xUnneg().hashCode()) : h;
    }

    @Override
    public boolean test(Term y) {

        Term x;
        if (neg) {
            if (!(y instanceof Neg))
                return false;
            x = this.x.unneg();
            y = y.unneg();
        } else {
            if (y instanceof Neg)
                return false;
            x = this.x;
        }

        if (x == y)
            return true;

        return opUnneg == y.opID()
               &&
               eqHash(y)
               &&
               x.equals(y);
    }

    private boolean eqHash(Term y) {
        return !(y instanceof Hashed)
                ||
                hashX() == y.hashCode();
    }
}