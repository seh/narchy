package nars.term.util.transform;

import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.subterm.TmpTermList;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.Termlike;
import nars.term.atom.Atomic;
import nars.term.buffer.TermBuffer;
import nars.term.compound.LightCompound;
import nars.term.compound.LightDTCompound;
import nars.term.util.builder.TermBuilder;
import nars.time.Tense;
import org.jetbrains.annotations.Nullable;

import java.util.function.UnaryOperator;

import static nars.Op.*;
import static nars.term.atom.Bool.*;


/**
 * I = input target type, T = transformable subterm type
 */
public abstract class RecursiveTermTransform implements TermTransform {

    TermBuilder builder =
        terms
        //SimpleHeapTermBuilder.the
        //LightHeapTermBuilder.the
        ;

    private static Term evalInhSubs(Subterms inhSubs) {
        Term p = inhSubs.sub(1); /* pred */
        if (p instanceof InlineFunctor) {
            Term s = inhSubs.sub(0);
            if (s.PROD())
                return ((InlineFunctor) p).applyInline(s.subtermsDirect());
        }
        return null;
    }

    /** apply any shifts caused by internal range changes (ex: True removal) */
    private static int realign(int ydt, Subterms xx, Subterms yy) {
        if (xx == yy) {
            //Util.nop();
        } else if (xx.subs() == 2 && yy.subs() == 2) {
            int d = seqDurDiff(xx, yy, 0) + seqDurDiff(xx, yy, 1);
            if (d!=0)
                return (ydt==DTERNAL ? 0 : ydt) + d;
        }

        return ydt;
    }

    /** TODO what if order is reversed, then compare sub against 1-sub */
    private static int seqDurDiff(Subterms xx, Subterms yy, int sub) {
        Term x = xx.sub(sub);
        Term y = yy.sub(sub);

//        if (x.unneg().dt()==XTERNAL || y.unneg().dt()==XTERNAL)
//            return 0; //HACK special case.  may need to check for recursively contained XTERNAL

        if (x == y) return 0;

        int xd = x.seqDur(true);
        if (xd == XTERNAL)
            return 0; //??
        int yd = y.seqDur(true);
        if (yd == XTERNAL)
            return 0; //??
        return xd - yd;
    }

    /**
     * returns 'x' unchanged if no changes were applied,
     * returns 'y' if changes
     * returns null if untransformable
     * <p>
     * superOp is optional (use ATOM as the super-op to disable its use),
     * providing a hint about the target operator the subterms is being constructed for
     * this allows certain fail-fast cases
     */
    public static @Nullable Subterms transformSubs(Subterms x, UnaryOperator<Term> f) {

        TermList y = null;

        int s = x.subs();

        for (int i = 0; i < s; i++) {

            Term xi = x.sub(i);

            Term yi = f instanceof RecursiveTermTransform r ?
                r.applyInner(xi) : //HACK this seems necessary.
                f.apply(xi);

            if (yi == Null)
                return null; //short-circuit

            if (y == null) {
                if (xi!=yi) {
//                    if (xi instanceof Compound && xi.equals(yi))
//                        Util.nop(); //TEMPORARY
                    y = new TmpTermList(s, i);
                }
                //else: why?
            }

            if (y != null)
                y.addFast(yi);
        }

        return y == null ? x : y.fill(x);
    }


    public final Term apply(Term x) {
        return x instanceof Neg ? applyNeg(x) : applyInner(x);
    }

    private static Term post(Term y) {
        if (y instanceof LightCompound || y instanceof LightDTCompound) {
            Subterms ys = y.subtermsDirect();
            Subterms zs = ys.transformSubs(RecursiveTermTransform::post);
            return y.op().the(y.dt(), zs);
        }

        return y;
    }

    private Term applyInner(Term x) {
        Term y = (x instanceof Compound c) ?
            applyCompound(c)
            :
            applyAtomic((Atomic) x);
        return y == x ? x : post(y);
    }

    protected Term applyAtomic(Atomic a) {
        return a;
    }

    protected Term applyCompound(Compound x) {
        return x instanceof Neg ? applyNeg(x) : applyCompound(x, x.dt());
    }

    protected final Term applyCompound(Compound x, int yDt) {
        return applyCompoundInline(x, null, yDt);
    }

    private Term applyNeg(Compound x) {
        Term xu = x.unneg();
        Term yu = xu instanceof Compound ? applyCompound((Compound)xu) : applyAtomic((Atomic)xu);
        return yu == xu ? x : yu.neg();
    }


//    public RecursiveTermTransform buffered() {
//        return new RecursiveTermTransform() {
//            @Override
//            protected Term applyAtomic(Atomic a) {
//                return RecursiveTermTransform.this.applyAtomic(a);
//            }
//
//
//            @Override
//            protected Term _applyCompound(Compound x, int yDt) {
//                return applyCompoundBuffered(x);
//            }
//
//        };
//    }

    /** TODO test */
    public Term applyCompoundBuffered(Compound x) {
        final TermBuffer b = new TermBuffer();
        b.append(x, xx -> {
            if (xx instanceof Atomic) return applyAtomic((Atomic)xx);
            return xx;
        });
        return b.term();
    }

//    /** TODO test */
//    public Term applyCompoundUnverified(Compound x, Op yOp, int yDt) {
//        Subterms xx = x.subtermsDirect();
//        Subterms yy = transformSubs(xx, this);
//        boolean xEqY = xx == yy;
//
//        if (xEqY && x.opID==yOp.id && x.dt()==yDt)
//            return x; //unchanged
//
//        if (yy instanceof TmpTermList)
//            yy = new TermList(((TmpTermList)yy).arrayTake());
//
//        return yy!=null ? //Unverified.verify(
//            new UnverifiedCompound(yOp.id,
//                dtAlign(yOp, yDt, xx, yy, xEqY),
//                yy)
//         : Null;
//    }

    private Term applyCompoundInline(Compound x, @Nullable Op yOp, int yDt) {

        Subterms xx = x.subtermsDirect();

        Subterms yy = transformSubs(xx, this);
        if (yy == null)
            return Null;

        if (yOp == null) yOp = x.op();

        switch (yOp) {
            case CONJ -> {
                if (xx != yy) {
                    Termlike yyy = reduceConj(yy, yDt);
                    if (yyy instanceof Term)
                        return (Term) yyy;

                    yy = (Subterms) yyy;
                }
            }
            case INH -> {
                if (evalInline()) {
                    Term v = evalInhSubs(yy);
                    if (v != null)
                        return v;
                }
            }
        }

        try {
            boolean xEqY = yy == xx;

            if (xEqY && x.opID == yOp.id && x.dt() == yDt) return x; //no change

            int yDt2 = dtAlign(yOp, yDt, xx, yy, xEqY);

            return yOp.build(builder, yDt2, yy);

        } finally {
            if (yy instanceof TmpTermList)
                ((TmpTermList) yy).delete(); //ensure deleted
        }
    }

    private static Termlike reduceConj(Subterms yy, int yDt) {
        switch (yy.subs()) {
            case 0: return True;
            case 1: return yy.sub(0);
            default:
                if (yy.containsInstance(False))
                    return False;
                if (Tense.parallel(yDt)) { //parallel reduction
                    if (((TermList) yy).removeInstances(True)) {
                        switch (yy.subs()) {
                            case 0: return True;
                            case 1: return yy.sub(0);
                        }
                    }
                }
                break;
        }
        return yy;
    }

    protected int dtAlign(Op yOp, int yDt, Subterms xx, Subterms yy, boolean xEqY) {
        if (yOp.temporal) {
            if (!xEqY && yDt != XTERNAL) yDt = realign(yDt, xx, yy);
            if (yDt == 0) yDt = DTERNAL; //HACK
            return yDt;
        } else {
            assert(yDt == DTERNAL);
            return DTERNAL;
        }
    }


    /**
     * enable for inline functor evaluation
     *
     * @param args
     */
    public boolean evalInline() {
        return false;
    }

    protected final Term applyNeg(Term/*Neg*/ x) {
        Term xu = x.unneg();
        Term yu = applyInner(xu);
        return yu==xu ? x : yu.neg();
    }

}