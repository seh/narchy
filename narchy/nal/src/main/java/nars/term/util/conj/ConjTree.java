package nars.term.util.conj;

import jcog.TODO;
import jcog.WTF;
import jcog.data.list.Lst;
import nars.NAL;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.subterm.TmpTermList;
import nars.term.Compound;
import nars.term.Neg;
import nars.term.atom.Bool;
import nars.term.compound.LightCompound;
import nars.term.util.TermTransformException;
import nars.term.util.builder.TermBuilder;
import org.eclipse.collections.api.RichIterable;
import org.eclipse.collections.api.iterator.LongIterator;
import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.api.tuple.primitive.IntObjectPair;
import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.eclipse.collections.impl.map.mutable.primitive.ObjectIntHashMap;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.UnaryOperator;

import static java.util.Collections.EMPTY_SET;
import static nars.Op.*;
import static nars.term.atom.Bool.*;
import static nars.time.Tense.occToDT;

/**
 * conj tree node
 */
public class ConjTree implements ConjBuilder, AutoCloseable {

    private boolean inhBundle = true;
    private IntObjectHashMap<ConjTree> seq;
    private Set<Term> pos = EMPTY_SET, neg = EMPTY_SET;
    private Term terminal;
    private long shift = TIMELESS;

    private static MutableSet<Term> _newSet() {
        //return new TreeSet<>();
        return new UnifiedSet<>(1);
        //    return new ArrayHashSet<>(4);
        //return new LinkedHashSet<>(4, 0.99f);
//        return new HashSet<>(2);
        //new ArrayUnenforcedSet<>(0, new Term[1]); //TODO try
    }

//    private static boolean subsumesNN(Compound c, Term x, boolean eternal) {
//        if (eternal || !c.SEQ()) {
//            return c.eventOf(x);
//        } else {
//            return Conj.firstEvent(c).equals(x);
//        }
//    }

    private static Set<Term> _add(Set<Term> s, Term t) {
        //assert(!(t instanceof Neg));
        if (!(s instanceof MutableSet) && s.isEmpty())
            s = _newSet();
        s.add(t);
        return s;
    }

    private static Term first(Collection<Term> s) {
        if (s instanceof UnifiedSet)
            return ((UnifiedSet<Term>) s).getFirst();
        else if (s instanceof TreeSet)
            return ((TreeSet<Term>) s).first();
        else
            return s.iterator().next();
    }

    static boolean complexPar(TmpTermList PN) {
        boolean hasP = false, hasN = false;
        for (Term x : PN) {
            if (x instanceof Neg) hasN = true;
            else hasP = true;

            if ((hasN && hasP) || x.unneg().CONJ()) //not .CONDS()
                return true;
        }
        return false;
    }

    private boolean addParallel(Term x) {

        assert(terminal==null); //if (terminal != null) throw new TermException(ConjTree.class.getSimpleName() + " already terminated in addParallel");

        if (x == True)
            return true;
        else if (ends(x)) {
            end(x);
            return false;
        } else
            return result(x instanceof Neg ? addParallelN(x) : addParallelP(x));
    }

    private boolean addParallelNeg(Term x) {
        return addParallel(x.neg());
    }

//    private @Nullable Term reinsertNN(Term nx, Lst<Term> toAdd, long nxshift) {
//        //add at shifted time
//        if (nxshift == ETERNAL || nxshift == 0) {
//            //continue, adding at present parallel time
//        } else {
//            if (!addEvent(nxshift, nx.neg()))
//                return False;
//            else {
//                assert(toAdd == null): "TODO";
//                return True;
//            }
//        }
//        return null;
//    }

    private boolean addParallelP(Term p) {
        assert (!(p instanceof Neg));

//        if (p.CONJ()) {
//            Compound P = (Compound) p;
//            if (p.dt() != XTERNAL && !p.SEQUENCE()) {
//                return P.AND(this::addParallel); //decompose parallel conj
//            }
//        }

        if (!neg.isEmpty()) {
            p = reducePN(p, neg);
            if (p instanceof Neg)
                return addParallelN(p); //became negative
            if (p == False) return false;
            if (p == True) return true; //absorbed
            if (p == Null) return false; /*conflict */
        }

        posAdd(p);
        return true;
    }

    private boolean addParallelN(Term _n) {
        assert (_n instanceof Neg);
        Term n = _n.unneg();

        if (neg.contains(n))
            return true;

        n = reducePN(n, pos);
        if (n instanceof Neg)
            return addParallelP(n.unneg()); //became positive


        if (!(n instanceof Bool)) {
            n = reduceNN(n, true);
            if (n instanceof Neg)
                return addParallelP(n.unneg()); //became positive
            if (n == False) return true; //absorbed

        }


        if (n == True)
            return true; //absorbed
        else if (ends(n)) {
            end(n);
            return false; /*conflict */
        }

        negAdd(n);
        return true;
    }

    private Term reduceNN(Term x, boolean eternal) {
        //assert (nx.op() != NEG);
        if (neg.isEmpty()) return x;

        Lst<Term> toAdd = null;


        outer: for (Iterator<Term> nyi = neg.iterator(); nyi.hasNext(); ) {
            Term yy = nyi.next();
            if (x.equalsNeg(yy))
                return end(False); //eliminated

            if (x.CONDS() && yy.CONDS()) {
                //TODO combine with other conditions in this method
                //TODO correct seq handling
                try (var xxe = ConjList.conds(x, 0, true, true, true)) {
                    //var yye = ConjList.events(yy, 0, true, true, true);
                    //TODO better symmetric intersection
                    if (xxe.OR(xxxe ->
                            //yye.contains(xxxe)
                            ((Compound) yy).condOf(xxxe)
                    ))
                        return end(False);
                }
            }

            if (x.CONDS() && possiblyInterferes(x, yy)) {
                Compound X = (Compound) x;


                if (X.condOf(yy)) //if (subsumesNN(X, yy, true)) // Conj.eventOf(nxc, ny))
                    return yy; //HACK reduce nx to ny, so that it can be added at the correct sequence position.  for parallel this isnt needed and could return True?
                else if (X.condOf(yy,-1)) {
                    //return True; //HACK this gets inverted to False by callee
                    Term x2 = CondDiff.diffAll(X, yy.neg());
                    if (x2 == True)
                        return end(False); //elmimnated HACK TODO this is confusing; masks boolean override signal
                    x = x2;
                    if (!(x2 instanceof Compound)) {
                        break outer;
                    } else {
                        X = (Compound) x;
                    }

                }

                //HACK check forany X subterms which are || that contain y, and if so, eliminate that subterm
                //TODO apply to inhbundles, not just CONJ
                if (innerDisj(X, yy)) {
                    x = eventTransform(X, new ConjEliminator(yy, false));
                    continue;
                }

            }
            if (yy.CONDS() && possiblyInterferes(yy, x)) {
                Compound nyc = (Compound) yy;
//                if (subsumesNN(nyc, x, eternal)) {
//                    //subsume more general negated condition
//                    nyi.remove();
//                    continue;
//                }

                if (eternal && nyc.dt() != XTERNAL && nyc.condOf(x, -1)) {
                    //prune
                    nyi.remove();
                    Term z = CondDiff.diffAll(yy, x.neg());
                    if (z == True) {
                        //eliminated y
                    } else if (ends(z)) {
                        throw new TermTransformException("reduceNN fault: Conj.diffAll(" + yy + "," + x.neg() + ")", yy, z);
//                        terminate(z);return z;
                    } else {
                        if (toAdd == null) toAdd = new Lst(1);
                        toAdd.add(z);
                    }
                }
            }


        }
        return toAdd != null && !toAdd.AND(this::addParallelNeg) ? False : x;
    }

    private static boolean innerDisj(Compound x, Term y) {
        return x.hasAll(CONJ.bit | NEG.bit) &&
               x.condsOR(z -> z instanceof Neg && z.unneg().CONJ/*EVENTS*/(), !(y.unneg().CONDS() && !y.SEQ()), y.dt()!=XTERNAL);
    }

    private static Term eventTransform(Compound x, UnaryOperator<Term> f) {
        if (x.SEQ()) {
            Term y;
            try (ConjList xx = ConjList.conds(x)) {
                y = xx.applyIfChanged(f);
            }
            return y == null ? x : y;
        } else {
            Subterms xx = x.subtermsDirect();
            Subterms yy = x.transformSubs(f);
            return xx == yy ? x : CONJ.the(x.dt(), yy);
        }
    }


    private Term reducePN(Term x, Collection<Term> y) {
        return reducePN(x, y, false);
    }
    private Term reducePN(Term x, Collection<Term> y, @Deprecated boolean elimMode) {
        if (y.isEmpty()) return x;

        //assert (x.op() != NEG);

        if (y.contains(x))
            return end(False); //contradiction

        for (Term yy : y) {
            if (yy instanceof Compound && ((Compound) yy).condOf(x))
                return end(False);

            //                    if (((Compound) yy).eventOf(x.neg()))
            //                        return ??
        }

        //TODO defer to ConjPar
        if (x.CONDS()) {
            Compound X = (Compound) x;
            outer: for (Term yy : y) {
                if (!possiblyInterferes(X, yy))
                    continue;

                if (X.condOf(yy, +1)) {
                    if (!elimMode) {
                        Term x2 = CondDiff.diffAll(X, yy);
                        if (x2 == True) //elmimnated
                            return end(False); //HACK this sucks; masks boolean override signal
                        x = x2;
                        if (!(x2 instanceof Compound)) {
                            break outer;
                        } else {
                            X = (Compound) x;
                        }
                    } else {
                        return end(False);
                    }
                }
                if (X.condOf(yy, -1)) {
                    //invert component HACK
                    //is this correct?
                    return yy.neg();
//                    if (xy == null) xy = new TmpTermList(1);
//                    xy.add(yy);
                    //return True;
                }

                //if (X.hasAll(yy.unneg().structure() & ~CONJ.bit)) {//!X.impossibleSubTerm(yy.unneg()))  {
                if (innerDisj(X, yy)) {
                    Term Xnext = eventTransform(X, new ConjEliminator(yy, true));
                    if (!(Xnext instanceof Compound)) {
                        //early exit
                        x = Xnext;
                        break outer;
                    } else {
                        X = (Compound) (x = Xnext);
                    }
                }
            }

        }

        return x;
    }

    private static boolean possiblyInterferes(Term X, Term yy) {
        return X.hasAll(yy.unneg().structure() & ~CONJ.bit);
    }

    private Term end(Term t) {
        Term x = terminal;
        if (t == Null) {
            x = terminal = Null;
        } else if (t == False) {
            if (x != Null)
                x = terminal = False;
        } else
            throw new WTF();
        return x;
    }

    @Override
    public final boolean addEvent(long at, Term x) {
        return at == ETERNAL ? addParallel(x) : addAt(at, x);
    }

    /**
     * this can not eliminate matching parallel pos/neg content until after a sequence is given a chance to be defined
     * otherwise it could erase the sequence before it even becomes factorable.
     */
    private boolean addAt(long at, Term x) {
        if (at == ETERNAL || at == TIMELESS || terminal != null)
            throw new UnsupportedOperationException();
        if (NAL.DEBUG && (at == DTERNAL || at == XTERNAL))
            throw new UnsupportedOperationException("probably leak");

        if (!(x instanceof Neg)) {
            /*
            dont do something like this, here: because we need the event as a placeholder in case the wrapping eternal condition is inlined
            if (pos.contains(x))                return true;
            */

            x = reducePN(x, neg, true);

        } else {
            Term _xu = x.unneg();
            Term xu = reducePN(_xu, pos, true);
            if (xu != _xu) {
                //TODO check polarity HACK
                if (xu == False) {
                    end(False);
                    return false;
                } //??
                x = xu.neg();
            }

            if (x instanceof Neg && !(xu instanceof Bool)) {
                Term xu2 = reduceNN(xu, false);
                if (xu2 != xu) x = xu2.neg();
            }
        }

        if (terminal != null) return false; //HACK
        if (x == True) return true; //absorbed
        if (ends(x)) {
            end(x);
            return false; /*conflict */
        }

        return _addAt(at, x);
    }

    private boolean _addAt(long at, Term x) {
        if (seq == null) seq = new IntObjectHashMap<>(1);
        ConjTree seq = this.seq.getIfAbsentPut(occToDT(at), ConjTree::new);

        int tsBefore = seq.size();
        boolean ok = seq.addParallel(x);
        if (ok && seq.size() > tsBefore)
            shift = TIMELESS; //invalidate because of change

        if (!ok && seq.terminal != null) {
            end(seq.terminal);
            return false;
        }

        return result(ok);
    }

    @Deprecated private boolean result(boolean result) {
        if (terminal!=null)
            return false;
        else if (!result) {
            terminal = False;
            return false;
        }
        else
            return true;
    }

    @Override
    public boolean remove(long at, Term t) {
        if (at == ETERNAL) {

            if (t instanceof Neg ? negRemove(t.unneg()) : posRemove(t)) {
                shift = TIMELESS;
                return true;
            }

        } else {
            int aat = occToDT(at);
            ConjTree s = seq.get(aat);
            if (s == null) return false;
            if (s.remove(ETERNAL, t)) {
                if (s.isEmpty()) {
                    s.close();
                    seq.remove(aat);
                    shift = TIMELESS; //invalidate
                    if (seq.isEmpty()) seq = null;
                }
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private void posAdd(Term t) {
        pos = _add(pos, t);
    }

    private void negAdd(Term t) {
        neg = _add(neg, t);
    }

    private boolean posRemove(Term t) {
        assert (!(t instanceof Neg));
        Set<Term> s = this.pos;
        if (s != EMPTY_SET) {
            /*if (!(s instanceof UnifiedSet)) {
                if (s.contains(t)) { //assert(s.size()==1);
                    pos = EMPTY_SET;
                    return true;
                }
            } else*/
            if (s.remove(t)) {
                if (s.isEmpty())
                    this.pos = EMPTY_SET;
                return true;
            }
        }
        return false;
    }

    private boolean negRemove(Term t) {
        Set<Term> s = this.neg;
        assert (!(t instanceof Neg));
        if (s != EMPTY_SET) {
            if (!(s instanceof UnifiedSet)) {
                if (s.contains(t)) {
                    /* assert(s.size()==1);*/
                    neg = EMPTY_SET;
                    return true;
                }
            } else if (s.remove(t)) {
                if (s.isEmpty()) this.neg = EMPTY_SET;
                return true;
            }
        }
        return false;
    }

    @Override
    public int eventOccurrences() {
        return ((!pos.isEmpty() || !neg.isEmpty()) ? 1 : 0) + (seq != null ? seq.size() : 0);
    }

    @Override
    public int eventCount(long when) {
        if (when == ETERNAL)
            return (pos.isEmpty() ? 0 : pos.size()) + (neg.isEmpty() ? 0 : neg.size());
        else {
            if (seq != null) {
                ConjTree s = seq.get(occToDT(when));
                if (s != null)
                    return s.size();
            }
            return 0;
        }
    }

    @Override
    public long shift() {
        if (shift == TIMELESS)
            shift = (seq == null || seq.isEmpty()) ? (!pos.isEmpty() || !neg.isEmpty() ? ETERNAL : TIMELESS) :
                    //seq.keysView().min();
                    seq.keySet().min();

        return shift;
    }

    @Override
    public boolean removeAll(Term term) {
        boolean removed = removeParallel(term);

        if (seq != null) {
            if (!removed)
                removed = seq.countWith(ConjTree::removeAll, term) > 0;
        } else
            seq.forEachWith(ConjTree::removeAll, term);

        return removed;
    }

    private boolean removeParallel(Term term) {
        if (terminal != null)
            throw new UnsupportedOperationException();

        return term instanceof Neg ? negRemove(term.unneg()) : posRemove(term);
    }

    @Override
    public void negateEvents() {
        throw new TODO();
    }

    private int size() {
        return pos.size() +
               neg.size() +
               (seq != null ? (int) seq.sumOfInt(ConjTree::size) : 0);
    }

    @Override
    public Term term(TermBuilder B) {
        if (terminal != null)
            return terminal;

        return seq != null && !seq.isEmpty() ? termSeq(B) : termPar(B);
    }

    /**
     * sequence component of the result
     */
    private Term termSeq(TermBuilder B) {
        Term s = _termSeq(B);
        if (s == null) s = True;

        shift(); //cache the shift before clearing seq
        seq = null;

        return terminal != null ? terminal : s;
    }

    @Override
    public void clear() {
        if (pos != EMPTY_SET) {
            pos.clear();
            pos = EMPTY_SET;
        }
        if (neg != EMPTY_SET) {
            neg.clear();
            neg = EMPTY_SET;
        }
        if (seq != null) {
            seq.forEachValue(ConjTree::clear);
            seq.clear();
            seq = null;
        }
        terminal = null;
        shift = TIMELESS;
    }

    /**
     * sequence component of the result
     */
    private Term termPar(TermBuilder B) {
        @Nullable TmpTermList PN = null;

        int pp = pos.size(), nn = neg.size();
        if (pp > 0) {
            if (nn == 0 && pp == 1) {
                return first(pos);
            } else {
                PN = new TmpTermList(pp + nn);
                PN.addAll(pos);
            }
        }
        if (nn > 0) {
            if (nn == 1 && pp == 0) {
                return first(neg).neg();
            } else {
                if (PN == null) PN = new TmpTermList(nn);
                if (neg.isEmpty())
                    return True;
                PN.addAllNeg(neg);
            }
        }

        return PN == null ? True :
                ConjPar._parallel(PN, inhBundle, B);
                //ConjPar.parallel(PN, DTERNAL,inhBundle, B);
    }

    /**
     * construct the sequence component of the Conj
     *
     * @param b
     */
    private @Nullable Term _termSeq(TermBuilder B) {

        int ss = seq.size();

        Term s;
        if (ss == 1) {
            //special case: degenerate sequence of 1 time point (probably @ 0)
            s = termSeq1(B);
        } else {
            boolean factor;
            if (ss==2 && seq.getFirst().term().equals(seq.getLast().term()))
                factor = false; //do not attempt
            else
                factor = NAL.term.CONJ_FACTOR;

            s = termSeqN(factor, B);
        }

        if (ends(s)) return end(s);

        if (pos.isEmpty() && neg.isEmpty())
            return s == null ? True : s;
        else
            return (s == null || s == True) || addParallel(s) ? termPar(B) : terminal;
    }

    private Term termSeqN(boolean factor, TermBuilder B) {

//        Term pn;
//        if (distribute) {
//            pn = drainPN(B);
//            if (ends(pn)) return end(pn);
//            if (pn == True) pn = null;
//        } else {
//            pn = null;
//        }

        //eternal ambients
        Collection<Term> e = factor ? new UnifiedSet() : new Lst();
        drainEte(e);
        if (!factor && e.isEmpty()) e = null;


        int n = seq.size();

        ObjectIntHashMap<Term> counts = null, singles = null;
        ConjList s = new ConjList(n);
        TermList y = new TermList();
        for (IntObjectPair<ConjTree> wc : seq.keyValuesView()) {

            try {

                ConjTree wct = wc.getTwo(); assert(wct.seq == null);
                wct.drainEte(y);
                wct.close();

                //TODO special case if y.size()==1, dont need to create a set

                Term z;
                if (factor) {
                    if (y.size() == 1) {
                        z = y.sub(0);
                        if (z instanceof LightCompound)
                            throw new UnsupportedOperationException("TODO conflict with LightCompound use"); //HACK TODO

                        if (singles == null) singles = new ObjectIntHashMap(n);
                        singles.addToValue(z, +1);
                        if (counts == null) counts = new ObjectIntHashMap(n);
                        counts.addToValue(z, +1);
                    } else {

                        for (Term yy : y) {
                            if (counts == null) counts = new ObjectIntHashMap<>(n);
                            counts.addToValue(yy, +1);
//                            if (!e.isEmpty()) {
//                                if (e.contains(yy))
//                                    continue; //skip; already ambient
////                                if (e.contains(yy.neg()))
////                                    return end(False); //conflict
//                            }
                        }
                        z = new LightCompound(SETe, new TermList((Collection)y)/*clone*/); //HACK
                    }
                } else {
                    if (e != null) y.addAll(e);
                    z = B.conj(y); if (ends(z)) return end(z);
                }

                if (z != True)
                    s.addDirect(wc.getOne(), z);
            } finally {
                y.clear();
            }
        }

        if (factor) {
            _factor(B, e, n, counts, singles, s);

            if (s.containsInstance(Null)) return end(Null);
            if (s.containsInstance(False)) return end(False);
            s.removeInstances(True);

        } else {
            if (e!=null)
                e.clear();
        }


        if (!s.isEmpty()) {
            Term S = s.seq(B); if (ends(S)) return end(S);
            if (e == null || e.isEmpty())
                return S;
            e.add(S);
        }


        Term[] ee = e.toArray(EmptyTermArray);
        var SS = ee.length > 1 ? B.compound(CONJ, ee) : ee[0];
        if (ends(SS)) return end(SS);
        return SS;
    }


    private void drainEte(Collection<Term> each) {
        boolean pp = !pos.isEmpty(), nn = !neg.isEmpty();

        var neg = this.neg; this.neg = EMPTY_SET;
        var pos = this.pos; this.pos = EMPTY_SET;

        if (each instanceof Lst)
            ((Lst)each).ensureCapacity(pos.size()+neg.size());

        if (nn) {
            if (each instanceof TermList){
                ((TermList)each).addAllNeg(neg);
            } else {
                for (Term x : neg) each.add(x.neg());
            }
        }
        if (pp) each.addAll(pos);
    }


    private static void _factor(TermBuilder B, Collection<Term> e, int n, ObjectIntHashMap<Term> counts, ObjectIntHashMap<Term> singles, ConjList s) {

//        {
//            int ss = singles!=null ? singles.size() : 0;
//            if (n == ss && ((counts!=null ? counts.size() : 0) == ss))
//                return; //all unique
//        }

        if (counts != null) {
            for (var z : counts.keyValuesView())
                if (z.getTwo()== n) e.add(z.getOne());
        }

        if (singles != null && !e.isEmpty()) {
            for (var z : singles.keyValuesView())
                if (z.getTwo()== n)
                    e.remove(z.getOne());
        }

        Collection<Term> E = e.isEmpty() ? null : e;
//        if (e!=null && (e.size() == (counts!=null ? counts.size() : 0))) {
//            //would totally erase
//        } else {
////            if (E != null) {
////                for (Term c : s) {
////                    if (!(c instanceof LightCompound)) //HACK
////                        continue;
////
////                    ((TermList) (c.subterms())).removeIf(e::contains);
////                }
////            }
//        }

        //finally, convert each sequence component to CONJ
        s.replaceAll(c -> {
            //TODO if c is neg?

            Term d;
            if (c instanceof LightCompound) {
                assert(c.SETe());
                var kk = (TermList)c.subterms();
                kk.removeIf(e::contains);
                int kkn = kk.size();
                d = (kkn == 0 ? True : (kkn == 1 ? kk.sub(0) : B.conj(kk)));
            } else
                d = c;

            if (E!=null) {
                if (d instanceof Neg) {
                    Term du = d.unneg();
//                    if (E.contains(du)) {
//                        return True;
                    if (du.CONJ()) {
                        if (du.dt()==DTERNAL || du.dt()==XTERNAL /* TODO seq? */) {
                            final Term du0 = du;
                            for (var ee : E)
                                du = CondDiff.diffAll(du, ee);
                            return du == du0 ? d : du.neg();
                        }
                        //return CONJ.the(Op.DTERNAL, du, ((UnifiedSet<Term>) E).getFirst().neg())
                    }
                } else {
                    if (E.contains(d))
                        return True;
                    //doesnt seem to happen:
                    //else if (E.contains(d.neg())) return False;
                    //assert(!E.contains(d.neg()));
                }


            }
            return d;
        });
        if (singles!=null) {
            //distribute when a single overlaps an eternal
            e.removeIf(ee -> {
                if (singles.containsKey(ee)) {
                    s.replaceAll(ss ->
                        ss == ee ? ee : CONJ.the(ss, ee));
                    return true;
                }
                return false;
            });
        }
    }

    private static boolean ends(Term z) {
        return z == False || z == Null;
    }





    @Nullable
    private Term termSeq1(TermBuilder B) {
        RichIterable<IntObjectPair<ConjTree>> skv = seq.keyValuesView();
        IntObjectPair<ConjTree> only = skv.getOnly();
        ConjTree x = only.getTwo();

        shift = skv.minBy(IntObjectPair::getOne).getOne();

        if (x.seq != null) {
            return x.term(B);
        } else {
            //flatten point
            return addAllAt(only.getOne(), x, B) ? null : terminal;
        }
    }

    private boolean addAllAt(int at, ConjTree x, TermBuilder B) {
        if (x.seq != null)
            return addAt(at, x.term(B));
        else {
            if (!x.pos.isEmpty()) {
                for (Term p : x.pos) {
                    if (!addParallel(p))
                        return false;
                }
            }
            if (!x.neg.isEmpty()) {
                for (Term term : x.neg)
                    if (!addParallelNeg(term))
                        return false;
                return true;
            }
        }
//        if (x.seq!=null) {
//
//            if (!x.seq.keyValuesView().allSatisfy(ww -> {
//                return addAt(ww.getOne() + at, ww.getTwo());
//            })) {
//                terminate(False);
//                return false;
//            }
//        }
        return true;
    }


//    private boolean addTo(TermList t, Term term) {
//        if (term == True)
//            return true;
//        else if (term == False || term == Null) {
//            terminate(term);
//            return false;
//        } else {
//
//            if (false) {
//                terminate(False);
//                return false;
//            } else {
//                t.add(term);
//                return true;
//            }
//        }
//    }

    @Override
    public LongIterator eventOccIterator() {
        throw new TODO();
    }


    @Override
    public void close() {
        clear();
    }


    Term _parallel(TmpTermList x, int dt, boolean inhBundle, TermBuilder B) {
        this.inhBundle = inhBundle;

        long sdt = dt == DTERNAL ? ETERNAL : 0;

        //MetalBitSet conjs = null;
        //add (non-conj/non-disj) terms first
        if (!this.takeSimple(x, sdt))
            return this.terminal;

        int n = x.size();

        Term y;
        if (n == 1 && isEmpty())
            y = x.getFirst();
        else {
            if (n > 0 && !takeComplex(x, sdt))
                return this.terminal;
            y = term(B);
        }

        return y;
    }

    private boolean takeSimple(TmpTermList t, long sdt) {
        for (Iterator<Term> tt = t.iterator(); tt.hasNext(); ) {
            Term x = tt.next();
            if (!x.unneg().CONJ()) {
                if (!add(sdt, x))
                    return false;

                tt.remove();
            }
        }
        return true;
    }

    private boolean takeComplex(TmpTermList t, long sdt) {
        int n = t.size();
        if (sdt == ETERNAL && t.sub(0).seqDur()!=0 && ((n == 1) || (n > 1 && sameRange(t))))
            sdt = 0; //time frame relative to the only sequence

        for (Term x : t) {
            if (!add(sdt, x))
                return false;
        }

        return true;
    }
    private static boolean sameRange(TmpTermList t) {
        int n = t.size();
        if (n > 1) {
            int r = -1;
            for (Term i : t) {
                int di = i.seqDur();
                if (r < 0) {
                    r = di; //initialize
                } else {
                    if (di != r)
                        return false;
                }
            }
        }
        return true;
    }

    private static class ConjEliminator implements UnaryOperator<Term> {
        private final Term yy;
        final boolean polarity;

        public ConjEliminator(Term yy, boolean polarity) {
            this.yy = yy;
            this.polarity = polarity;
        }

        @Override
        public Term apply(Term xx) {
            if (xx instanceof Neg) {
                Term xu = xx.unneg();
                if (xu.CONJ /*EVENTS*/()) {
                    if (((Compound) xu).condOf(yy, polarity ? -1 : +1))
                        return True; //eliminated
                    Term xu2 = CondDiff.diffAll(xu, yy.negIf(!polarity));
                    if (xu2 != xu)
                        return xu2.neg();
                }
            }
            return xx;
        }
    }
}