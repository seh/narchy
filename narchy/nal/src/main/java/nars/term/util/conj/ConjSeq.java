package nars.term.util.conj;

import nars.Op;
import nars.Term;
import nars.subterm.TmpTermList;
import nars.term.util.builder.TermBuilder;

import static nars.Op.*;
import static nars.term.atom.Bool.*;
import static nars.time.Tense.occToDT;

/**
 * utilities for working with conjunction sequences (raw sequences, and factored sequences)
 */
public enum ConjSeq {
    ;


    /**
     * constructs a correctly merged conjunction from a list of events, in the sublist specified by from..to (inclusive)
     * assumes that all of the event terms have distinct occurrence times
     *
     * @param leftIndex  start index, inclusive
     * @param rightIndex end index, exclusive
     */
    static Term seqBalanced(TermBuilder B, ConjList events, int leftIndex, int rightIndex) {

        long dt;
        Term left, right;
        switch (rightIndex - leftIndex) {
            case 0:
                throw new NullPointerException("should not be called with empty events list");
            case 1:
                return events.get(leftIndex);
            case 2: {
                left =  events.get(leftIndex);
                right = events.get(--rightIndex);
                long leftWhen = events.when(leftIndex);
                long rightWhen = events.when(rightIndex);
                dt = rightWhen - leftWhen - left.seqDur();
                break;
            }
            default: {
                int center =
                    ConjList.centerByIndex(leftIndex, rightIndex);
                    //events.centerByVolume(start, end);
                left = seqBalanced(B, events, leftIndex, center + 1);
                if (left == Null) return Null;
                if (left == False) return False;
                right = seqBalanced(B, events, center + 1, rightIndex);
                if (right == Null) return Null;
                dt = events.when(center + 1) - events.when(leftIndex) - left.seqDur();
                break;
            }

        }

        return conjSeq(B, occToDT(dt), left, right);
    }


    /**
     * conj sequence of temporally-atomic events
     */
    private static Term conjSeq(TermBuilder B, int dt, Term left, Term right) {
        //assert(x.length==2);
        if (left == Null || right == Null) return Null;

        if (left == False || right == False) return False;

        if (left == True) return right;
        if (right == True) return left;

        return __conjSeq(B, dt, left, right);
    }

    private static Term __conjSeq(TermBuilder B, int dt, Term left, Term right) {
        return dt == 0 || dt == DTERNAL ?
                B.conj(left, right) //parallel
                :
                _conjSeq(B, left, dt, right);
    }

    private static Term _conjSeq(TermBuilder B, Term a, int dt, Term b) {
        assert (dt != XTERNAL);

        int dt1 = dt;
        //assert(x.length==2);
        int c = a.compareTo(b);
        if (c > 0) {
            dt1 = -dt1;
            //swap order
            Term t = b;
            b = a;
            a = t;
        } else if (c == 0) {
            //equal.  sequence of repeating terms; share identity
            b = a;
            if (dt1 < 0)
                dt1 = -dt1;
        }

        return B.compoundNew(CONJ, dt1, a, b);
    }


    public static Term conjAppend(Term x, int dt, Term y, TermBuilder B) {
        return conj(x, dt, y, true, B);
    }

    /**
     * attaches two events together with dt separation
     */
    public static Term conj(Term x, int dt, Term y, boolean append, TermBuilder B) {

        if (x == Null || y == Null) return Null;
        if (x == False || y == False) return False;
        if (x == True) return y;
        if (y == True) return x;

        if (dt == XTERNAL || dt == DTERNAL)
            return B.conj(dt, x, y);

        int aRange = x.seqDur(), bRange = y.seqDur();

        int xToY;
        if (append) {
            long bStart;
            if (dt >= 0) {
                bStart = +dt + aRange;
            } else {
                bStart = -dt + bRange;
                Term ab = x;
                x = y;
                y = ab; //swap
            }
            xToY = occToDT(bStart);
        } else {
            xToY = dt;
        }

        boolean simple = //NAL.term.CONJ_FACTOR ?
            //dt!=0
            //(dt!=0 || !xu.equals(yu))
                     (!x.unneg().CONDS() && !y.unneg().CONDS());
            //: (aRange==0 && bRange==0);

        if (simple)
            return __conjSeq(B, xToY, x, y);


        return conjSeqComplex(B, xToY, x, y);
    }

    private static Term conjSeqComplex(TermBuilder B, int xToY, Term x, Term y) {
        var c = new ConjList(2);
        if (c.add(0L, x))
            c.add((long)xToY, y);
        Term xy = c.term(B);
        c.delete();
        return xy;

//        try (ConjTree c = new ConjTree()) {
//            if (c.add(0, x))
//                c.add(xToY, y);
//            return c.term(B);
//        }
    }

    static Term conjSeqComplex(TermBuilder B, int n, Term[] items, long[] when) {
        try (ConjTree c = new ConjTree()) {
            TmpTermList ete = null;
            for (int i = 0; i < n; i++) {
                long wi = when[i];
                Term ii = items[i];
                if (wi == ETERNAL) {
                    if (ete == null) ete = new TmpTermList();
                    ete.add(ii);
                } else {
                    if (!c.add(wi, ii))
                        break;
                }
            }
            Term d = c.term(B);
            if (d == Null) return Null;
            if (d == False) return False;

            return ete != null ? B.conj(B.conj(ete), d) : d;
        }
    }

    /**
     * creates XTERNAL-chain for the sequence of terms
     */
    public static Term xSeq(Iterable<Term> xx) {
        Term y = null;
        for (Term x : xx)
            y = y == null ? x : xSeq(y, x);
        return y;
    }

    private static Term xSeq(Term a, Term b) {
        //return CONJ.the(XTERNAL, y, x);

        //commute
        final int ab = a.compareTo(b);
        if (ab > 0) {
            Term c = a; a = b; b = c;
        } else if (ab == 0) {
            //share
            //if (a!=b)
                b = a;
        }
        return Op.terms.compoundNew(CONJ, XTERNAL, a, b);
    }

    public static Term xSeqHierarchy(TermBuilder B, Term[] x) {
        return xSeqHierarchy(B, x, 0, x.length - 1);
    }

    /**
     * see: ConjSeq.seqBalancedLeaf...  start,end inclusive
     */
    private static Term xSeqHierarchy(TermBuilder B, Term[] x, int start, int end) {
        int n = end - start + 1;
        switch (n) {
            case 0:
                throw new UnsupportedOperationException();
            case 1:
                return x[start];
            case 2:
                return B.conj(XTERNAL, x[start], x[end]);
            case 3:
                return B.conj(XTERNAL, xSeqHierarchy(B, x, start, start + 1), x[end]);
            default: {
                int mid = start + n / 2;
                if (n % 2 == 1)
                    mid++; //if odd: larger in first arg, as this will (likely) follow how it will need to sort
                return B.conj(XTERNAL,
                        xSeqHierarchy(B, x, start, mid),
                        xSeqHierarchy(B, x, mid + 1, end));
            }
        }
    }


}