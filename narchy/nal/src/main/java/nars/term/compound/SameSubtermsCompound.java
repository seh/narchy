package nars.term.compound;

import nars.subterm.Subterms;
import nars.term.Compound;

import static nars.Op.XTERNAL;

/**
 * provides access to subterms via its own methods (or dynamically)
 * as opposed to forwarding to another Subterms instance.
 */
public abstract class SameSubtermsCompound extends Compound {

	private boolean normalized, normalizedKnown;

	protected SameSubtermsCompound(int op) {
		super(op);
	}


	@Override
	public void setNormalized() {
		normalized = true; normalizedKnown = true;
	}

	@Override
	public final boolean NORMALIZED() {
		if (!normalizedKnown) {
			normalized = super.NORMALIZED();
			normalizedKnown = true;
		}
		return normalized;
	}

//	private boolean _NORMALIZED() {
//		if (this instanceof Neg) {
//			Term u = unneg();
//			return !(u instanceof Compound) || ((Compound) u).NORMALIZED();
//		} else
//			return super.NORMALIZED();
//	}

	@Override
	public Subterms subtermsDirect() {
		return this;
	}

	@Override
	public boolean TEMPORAL_VAR() {
		return dt() == XTERNAL || super.TEMPORAL_VAR();
	}

}