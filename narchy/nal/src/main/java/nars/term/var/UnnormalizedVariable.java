package nars.term.var;

import jcog.The;
import nars.Op;
import nars.io.IO;
import nars.term.Variable;

/**
 * Unnormalized, labeled variable
 */
public class UnnormalizedVariable extends Variable implements The {

    private final int op;

//    public UnnormalizedVariable(Op type, byte[] label) {
//        super(IO.SPECIAL_BYTE, label);
//        this.type = type.id;
//    }

    public UnnormalizedVariable(Op op, String label) {
        this(op.id, label);
    }

    public UnnormalizedVariable(int opID, String label) {
        this(opID, label.getBytes());
    }

    protected UnnormalizedVariable(Op op, byte[] bytes) {
        this(op.id, bytes);
    }

    protected UnnormalizedVariable(int opID, byte[] bytes) {
        super(IO.SPECIAL_BYTE, bytes);
        this.op = opID;
    }

    @Override
    public final int opID() {
        return op;
    }


}