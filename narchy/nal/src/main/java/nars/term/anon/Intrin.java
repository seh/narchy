package nars.term.anon;

import jcog.Is;
import nars.NAL;
import nars.Term;
import nars.term.Neg;
import nars.term.atom.Atomic;
import nars.term.atom.Int;
import nars.term.var.NormalizedVariable;
import org.eclipse.collections.impl.map.mutable.primitive.ShortObjectHashMap;

import static nars.Op.*;

/**
 * INTrinsic terms
 *   a finite set of terms canonically addressable by an integer value
 *   used by the system in critical parts to improve space and time efficiency
 *
 * indicates the target has an anonymous, canonical integer identifier
 */
@Is("G%C3%B6del_numbering_for_sequences")
public enum Intrin  { ;

    /** code pages: categories of 8-bit numerically indexable items. */
    public static final short ANOMs = 0;

    //keep the variables in this order
    public static final short VARDEPs = 1;
    public static final short VARINDEPs = 2;
    public static final short VARQUERYs = 3;
    public static final short VARPATTERNs = 4;

    public static final short IMGs = 5; // TODO make this a misc category

    /** 0..255 */
    public static final short INT_POSs = 6;

    /** -1..-255 */
    public static final short INT_NEGs = 7;

    /** ASCII 0..255 */
    public static final short CHARs = 8;



    /** @param i positive values only  */
    public static Term _term(short /* short */ i) {
        int num = /*(byte)*/ (i & 0xff);
        return switch (group(i)) {
            case ANOMs -> Anom.the(num);
            case IMGs -> num == '/' ? ImgExt : ImgInt;
            case VARDEPs -> NormalizedVariable.the(VAR_DEP.id, num);
            case VARINDEPs -> NormalizedVariable.the(VAR_INDEP.id, num);
            case VARQUERYs -> NormalizedVariable.the(VAR_QUERY.id, num);
            case VARPATTERNs -> NormalizedVariable.the(VAR_PATTERN.id, num);
            case INT_POSs -> Int.the(num);
            case INT_NEGs -> Int.the(-num);
            case CHARs -> Atomic.the((char) num);
            default -> throw new UnsupportedOperationException();
        };
    }

    public static Term term(int /* short */ i) {
        return term((short)i);
    }

    public static Term term(short i) {
        return i < 0 ? neg((short) -i) : _term(i);
    }
    public static byte op(short i) {
        if (i < 0) return NEG.id;
        else {
            //HACK TODO determine without lookup if possible
            return (byte) _term(i).opID();
        }
    }

    private static final ShortObjectHashMap<Neg.NegIntrin> negs = new ShortObjectHashMap<>(4096);

    public static Neg.NegIntrin neg(short i) {
        return NAL.term.NEG_INTRIN_CACHE ? _negCached(i) : _neg(i);
    }

    private static Neg.NegIntrin _negCached(short i) {
        var y = negs.get(i);
        if (y == null)
            negs.put(i, y = _neg(i));
        return y;
    }

    private static Neg.NegIntrin _neg(short i) {
        return new Neg.NegIntrin(i);
    }

    public static int group(int i) {
        return (i & 0xff00)>>8;
    }

    /** returns 0 if the target is not anon ID or a negation of one */
    public static short id(Term t) {
        if (t instanceof Neg)
            return (short)-(t instanceof Neg.NegIntrin ?
                ((Neg.NegIntrin) t).sub :
                _id(t.unneg()));
        else
            return _id(t);
    }

    private static short _id(Term t) {
        return t instanceof IntrinAtomic ? ((IntrinAtomic) t).intrin() : 0;
    }

    public static boolean intrin(Term[] xx) {
        //return Util.and(Intrin::intrin, xx);

        for (Term x : xx) if (!intrin(x)) return false;
        return true;
    }

    public static boolean intrin(Term x) {
        return id(x)!=0;
    }

    /** returns -1 if not variable */
    public static int varID(short i) {
        return isVar(i) ? i & 0xff : 0;
    }

    private static boolean isVar(int i) {
        int g = group(i);
        return g >= VARDEPs && g <= VARPATTERNs;
    }
}