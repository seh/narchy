package nars.term.anon;

import jcog.The;
import nars.Op;

public interface IntrinAtomic extends The {
	short intrin();

	boolean equals(Object x);

	static short termToId(Op o, byte id) {
		return (short)(id | (switch (o) {
			case ATOM -> Intrin.ANOMs;
			case VAR_DEP -> Intrin.VARDEPs;
			case VAR_INDEP -> Intrin.VARINDEPs;
			case VAR_QUERY -> Intrin.VARQUERYs;
			case VAR_PATTERN -> Intrin.VARPATTERNs;
			case IMG -> Intrin.IMGs;
			default -> throw new UnsupportedOperationException();
		} << 8));
	}

	/** meant to be a perfect hash among all normalized variables */
	default byte id() {
		return (byte)intrin();
	}


}