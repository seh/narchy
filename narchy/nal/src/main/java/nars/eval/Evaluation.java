package nars.eval;

import com.google.common.collect.Iterables;
import jcog.Is;
import jcog.Util;
import jcog.data.iterator.CartesianIterator;
import jcog.data.list.Lst;
import jcog.random.RandomBits;
import jcog.util.ArrayUtil;
import jcog.version.VersionMap;
import jcog.version.Versioning;
import nars.NAL;
import nars.Term;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.util.TermException;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;

import static nars.term.atom.Bool.*;

/**
 * see: https://www.swi-prolog.org/pldoc/man?section=preddesc
 * //TODO extends Unify
 */
@Is("Meta-circular_evaluator")
public class Evaluation {

    private final Evaluator e;
    private final Predicate<Term> each;
    private VersionMap<Term, Term> subs;
    /**
     * current sub-evaluation item
     */
    public transient Term z;
    private Lst<Iterable<Predicate<Evaluation>>> termutes;
    private Versioning v;
    private Term x;
    @Deprecated
    private transient Set<Term> out;

    public Evaluation(Evaluator e, @Nullable Predicate<Term> each) {
        this.e = e;
        this.each = each != null ? each : (Predicate<Term>) this;
    }

    /**
     * 2-ary AND
     */
    public static Predicate<Evaluation> assign(Term x, Term xx, Term y, Term yy) {
        if (x.equals(xx))
            throw new TermException("assign cycle", x);
        if (y.equals(yy))
            throw new TermException("assign cycle", y);

        return m -> m.is(x, xx) && m.is(y, yy);
    }

    private static Predicate<Evaluation> assign(Term x, Term y) {
        if (x.equals(y))
            throw new TermException("assign cycle", x);
        return (subst) -> subst.is(x, y);
    }

    protected RandomBits random() {
        return new RandomBits(ThreadLocalRandom.current());
    }

    private boolean termute1(Term y, int start) {
        for (Predicate<Evaluation> tt : termutes.remove(0)) {

            if (tt.test(this)) {
//				try {
                if (!eval(transform(y)))
                    return false;
//				} catch (StackOverflowError e) { //HACK TODO TEMPORARY
//					//System.err.println("termute overflow: " + termutes.toString());
//					return false;
//				}
            }

            if (!revert(start))
                return false;
        }
        return true;
    }

    private boolean revert(int start) {
        if (!this.v.use(NAL.derive.TTL_COST_TRY))
            return false; //CUT
        v.revert(start);
        return true;
    }

    private boolean termuteN(Term y, int start) {

        Iterable<Predicate<Evaluation>>[] tt = termutes.toArray(Iterable[]::new);
        termutes.clear();

        for (int i = 0, ttLength = tt.length; i < ttLength; i++) tt[i] = tt[i];

        if (tt.length > 1 && NAL.unify.SHUFFLE_TERMUTES) {
            ArrayUtil.shuffle(tt, random());
        }

//		QuickSort.sort(tt, 0, tt.length, (int x)-> Iterables.size(tt[x]));

        CartesianIterator<Predicate<Evaluation>> pp = new CartesianIterator<>(Predicate[]::new, tt);
        while (pp.hasNext()) {
            boolean appliedAll = true;
            for (Predicate<Evaluation> p : pp.next()) {
                if (!p.test(this)) {
                    appliedAll = false;
                    break;
                }
            }

            if (appliedAll) {
                Term z = transform(y);
                if (!y.equals(z))
                    eval(z);
            }

            if (!revert(start))
                return false;
        }

        return true;
    }

    public void clear() {
        this.x = null;
        this.z = null;

        if (this.out!=null)
            out.clear();

        if (v != null) {
            if (termutes != null)
                termutes.clear();
            v.clear();
//            subs.clear();
        }
    }

//    private void canBe(Predicate<Termerator> x) {
//        canBe(List.of(x));
//    }

    /**
     * assign 1 variable
     * returns false if it could not be assigned (enabling callee fast-fail)
     */
    public final boolean is(Term x, Term y) {

        if (x == Null || y == Null) return false;

        if (x.equals(y)) return true;


        if (y instanceof Compound && /*x instanceof Variable && */ ((Compound)y).containsRecursively(x))
            return false; //infinite recursion
//        if (x instanceof Compound && /*y instanceof Variable && */ x.containsRecursively(y))
//            return false; //infinite recursion

//        if (v != null && v.size() > 0) {
//            Term xx = x.replace(subs), yy = y.replace(subs);
//            if (!xx.equals(x) || !yy.equals(y))
//                return is(xx, yy); //recurse
//
//            //replace existing subs
//            if (!subs.replace((sx, sy)-> x.equals(sx) ? sy : sy.replace(x, y)))
//                return false;
//
//        } else {
        ensureReady();
//        }

        if (subs.set(x, y)) return true;     //assert(set); //return true;

        if (y instanceof Variable) {
            //try reverse assignment
            Term xx = subs.get(x);
            if (xx != null) x = xx;
            return subs.set(y, x);
        }
        return false;
    }

    /**
     * assign 2-variables at once.
     * returns false if it could not be assigned (enabling callee fast-fail)
     */
    public boolean is(Term x, Term xx, Term y, Term yy) {
        return is(x, xx) && is(y, yy);
    }

    /**
     * OR, forked
     * TODO limit when # termutators exceed limit
     */
    private void canBe(Iterable<Predicate<Evaluation>> x) {
        if (termutes == null)
            termutes = new Lst<>(1);
        else if (termutes.contains(x)) //HACK
            return;

        termutes.add(x);
    }

    public boolean canBe(Term x, Iterable<Term> y) {
        int n = sizeEstimate(y);

        switch (n) {
            case 0:
                return true; //no effect

            case 1:
                //deterministic substitution
                return is(x, y.iterator().next());
            default:
                if (!termutable())
                    return false;

                canBe(new ChoiceIterable(x, y));
                return true; //HACK
        }
    }

    private static int sizeEstimate(Iterable<Term> y) {
        int n;
        //TODO Term commute for the set of terms in y, not just the direct iteration
        if (y instanceof Subterms)
            n = ((Subterms) y).subs();
        else if (y instanceof Collection)
            n = ((Collection) y).size();
        else
            n = Iterables.size(y); //TODO parameterize this because some big Iterables would be expensive just to count here
        return n;
    }

    protected boolean termutable() {
        return true;
    }

    private void ensureReady() {
        if (v == null) {
            v = new Versioning<>(NAL.unify.UNIFICATION_STACK_CAPACITY, NAL.EVALUATION_RECURSE_LIMIT);
            subs = new VersionMap<>(v);
        }
    }

    public boolean eval(Term y) {
        boolean initial = this.x == null;
        if (initial) this.x = y;

        if (out == null || !out.contains(y)) {
            EvaluationPhase ex = e.clauses(y);
            if (initial && ex == null)
                return false; //done without having allocated much
            else
                return ex != null ?
                    eval(ex) :
                    out(this.x, y);
        } else
            return true; //kontinue
    }

    private int now() {
        return v != null ? v.size() : 0;
    }

    private boolean eval(EvaluationPhase c) {

        Term y, xx, x = null;

        //until stable:
        do {

            y = c.start();
            if (x==null)
                x = y;

            int before = now();

            this.z = xx = y;
            Term z = c.apply(this);

            boolean changed = before == now();

            if (z instanceof Atomic) {
                if (z instanceof Variable)
                    z = changed ? z : transform(z); //last chance to transform
                y = z;
                break; //terminate
            } else {
                if (z == null) z = y;
                else { /*TODO check vol limit*/ }
                y = changed ? z : transform(z);
            }



        } while (!y.equals(xx) && (c = e.clauses(y))!=null);


        //if termutators, collect all results. otherwise 'cur' is the only result to return
        //Transformed Result (possibly same)
        int termutes;
        if (!(y instanceof Bool) && (termutes = termutators()) > 0) {
            int sizeBefore = now();
            return termutes == 1 ? termute1(y, sizeBefore) : termuteN(y, sizeBefore);
        } else {
//            if (y.equals(x))
//                return true; //unchanged
            return out(xx, y);
        }
    }

    /** Terminal Result */
    private boolean out(Term x, Term y) {
        if (y instanceof Bool)
            y = bool(x, (Bool) y);

        if (out == null) {
            //avoids allocating 'out' if 'each' filter fails
            if (!each.test(y))
                return false;
            out = new UnifiedSet<>(2);
            out.add(y);
            return true;
        } else {
            return !out.add(y) || each.test(y);
        }
    }

    private Term bool(Term x, Bool b) {
        if (b == True)
            return boolTrue(x);
        else if (b == False)
            return False; //boolFalse(x);
        else
            return Null;
    }

//	protected Term bool(Term x, Bool b) {
//		return b;
//	}

    protected Term boolTrue(Term x) {
        return x;
    }

    protected Term boolFalse(Term x) {
        return x.neg();
    }

    private int termutators() {
        return termutes != null ? termutes.size() : 0;
    }

    private Term transform(Term x) {
        return subs != null ? x.replace(subs, Evaluator.B) : x;
    }

    private record ChoiceIterable(Term x, Iterable<Term> y) implements Iterable<Predicate<Evaluation>> {

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ChoiceIterable that)) return false;
            return y.equals(that.y) && x.equals(that.x);
        }

        @Override
        public int hashCode() {
            return Util.hashCombine(y, x);
        }

        @Override
        public Iterator<Predicate<Evaluation>> iterator() {
            return new ChoiceIterable.ChoiceIterator(x, y);
        }

        private static final class ChoiceIterator implements Iterator<Predicate<Evaluation>> {

            final Iterator<Term> y;
            private final Term x;

            ChoiceIterator(Term x, Iterable<Term> y) {
                this(x, y.iterator());
            }

            ChoiceIterator(Term x, Iterator<Term> y) {
                this.x = x;
                this.y = y;
            }

            @Override
            public boolean hasNext() {
                return y.hasNext();
            }

            @Override
            public Predicate<Evaluation> next() {
                return assign(x, y.next());
            }
        }
    }

    public static class First implements Predicate<Term> {
        public Term the;

        @Override
        public boolean test(Term what) {
            //TODO move this to a separate predicate
//			if (what instanceof Bool) {
//				return true; //ignore and continue try to find a non-bool solution
//			} else {
            the = what;
            return false;
//			}
        }
    }

    public static class All implements Predicate<Term> {

        private final Set<Term> the;

        public All() {
            this(new UnifiedSet<>(0, 0.5f));
        }

        All(UnifiedSet<Term> ee) {
            this.the = ee;
        }

        @Override
        public boolean test(Term t) {
            the.add(t);
            return true;
        }

        public final Set<Term> the() {
            return the.isEmpty() ? Collections.EMPTY_SET : the;
        }
    }

}