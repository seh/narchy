package nars.eval;

import jcog.data.bit.MetalBitSet;
import jcog.data.list.Lst;
import jcog.data.map.UnifriedMap;
import jcog.data.set.ArrayHashSet;
import nars.$;
import nars.Op;
import nars.Term;
import nars.subterm.Subterms;
import nars.subterm.TermList;
import nars.term.Compound;
import nars.term.Functor;
import nars.term.Neg;
import nars.term.Variable;
import nars.term.atom.Atom;
import nars.term.atom.Atomic;
import nars.term.atom.Bool;
import nars.term.util.builder.SimpleHeapTermBuilder;
import nars.term.util.builder.TermBuilder;
import nars.term.util.transform.Replace;
import nars.term.util.transform.TermTransform;
import org.jetbrains.annotations.Nullable;

import java.util.ListIterator;
import java.util.Set;
import java.util.function.Function;

import static nars.Op.*;
import static nars.term.atom.Bool.*;

/**
 * an evaluation plan
 * discovers functors within the provided target, or the target itself.
 * transformation results should not be interned, that is why DirectTermTransform used here
 */
public class Evaluator {

//    static final Comparator<? super Term> complexitySort = (a, b) -> {
//        int vars = Integer.compare(a.vars(), b.vars());
//        if (vars != 0)
//            return vars;
//        int vol = Integer.compare(a.volume(), b.volume());
//        if (vol != 0)
//            return vol;
//
//        return a.compareTo(b);
//    };

//    private final TermBuffer compoundBuilder = new TermBuffer(Op._terms);
    private final Function<Atom, Functor> funcResolver;

    public Evaluator(Function<Atom, Functor> funcResolver) {
        this.funcResolver = funcResolver;
    }

    public Evaluator clone() {
        return new Evaluator(funcResolver);
    }

    @Nullable
    final EvaluationPhase clauses(Term x) {
        if (!Functor.evalable(x)) return null;

        EvaluationPhase y = compile((Compound) x);
        return y == EvaluationPhase.NULL ? null : y;
    }

    EvaluationPhase compile(Compound x0) {
        ArrayHashSet<Compound> e = new ArrayHashSet<>(2);

        Term x = clauses(x0, e);

        EvaluationPhase y = compile(x0, x, e.list);

        e.clear();

        return y;
    }

    private static EvaluationPhase compile(Compound x0, Term x, Lst<Compound> yy) {
        int len = x instanceof Atomic ? 0 : yy.size();

        return switch (len) {
            case 0 -> x.equals(x0) ?
                EvaluationPhase.NULL :
                EvaluationPhase.Constant.the(x == True ? x0 : x);
            case 1 -> {
                Compound y0 = yy.get(0);

                yield x.equals(y0) ?
                        (x instanceof Compound ? new EvaluationPhase.Itself((Compound) x) : EvaluationPhase.Constant.the(x)) :
                        new EvaluationPhase.PartOfItself(x, y0);
            }
            default -> new EvaluationPhase.Recipe(x, yy);

        };
    }

    private Term clauses(Compound x, ArrayHashSet<Compound> e) {

        UnifriedMap<Term, Term> m = new UnifriedMap<>(0);

        return clauseRemap(
                clauseFind(x, null, e, m),
                m, e);
    }

    private static Term clauseRemap(Term y, UnifriedMap<Term, Term> remap, ArrayHashSet<Compound> e) {
        while (!remap.isEmpty()) {
            TermTransform t = Replace.replace(remap, Evaluator.B);
            remap.clear(); //remap = null;

            y = t.apply(y);
            if (!e.isEmpty())
                clauses(e, remap, t);
        }
        return y;
    }

    private static void clauses(ArrayHashSet<Compound> e, UnifriedMap<Term, Term> remap, TermTransform t) {
        ListIterator<Compound> l = e.listIterator();
        while (l.hasNext()) {
            Compound ll = l.next();
            Term z = t.apply(ll);
            if (z.EQ()) {
                Term zz = equalConstantReduce(z.subtermsDirect(), remap);
                if (zz!=null)
                    z = zz;
            }

            if (z instanceof Compound)
                l.set((Compound) z);
            else
                l.remove();
        }
    }

    /** resolves functor atoms, extracts evaluable sub-expressions, and returns possibly transformed compound */
    private Term clauseFind(Compound x, Term superTerm, Set<Compound> e, UnifriedMap<Term, Term> remap) {

        if (x instanceof Neg)
            return clauseFindNeg(x, e, remap);


        Op xOp = x.op();
        boolean xConj = xOp == CONJ;

        Subterms yy = x.subtermsDirect();
        boolean xsChanged = false;
        if (Functor.evalable(yy)) {
            int n = yy.subs();
            //TODO only do this if there are actually >1 evalable subterms.

            //TODO use Subterm.transformSubs
            TermList xy = new TermList(n);

            /* reverse direction, which visits simpler terms first according to the canonical sorting order */
            for (int i = n - 1; i >= 0; i--) {
                Term xx = yy.sub(i);
                if (Functor.evalable(xx)) {

                    Term zz = clauseFind((Compound) xx, x, e, remap);
                    if (zz == Null)
                        return Null; //CUT

                    if ((zz!=xx)) {
                        xsChanged = true;
                        if (xConj && zz instanceof Bool) {
                            if (zz == False)
                                return False;
                            /*else if (xxx == True) { skip }*/
                            continue;
                        }
                        xx = zz;
                    }
                }
                xy.addFast(xx);
            }

            if (xsChanged) {
                /* reverse since the subterms were iterated and added in descending order */
                xy.reverseThis();
                yy = xy;
            } else {
                xy.delete();
            }
        }

        return clauseFindFinal(x, superTerm, e, remap, xOp, xConj, yy, xsChanged);
    }

    /** add this after the children have recursed */
    private Term clauseFindFinal(Compound x, Term superTerm, java.util.Set<Compound> e, UnifriedMap<Term, Term> remap, Op xOp, boolean xConj, Subterms yy, boolean xsChanged) {
        boolean evalable;

        Term xf = Functor.func(x);
        if (xf instanceof Functor) {
            evalable = true;
        } else if (xf instanceof Atom) {
            evalable = false;
            Functor h = funcResolver.apply((Atom) xf);
            if (h != null) {
                if (!xsChanged) {
                    yy = yy.toList();
                    xsChanged = true;
                }
                ((TermList) yy).setFast(1, h);
                evalable = true;
            }
        } else
            evalable = false;

        Term y;
        if (xsChanged) {
            if (xConj) {
                switch (yy.subs()) {
                    case 0: return True;
                    case 1: return yy.sub(0);
                }
            }
            y = xOp.build(B, x.dt(), yy);
            //TEMPORARY
//            if (y.subtermsDirect() instanceof TermList)
//                throw new WTF();

            if (evalable && !Functor.isFunc(y))
                evalable = false;
        } else {
            y = x;
        }


        if (y.EQ()) {
            if (!(superTerm instanceof Neg)) {
                //dont assign if the equality is wrapped in negation
                Term z = equalConstantReduce(y.subtermsDirect(), remap);
                if (z != null)
                    return z;
            }
            evalable = true;
        }

        if (evalable && y instanceof Compound && y.isAny(EQ.bit|INH.bit))
            e.add((Compound) y);

        return y;
    }

    private Term clauseFindNeg(Compound x, java.util.Set<Compound> e, UnifriedMap<Term, Term> remap) {
        Term ux = x.unneg();
        Term uy = ux instanceof Compound ? clauseFind((Compound) ux, x, e, remap) : ux;
        return uy == ux ? x : uy.neg();
    }


    private static Term equalConstantReduce(Subterms x, UnifriedMap<Term, Term> remap) {
        assert(x.subs()==2);
        return equalConstantReduce(x.sub(0), x.sub(1), remap);
    }

    @Nullable
    private static Term equalConstantReduce(Term a, Term b, UnifriedMap<Term, Term> remap) {
        if (a.PROD() && b.PROD()) {
            int n = a.subs();
            if (n == b.subs()){
                //try component-wise assignment
                boolean indeterminate = false;
                MetalBitSet eliminations = null;
                for (int i = n - 1; i >= 0; i--) {
                    switch (inlineAssign(a.sub(i), b.sub(i), remap)) {
                        case -1: return False;
                        case 0: indeterminate = true; break;
                        case 1: {
                            if (eliminations == null) eliminations = MetalBitSet.bits(i);
                            eliminations.set(i);
                            break;
                        }
                    }
                }
                if (indeterminate) {
                    if (eliminations!=null) {
                        //TODO
                        Term aa = $.pFast(((Compound)a).removing(eliminations));
                        Term bb = $.pFast(((Compound)b).removing(eliminations));
                        if (aa.subs()== 1) {
                            assert(bb.subs()==1);
                            aa = aa.sub(0); bb = bb.sub(0);
                        }
                        return equalConstantReduce(aa, bb, remap);
                    } else
                        return null;
                } else
                    return True;
            }
        }

        return switch (inlineAssign(a, b, remap)) {
            case +1 -> True;
            case -1 -> False;
            default -> null;
        };
    }

    private static int inlineAssign(Term a, Term b, UnifriedMap<Term, Term> remap) {
        boolean aNeg = a instanceof Neg, bNeg = b instanceof Neg;
        if (aNeg == bNeg && a.equals(b))
            return +1;
        if (aNeg ^ bNeg && a.unneg().equals(b.unneg()))
            return -1;

        boolean v0 = (aNeg ? a.unneg() : a) instanceof Variable;
        boolean v1 = (bNeg ? b.unneg() : b) instanceof Variable;
        if (v0 || v1) {
            if (v0 && aNeg) {
                a = a.unneg();
                b = b.neg();
            } else {
                if (v1 && bNeg) {
                    b = b.unneg();
                    a = a.neg();
                }
            }
            if (v0 ^ v1) {
                //pre-assign constant -> variable
                Term v = v0 ? b : a;
                if (!Functor.evalable(v)) {
                    Term k = v0 ? a : b;
                    Term vExist = remap.get(k);
                    if (vExist!=null)
                        return vExist.equals(v) ? +1 : -1;
                    remap.put(k, v);
                    return +1;
                }
            }
        }
        return 0;
    }



    @Nullable
    public final Term first(Compound x) {
        if (Functor.evalable(x)) {
            Evaluation.First f = new Evaluation.First();
            new Evaluation(this, f).eval(x);
            return f.the;
        }

        return null;
    }

    static final TermBuilder B =
            SimpleHeapTermBuilder.the;
            //LightHeapTermBuilder.the;


}