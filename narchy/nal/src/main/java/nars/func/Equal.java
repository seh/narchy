package nars.func;

import jcog.data.bit.MetalBitSet;
import nars.Op;
import nars.Term;
import nars.eval.Evaluation;
import nars.subterm.Subterms;
import nars.term.Compound;
import nars.term.Functor;
import nars.term.Neg;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.term.functor.InverseFunctor;
import nars.unify.Unifier;
import org.jetbrains.annotations.Nullable;

import static nars.Op.PROD;
import static nars.term.atom.Bool.False;
import static nars.term.atom.Bool.True;

public enum Equal  { ;

    public static Term compute(Evaluation e, Term x, Term y) {
        if (x == y) return True;

        //handle negated vars by negating one or both sides like unification
        if (x instanceof Neg && y instanceof Neg) {
            x = x.unneg();
            y = y.unneg();
        } else if (x instanceof Neg) {
            Term xu = x.unneg();
            if (xu.equals(y)) return False; //co-negated
            if (xu instanceof Variable) {
                if (y instanceof Variable)
                    return null; //do nothing; maintain negation relationship
                x = x.unneg();
                y = y.neg();
            }
        } else if (y instanceof Neg) {
            Term yu = y.unneg();
            if (yu.equals(x)) return False; //co-negated
            if (yu instanceof Variable) {
                if (x instanceof Variable)
                    return null; //do nothing; maintain negation relationship
                y = y.unneg();
                x = x.neg();
            }
        }

        boolean xIsVar = x instanceof Variable, yIsVar = y instanceof Variable;
        if (xIsVar == yIsVar) {
            if (x.equals(y))
                return True;
            //if (x instanceof Atomic &&) ..
        }


        //TODO maybe only for certain var types
        if (xIsVar && yIsVar) { //var equivalence
            //Variable common = CommonVariable.common((Variable)x, (Variable)y); //return e.is(x, common) && e.is(y, common) ? True : Null;

            if (e!=null) {
                if (e.is(y, x))
                    return null;
                else if (e.is(x, y)) //try assigning in reverse
                    return null;
            }

            return null;
        }


        boolean xHasVar = xIsVar || x.hasVars(), yHasVar = yIsVar || y.hasVars();
        if (!xHasVar && !yHasVar) {
            return False; //inequal constant
        }

        if (x instanceof Compound || y instanceof Compound) {
            Term i = reverse(e, x, xHasVar, y, yHasVar);
            if (i != null)
                return i;
        }

        if (e!=null) {


            if (xIsVar)  return e.is(x, y) ? True : False;
            if (yIsVar)  return e.is(y, x) ? True : False;
        }

        //return xhv || yhv ? null : False; //closed-world (atoms as constants)

        return
                (xHasVar || yHasVar) /* ... && x.unifiable(e.dt) */ ?
                null :
                False;
    }

    @Nullable private static Term reverse(Evaluation e, Term x, boolean xHasVars, Term y, boolean yHasVars) {
        Op xOp = x.op(), yOp = y.op();
        if (xHasVars || yHasVars) {
            if (((xOp == PROD || xOp.set) || (yOp == PROD || yOp.set))){
                if (xOp != yOp)
                    return False;

                Subterms xx = x.subterms(), yy = y.subterms();
                if (xOp != PROD || xx.subs() != yy.subs()) {
                    return reverseArityDiffer(e, xOp, xx, yy);
                } else {
                    Term pxy = eqVector(e, xx, yy);
                    if (pxy != null) return pxy;
                }
            }
        }
        
        if (xOp == Op.INH) {
            Term xa = reverse(e, (Compound) x, y);
            if (xa != null) return xa;
        }

        if (yOp == Op.INH) {
            Term ya = reverse(e, (Compound) y, x);
            if (ya != null) return ya;
        }

        return null;
    }

    @Nullable
    private static Term reverseArityDiffer(Evaluation e, Op xOp, Subterms xx, Subterms yy) {
        if (xOp == PROD) return False;
        else {
            //sets with different arity might unify if containing a variable that has the value equal to other set items
            Subterms[] xy = Unifier.eliminate(xx, yy);
            if (xy != null) {
                if (xy[0].subs() == 1 && xy[1].subs() == 1) {
                    //reduced to single term
                    return compute(e, xy[0].sub(0), xy[1].sub(0));
                }
                //TODO other cases where values can be assigned to multiple variable outputs
            }
            return null;
        }
    }

    @Nullable
    private static Term reverse(Evaluation e, Compound x, Term y) {
        Atomic xf = Functor.func(x);
        return xf instanceof InverseFunctor ? ((InverseFunctor) xf).equality(e, x, y) : null;
    }



    //                if (xa1 instanceof Variable && xa0.op() == INT) //shouldnt be necessary if sorted in correct order
//                    throw new TODO();
    private static Term eqVector(Evaluation e, Subterms xx, Subterms yy) {
        int n = xx.subs();
        MetalBitSet remain = MetalBitSet.bits(n);
        //1. if any constant subterms are not equal, it's false
        for (int i = 0; i < n; i++) {
            Term a = xx.sub(i), b = yy.sub(i);
            if (!a.equals(b)) {
                if (!a.hasVars() && !b.hasVars()) return False; //non-constant mismatch
                else remain.set(i);
            }
        }
//
//        for (int i = 0; i < n; i++) {
//            if (remain.test(i)) {
//                Term xy = compute(e, xx.sub(i), yy.sub(i));
//                if (xy == Null || xy == False)
//                    return xy; //fail immediately
//
//                if (xy == null)
//                    continue;
//
//                remain.clear(i);
//                if (xy == True)
//                    continue;
//            }
//        }
        int numRemain = remain.cardinality();
        if (numRemain==0) return True;

        if (numRemain == 1) {
            int i = remain.first(true);
            return Op.EQ.the(xx.sub(i), yy.sub(i));
        } else {
            //create an equality vector of the remaining conditions
            return Op.EQ.the(
                PROD.the(xx.subsIncExc(remain, true)),
                PROD.the(yy.subsIncExc(remain, true))
            );
        }
    }


}