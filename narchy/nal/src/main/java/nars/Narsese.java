package nars;

import com.github.fge.grappa.buffers.CharSequenceInputBuffer;
import com.github.fge.grappa.buffers.InputBuffer;
import com.github.fge.grappa.matchers.base.Matcher;
import com.github.fge.grappa.rules.Rule;
import com.github.fge.grappa.run.MatchHandler;
import com.github.fge.grappa.run.ParsingResult;
import com.github.fge.grappa.run.context.DefaultMatcherContext;
import com.github.fge.grappa.run.context.MatcherContext;
import com.github.fge.grappa.stack.ArrayValueStack;
import com.github.fge.grappa.stack.ValueStack;
import com.github.fge.grappa.transform.ParserTransformer;
import com.google.common.annotations.VisibleForTesting;
import jcog.Util;
import jcog.data.list.Lst;
import nars.io.NarseseParser;
import nars.task.AbstractCommandTask;
import nars.task.NALTask;
import nars.term.atom.Atomic;
import nars.term.obj.QuantityTerm;
import nars.time.Tense;
import nars.time.Time;
import nars.truth.Truth;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static nars.Op.*;
import static nars.Term.nullIfNull;

/**
 * NARese, syntax and language for interacting with a NAL in NARS.
 * https:
 */
public final class Narsese {

    private static final ThreadLocal<Narsese> parsers;

    static {
        Constructor<? extends NarseseParser> parseCtor;
        try {
            parseCtor = ParserTransformer.transformParser(NarseseParser.class).getConstructor();
//			parseCtor = NarseseParser$$grappa.class.getConstructor();
        } catch (NoSuchMethodException | IOException e) {
            throw new RuntimeException(e);
        }


        parsers = ThreadLocal.withInitial(() -> {
                NarseseParser p;
                try {
                    p = parseCtor.newInstance();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                return new Narsese(p);
            }
        );


    }

    private final NarseseParser parser;
    private MyParseRunner inputParser;
    private MyParseRunner termParser;

    private Narsese(NarseseParser p) {
        this.parser = p;
    }

    public static Narsese the() {
        return parsers.get();
    }

    /**
     * returns number of tasks created
     */
    public static void tasks(String input, Collection<Task> c, NAL m) throws NarseseException {
        Narsese p = the();

        int parsedTasks = 0;

        ValueStack rv = p.inputParser().run(input).getValueStack();

        int size = rv.size();

        for (int i = size - 1; i >= 0; i--) {
            Object o = rv.peek(i);

            Object[] y;
            if (o instanceof Task) {
                y = (new Object[]{o});
            } else if (o instanceof Object[]) {
                y = ((Object[]) o);
            } else {
                throw new NarseseException("Parse error: " + input);
            }

            c.add(decodeTask(m, y));
            parsedTasks++;

        }

        if (parsedTasks == 0)
            throw new NarseseException("nothing parsed: " + input);


    }

    public static List<Task> tasks(String input, NAL m) throws NarseseException {
        List<Task> result = new Lst<>(1);
        tasks(input, result, m);

        return result;
    }

    /**
     * parse one task
     */
    public static <T extends Task> T task(String input, NAL n) throws NarseseException {
        List tt = tasks(input, n);
        if (tt.size() != 1)
            throw new NarseseException(tt.size() + " tasks parsed in single-task parse: " + input);
        return (T) tt.get(0);
    }

    static Task decodeTask(NAL nar, Object[] x) {
        if (x.length == 1 && x[0] instanceof Task) {
            return (Task) x[0];
        }

        Term content = ((Term) x[1]).normalize();
            /*if (!(content instanceof Compound)) {
                throw new NarseseException("Task target unnormalizable: " + contentRaw);

            } else */

        Object px = x[2];

        byte punct =
                px instanceof Byte ?
                        (Byte) x[2]
                        :
                        (byte) (((Character) x[2]).charValue());

        if (punct == COMMAND)
            return new AbstractCommandTask(content);

        Object _t = x[3];
        Truth t;

        if (_t instanceof Truth)
            t = (Truth) _t;
        else if (_t instanceof Float)
            t = $.t((Float) _t, nar.confDefault(punct));
        else
            t = null;

        if (t == null && (punct == BELIEF || punct == GOAL))
            t = $.t(1, nar.confDefault(punct)); //HACK

        long[] occ = occurrence(nar.time, x[4]);

        Task y = NALTask.task(content, punct, t, occ[0], occ[1], nar.evidence());

        y.pri(x[0] == null ? nar.priDefault(punct) : (Float) x[0]);

        return y;

    }

    private static long[] occurrence(Time t, Object O) {
        if (O == null)
            return new long[]{ETERNAL, ETERNAL};
        else if (O instanceof Tense) {
            long o = t.relativeOccurrence((Tense) O);
            return new long[]{o, o};
        } else if (O instanceof QuantityTerm) {
            long qCycles = t.toCycles(((QuantityTerm) O).quant);
            long o = t.now() + qCycles;
            return new long[]{o, o};
        } else if (O instanceof Integer) {

            long o = t.now() + (Integer) O;
            return new long[]{o, o};
        } else if (O instanceof Object[]) {
            long[] start = occurrence(t, ((Object[]) O)[0]);
            if (start[0] != start[1] || start[0] == ETERNAL || start[0] == TIMELESS)
                throw new UnsupportedOperationException();
            long[] end = occurrence(t, ((Object[]) O)[1]);
            if (end[0] != end[1] || end[0] == ETERNAL || end[0] == TIMELESS)
                throw new UnsupportedOperationException();
            if (start[0] <= end[0]) {
                start[1] = end[0];
                return start;
            } else {
                end[1] = start[0];
                return end;
            }
        } else if (O instanceof long[]) {
            return (long[]) O;
        } else {
            throw new UnsupportedOperationException("unrecognized occurrence: " + O);
        }
    }

    public static Term term(String s, boolean normalize) throws NarseseException {
        Term y = term(s);
        return normalize ? nullIfNull(y.normalize()) : y;
    }

    public static Term term(String s) throws NarseseException {
        return the()._term(s);
    }

    private MyParseRunner inputParser() {
        if (inputParser == null)
            this.inputParser = new MyParseRunner(parser.Input());
        return inputParser;
    }

    private MyParseRunner termParser() {
        if (termParser == null)
            this.termParser = new MyParseRunner(parser.Term());
        return termParser;
    }

    /**
     * parse one target NOT NORMALIZED
     */
    Term _term(String s) throws NarseseException {

        ValueStack stack = __term(s);

        int ss = stack.size();
        if (ss==1) {
            Object x = stack.pop();

            if (x instanceof String)
                return Atomic.the((String) x);
            else if (x instanceof Term)
                return (Term) x;
        }

        throw new NarseseException("parse fail: " + Arrays.toString(Util.arrayOf(stack::peek, 0, ss, Object[]::new)));
    }

    private ValueStack __term(String s) throws NarseseException {
        MyParseRunner p = termParser();
        try {
            return p.run(s).getValueStack();
        } catch (Throwable e) {
            throw new NarseseException(s, null, e);
        }
    }


    static class MyParseRunner<V> implements MatchHandler {

        private final Matcher rootMatcher;
        private ValueStack<V> valueStack;

        /**
         * Constructor
         *
         * @param rule the rule
         */
        MyParseRunner(Rule rule) {
            rootMatcher = (Matcher) rule;
        }

        @Override
        public boolean match(MatcherContext context) {
            return context.getMatcher().match(context);
        }

        public ParsingResult run(String input) {
            return run(new CharSequenceInputBuffer(input));
        }

        public ParsingResult<V> run(InputBuffer inputBuffer) {
            //Objects.requireNonNull(inputBuffer, "inputBuffer");
            resetValueStack();

            MatcherContext<V> context = createRootContext(inputBuffer, this);

            return createParsingResult(context.runMatcher(), context);
        }

        private void resetValueStack() {
            // TODO: write a "memoizing" API
            if (valueStack == null || !valueStack.isEmpty())
                valueStack = new ArrayValueStack<>();
            else
                valueStack.clear();//Util.nop();
        }

        @VisibleForTesting
        MatcherContext<V> createRootContext(
                InputBuffer inputBuffer, MatchHandler matchHandler) {
            return new DefaultMatcherContext<>(inputBuffer, valueStack,
                    matchHandler, rootMatcher);
        }

        @VisibleForTesting
        ParsingResult<V> createParsingResult(boolean matched,
                                             MatcherContext<V> context) {
            return new ParsingResult<>(matched, valueStack, context);
        }


    }

    /**
     * Describes an error that occurred while parsing Narsese
     */
    public static final class NarseseException extends Exception {

        public final @Nullable ParsingResult result;

        /**
         * An invalid addInput line.
         *
         * @param message type of error
         */
        public NarseseException(String message) {
            super(message);
            this.result = null;
        }

        public NarseseException(String input, Throwable cause) {
            this(input, null, cause);
        }

        public NarseseException(String input, ParsingResult result, Throwable cause) {
            super(input + '\n' + (result != null ? result : cause), cause);
            this.result = result;
        }
    }

}