package nars;


import jcog.Is;
import jcog.pri.op.PriMerge;
import jcog.signal.FloatRange;
import jcog.signal.FloatRangeRounded;
import jcog.signal.IntRange;
import jcog.thing.Thing;
import jcog.util.Range;
import nars.focus.PriNode;
import nars.focus.PriSource;
import nars.task.NALTask;
import nars.term.Termed;
import nars.term.atom.Atom;
import nars.term.util.transform.Conceptualization;
import nars.term.util.transform.Retemporalize;
import nars.time.Time;
import nars.truth.evi.EviCurve;
import nars.truth.func.TruthFunctions;
import nars.truth.util.ConfRange;
import nars.util.Timed;
import org.eclipse.collections.api.block.function.primitive.FloatFunction;

import java.util.Random;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

import static jcog.Config.IS;
import static jcog.Util.curve;
import static nars.Op.*;

/**
 * NAR Parameters
 */
public abstract class NAL<W extends NAL<?>> extends Thing<W, Term> implements Timed {


    /** batching should improve performance but reordering affects attention dynamics in realtime cases */
    public static final boolean REMEMBER_BATCHED = true;

    /** base curiosity period, in game durs.
     *  larger value means less curiosity */
    public static final float CURIOSITY_DURS =
        24;
        //16;
        //20;
        //32;
        //64;

    /** keep this to false so that multithreaded NAR shutdowns arent interfered with */
    @Deprecated public static final boolean NAR_PARTS_NULLIFY_NAR_ON_DELETE = false;

    public static final boolean HAPPINESS_GEOMETRIC = false;


    /** whether to link task even if not 'novel'.  HACK TODO 'novel' may not be entirely accurate so its probably safer to link always (true) */
    @Deprecated public static final boolean LINK_TASK_ALWAYS = true;

    /** whether to compute happiness using dur = 0 (exact), or dur = game's dur */
    public static final boolean HAPPINESS_DUR_ZERO = false;

    //10;
            //32;
            //40;
            //0.06f;
            //0.35f;
            //1;
            //0.1f;
            //0.05f;

    /**
     * use this for advanced error checking, at the expense of lower performance.
     * it is enabled for unit tests automatically regardless of the value here.
     */
    public static boolean DEBUG;
    public static boolean DEBUG_DERIVED_NALTASKS;
    public static final PriMerge taskPriMerge =
            PriMerge.max;


    //PriMerge.plus;

    public enum answer { ;

        /** determines answer capacity in proportion to STAMP_CAPACITY.
         *  determines the rate of evidence accumulation via projection, dynamic truth, etc
         *
         *  this is effectively a 'truth precision strength' parameter.
         *  proportional to how much 'justice' a belief can potentially get in a judgment.
         *
         *  TODO should be 'density': capacity/dur
         */
        public static final int ANSWER_CAPACITY = 7;
        public static final int PROJECTION_CAPACITY = ANSWER_CAPACITY - 1;

        /** precision/quality factor
         *  >=1 */
        public static final int ANSWER_TRIES_PER_CAPACITY =
            6;
            //4;
            //3;
            //2;
            //1;
            //8;
            //16;

        public static final int ACTION_ANSWER_CAPACITY =
            //1;
            //ANSWER_CAPACITY;
            ANSWER_CAPACITY*2;
            //8;
            //6;
            //12;
            //6;


        /** increase for more clustering iterations */
        public static final double NAVIGABLE_MAP_COMPRESSION_CLUSTERING_PRECISION =
            5;
            //3;
            //2;
            //4;

        /** in ranking answer tasks:
         *    proportion of evidence to sacrifice for tasks with range exceeding the query range.
         *    any non-zero value should theoretically prefer the closest-matching ranged task, all else being equal on the target interval.
         *
         *  it is a benefit-of-the-doubt that a weaker but more temporally relevant task
         *  should have an increased chance against a stronger but less temporally relevant.
         * */
        public static final float RANGE_SPECIFIC =
            0.5f;
            //0.9f;
            //1;
            //0.5f;
            //0.1f;
            //0.25f;
            //0.75f;
            //0;
            //0.1f;
            //0.25f;

        /** intermpolation specificity; balance occurrence relevance with internal temporal structure relevance
         *  probably should be greater than RANGE_SPECIFIC (pressuring merge with similar DT more than similar range)
         *
         *  partitions tasks with differring time-scales within the same concept from intermpolation during revision
         * */
        public static final float DT_SPECIFIC =
            //2;
            0.9f;
            //1;
            //1.5f;
            //0.5f;
            //0.5f;
            //3;
            //4;
            //1 + 1/3f;

//        /** completely different modes.. not sure which is better */
//        public static final boolean DERIVE_PROJECTION_ABSOLUTE_OR_RELATIVE = true;
//        public static final boolean DYN_PROJECTION_ABSOLUTE_OR_RELATIVE = DERIVE_PROJECTION_ABSOLUTE_OR_RELATIVE;
//        public static final boolean ANSWER_PROJECTION_ABSOLUTE_OR_RELATIVE = true;
//        public static final boolean TEMPORAL_REVISION_ABSOLUTE_OR_RELATIVE = true;


    }

    public enum concept { ;
        private static final ToIntFunction<Termed> termVolume = c->c.term().volume();

        /* shared eternal belief and goal capacity curve */
        static ToIntFunction<Termed> beliefEternalCapacity = curve(termVolume,
                1, 8,
                        16, 6,
                        32, 4
        );

        /* shared temporal belief and goal capacity curve */
//        ToIntFunction<Concept> bgTemporal = c -> 128; //flat
        static ToIntFunction<Termed> beliefTemporalCapacity = curve(termVolume,
//                1, 128,
//                8, 96,
//                16, 64,
//                32, 16
                1, 128,
                8, 64,
                16, 48,
                32, 32

//                1, 128,
//                16, 32


//                1, 64,
//                16, 16

//                1, 256,
//                8, 128,
//                16, 64,
//                32, 32,
//                64, 16
        );

        /* shared question and quest capacity curve */
        static ToIntFunction<Termed> questionCapacity = curve(termVolume,
                1, 8,
                64, 6
        );

    }

    /** Term -> Concept mapping */
    public static final Retemporalize conceptualization =
        Conceptualization.Hybrid;
        //Conceptualization.Xternal; //almost works
        //Conceptualization.Flatten;  //probably doesnt
        //Conceptualization.Sequence; //probably doesnt


    /**
     * TODO needs tested whether recursive Unification inherits TTL
     */
    public static final int EVALUATION_RECURSE_LIMIT = 10;


    public static final boolean ABSTRACT_TASKS_ALLOWED = true;

    @Deprecated public static final boolean CAREFUL_CONCEPTUALIZATION = false;


    /**
     * Evidential Horizon, the amount of future evidence to be considered
     */
    public static final double HORIZON = 1;

    /**
     * Maximum length of the evidental base of the Stamp, a power of 2
     * TODO IntRange
     */
    public static final int STAMP_CAPACITY = 16;
    /**
     * TODO make this NAR-specific
     */
    public static final IntRange causeCapacity = new IntRange(0, 0, 64);


    /** TODO auto-size according to Game vocabulary size */
    @Deprecated public static final int DEFAULT_GAME_CAPACITY =
        1024;
        //2048;
        //512;

//    /** experimental */
//    public static final boolean SEQUENCE_NEG_FLATTEN = false;

    /**
     * enables higher-order (recursive) implication statements
     *
     * if true: allows recursive implication constructions
     * if false: reduce to Null as an invalid statement
     *      ex: (((a==>b) && x) ==> y)
     */
    public static final boolean IMPLICATION_SUBJECT_CAN_CONTAIN_IMPLICATION = true;


    /** various post-processing of contained variables prior to use in Task content */
    public static final boolean POST_NORMALIZE = true;

    /** probability of unifying subterms randomly (not using any heuristics as usual) */
    public static final float SUBTERM_UNIFY_ORDER_RANDOM_PROBABILITY = 0;

//    /** seems safe and will reduce equivalent permutations cluttering tasklink bags */
//    public static final boolean TASKLINK_NORMALIZE_IMAGES = false;





    /** 1.0 = perfectly sorted each duration (commit); values closer to zero are less accurate but faster */
    public static final float tasklinkSortedness =
            1f;


//    public static double valueBeliefOrGoal(NALTask t, Timed n) {
//        return
//            1
//            //t.conf()
//            //(Evidence.eviRelative(t, n.time(), n.dur()))
//            //((t.conf() * (t.isEternal() ? n.dur() : t.range())) / t.volume())
//            ;
//    }
//    public static float valueQuestion(Task t) {
//        return
//            1
//            //t.volume()
//            //t.priElseZero()
//            //1f / t.volume()
//            //t.originality()
//            //1 / t.term().volume()
//        ;
//    }


    /**
     * SignalTask's
     */
    public enum signal {
        ;

        public static final boolean SIGNAL_TABLE_FILTER_NON_SIGNAL_TEMPORAL_TASKS_ON_SIGNAL = true;
        public static final boolean SIGNAL_TABLE_FILTER_NON_SIGNAL_TEMPORAL_TASKS_ON_REMEMBER = true;

        public static final int SIGNAL_BELIEF_TABLE_SERIES_SIZE = 512;

        /**
         * sensor temporal resolution parameter
         * maximum time (in durations) that a signal task can stretch the same value
         * until a new task (with new evidence) is created (seamlessly continuing it afterward)
         * 
         * this can probably, safely, be arbitrarily large
         *
         */
        public static final float SIGNAL_STRETCH_DURS_MAX =
            //8;
            //16;
            //32;
            //64;
            //128;
            //256;
            //512;
            //1024;
            4096;


        /** sensor vigilance:
         *    minimum re-activation frequency
         *    sleeping pixel wakeup period
         *  can be interpreted as min % of total pixels updated per duration.
         *     ex: every 4 durs = min 1/4 update rate
         *  */
        @Is("Figure–ground_(perception)")
        public static final float SIGNAL_SLEEP_DURS_MAX =
            //1; //always
            //2; //subharmonic
            //3; //2:1
            //4;
            //6;
            //7;
            //8;
            16;
            //32;


        /** (reducing) conf factor for the eternal belief truth in the EternalDefaultTable when freqUsually is set */
        public static final double REWARD_USUALLY_CONF_FACTOR = 1/4f;

        static {
            //noinspection ConstantConditions
            assert(SIGNAL_SLEEP_DURS_MAX >= 0);
            //noinspection ConstantConditions
            assert(SIGNAL_SLEEP_DURS_MAX <= SIGNAL_STRETCH_DURS_MAX);
        }





        /**
         * maximum time between signal updates to stretch an equivalently-truthed
         * data point across.
         * ie. stretches perception across amounts of lag
         *
         * values greater than 1 and less than 2 are probably best, since
         * it is unrealistic to expect it will be perfectly on time (1.0)
         * and increasing latching will further distort the recorded signal.
         */
        public static final float SIGNAL_LATCH_LIMIT_DURS =
            1.5f;
            //2;
            //1;


//        public static final float SENSOR_SURPRISE_MIN_DEFAULT =
//            0.98f;
//            //0.75f;
//            //0.5f;
//
//        public static final float SENSOR_SURPRISE_MIN_DEFAULT_MOTOR =
//            0.99f;
//            //0.9f;
//            //0.5f;


    }

    /** max tolerance time difference (in durations) for unification of temporal terms */
    public final FloatRange unifyDurs = new FloatRange(0.5f, 0, 2 );

    @Deprecated public final FloatRange questionForgetting = new FloatRange(0.5f, 0, 1);


    /**
     * how many cycles above which to dither dt and occurrence time
     * TODO move this to Time class and cache the cycle value rather than dynamically computing it
     */
    public final IntRange dtDither = new IntRange(1, 1, 1024);

    /**
     * hard upper-bound limit on Compound target complexity;
     * if this is exceeded it may indicate a recursively
     * malformed target due to a bug or unconstrained combinatorial explosion
     */
    public final IntRange volMax = new IntRange(64, 0, 384 /*term.COMPOUND_VOLUME_MAX*/);
    public final int volMax() { return volMax.intValue(); }

    /**
     * truth confidence threshold necessary to form tasks
     */
    public final ConfRange confMin = new ConfRange();
    /**
     * global truth frequency resolution by which reasoning is dithered
     */
    public final FloatRange freqResolution = new FloatRangeRounded(truth.TRUTH_EPSILON, truth.TRUTH_EPSILON, 1f, truth.TRUTH_EPSILON);
    /**
     * global truth confidence resolution by which reasoning is dithered
     */
    public final FloatRange confResolution = new FloatRangeRounded(truth.TRUTH_EPSILON, truth.TRUTH_EPSILON, 0.5f, truth.TRUTH_EPSILON);
//    {
//        @Override
//        public void set(float value) {
//            super.set(value);
//            value = this.get();
//            if (NAL.this.confMin.floatValue() < value)
//                NAL.this.confMin.set(value);
//        }
//    };
    /**
     * Default priority of input question
     */


    /**
     * Default priority of input question
     */

    public final ConfRange beliefConfDefault = new ConfRange(0.9f);
    public final ConfRange goalConfDefault = new ConfRange(0.9f);

    /** HACK use PriNode.amp(..) to set these.  will figure this out.  pri wont work right, as this is the actual value vs. the advised (user provided) */
    public final PriSource beliefPriDefault = new PriSource("pri.", 0.5f);
    public final PriSource goalPriDefault = new PriSource("pri!", 0.5f);
    public final PriSource questionPriDefault = new PriSource("pri?", 0.5f);
    public final PriSource questPriDefault = new PriSource("pri@", 0.5f);

    public final Time time;

    protected final Supplier<Random> random;

    protected NAL(Time time, Supplier<Random> rng) {
        super();
        this.random = rng;
        (this.time = time).reset();
    }

    /**
     * creates a new evidence stamp
     */
    public final long[] evidence() {
        return new long[]{time.nextStamp()};
    }

    static Atom randomSelf() {
        return $.uuid(/*"I_"*/);
    }

    /**
     * number of time units (cycles) to dither into; >= 1
     */
    public final int dtDither() {
        return this.dtDither.intValue();
    }

    /** TODO double */
    public final float confDefault(byte punctuation) {

        return switch (punctuation) {
            case BELIEF -> this.beliefConfDefault.floatValue();
            case GOAL -> this.goalConfDefault.floatValue();
            default -> throw new RuntimeException("Invalid punctuation " + punctuation + " for a TruthValue");
        };
    }

    public final float priDefault(byte punctuation) {
        PriNode p;
        switch (punctuation) {
            case BELIEF: p = this.beliefPriDefault; break;
            case GOAL: p = this.goalPriDefault; break;
            case QUESTION: p = this.questionPriDefault; break;
            case QUEST: p = this.questPriDefault; break;
            case COMMAND: return 1;
            default: throw new RuntimeException("Unknown punctuation: " + punctuation);
        }
        return p.pri();
    }


    /** temporal momentum. time dilation factor.
     *  allows memorable evidence to accumulate faster than it can be forgotten */
    public FloatFunction<NALTask> eternalization = x -> 0;

    /**
     * provides a Random number generator
     */
    @Override
    public final Random random() { return random.get(); }

    public enum truth { ;

        /**
         * internal granularity which truth components are rounded to
         * minimum resolution for freq and conf components
         */
        public static final float TRUTH_EPSILON =
            1f/Short.MAX_VALUE;
            //1f/100;
            //1f/256;
            //1f/512;
            //1f/1024;
            //1f/8192;

        /** discrete confidence step for fine-grain 23-bit taskregion conf */
        public static final double TASK_REGION_CONF_EPSILON = 1.0/(1<<23);

        public static final double CONF_MAX = 1.0 - TRUTH_EPSILON;
        public static final double EVI_MAX = TruthFunctions.c2e(CONF_MAX);


        /** since conf will be lower than evi approaching zero,
          * define the evidence min in terms of a confidence min
          * that respects 32-bit Float precision limits.
          *
          * expect ~16 digits of reliable 32-bit float precision:
          *    https://en.wikipedia.org/wiki/Floating-point_arithmetic#Internal_representation
          *
          * it's important for precision to tolerate low-confidence internal truth calculations
          * that may eventually accumulate to threshold levels.
          */
        public static final double CONF_MIN =
            //TRUTH_EPSILON;
            ((double)TRUTH_EPSILON) / answer.PROJECTION_CAPACITY;
            //((double)TRUTH_EPSILON) / sqr(answer.PROJECTION_CAPACITY);
            //((double)TRUTH_EPSILON) / (2 * answer.REVISION_CAPACITY);
            //Util.sqr((double)TRUTH_EPSILON) / answer.REVISION_CAPACITY;
            //Util.sqr((double)TRUTH_EPSILON / answer.REVISION_CAPACITY);
            //1.0E-3;
            //1.0E-4;
            //1.0E-5;
            //1.0E-7;
            //1.0E-8;
            //1.0E-12;
            //1.0E-16;
            //1.0E-24;
            //1.0E-32;
            //1.0E-34;
            //1.0E-16;
            //1.0E-15;
            //1.0E-14;
            //1.0E-13;
            //1.0E-9;
            //1.0E-3;
            //1.0E-6;
            //c2wSafe(TRUTH_EPSILON);
            //Float.MIN_NORMAL;

        public static final double EVI_MIN = TruthFunctions.c2e(CONF_MIN);
        public static final double EVI_MIN_safe = Math.max(EVI_MIN - Double.MIN_NORMAL, Double.MIN_NORMAL); //safety margin

        public static final boolean ABSORB_ON_TASK_MERGE = false;

        static {
            if (TruthFunctions.e2c(EVI_MIN_safe) < Double.MIN_NORMAL)
                throw new RuntimeException("CONF_MIN too small");
        }

//        public static final boolean NEED_HARD = false;

        /** may significantly distort results if false */
        public static final boolean INTERMPOLATE_DISCOUNT_EVIDENCE = true;

        /** strict: templateMatch will fail in cases where a CONJ template resolves to a bundled INH, which is valid */
        public static final boolean TEMPLATE_MATCH_STRICT = false;

        /** false will increase temporal accuracy at the expense of
         * creating a potentially larger variety of tasks
         */
        public static final boolean DYNTASKIFY_OCC_DITHER = true;
        public static final boolean DYN_DT_DITHER = DYNTASKIFY_OCC_DITHER;


        public static final boolean REPROJECT_OCCURRENCE_TRUTH = false;
        public static final boolean REPROJECT_OCCURRENCE_RANGE = false;
        public static final boolean REPROJECT_TRIANGULAR = true;


        public static final boolean CONF_DITHER_ROUND_UP = false;

        public static final boolean DITHER_TEMPORAL_MERGE = false;

        public static final boolean ACTION_GOAL_OVERLAP_ALLOW = false;

        /**
         * PRE is potentially lossy, especially since projection time may change after filtering.
         * TODO analyze benefits/risk, considering concentration */
        public static final boolean TRUTH_PROJECTION_FILTER_WEAK_PRE = false;
        public static final boolean TRUTH_PROJECTION_FILTER_WEAK_POST = true;



        @Deprecated public static final float CONF_MINf = (float)CONF_MIN;

        /** concentrate the temporal region to minimize evidence dilution that a naive temporal union might result in
         * TODO currently has a bug, so disabled.
         */
		public static final boolean CONCENTRATE_PROJECTION = true;

		public static final EviCurve curve =
            //new EviCurve.InverseSquareOuter();
            new EviCurve.InverseExponential(
                    //1
                    //2
                    4
                    //8
                    //16
                    //64
            );

            //new EviCurve.InverseLinear(1)

//            new EviCurve.InverseSquare(
//                1
//                //2
//                //4
//                //8
//                //16
//                //64
//            )


            //EviCurve.InverseSqrt;

            //EviCurve.InverseLinear;

            //EviCurve.Exponential;

        ;

        /** automatic / virtual impl temporal induction */
        public static final boolean DYNAMIC_CONJ_TRUTH = true;

        public static final boolean DYNAMIC_INH_SECT_TRUTH = true;

        public static final boolean DYNAMIC_IMPL = true;
        public static final boolean DYNAMIC_IMPL_filter = true;

        public static final boolean DYNAMIC_IMPL_SUBJ_DISJ_MIX_TRUTH = true;
        @Deprecated public static final boolean DYNAMIC_IMPL_SUBJ_CONJ_TRUTH = false;

        public static final boolean DYNAMIC_IMPL_PRED_CONJ_TRUTH = true;

        public static final boolean DYNAMIC_DELTA = true;
        public static final boolean DYNAMIC_DELTA_GOALS = false;

        /** TODO debug contraposition=true */
        public static final boolean DYNAMIC_IMPL_CONTRAPOSITION = true;


        /** divide truth function doubt option */
        public static final boolean DIVIDE_DOUBT = true;

        /** experimental */
        @Deprecated public static final boolean OPEN_WORLD = false;

//        /** experimental */
//        public static final boolean STRONG_COMPOSITION = IS("STRONG_COMPOSITION");


        /** structural reduction confidence discount rate;
         *  (working name until something better)
         *  OpenNARS calls this: 'reliance'
         *  */
        public static final double GULLIBILITY = 0.9f;


        /** isosceles is less strict */
        public static final boolean RELATIVE_PROJECTION_MODE_CLASSIC_OR_ISOSCELES = true;

//        public static final boolean INTERSECTION_FADE_NONPOLAR = false;

        public static final PriMerge CONF_COMPOSITION =
            PriMerge.and;
            //PriMerge.mean;
//            PriMerge.min;
    }

    public enum premise {
        ;

        /** TODO move elsewhere */
        public static final int TERMLINK_CAP_MAX = 1024;
        /** TODO move elsewhere */
        public static final int TERMLINK_CAP_MIN = 512;

        public static final boolean TRIM_PREMISE_OCC = true;


        /** whether to avoid double-premise stamp zipping when the premise's
         * task is a question, and the conclusion punc is not a question
         * TODO make this consider hasBelief()
         * */
        @Deprecated public static final boolean QUESTION_TO_BELIEF_DONT_ZIP = false;
    }

    public enum temporal {
        ;
        /** experimental */
        @Deprecated public static final boolean EVENT_BAG = false;

        public static final boolean SCAN_START_RANDOM_OR_MID = true;

        public static final boolean CONCENTRATE_COMPRESS_MERGE =
            false;

        /** TODO 'both', not just 'either' (random) */
        @Is("Arrow_of_time")
        public static final boolean TEMPORAL_INDUCTION_IMPL_BIDI =
            false;
            //true;

        public static final boolean TEMPORAL_INDUCTION_IMPL_BOTH_SUBJS =
            //true;
            false;

        /** whether to also generate the disj after conj */
        public static final boolean TEMPORAL_INDUCTION_DISJ =
            true;
            //false;

        public static final boolean TEMPORAL_INDUCTION_IMPL = true;
    }


    public enum belief {
        ;

        /**
         * true will filter sub-confMin revision results.  false will not, allowing sub-confMin
         * results to reside in the belief table (perhaps combinable with something else that would
         * eventually raise to above-confMin).  generally, false should be more accurate with a tradeoff
         * for overhead due to increased belief table churn.
         */
        public static final boolean REVISION_MIN_EVI_FILTER = false;
//        /**
//         * perceptible priority increase % (over the higher pri of the two merged) that warrants automatic reactivation.
//         * used during Remember's merge repeat suppression filter
//         */
//        public static final float REMEMBER_REPEAT_PRI_PCT_THRESHOLD = 0.01f;
        /**
         * memory reconsolidation period - time period for a memory to be refreshed as new
         *
         * memory momentum.
         *
         * useful as a novelty threshold:
         * >=0, higher values decrease the rate at which repeated tasks can be reactivated
         */
        public static final double NOVEL_DURS =
                //0.5f; //high-precision
                //Util.PHI_min_1f
                1
                //2 //low-precision
                //8
        ;



        /**
         * maximum span of a Task, in cycles.
         * beyond a certain length, evidence integration precision suffers accuracy diminishes and may become infinite
         */
        public static final long TASK_RANGE_LIMIT = 1L << 61 /* estimate */;


        //false;

        /** TODO test */
        public static final boolean QUESTION_MERGE_AGGRESSIVE = false;

    }


    public enum term {
        ;

        public static final boolean INH_PRINT_COMPACT = IS("INH_PRINT_COMPACT", false);

        /**
         * whether INT atoms can name a concept directly
         */
        public static final boolean INT_CONCEPTUALIZABLE = IS("INT_CONCEPTUALIZABLE");

//        /**
//         * EXPERIMENTAL logical closure for relations of negations
//         *
//         * applies certain reductions to INH and SIM terms when one or both of their immediate subterms
//         * are negated.  in assuming a "closed-boolean-world" in which there is one and only one
//         * opposite for any truth frequency in 0..1,
//         * <p>
//         * then (some of) the following statements should be equivalent:
//         * <p>
//         * INH
//         * (x --> --y)    |-  --(x --> y)
//         * (--x --> y)    |-  --(x --> y)
//         * (--x --> --y)  |-    (x --> y)
//         * <p>
//         * SIM (disabled)
//         * (x <-> --y)    |-  --(x <-> y)
//         * (--x <-> --y)  |-    (x <-> y)
//         */
//        @Skill({"List_of_dualities", "Nondualism", "Möbius_strip"})
//        @Deprecated public static final boolean INH_CLOSED_BOOLEAN_DUALITY_MOBIUS_PARADIGM= IS("INH_CLOSED_BOOLEAN_DUALITY_MOBIUS_PARADIGM");

        /**
         * absolute limit for constructing terms in any context in which a NAR is not known, which could provide a limit.
         * typically a NAR instance's 'compoundVolumeMax' parameter will be lower than this
         *
         * it helps if this is as low as possible for precision in measuring aggregate statistics involving term complexity
         */
        public static final int COMPOUND_VOLUME_MAX =
            4096;
            //Short.MAX_VALUE;

        /**
         * limited because some subterm paths are stored as byte[]. to be safe, use 7-bits
         */
        public static final int SUBTERMS_MAX = Byte.MAX_VALUE;
        public static final int MAX_INTERNED_VARS = Byte.MAX_VALUE;
        /**
         * how many INT terms are canonically interned/cached. [0..n)
         */
        public static final int ANON_INT_MAX = Byte.MAX_VALUE;

        public static final int TERM_BUFFER_MIN_REPLACE_AHEAD_SPAN = 2;
//        public static final int TERM_BUFFER_MIN_INTERN_VOL = 2;


        /** prevent variable introduction from erasing negative compounds,
         *  though content within negatives can be var introduced as normal. */
        public static final boolean VAR_INTRODUCTION_NEG_FILTER = true;

        /** minimum product subterms for image structural transformations */
        public static final int imageTransformSubMin = 2;

        /** false to prevent NAR from attempting conceptualization of terms exceeding its max volume */
        public static final boolean CONCEPTUALIZE_OVER_VOLUME = false;

        /** HOL */
        @Is("Higher-order_logic")
        public static final boolean IMPL_IN_CONJ =
                true;
                //false;

        public static final boolean CONJ_FACTOR =
                true;
                //false;

        /** prevents erasing temporal sequence data by sacrificing the entire expression */
        public static final boolean SEQ_CAREFUL = true;

        /** master INH bundle enable */
        public static final boolean INH_BUNDLE = true;

        /** if false, helps to separate the set of HOL (impl) concepts, from event-based concepts.
         *  consider if a CONJ goal with an IMPL sub-event decomposes an IMPL goal
         *  which may (and probably is set to) be invalid.
         * */
        public static final boolean CONJ_INDUCT_IMPL = term.IMPL_IN_CONJ;

        public static final boolean NEG_INTRIN_CACHE = true;

//        /** experimental */
//        public static final boolean CONJ_RANGE_ECHO = false;

//        /** experimental TODO fix */
//        public static final boolean CONJ_FACTOR = false;
    }

    public enum test {
        ;

        /**
         * for NALTest's: extends the time all unit tests are allowed to run for.
         * normally be kept to 1 but for debugging this may be increased to find what tests need more time
         */
        public static final float TIME_MULTIPLIER = 1f;

        /**
         * how precise unit test Truth results must match expected values to pass
         */
        public static final float TEST_EPSILON =
                1/100f; //standard NAL test precision

        public static final boolean DEBUG_EXTRA = false;
        public static final boolean DEBUG_ENSURE_DITHERED_TRUTH = false;
        public static final boolean DEBUG_ENSURE_DITHERED_OCCURRENCE= false;
        public static final boolean DEBUG_ENSURE_DITHERED_DT = false;
    }

    public enum derive {
        ;

//        /**
//         * may cause unwanted "sticky" event conflation. may only be safe when the punctuation of the task in which the event contained is the same
//         */
//        public static final boolean TIMEGRAPH_ABSORB_CONTAINED_EVENT= IS("TIMEGRAPH_ABSORB_CONTAINED_EVENT");
//        /**
//         * if false, keeps intersecting timegraph events separate.
//         * if true, it merges them to one event. may cause unwanted "sticky" event conflation
//         * may only be safe when the punctuation of the task in which the event contained is the same
//         */
//        public static final boolean TIMEGRAPH_MERGE_INTERSECTING_EVENTS= IS("TIMEGRAPH_MERGE_INTERSECTING_EVENTS");


        /** TODO break into individual ones as this is used in different places */
        public static final int TTL_UNISUBST = 64;

        public static final int TTL_CONJ_MATCH = Math.max(1, TTL_UNISUBST/4);


        /**
         * cost of executing a termute match (leaf of the tree)
         */
        public static final int TTL_COST_MATCH = Math.max(1, TTL_UNISUBST/2);

        /**
         * cost of attempting to grow a branch
         */
        public static final int TTL_COST_TRY = 1;




//        /**
//         * attempt to create a question/quest task from an invalid belief/goal (probably due to missing or unsolved temporal information
//         * in some cases, forming the question may be answered by a dynamic truth calculation later
//         */
//        public static final boolean DERIVE_QUESTION_FROM_AMBIGUOUS_BELIEF_OR_GOAL = false;


        /** override to allow all evidence overlap */
        public static final boolean OVERLAP_ALLOW = false;


        /** derivation explosion rate; >=1 TODO allow float */
        public static final int PREMISE_UNIFICATION_TASKIFY_LIMIT = 1;

        public static final int POST_PROCESS_EVAL_TRIES = 4;

        /** if always eval, it will undo some plugins like Explode, etc
         * TODO move this to a pluggable filter
         * */
        @Deprecated public static final float AUTO_EVAL_NALTASKS = 0.5f;

        /** for extra testing */
        public static final boolean FILTER_SUB_EVI_NALTASK = false;

        public static final boolean FILTER_SUB_EVI_TASKPREMISE = true;

        public static final boolean QUESTION_SALVAGE = false;

        /** TODO decide if this is any faster */
        public static final boolean ACTION_METHODHANDLE = false;
        public static final boolean RUNNER_METHODHANDLE = false;

        /** if false, unification and taskification are two separate premise steps */
        public static final boolean TASKIFY_INLINE = true;
        public static final boolean COMPILE_TO_LAMBDA = false;

        /** may not be helpful, TODO determine */
        public static final boolean REMAING_AMONG_REDUCER = false;

        public static final boolean VARIABLE_INTRODUCE_QUESTIONS = true;

        public static final boolean CAUSE_PUNC = false;



        /**
         * how quickly the termbag can be filled.
         * this establishes a frequency of approximately how many durations until refill */
        public static final float TERMLINK_FILL_RATE =
            1;
            //1/2f;

        /** TODO FloatRange */
        public static final float TERMLINK_FORGET_RATE =
            1;
    }

    public enum derive_temporal {
        ;

        /**
         * whether timegraph should not return solutions with volume significantly less than the input's.
         * set 0 to disable the filter
         */
        public static final float TIMEGRAPH_DEGENERATE_SOLUTION_THRESHOLD_FACTOR = 0.75f;

        /** for dt's and absolute events for solution events */
        public static final boolean TIMEGRAPH_DITHER_EVENTS_EXTERNALLY =
            true;

        /**
         * whether to dither events as they are represented internally.  output events are dithered for the NAR regardless.
         */
        public static final boolean TIMEGRAPH_DITHER_EVENTS_INTERNALLY =
            true;
            //false;

        @Range(min = 1, max = 8)
        public static final int TIMEGRAPH_ITERATIONS = 1;
    }
    public enum unify {
        ;

        /**
         * max variable unification recursion depth as a naive cyclic filter
         * includes indirections through common variables so should be at least 3.
         */
        public static final int UNIFY_VAR_RECURSION_DEPTH_LIMIT = 16;
        public static final int UNIFY_COMMON_VAR_MAX = UNIFY_VAR_RECURSION_DEPTH_LIMIT;
        public static final int UNIFICATION_STACK_CAPACITY = 64;

        public static final boolean SHUFFLE_TERMUTES = IS("SHUFFLE_TERMUTES", true);

        public static final boolean UNISUBST_RECURSION_FILTER = false;
    }

    public static final boolean NORMALIZE_STRICT = true;


//    /** conjunction factoring, experimental */
//    @Deprecated public static final boolean CONJ_FACTOR = false;

    public static final float BUTTON_THRESHOLD_DEFAULT = 0.5f;

}