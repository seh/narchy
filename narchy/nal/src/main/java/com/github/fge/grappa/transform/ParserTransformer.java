/*
 * Copyright (C) 2009-2011 Mathias Doenitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.fge.grappa.transform;

import com.github.fge.grappa.transform.base.ParserClassNode;
import com.github.fge.grappa.transform.base.RuleMethod;
import com.github.fge.grappa.transform.generate.ActionClassGenerator;
import com.github.fge.grappa.transform.generate.ClassNodeInitializer;
import com.github.fge.grappa.transform.generate.ConstructorGenerator;
import com.github.fge.grappa.transform.generate.VarInitClassGenerator;
import com.github.fge.grappa.transform.process.*;
import com.google.common.annotations.VisibleForTesting;
import org.objectweb.asm.ClassWriter;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public final class ParserTransformer
{
    private ParserTransformer()
    {
    }

    // TODO: remove "synchronized" here
    // TODO: move elsewhere
    public static <T> Class<? extends T> transformParser(Class<T> parserClass) throws IOException {
        synchronized (parserClass /*ParserTransformer.class*/) {
            return (Class<? extends T>) extendParserClass(parserClass).getExtendedClass();
        }
    }

    /**
     * Dump the bytecode of a transformed parser class
     *
     * <p>This method will run all bytecode transformations on the parser class
     * then return a dump of the bytecode as a byte array.</p>
     *
     * @param parserClass the parser class
     * @return a bytecode dump
     *
     * @throws Exception FIXME
     * @see #extendParserClass(Class)
     */
    // TODO: poor exception specification
    public static byte[] getByteCode(Class<?> parserClass) throws IOException
    {
        return extendParserClass(parserClass).getClassCode();
    }

    @VisibleForTesting
    public static ParserClassNode extendParserClass(Class<?> parserClass)
            throws IOException {
        ParserClassNode classNode = new ParserClassNode(parserClass);
        new ClassNodeInitializer().process(classNode);
        runMethodTransformers(classNode);
        new ConstructorGenerator().process(classNode);
        defineExtendedParserClass(classNode);
        return classNode;
    }

    // TODO: poor exception handling again
    private static void runMethodTransformers(ParserClassNode classNode) {

        // TODO: comment above may be right, but it's still dangerous
        // iterate through all rule methods
        // since the ruleMethods map on the classnode is a treemap we get the
        // methods sorted by name which puts all super methods first (since they
        // are prefixed with one or more '$')
        Collection<RuleMethod> mm = classNode.getRuleMethods().values();

        //HACK TODO remove and allow parallelism
        mm./*parallelStream().*/forEach(m -> {
            if (!m.hasDontExtend()) {
                for (RuleMethodProcessor methodProcessor : ruleMethodProcessors)
                    if (methodProcessor.appliesTo(classNode, m)) {
                        try {
                            methodProcessor.process(classNode, m);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
            }
            if (!m.isGenerationSkipped()) {
                classNode.methods.add(m);
            }
        });

//        for (final RuleMethod m: mm) {
//            if (!m.hasDontExtend()) {
//                for (final RuleMethodProcessor methodProcessor : methodProcessors)
//                    if (methodProcessor.appliesTo(classNode, m))
//                        methodProcessor.process(classNode, m);
//            }
//
//        }
//
//        for (final RuleMethod ruleMethod: mm)
//            if (!ruleMethod.isGenerationSkipped())
//                classNode.methods.add(ruleMethod);
    }

    private static final List<RuleMethodProcessor> ruleMethodProcessors = List.of(
            new UnusedLabelsRemover(),
            new ReturnInstructionUnifier(),
            new InstructionGraphCreator(),
            new ImplicitActionsConverter(),
            new InstructionGroupCreator(),
            new InstructionGroupPreparer(),
            new ActionClassGenerator(false),
            new VarInitClassGenerator(false),
            new RuleMethodRewriter(),
            new SuperCallRewriter(),
            new BodyWithSuperCallReplacer(),
            new VarFramingGenerator(),
            new LabellingGenerator(),
            new CachingGenerator()
        );


    private static void defineExtendedParserClass(ParserClassNode node)
    {
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        node.accept(writer);
        node.setClassCode(writer.toByteArray());

        String className = node.name.replace('/', '.');
        byte[] bytecode = node.getClassCode();

//        final ClassLoader classLoader = node.getParentClass().getClassLoader();
        Class<?> extendedClass;

//        try (
//            final ReflectiveClassLoader loader
//                = new ReflectiveClassLoader(classLoader);
//        ) {
//            extendedClass = loader.loadClass(className, bytecode);
//        }
        try {
            extendedClass = GroupClassGenerator.CL.defineClass(className, bytecode);
            node.setExtendedClass(extendedClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}