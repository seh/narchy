package nars.term;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static nars.$.$$;
import static nars.$.$$c;
import static nars.term.util.Testing.assertEventOf;
import static nars.term.util.Testing.assertNotEventOf;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CondOfTest {

    @Test
    void CondOfSimple() {
        //assertFalse(Conj.eventOf($$("x"), $$("x")));
        assertEventOf("(x &&+1 --x)", "x");
        assertEventOf("(x &&+1 --x)", "--x");
        assertEventOf("(x && y)", "x");
        assertEventOf("(x &&+1 y)", "x");
        assertEventOf("((x && y) &&+1 z)", "z");
    }

    @Test
    void CondOfSimple2() {
        assertEventOf("((x&&y) &&+1 z)", "(x&&y)");
    }

    @Test
    void CondOfSubSeq() {
        assertEventOf("(z &&+1 (x &&+1 y))", "(x &&+1 y)");
        assertEventOf("(z &&+1 (x &&+1 (y &&+1 z)))", "(x &&+1 y)");
        assertEventOf("(z &&+1 (x &&+1 y))", "(z &&+1 x)");
        assertEventOf("(z &&+1 (x &&+1 y))", "(z &&+2 y)");
        assertFalse($$c("(z &&+1 (x &&+2 y))").condOf($$("(x &&+1 y)")));
    }
    @Test
    void CondOfSubSeqFactored() {
        assertEventOf("(z && (x &&+1 y))", "(x &&+1 y)");
        assertEventOf("(z && (x &&+1 (y &&+1 z)))", "(x &&+1 y)");
        assertEventOf("(z && (x &&+1 y))", "(z && x)");
        assertEventOf("(z && (x &&+1 y))", "(z && y)");
        assertFalse($$c("(z && (x &&+2 y))").condOf($$("(x &&+1 y)")));
    }
    @Test
    void CondOfSubSeqFactoredPartial() {
        assertEventOf("(z && (x &&+1 y))",
                "(x&&z)");
    }
    @Test
    void CondOfSubSeqFactoredPartial2() {
        assertEventOf("(z && (x &&+1 y))",
                "((z && x) &&+1 y)");

        assertNotEventOf("(z && (x &&+1 y))",
                "((z && w) &&+1 y)");
    }

    @Test
    void CondOfXternal1() {
        assertEventOf("(x &&+- y)", "x");
        assertEventOf("(x &&+- y)", "y");
    }

    @Test
    void CondOfXternal3() {
        assertEventOf("((x&&z) &&+- y)", "x");
        assertEventOf("((x&&z) &&+- y)", "(x&&z)");
    }

    @Test
    void CondOfXternal4() {
        assertEventOf("((x &&+1 z) &&+- y)", "x");
        assertEventOf("((a:(b&c) &&+1 z) &&+- y)", "a:b");
    }

    @Test
    void CondOfXternal5() {
        assertEventOf("((x &&+- z) && y)", "(x &&+- z)");
        assertEventOf("((x &&+- z) && y)", "y");
        assertEventOf("((x &&+- z) &&+- y)", "(x &&+- z)");
        assertEventOf("((x &&+- z) &&+1 y)", "(x &&+- z)");

        assertEventOf("((x &&+- z) && y)", "(x && y)");
    }

    @Test
    void CondOfXternal2() {
        assertEventOf("((x &&+1 z) &&+- y)", "(x &&+1 z)");
    }

    @Test
    void CondOfXternalSuper1() {
        assertEventOf("((x && z) &&+- (a && b))", "a");
    }
    @Test
    void CondOfXternalSuper2() {
        assertEventOf("((x && z) &&+- (a && b))", "(x && z)");
    }
    @Test
    void CondOfXternalSuper3() {
        assertEventOf("((x && z) &&+- (a && b))", "(z &&+- a)");
    }
    @Test
    void CondOfXternalSuper4() {
        assertEventOf("((x && z) &&+- (a && b))", "((x && z) &&+- a)");
    }

    @Test
    void CondOfXternalSuper5() {
        assertEventOf("((x && z) &&+- (a && b))", "(x &&+- z)");
    }

    @Test
    void CondOfXternalSuperNot() {
        assertNotEventOf("((x && z) &&+- (a && b))", "(z && a)");
    }


    @Test
    void ConjEventOfXternalDisj1() {
        assertTrue(
    $$c("(--x &&+- --x)").condOf($$("x"), 0)
        );
    }

    @Disabled
    @Test
    void ConjEventOfXternalDisj2() {
        assertTrue(
                $$c("(--(--x &&+- --x) &&+- --(--x &&+- --x))").condOf($$("x"), 0)
        );
    }


    @Test
    void SequentialDisjunctionAbsorb2() {
        Compound x = $$c("(--R &&+600 jump)");
        assertTrue(x.condOf($$("--R")));
        assertFalse(x.condOf($$("R")));
    }

}