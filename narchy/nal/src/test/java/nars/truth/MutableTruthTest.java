package nars.truth;

import nars.NAL;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

class MutableTruthTest {

	@Test void distinguishBetweenNaN_and_eviMin() {
		MutableTruth m = new MutableTruth(0.5f, 0);
		MutableTruth n = new MutableTruth();
		MutableTruth q = new MutableTruth(0.5f, NAL.truth.EVI_MIN);
        assertNotEquals(m, n);
        assertNotEquals(q, n);
	}
}