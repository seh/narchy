package nars;

import nars.time.clock.CycleTime;

import java.util.concurrent.ThreadLocalRandom;

public class DummyNAL extends NAL {

    public DummyNAL() {
        super(new CycleTime(), ThreadLocalRandom::current);
    }

    @Override
    public float dur() {
        return 1;
    }

    @Override
    public long time() {
        return 0;
    }
}
