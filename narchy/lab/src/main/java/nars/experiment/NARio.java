package nars.experiment;

import jcog.Is;
import jcog.data.list.Lst;
import jcog.math.FloatSupplier;
import jcog.signal.FloatRange;
import jcog.signal.IntRange;
import jcog.signal.wave2d.MonoBufImgBitmap2D;
import nars.$;
import nars.NAR;
import nars.Player;
import nars.Term;
import nars.experiment.mario.LevelScene;
import nars.experiment.mario.MarioComponent;
import nars.experiment.mario.Scene;
import nars.experiment.mario.level.Level;
import nars.experiment.mario.sprites.Mario;
import nars.game.Game;
import nars.game.action.AbstractAction;
import nars.game.action.BiPolarAction;
import nars.game.sensor.AbstractSensor;
import nars.game.sensor.SelectorSensor;
import nars.gui.GameUI;
import nars.sensor.PixelBag;
import nars.video.AutoClassifiedBitmap;
import spacegraph.space2d.container.grid.Gridding;

import javax.swing.*;
import java.util.List;

import static nars.$.*;
import static nars.experiment.mario.level.Level.*;
import static nars.experiment.mario.sprites.Mario.KEY_JUMP;
import static nars.game.GameTime.fps;
import static spacegraph.SpaceGraph.window;

public class NARio extends Game {

	@Is({"Afterlife", "Near-death_experience", "Bardo", "Hell", "Womb"})
	public final IntRange afterLifeTime = new IntRange(100, 1, 2000);

	static final float narFPS = 50, gameFPS = 25;

	private final boolean senseCamera = true;
	private final boolean senseMotion = true;
	private final boolean senseTiles = true;

	private final MarioComponent game = new MarioComponent(640, 480);
	int lastCoins;
	float lastX;
	private Mario theMario;

	public final FloatRange fps = game.fps;

	public NARio() {
		this($$("nario"));
	}

	public NARio(Term id) {
		super(id, fps(gameFPS));
	}

	public static void main(String[] args) {

		NARio g = new NARio();
		new Player(narFPS, n -> n.add(g))
			.ready(n-> {
				window(new Gridding(
					g.s.stream().map(z -> GameUI.col(z, g)
						//new VectorSensorChart((VectorSensor)z, nar).withControls())
					)), 200, 200);
			}).start();
	}


	private final Lst<AbstractSensor> s = new Lst();

	@Override
	protected void init() {
		SwingUtilities.invokeLater(()->{
			JFrame f = new JFrame("Infinite NARio");
			f.setIgnoreRepaint(true);
			f.setResizable(false);
			f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			f.setContentPane(game);
			f.pack();
			f.setVisible(true);
		});

		initButton();
		//initBipolar();

		AbstractAction speed = actionPushButton(inh(id, "speed"), n -> {
			if (game.scene != null) {
				Scene.key(Mario.KEY_SPEED, n);
				return n;
			}
			return false;
		});

		PixelBag cc;
		if (senseCamera) {
			cc = new PixelBag(new MonoBufImgBitmap2D(() -> game.image)
				//.mode(ColorMode.Hue)
					//,32, 24
					,64, 32
					//,128, 96
			) {
				{
					panRate = 1;
				}
			}.actions(id, false, false, true, 0.5f);



			int aeStates =
				//10;
				//7;
				6;
				//8;
				//12;
				//16;
				//20;

			AutoClassifiedBitmap camAE = new AutoClassifiedBitmap(
					//$.inh(p(varDep(3), id), p(varDep(1), varDep(2))),
					inh(p(varDep(3), id), p(varDep(1), varDep(2))),
					cc,
					//8, 4,
					16,8,
					//(subX, subY) -> new float[]{/*cc.X, cc.Y, *//*cc.Z*/},
					aeStates, this);
			cc.setMinZoom(1);
			cc.setMaxZoom(0.3f);
			camAE.resolution(
				0.04f
				//0.1f
				//1
				//0.5f
				//0.1f
			);
			camAE.alpha(0.03f);

			nar.runLater(()-> window(camAE.newChart(), 500, 500));

		} else {
			cc = null;
		}

		if (senseTiles) {
			Term[] types = {
				$$("bumpable"), $$("block")
			};
			Term[] dir = {
				p(-1, 0), p(+1, 0), p(0, -1), p(0, +1),
				p(-1, -1), p(+1, -1), p(-1, +1), p(+1, +1)
			};
			s.add(
				addSensor(new SelectorSensor(
					inh(id, "tile"), 2, 7, a -> switch(a) {
						case 0 -> tile(-1, 0);
						case 1 -> tile(+1, 0);
						case 2 -> tile(0, -1);
						case 3 -> tile(0, +1);
						case 4 -> tile(-1, -1);
						case 5 -> tile(+1, -1);
						case 6 -> tile(-1, +1);
						case 7 -> tile(+1, +1);
						default -> throw new UnsupportedOperationException();
					},
					(a, v) ->
						inh(id, p(dir[a], types[v])),
						//$.inh($.p(id, dir[a]), types[v]),
					nar
				))
			);
		}
		if (senseMotion) {

//			Term MOTION = p(the("motion"), id);

			float res = 0.05f;

			final FloatSupplier dx = () -> theMario != null ? theMario.x : 0;
			final FloatSupplier dy = () -> theMario != null ? theMario.y : 0;

			AbstractSensor vx =
					senseDiffBi($.p(id, "move", "x"), 8, dx);
//					senseDiffBi(dx,
		//				inh(MOTION, p(the(-1), the(0))),
//						inh(MOTION, p(the(+1), the(0)))
//					);

			AbstractSensor vy =
					senseDiffBi($.p(id, "move", "y"), 8, dy);
					//senseDiff(inh(id, $.p("move", "y")), 8, dy);
//				senseDiffBi(8, dy,
//					inh(MOTION, p(the(0), the(-1))),
//					inh(MOTION, p(the(0), the(+1)))
//			);

			List.of(vx, vy).forEach(v -> v.resolution(res));
			s.addAll(vx, vy);

		}




//        window(NARui.beliefCharts(this.nar, Stream.of(vx, vy).flatMap(x->x.sensors.stream()).collect(toList())), 400, 300);


		//unitize(Math.max(0, (curX - lastX)) / 16f * MoveRight.floatValue());
		final FloatSupplier rightward = () -> {

			float curX = theMario != null && theMario.deathTime <= 0 ?
					theMario.x : Float.NaN;
			int thresh = 2;
			float reward;
			if ((curX == curX && lastX == lastX) && curX - lastX > thresh) {
				reward = 1;
			} else {
				reward = 0;
			}
			lastX = curX;

			return reward;
		};

		reward("right",
			rightward
		);//.usually(0); //.strength(0.1f);


		reward("money", () -> {
			int coins = Mario.coins;
			int deltaCoin = coins - lastCoins;
			if (deltaCoin <= 0)
				return 0;

			float reward = deltaCoin;// * EarnCoin.floatValue();
			lastCoins = coins;
			return reward;
		});//.usually(0);
		//getCoins.setDefault($.t(0, 0.75f));

		reward("alive", () -> {
//            if (dead)
//                return -1;
//
			if (theMario == null)
				return Float.NaN;

			return theMario.deathTime > 0 ? 0 : /*Float.NaN*/ +1;
		}).strength(0.5f);//.usually(1);

		onFrame(() -> {

			Scene ss = game.scene;

			if (ss instanceof LevelScene level) {
                Mario M = theMario = level.mario;
				if (senseCamera) {
					cc.setXRelative((M.x - level.xCam) / 320);
					cc.setYRelative((M.y - level.yCam) / 240);
				}

				theMario.afterlifeDuration = afterLifeTime.intValue();

			} else {
				theMario = null;
			}

		});

//		nar.add(new BeliefPredict(
//			actions,
//			actions,
//			8,
//			Math.round(2 * nar.dur()),
//			3,
//			new LivePredictor.LSTMPredictor(0.1f, 1),
//			nar,
//			false));



		game.paused = false;
		game.thread.start();
	}



	@Override
	protected void stopping(NAR nar) {
		game.paused = true;
		super.stopping(nar);
	}

//	private SelectorSensor tileSwitch(int dx, int dy) {
////		senseSwitch(new Term[] { $$("bumpable"), $$("block") },
////			() -> tile(dx, dy),
////			inh(p(varDep(1),id), p(dx, dy))
////		);
//	}

	int tile(int dx, int dy) {
		if (game.scene instanceof LevelScene s) {
            Level ll = s.level;
			if (ll != null) {
				Mario mm = s.mario;
				byte block = ll.getBlock(Math.round((mm.x - 8) / 16f) + dx, Math.round((mm.y - 8) / 16f) + dy);
				byte t = Level.TILE_BEHAVIORS[block & 0xff];
				if ((t&BIT_BREAKABLE)!=0 || (t&BIT_BUMPABLE)!=0 || (t&BIT_PICKUPABLE)!=0)
					return 0;
				if ((t&BIT_BLOCK_ALL)!=0)
					return 1;
			}
		}
		return -1;
	}

	private void initButton() {
//		Term MOVE = the("move");
//		Term left = inh(id, p(MOVE, p(-1, 0)));
//		Term right = inh(id, p(MOVE, p(+1, 0)));
//		Term jump = inh(id, p(MOVE, p(0, +1)));
		Term left = inh(id, "L") /*inh(p(id, MOVE), p(-1, 0))*/;
		Term right = inh(id, "R") /*inh(p(id, MOVE), p(+1, 0))*/;
		Term jump = inh(id, "J")/*inh(p(id, MOVE), p(0, +1))*/;

		actionToggle(left, right,
			n -> {
				var s = game.scene; if (s != null) Scene.key(Mario.KEY_LEFT, n);
			},
			n -> {
				var s = game.scene; if (s != null) Scene.key(Mario.KEY_RIGHT, n);
			}
		);

		AbstractAction j = actionPushButton(jump, n -> {
				Scene s = game.scene;
				if (s != null) {
					Mario m = this.theMario;
					if (m != null) {
						Scene.key(KEY_JUMP, n);
						return n;
					}
				}
				return false;
			});

		//j.actionDur(1);


//        actionPushButton($$("down"),
//                n -> { game.scene.key(Mario.KEY_DOWN, n); return n; } );


		//s.actionDur(1);

		//bias
//        j.goalDefault($.t(0, 0.01f), nar);
//        ss.goalDefault($.t(0, 0.01f), nar);
	}

	void initTriState() {
		actionTriState(inh("x", id), i -> {
			boolean n, p;
			switch (i) {
				case -1 -> {
					p = false;
					n = true;
				}
				case +1 -> {
					p = true;
					n = false;
				}
				case 0 -> {
					p = false;
					n = false;
				}
				default -> throw new RuntimeException();
			}
			Scene.key(Mario.KEY_LEFT, n);
			Scene.key(Mario.KEY_RIGHT, p);
			return true;
		});
		actionTriState(inh("y", id), i -> {
			boolean n, p;
			switch (i) {
				case -1 -> {
					p = false;
					n = true;
				}
				case +1 -> {
					p = true;
					n = false;
				}
				case 0 -> {
					p = false;
					n = false;
				}
				default -> throw new RuntimeException();
			}
			Scene.key(Mario.KEY_DOWN, n);
			Scene.key(KEY_JUMP, p);
			return true;
		});


	}

	public void initBipolar() {
		float thresh = 0.25f;


		BiPolarAction X = this.actionBipolar(p(id, "x"), (x) -> {
			if (game == null || game.scene == null) return Float.NaN; //HACK

			float boostThresh = 0.75f;
			if (x <= -thresh) {
				Scene.key(Mario.KEY_LEFT, true);
				Scene.key(Mario.KEY_RIGHT, false);
				Scene.key(Mario.KEY_SPEED, x <= -boostThresh);

				return x <= -boostThresh ? -1 : -boostThresh;
			} else if (x >= +thresh) {
				Scene.key(Mario.KEY_RIGHT, true);
				Scene.key(Mario.KEY_LEFT, false);
				Scene.key(Mario.KEY_SPEED, x >= +boostThresh);

				return x >= +boostThresh ? +1 : +boostThresh;
			} else {
				Scene.key(Mario.KEY_LEFT, false);
				Scene.key(Mario.KEY_RIGHT, false);
				Scene.key(Mario.KEY_SPEED, false);


				return 0f;

			}
		});
		BiPolarAction Y = this.actionBipolar(p(id, "y"), (y) -> {
			if (game == null || game.scene == null) return Float.NaN; //HACK

			if (y <= -thresh) {
				Scene.key(Mario.KEY_DOWN, true);
				Scene.key(KEY_JUMP, false);
				return -1f;

			} else if (y >= +thresh) {
				Scene.key(KEY_JUMP, true);
				Scene.key(Mario.KEY_DOWN, false);
				return +1f;

			} else {
				Scene.key(KEY_JUMP, false);
				Scene.key(Mario.KEY_DOWN, false);

				return 0f;

			}
		});/*.forEach(g -> {
            g.resolution(0.1f);
        });*/

		//window(NARui.beliefCharts(nar, List.of(X.pos, X.neg, Y.pos, Y.neg)), 700, 700);
	}

}