package nars.experiment.market;

import jcog.Fuzzy;
import jcog.Str;
import jcog.TODO;
import jcog.Util;
import jcog.data.list.Lst;
import jcog.math.FloatDifference;
import jcog.ql.dqn.ValuePredictAgent;
import jcog.random.RandomBits;
import jcog.signal.ArraySensor;
import jcog.signal.anomaly.ewma.Ewma;
import nars.$;
import nars.Player;
import nars.experiment.RLPlayer;
import nars.game.Game;
import nars.game.action.util.Reflex;
import nars.gui.NARui;
import nars.sensor.BitmapSensor;
import org.hipparchus.linear.ArrayRealVector;
import spacegraph.space2d.ReSurface;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.Stacking;
import spacegraph.video.Draw;
import tech.tablesaw.io.Source;
import tech.tablesaw.io.csv.CsvReader;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static spacegraph.SpaceGraph.window;

public class Market {

    /**
     * ticker symbol index
     */
    final Map<String, Stock> sym = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        final Game g = new Market().game(
                "/home/me/crypto/all.csv", 10000,
                "BTC", "LTC", "ETH", "XRP"/*,"DOGE"*/);

        boolean nars = false;
        boolean rlBoost = true;

        if (nars) {
            Reflex r = rlBoost ? new Reflex(
                    //ValuePredictAgent::DQN
                ValuePredictAgent::DQrecurrent
                    , g, 1, 2, 4, 8) : null;

            Player p = new Player(g).fps(50);

            p.start();

            if (rlBoost)
                window(NARui.reflexUI(r), 900, 900);
        } else {
            RLPlayer.run(g, 0, ValuePredictAgent::DQNbig);
        }
    }


    public Game game(String csvPath, double initialCash, String... portfolio_) {
        return new MarketGame(csvPath, initialCash, portfolio_);
    }

    private Stock sym(String s) {
        return sym.get(s);
    }


    public Stock symbolAdd(String s) {
        return sym.computeIfAbsent(s, (S)-> new Stock(S));
    }

    public double value(Account a) {

        double totalValue = 0;

        for (Stock s : sym.values())
            totalValue += value(s, a);

        return totalValue;
    }

    private double value(Stock s, Account a) {
        double p = s.price;
        return p == p ?
                p * s.own.getOrDefault(a, 0)
                :
                0;
    }

    public Stock symbol(String symbol) {
        return sym.get(symbol);
    }

    /**
     * snapshot of portfolio state
     */
    static class Performance {
        final long time;
        final double value;
        final double valueChange;
        final private long iteration;

        Performance(long time, double value, double valueChange, long iteration) {
            this.time = time;
            this.value = value;
            this.valueChange = valueChange;
            this.iteration = iteration;
        }
        //TODO store complete portfolio clone
    }

    private class MarketGame extends Game {

        static final double feePerVolume = 0.001; //0.1% "taker" fee
        @Deprecated static final int historyYearMin = 2012;
        @Deprecated static final int historyYearMax = 2023;
        static private final boolean trainCalendar = true;
        static private final boolean trainCalendarYear = false;
        private static final boolean buySellSeparate = false;
//        final Map<String/*Sym*/, TreeMap<Long/*date*/, Float>> prices;
//        final Map<String,Map<Long,Float>> volumes = new TreeMap();
        final NavigableMap<Long, Consumer<Market>> events = new TreeMap<>();


        final Lst<Stock> portfolio;
        final double[] buy, sell, buyActual, sellActual;

        @Deprecated
        final Map<Integer, Integer> BUYING;
        @Deprecated
        final Map<Integer, Integer> SELLING;
        private final double initialCash;
        private final String[] portfolio_;
        private final long START, END;
        private final boolean globalPurchaseAmp = false;

        private final MarketPerformance view;
        private final MarketPerformanceCloud view2;

        float maxBuyRate = 0.25f;
        float maxSellRate = 0.5f;
        /**
         * hard limits on buy and sell rates, as proportions of cash
         */
        float buying, selling;
        /**
         * allows memory-dependent sensor functions a cycle to clear on rewind
         */
        int reset;
        private long iteration;
        private double valueChange;
        private double valueBefore;
        private Account a;

        /** HACK cycles between episodes allowing history values to clear */
        @Deprecated static final int RESET_PERIOD = 32;

        /**
         * in epoch_days
         */
        private long t, marketEnd = Long.MAX_VALUE;

        /** simulated seconds per iteration */
        private final long cycleSeconds = 6 /* hr */ * 60 * 60;

        public MarketGame(String csvPath, double initialCash, String... portfolio_) {
            super("market");
            assert (portfolio_.length > 0);
//TODO        assert(Set.copyOf(portfolio).size()==portfolio.size()): "all unique";
            this.initialCash = initialCash;
            this.portfolio_ = portfolio_;

            portfolio = new Lst(portfolio_.length);

            try {
                var t = new CsvReader().read(new Source(new File(csvPath)));

                t.removeColumns(0, 1);
                //[Integer column: SNo, String column: Name,
                // String column: Symbol, DateTime column: Date, Double column: High, Double column: Low, Double column: Open, Double column: Close, Double column: Volume, Double column: Marketcap]
                t.forEach(r -> {

                    final LocalDateTime WHEN = r.getDateTime(1);
                    //WHEN.toEpochSecond(ZoneOffset.UTC)
                    long when = (WHEN.getLong(ChronoField.EPOCH_DAY)+1)*(24*60*60);

                    String sym = r.getString(0);
                    double priceOpen = r.getDouble(4);
                    double priceClose = r.getDouble(5);
                    //double priceOpenClose = (priceOpen + priceClose) / 2;
                    double volume = r.getDouble(6);

                    Stock s = symbolAdd(sym);

                    addEvent(when + (9 /* 9am */ * 60 * 60), m -> {
                        s.price = priceOpen;
                        s.volume = volume;
                    });

                    addEvent(when + (17 /* 5pm*/ * 60 * 60) /* 12hrs later */, m->{
                        s.price = priceClose;
                    });
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
            START = events.firstKey();
            END = events.lastKey();


            for (String p : portfolio_) {
                Stock s = sym(p);
                if (s == null)
                    throw new NullPointerException();
                portfolio.add(s);
            }

            buying = 1;
            selling = 1;
            valueBefore = Double.NaN;
            reset = 0;
            t = Long.MIN_VALUE;

            a = new Account();
            a.cash = initialCash;

            buy = new double[portfolio.size()];
            sell = new double[portfolio.size()];

            buyActual = new double[portfolio.size()];
            sellActual = new double[portfolio.size()];


//                try {
//                    rewardLog = new PrintStream(new File("/tmp/" + Market.class.getSimpleName() + ".csv"));
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }


            /* SENSE: stock prices */
            Stock[] syms = sym.values().toArray(new Stock[0]);

            addSensor(new BitmapSensor(new ArraySensor(syms.length, true) {
                @Override
                protected float value(int i) {
                    return /*soften*/(float) syms[i].price;
                }
            }, (x,y)->$.inh(x >= 0 ? $.the(syms[x].id) : $.varDep(1), "price"))).resolution(0);

            int portfolioSize = portfolio.size();

            /* SENSE: owned portfolio wealth distribution */
            addSensor(new BitmapSensor(new ArraySensor(1+portfolioSize, true) {
                @Override
                protected float value(int i) {
                    double amt;
                    if (i == portfolioSize)
                        amt = a.cash;
                    else {
                        Stock s = portfolio.get(i);
                        double p = s.price;
                        if (p != p) return 0;
                        amt = p * s.owns(a);
                    }
                    //return (float) (amt / valueBefore);
                    return (float) amt;
                }
            }, (x,y)->$.inh(x < 0 ? $.varDep(1) : (x < portfolioSize ? $.the(portfolio.get(x).id) : $.the("cash")), "own"))).resolution(0);

            /* sense: market volume */
            addSensor(new BitmapSensor(new ArraySensor(syms.length, true) {
                @Override
                protected float value(int i) {
                    return (float) syms[i].volume;
                }
            }, (x,y)->$.inh(x>=0 ? $.the(syms[x].id) : $.varDep(1), "volume"))).resolution(0);


            sense($.inh(id, $.p("second", "day")), () -> {
                return (t%(24*60*60)) / (24f*60*60);
            }).resolution(0);

            /* SENSE: calendar */
            sense($.inh(id, $.p("day", "week")), () -> {
                int dayOfWeek = LocalDate.ofEpochDay(t/(24*60*60)).get(ChronoField.DAY_OF_WEEK); //1..7
                return (dayOfWeek - 1) / (7f - 1);
            }).resolution(0);

            if (trainCalendar) {
                sense($.inh(id, $.p("day", "month")), () -> {
                    int dayOfMonth = LocalDate.ofEpochDay(t/(24*60*60)).get(ChronoField.DAY_OF_MONTH); //1..31
                    return (dayOfMonth - 1) / (31f - 1) /* HACK */;
                }).resolution(0);
                sense($.inh(id, $.p("month", "year")), () -> {
                    int monthOfYear = LocalDate.ofEpochDay(t/(24*60*60)).get(ChronoField.MONTH_OF_YEAR); //1..12
                    return (monthOfYear - 1) / (12f - 1);
                }).resolution(0);

                if (trainCalendarYear) {
                    sense($.inh(id, $.p("year", "history")), () -> {
                        int year = LocalDate.ofEpochDay(t/(24*60*60)).getYear(); //1..12
                        return Util.normalize(year, historyYearMin, historyYearMax);
                    }).resolution(0);
                }
            }

            /* buy/sell actions */
            for (int i = 0; i < portfolioSize; i++) {
                int I = i;
                final Stock s = portfolio.get(I);
                if (buySellSeparate) {
                    //TODO provide actual feedback which must involve separate/async collect and apply stages that the action API does not yet support
                    var B = action($.inh($.p(id, s.id), "buy"), x -> {
                        if (s.price != s.price)
                            x = 0; //doesnt exist yet
                        //dont prefilter: they need to be positive to cancel out sell even if impossible to buy
//                        if (a.cash < s.price)
//                            x = 0; //not enough cash to buy even one
                        buy[I] = x;
                        return x;
                    }, z -> (float) buyActual[I]);

                    var S = action($.inh($.p(id, s.id), "sell"), x -> {
                        if (s.price != s.price)
                            x = 0; //doesnt exist yet
                        //dont prefilter: they need to be positive to cancel out buy even if impossible to sell
//                        if (s.owns(a) <= 0)
//                            x = 0; //owns none, so nothing to sell
                        sell[I] = x;
                        return x;
                    }, z -> (float) sellActual[I]);
                    B.poles = S.poles = 1;
                } else {
                    action($.inh($.p(id, s.id), "own"), x -> {
                        if (s.price != s.price)
                            return 0.5f; //doesnt exist yet

                        double d = Fuzzy.polarize(x);
                        if (d >= 0) {
                            if (s.price <= 0) {
                                //symbol doesnt exist at this time
                                buy[I] = sell[I] = 0;
                                return 0;//return 0.5f;
                            } else {
                                buy[I] = d;
                                sell[I] = 0;
                            }
                        } else {
                            if (s == null || s.own.getOrDefault(a, 0) == 0) {
                                //none owned
                                buy[I] = sell[I] = 0;
                                return Float.NaN;
                                //return 0;
                                //return 0.5f;
                            } else {
                                sell[I] = -d;
                                buy[I] = 0;
                            }
                        }
                        return x;
                    }, z ->(float)
                        // Fuzzy.unpolarize(buyActual[I] - sellActual[I])
                        (buyActual[I] + sellActual[I] > Float.MIN_NORMAL ? (buyActual[I]/(buyActual[I] + sellActual[I])) : Float.NaN)
                    );

                }
            }

            if (globalPurchaseAmp) {
                //global buy/sell strength, relative to % of cash
                action($.inh("buy", "more"), x -> {
                    buying = x;
                });
                action($.inh("sell", "more"), x -> {
                    selling = x;
                });
            }

//            rewardNormalizedPolar($.inh(id, "value"),
            float pctChangeRange =
                0.1f; //more sensitive
                //0.25f;
                //1;
            reward($.inh(id, "value"),
                    new FloatDifference(() -> (float) value(), FloatDifference.Mode.PctChange, this::time) {
                        @Override
                        public float asFloat() {
                            double v = super.asFloat();
                            valueChange = v;
                            double r = Fuzzy.unpolarize(
                                Util.clampSafe(v/pctChangeRange, -1, +1)
                            );
                            return (float)r;
                        }
                    }
            ).resolution(0);

            onFrame(this::pre);
            afterFrame(this::post);
            BUYING = new LinkedHashMap();
            SELLING = new LinkedHashMap();

            view = new MarketPerformance();
            view2 = new MarketPerformanceCloud();
            window(new Stacking(view2,view), 600, 600);
        }

        private void addEvent(long when, Consumer<Market> e) {
            events.merge(when, e, (a, b)->{
                return a.andThen(b);
            });
        }

        /* HACK dynamic range compression to help avoid small values being rounded to zero by precision limitations */
        private static float soften(double x) {
//            return (float) x;
            //return (float) Math.pow(x, 1/2f);
            return (float)Util.logUnit(x, 2);
        }

        /**
         * gather the buy/sell vectors and attempt to execute trades,
         * TODO form feedback vector representing actual trades.
         */
        private void pre() {
            long now = t;
            long prev = now;


            if (reset == 0) {
                boolean RESET = now == Long.MIN_VALUE || now >= marketEnd;
                /*now >= times.last()*/
                if (RESET) {
                    rewind();
                    long s = timeFocus();
                    long e = timeFocus();
                    if (s > e) {
                        long se = e;
                        e = s;
                        s = se;
                    }
                    prev = now = t = s;
                    marketEnd = e;
                } else {
//                    try {
                    //marketDay = now = times.higher(now);
                    now = t = now + cycleSeconds;
//                    } catch (Exception e) {
//                        rewind();
//                        marketDay = now = times.first();
//                    }
                }
            }
            events.subMap(prev, false, now, true)
                    .forEach((k,v) -> v.accept(Market.this));

            valueBefore = value();
        }

        private long timeFocus() {
            RandomBits r = rng();
            //return r.nextLong(START, END); //FLAT
            return Util.lerpLong((float) (1 - Math.pow(r.nextFloat(), 4)), START, END); //biased towards future
        }

        private void post() {
            var B = new ArrayRealVector(buy);
            var S = new ArrayRealVector(sell);
            if (globalPurchaseAmp) {
                double bb = B.getL1Norm();
                if (bb > Double.MIN_NORMAL)
                    B.mapMultiplyToSelf(1.0 / bb);
                else
                    B.mapMultiplyToSelf(0); //zero

                double ss = S.getL1Norm();
                if (ss > Double.MIN_NORMAL)
                    S.mapMultiplyToSelf(1.0 / ss);
                else
                    S.mapMultiplyToSelf(0); //zero
            }

            //assert(Util.sum(b.getDataRef()) <= 1);
            //assert(Util.sum(s.getDataRef()) <= 1);

            double base =
                    //a.cash;  //CASH
                    a.value(Market.this); //ENTIRE PORTFOLIO value

            double buyRate = buying * maxBuyRate * base;
            double sellRate = selling * maxSellRate * base;

            BUYING.clear();
            SELLING.clear();
            for (int i = 0, portfolioSize = portfolio.size(); i < portfolioSize; i++) {
                Stock x = portfolio.get(i);
                double xPrice = x.price;
                double buyAmt = buyRate * B.getEntry(i);
                double sellAmt = sellRate * S.getEntry(i);
                if (buyAmt >= sellAmt) {
                    //TODO optional shares quantization
                    int q = (int) Math.round((buyAmt - sellAmt) / xPrice);
                    if (q > 0)
                        BUYING.put(i, q);
                } else {
                    //TODO optional shares quantization
                    int q = (int) Math.round((sellAmt - buyAmt) / xPrice);
                    if (q > 0)
                        SELLING.put(i, q);
                }
            }

            if (globalPurchaseAmp)
                throw new TODO();

            Arrays.fill(buyActual, 0);
            Arrays.fill(sellActual, 0);

            //sell first to liquidate to cash before buying
            SELLING.forEach(new BiConsumer<>() {
                int k;

                @Override
                public void accept(Integer n, Integer shares) {
                    Stock x = portfolio.get(n);
                    float S;
                    int sold;
                    if (shares > 0 && (sold = x.sell(shares, a)) > 0) {
                        System.out.println("\tsell " + x.id + " * " + shares);
                        double sellVolume = sold * x.price;
                        a.cash -= sellVolume * feePerVolume;
                        S = (float) (sellVolume / sellRate);
                    } else
                        S = 0;
                    sellActual[k++] = S;
                }
            });

            BUYING.forEach(new BiConsumer<>() {
                int k;

                @Override
                public void accept(Integer n, Integer shares) {
                    Stock x = portfolio.get(n);
                    float B;
                    int bought;
                    if (shares > 0 && (bought = x.buy(shares, a)) > 0) {
                        System.out.println("\tbuyy " + x.id + " * " + shares);
                        double buyVolume = bought * x.price;
                        a.cash -= buyVolume * feePerVolume;
                        B = (float) (buyVolume / buyRate);
                    } else
                        B = 0;

                    buyActual[k++] = B;
                }
            });
            Performance p = performance();

            //System.out.println(Str.n2(toFloat(buyActual)) + " "  + Str.n2(toFloat(sellActual)));
            double valueAfter = p.value;
            if (Double.isFinite(valueAfter)) {
                System.out.println(t + "\tvalue=" + Str.n2(valueAfter));
                view.add(p);
                view2.add(p);
            }
//                rewardLog.println(v);
//                rewardLog.flush();

            iteration++;
        }

        private Performance performance() {
            Performance p = new Performance(t, value(), valueChange, iteration);
            return p;
        }

        private void rewind() {
            a.print(Market.this);

            sym.values().forEach(s -> { s.price = s.volume = 0; } );
            t = Long.MIN_VALUE; //TODO random
            a = new Account();
            a.cash = initialCash;
            reset = RESET_PERIOD;
        }

        private double value() {
            if (reset > 0) {
                --reset;
                return Float.NaN;
            } else
                return a.value(Market.this);
        }

        /**
         * vertical zoom
         */
        float vcMax = 0.1f;

        private float vy(double vc) {
            return Math.min(1, (float) Util.normalize(vc, -vcMax, +vcMax));
        }

        class MarketPerformanceCloud extends Surface {
            static final float performanceSamplingRate = 0.02f;
            static final int performanceSamplingCapacity = 512;
            final Deque<Performance> history = new ArrayDeque(performanceSamplingCapacity);

            @Override
            protected void render(ReSurface r) {
                float pr = 4 * (Math.min(w(), h()) / /*Math.sqrt*/(1 + performanceSamplingCapacity));
                final float w = w();// - 2 * pr;
                final float h = h();// - 2 * pr;
                //long now = marketDay;
                float timeRange = END - START;
                for (Performance p : history) {
                    final double pv = p.valueChange;
                    if (!Double.isFinite(pv)) continue;

                    float x = (p.time - START) / timeRange * w;
                    //float v = (float) Util.normalize(p.value, 0, 10_000_000) * h;



                    float y = vy(pv) * h;
                    r.gl.glPushMatrix();
                    r.gl.glTranslatef(x, y, 0);

                    float a = 1 / (1 + (iteration - p.iteration) / 5000f);

                    float re, g, bl;
                    if (p.valueChange >= 0) {
                        g = y;
                        re = 0;
                        bl = 0.5f;
                    } else {
                        re = y;
                        g = 0;
                        bl = 0.5f;
                    }
                    r.gl.glColor4f(re, g, bl, 0.25f * a);
                    Draw.poly(6, pr, true, r.gl);
                    r.gl.glPopMatrix();
                }
            }
            public void add(Performance p) {
                if (!rng().nextBoolean(performanceSamplingRate))
                    return;

                while (history.size() >= performanceSamplingCapacity)
                    history.removeFirst();
                history.add(p);
            }

        }
        class MarketPerformance extends Surface {


            final int EWMA_BARS = 192;
            final Ewma[] bars = Util.arrayOf(i -> new Ewma().reset(0)
                    .period(Math.round(19000f/EWMA_BARS)), new Ewma[EWMA_BARS]);



            public void add(Performance p) {
                if (Double.isFinite(p.valueChange)) {
                    int b = Util.bin((p.time - START) / ((float) (END - START)), bars.length);
                    bars[b].accept(p.valueChange);
                }
            }

            @Override
            protected void render(ReSurface r) {
//                float pr = 4 * (float) (Math.min(w(), h()) / /*Math.sqrt*/(1 + performanceSamplingCapacity));
                final float w = w(), h = h();// - 2 * pr;
                //long now = marketDay;
//                float timeRange = END - START;

                //float barHeight = 0.005f;

                int i = 0;
                for (Ewma b : bars) {
                    float x = b.meanFloat();
                    float dx = 1/(bars.length - 1f);
                    if (Float.isFinite(x)) {
                        float y = vy(x);
//                        r.gl.glPushMatrix();
//                        r.gl.glTranslatef(0, y, 0);
                        r.gl.glColor4f(x<0?1 : 0, x>=0 ? 1 : 0, 0, 0.9f);
//                        r.gl.glLineWidth(10);
                        float xStart = i*dx, xEnd = xStart+dx;
                        float y1, y2;
                        if (x >= 0) {
                            y1 = 0.5f + (y - 0.5f);
                            y2 = 0.5f;
                        } else {
                            y1 = 0.5f;
                            y2 = 0.5f + (y - 0.5f);
                        }
                        Draw.rect(xStart*w, y1*h, (xEnd-xStart)*w, (y2-y1)*h, r.gl);
//                        r.gl.glPopMatrix();
                    }
                    i++;
                }
            }

        }

    }

//    public void displayPortfolio(Account a) {
//        var df = df();
//        System.out.println("\n Account Value: \u00A3" + df.format(value()) + "      Cash: \u00A3" + df.format(cash) + "\n");
//
//        for (Stock s : Market.stocks)
//            System.out.println(s.symbol + "\t" +
//                    "\t" + "pri: \u00A3" + df.format(s.price) + "\t" +
//                    "qty: " + s.own + "\t" + "= \u00A3" +
//                    df.format(value(s)));
//
//    }


//    private void showMenu(Account ac) {
//        // starts market
//        MarketModel thread = new MarketModel();
//        thread.start();
//
//        // menu printed to console
//        System.out.println("""
//
//                Select an option:\s
//                 1. Portfolio
//                 2. Search Stock
//                 3. Trade stock
//                 4. Exit
//                """);
//        int selection;
//        // makes sure user enters in int
//        try {
//            selection = user_input.nextInt();
//        } catch (InputMismatchException ex) {
//            selection = 5;
//        }
//        user_input.nextLine(); // stops input skipping
//
//        switch (selection) {
//            case 1 ->
//                    // displays the users portfolio
//                    ac.displayPortfolio();
//            case 2 ->
//                    // search stock case
//                    searchStock();
//            case 3 ->
//                    // Trade stock case
//                    decideTransaction(ac);
//            case 4 ->
//                    // exit case
//                    exit();
//            default -> System.out.println("Invalid selection. Try Again.");
//        }
//
//    } // end showMenu method

//    private void decideTransaction(Account ac) {
//        // menu printed to console
//        System.out.println("""
//
//                Select an option:\s
//                 1. Buy
//                 2. Sell
//                 3. Sell Short
//                 4. Buy to Cover
//                 5. Back
//                """);
//        int selection;
//        // makes sure user enters in int
//        try {
//            selection = user_input.nextInt();
//        } catch (InputMismatchException ex) {
//            selection = 5;
//        }
//        user_input.nextLine(); // stops input skipping
//
//        switch (selection) {
//            case 1 -> new Buy(ac);
//            case 2 -> new Sell(ac);
//            case 3 -> new SellShort(ac);
//            case 4 -> new BuyToCover(ac);
//            default -> System.out.println("Invalid Selection. Go back to main menu.");
//        }
//    }

//    private void searchStock() {
//        System.out.println("Enter stock name: ");
//        String stockSymbol = user_input.nextLine();
//
//        // search stock name
//        Stock s = stock(stockSymbol, stocks);
//        // display stock and its characteristics
//        if (s == null) {
//            System.out.println("Sorry that stock is not trading");
//        } else {
//            // display stock symbol, description and current price
//            System.out.println(s.symbol + " trading at: \u00A3" + new DecimalFormat("#.00").format(s.price));
//        }
//
//    }

} // end class