package nars.experiment.market;

import java.io.Serializable;
import java.text.DecimalFormat;

public class Account implements Serializable {
    private final String name;
    public double cash;

    public Account() {
        this(null, 0);
    }

    private Account(String name, double cash) {
        this.name = name != null ? name :
                Integer.toString(System.identityHashCode(this), 36);
        this.cash = cash;
    }

    private static DecimalFormat df() {
        return new DecimalFormat("#.00");
    }

    @Override
    public String toString() {
        return name;
    }


    public double value(Market m) {
        return m.value(this) + cash;
    }


    public void print(Market m) {
        System.out.println("PORTFOLIO");
        System.out.println("Cash: " + cash);
        System.out.println("Shares");
        m.sym.values().forEach(s -> {
            int o = s.owns(this);
            if (o > 0) {
                System.out.println("\t" + s + ": " + o);
            }
        });


    }
}