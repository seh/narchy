package nars.experiment.market;

import jcog.Str;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Stock {

    public final String id;

    /**
     * total shares
     */
    private final int total;

    /**
     * share price
     */
    public double price; //, dayChange, yearHigh, yearLow;

    /**
     * available shares
     */
    public int available;

    /**
     * owner -> owned shares
     */
    public final Map<Account, Integer> own = new ConcurrentHashMap<>();

    public double volume;


    public Stock(String id) {
        this.id = id;
        this.price = Double.NaN;
        this.total = Integer.MAX_VALUE;
        this.available = total;
    }

    @Override
    public String toString() {
        return id + "@" + Str.n2(price);
    }

    public int buy(int quantity, Account a) {
        if (quantity==0) return 0;
        synchronized(own) {
            if (price!=price) return 0;
            quantity = Math.min(available, quantity);
            double totalPrice = price * quantity;

            if (a.cash < totalPrice)
                return 0; //System.out.println("insufficient cash");

            ownAdd(a, quantity);
            available -= quantity;
            a.cash -= totalPrice;
            return quantity;
        }
    }

    public int sell(int quantity, Account a) {
        if (quantity==0) return 0;
        synchronized(own) {
            if (price!=price) return 0;
            // validation on quantity
            quantity = Math.min(quantity, owns(a));
            if (quantity <= 0)
                return 0;

            double totalPrice = price * quantity;
                //System.out.println("The total comes to: \u00A3" + new DecimalFormat("#.00").format(totalPrice));
            ownAdd(a, -quantity);
            available += quantity;
            a.cash += totalPrice;
            return quantity;
        }
    }

    private void ownAdd(Account a, int quantity) {
        int ao = own.getOrDefault(a, 0);
        ao += quantity;
        if (ao == 0)
            own.remove(a);
        else
            own.put(a, ao);
    }

    public int owns(Account a) {
        return own.getOrDefault(a, 0);
    }

}