package nars.experiment.minicraft;

import jcog.signal.wave2d.MonoBufImgBitmap2D;
import nars.$;
import nars.Player;
import nars.Term;
import nars.experiment.minicraft.top.InputHandler;
import nars.experiment.minicraft.top.TopDownMinicraft;
import nars.game.Game;
import nars.sensor.PixelBag;
import nars.video.AutoClassifiedBitmap;

import static nars.$.$$;


/**
 * Created by me on 9/19/16.
 */
public class TopCraft extends Game {

    private final TopDownMinicraft craft = new TopDownMinicraft();
    private final AutoClassifiedBitmap camAE;
    //    private float prevHealth;
    float prevScore;
//    private BitmapSensor<PixelBag> pixels;

    public TopCraft() {
        super("cra");

        TopDownMinicraft.start(craft);
        //craft.changeLevel(1);

        PixelBag p = new PixelBag(new MonoBufImgBitmap2D(() -> craft.image), 64, 64)
                .actions(id, true, true, true, 0.25f);
        p.maxZoom = 0.5f;
        int nx = 16;
        //return new float[]{p.X, p.Y, p.Z};
        int states = 16;
        camAE = new AutoClassifiedBitmap($$("cae"), p.pixels, nx, nx, states, this);
        camAE.alpha(0.01f);
        //camAE.controlStart(this);
        onFrame(p::updateBitmap);


        //?
        senseSwitch($.the("dir"), new Term[] { $$("dirUp"), $$("dirDown"), $$("dirLeft"), $$("dirRight") /* TODO check these */},
                () -> craft.player.dir, $.func("dir", id));

        sense($.func("stamina", id), () -> (craft.player.stamina) / ((float) craft.player.maxStamina));
        //sense($.func("health", id), () -> (craft.player.health) / ((float) craft.player.maxHealth));

        int tileMax = 13;

        //TODO combine like NARio
        senseSwitch("tile:here", () -> craft.player.tile().id, 0, tileMax);
        senseSwitch("tile:up", () -> craft.player.tile(0, 1).id, 0, tileMax);
        senseSwitch("tile:down", () -> craft.player.tile(0, -1).id, 0, tileMax);
        senseSwitch("tile:right", () -> craft.player.tile(1, 0).id, 0, tileMax);
        senseSwitch("tile:left", () -> craft.player.tile(-1, 0).id, 0, tileMax);

        InputHandler input = craft.input;
        actionPushButton($.inh(id, "fire"), input.attack::pressed/*, 16*/);
        actionToggle($.inh(id, $.p("move", "l")), $.inh(id,$.p("move", "r")),
                input.left::pressed, input.right::pressed);
        actionToggle($.inh(id, $.p("move", "u")), $.inh(id, $.p("move", "d")),
                input.up::pressed, input.down::pressed);
        actionPushButton($.inh(id, "next"), (i) -> {
            if (craft.menu != null) {
                input.up.pressed(false);
                input.down.pressIfUnpressed();
            }
        });
        actionPushButton($.inh(id, "menu"), /*debounce(*/input.menu::pressIfUnpressed/*, 32f)*/);

        rewardNormalizedPolar($.inh(id, "score"), () -> {
            float nextScore = craft.frameImmediate();
//            return nextScore;
            float ds = nextScore - prevScore;
//            if (ds == 0)
//                return Float.NaN;
            this.prevScore = nextScore;
//            //System.out.println("score delta:" + ds);
            return ds;
        });
        reward("health", ()-> {
//            float nextHealth = craft.player.health;
//            float dh = nextHealth - prevHealth;
//            if (dh == 0)
//                return Float.NaN;
//            this.prevHealth = nextHealth;
//            //System.out.println("health delta: " + dh);
//            return dh;
            return (craft.player.health / ((float) craft.player.maxHealth));
        });

    }

    public static void main(String[] args) {

        TopCraft tc = new TopCraft();

        new Player(40, n -> n.add(tc)).ready(n->{
            //n.runLater(()-> SpaceGraph.window(tc.camAE.newChart(), 500, 500));
        }).start();


    }


}