package nars.experiment;

import jcog.Fuzzy;
import jcog.Util;
import jcog.math.v2;
import jcog.normalize.FloatNormalized;
import jcog.pid.MiniPID;
import nars.$;
import nars.Player;
import nars.Term;
import nars.game.Game;
import nars.game.reward.LambdaScalarReward;
import org.eclipse.collections.api.block.function.primitive.FloatToFloatFunction;
import spacegraph.SpaceGraph;
import spacegraph.space2d.phys.Dynamics2DView;
import spacegraph.space2d.phys.collision.shapes.PolygonShape;
import spacegraph.space2d.phys.dynamics.Body2D;
import spacegraph.space2d.phys.dynamics.BodyDef;
import spacegraph.space2d.phys.dynamics.Dynamics2D;
import spacegraph.space2d.phys.dynamics.FixtureDef;
import spacegraph.space2d.phys.dynamics.joints.RevoluteJoint;
import spacegraph.space2d.phys.dynamics.joints.RevoluteJointDef;

import java.util.function.Supplier;

import static java.lang.Math.PI;
import static jcog.Util.PHI;
import static org.hipparchus.util.MathUtils.normalizeAngle;
import static spacegraph.space2d.phys.dynamics.BodyType.DYNAMIC;
import static spacegraph.space2d.phys.dynamics.BodyType.STATIC;

public class Arm2D extends Game {

    final Dynamics2D world = new Dynamics2D();
    float worldDT = 0.05f;

    boolean gravity;
    static final float density =
        0.1f;
        //1.0f;

    final RevoluteJoint rotShoulder, rotElbow;
    private final RevoluteJoint rotFingerA, rotFingerB;
    private final Dynamics2DView view;
    private final Body2D fingerA;


    public Arm2D(String id) {
        super(id);

        if (!gravity)
            world.setGravity(new v2());

        float shoulderRad = 2;

        float upperLength = (float) (shoulderRad * Math.pow(PHI, 1));
        float upperThick = (float) (upperLength * Math.pow(PHI, -2));
        float lowerLength = (float) (upperLength * PHI);
        float lowerThick = (float) (upperThick/PHI);


        var upperArm = world.addBody(new BodyDef(DYNAMIC),
                new FixtureDef(
                    PolygonShape.box(upperLength, upperThick),
                        density, 0
                ));
        var lowerArm = world.addBody(new BodyDef(DYNAMIC),
                new FixtureDef(
                        PolygonShape.box(lowerLength, lowerThick),
                        density, 0
                ));

        var shoulder = new RevoluteJointDef(world.addBody(new BodyDef(STATIC),
                new FixtureDef(
                        PolygonShape.box(shoulderRad, shoulderRad),
                        density, 0
                )), upperArm, true)
                .localB(+upperLength*1.75f-shoulderRad, 0);
        shoulder.maxMotorTorque = 2048;
//        if (shoulder.enableLimit = false) {
//            shoulder.lowerAngle =
//                    (float) -PI;
//            //0;
//            shoulder.upperAngle = (float) PI;
//        }
        shoulder.enableLimit = true;
        shoulder.lowerAngle = (float) (-PI*1.5); //(float) -PI/2;
        shoulder.upperAngle = (float) (-PI/2); //(float) PI/2;

        rotShoulder = world.addJoint(shoulder);

        RevoluteJointDef elbow = new RevoluteJointDef(upperArm, lowerArm, true)
                .localA(-upperLength*0.9f, 0)
                .localB(+lowerLength*0.9f, 0);
        elbow.maxMotorTorque = 1024;
        final float elbowRange = Util.PHI_min_1f;
        elbow.lowerAngle = (float) (-elbowRange * PI);
        elbow.upperAngle = (float) (+elbowRange * PI);
        rotElbow = world.addJoint(elbow);

        {
            float f = lowerThick;
            //gripper
            float fingerLen = (float) (f * Math.pow(PHI, 3));
            float thumbLen = (float) (f * Math.pow(PHI, 2));
            float fingerThick = (float) (f *Math.pow(PHI, -1));


            PolygonShape fingerShape = new PolygonShape(0, fingerThick/2, fingerLen, 0, 0, -fingerThick/2f);
            fingerA = world.addBody(new BodyDef(DYNAMIC), new FixtureDef(
                    fingerShape, density, 0)
            );
            PolygonShape thumbShape = new PolygonShape(0, fingerThick/2, thumbLen, 0, 0, -fingerThick/2f);
            var fingerB = world.addBody(new BodyDef(DYNAMIC), new FixtureDef(
                    thumbShape, density, 0)
            );
            RevoluteJointDef rFingerA = new RevoluteJointDef(lowerArm, fingerA, true)
                    .localA(-lowerLength, -lowerThick)
                    .localB(0,0)
                    ;
            RevoluteJointDef rFingerB = new RevoluteJointDef(lowerArm, fingerB,true)
                    .localA(-lowerLength, +lowerThick)
                    .localB(0,0)
                    ;

            rFingerA.maxMotorTorque = rFingerB.maxMotorTorque = 128;
            rFingerA.lowerAngle = (float) (PI - .75f); rFingerA.upperAngle = (float) (PI + PI/2);
            rFingerB.lowerAngle = (float) (PI - PI/2); rFingerB.upperAngle = (float) (PI + .75f);
            //rFingerA.collideConnected = rFingerB.collideConnected = true;

            rotFingerA = world.addJoint(rFingerA);
            rotFingerB = world.addJoint(rFingerB);

//            world.addJoint(new GearJointDef(rotFingerA, rotFingerB, 1));
        }

        SpaceGraph.window(view = new Dynamics2DView(world), 1024, 800);

        onFrame(()-> world.step(worldDT, 8, 4));
    }

    class RevoluteDirectDrive {
        RevoluteDirectDrive(Term id, RevoluteJoint joint, float speed) {
            action(id, z->{
                joint.setMotorSpeed(Fuzzy.polarize(z)*speed);
            });
        }
    }

    /** uses PID to drive motor towards target angle */
    class DirectDrive implements FloatToFloatFunction {

        private final RevoluteJoint joint;
        private final float angMin, angMax;

//        static final float angTolerance = 0.01f;
        static final float speed = 15;

        DirectDrive(RevoluteJoint joint) {
            this.joint = joint;
            if (joint.isLimitEnabled()) {
                angMin = joint.getLowerLimit();
                angMax = joint.getUpperLimit();
            } else {
                angMin = (float) -PI;
                angMax = (float) +PI;
                //joint.enableLimit(true);
            }

            sense($.inh(id, $.quote("joint_" + System.identityHashCode(joint))),  new FloatNormalized(
                ()-> Util.normalize(joint.getJointAngle(), angMin, angMax)));

//            joint.setMotorSpeed(0.0001f);
//            joint.setMaxMotorTorque(1);
        }

        @Override
        public float valueOf(float angNorm) {
            float target = Util.lerp(angNorm, angMin, angMax);

            //joint.setLimits(target - angTolerance /2, target+ angTolerance /2);

            double actual = normalizeAngle(joint.getJointAngle(), (angMin + angMax) / 2);

            float da = Util.clampSafe((float) (target - actual), -DirectDrive.speed, +DirectDrive.speed);
            joint.setMotorSpeed(da);

            //return (float) Util.normalize(actual + da, angMin, angMax);
            return angNorm;
        }

    }

    /** uses PID to drive motor towards target angle */
    static class PIDServoDrive implements FloatToFloatFunction {

        private final RevoluteJoint joint;
        private final float speed;
        final MiniPID p = new MiniPID(0.25,1,0.5);

        PIDServoDrive(RevoluteJoint joint) {
            this.joint = joint;
            this.speed = 2f;
        }

        @Override
        public float valueOf(float angNorm) {
            double angMin, angMax;
            if (joint.isLimitEnabled()) {
                angMin = joint.getLowerLimit();
                angMax = joint.getUpperLimit();
            } else {
                angMin = -PI;
                angMax = +PI;
            }

            double target = Util.lerp(angNorm, angMin, angMax);

            final double angMid = (angMin + angMax) / 2;
            double actual = normalizeAngle(joint.getJointAngle(), angMid);


            double z;
            ////p.setSetpointRange(angMin-PI, angMax+PI);
            ////p.setOutRampRate(accelerationMax);
            p.outRange(-speed, +speed);
            z = p.out(actual, target);

//            z = target - actualAngle;
            //z = Util.clamp(z, -speed, +speed);

            joint.setMotorSpeed((float) z);

            return (float) Util.normalize(target, angMin, angMax);
        }

    }

    private LambdaScalarReward rewardNear(Term id, Supplier<v2> a, Supplier<v2> b) {
        //return rewardNormalized(id, 0, false, ()->{
        return rewardNormalized(id, 1, false, ()->{
            float dist = a.get().distance(b.get());
            //dist = dist*dist; //sqr
//            System.out.println(dist);
            //return dist;
            //return 1/(1 + dist);
            return 1/(1 + dist/100f);
        });
    }

    @Override
    protected void init() {
        //new RevoluteDirectDrive($.inh(id, "s"), rotShoulder, 1);

        float res = 0.05f;

        action($.inh(id, "s"), new DirectDrive(rotShoulder)).resolution(res);
        action($.inh(id, "e"), new DirectDrive(rotElbow)).resolution(res);
        action($.inh(id, $.p("f",1)), new DirectDrive(rotFingerA)).resolution(res);
        action($.inh(id, $.p("f",2)), new DirectDrive(rotFingerB)).resolution(res);

        float ballRad = 2;
        double ballSpeed = 0.7f;
        var ball = world.addBody(new BodyDef(DYNAMIC), new FixtureDef(PolygonShape.regular(6, ballRad), 1, 0));

        sense($.inh(id, $.p("hand", "x")),  new FloatNormalized(()-> {
            //if (ball != null) {
                //System.out.println(ball.pos.y);
                return rotFingerA.A().pos.x;
//            }
//            return Float.NaN;
        }));
        sense($.inh(id, $.p("hand", "y")),  new FloatNormalized(()-> {
            //if (ball != null) {
            //System.out.println(ball.pos.y);
            return rotFingerA.A().pos.y;
//            }
//            return Float.NaN;
        }));

        sense($.inh(id, $.p("ball", "y")),  new FloatNormalized(()-> {
            if (ball != null) {
                //System.out.println(ball.pos.y);
                return ball.pos.y+100;
            }
            return Float.NaN;
        }));

        rewardNear($.inh(id, "t"), ()->fingerA.pos, new Supplier<>() {
            final int motionRad = 8;

            @Override
            public v2 get() {
                double t = nar.time() / 1000.0 * ballSpeed;
                float bx = //ball.pos.x =
                        //(float) Math.cos(t)*(ballRad*motionRad);
                        ballRad * motionRad;
                float by = //ball.pos.y =
                        (float) Math.sin(t) * (ballRad * motionRad);
                ball.setTransform(new v2(bx, by), 0);

                return ball.pos;
            }
        }).resolution(res);

    }

    public static void main(String[] args) {

        final Player p = new Player(new Arm2D("a")).fps(30f);
        //p.rlBoost = true;
        p.start();
    }
}