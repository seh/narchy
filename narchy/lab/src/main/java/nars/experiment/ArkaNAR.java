package nars.experiment;


import com.google.common.collect.Streams;
import jcog.Is;
import jcog.Util;
import jcog.signal.FloatRange;
import jcog.signal.wave2d.ScaledBitmap2D;
import nars.$;
import nars.Player;
import nars.Term;
import nars.game.Game;
import nars.game.action.BiPolarAction;
import nars.game.sensor.Sensor;
import nars.gui.NARui;
import nars.gui.sensor.VectorSensorChart;
import nars.sensor.BitmapSensor;
import nars.term.Variable;
import nars.term.atom.Atomic;
import nars.video.SwingBitmap2D;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.grid.Gridding;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.toList;
import static nars.$.*;
import static spacegraph.SpaceGraph.window;

/**
 * NARkanoid
 */
@Is("Noon_Universe") public class ArkaNAR extends Game {

    final boolean numeric;

    static int GAMES = 4;

    static float fps = 50;

    /** a cheat to accelerate learning */
    static final boolean train = false;

    public final FloatRange paddleSpeed = new FloatRange(15, 1, 20);
    public final FloatRange ballSpeed = new FloatRange(1, 0.04f, 6);
    //final int visW = 48, visH = 32;
    //final int visW = 24, visH = 16;
    final int visW, visH;
    final Arkanoid noid = new Arkanoid();
    //final int visW = 8, visH = 6;
    //static final int visW = 12, visH = 8;
    BitmapSensor<ScaledBitmap2D> cc;



    public ArkaNAR(Term id) {
        this(id, false,
                //32, 16
                18,16
        );
    }

    public ArkaNAR(boolean numeric, int visW, int visH) {
        this(Atomic.atom("noid"), numeric, visW, visH);
    }

    public ArkaNAR(Term id, boolean numeric, int visW, int visH) {
        super(id/*, GameTime.durs(2)*/);
        this.numeric = numeric;
        this.visW = visW;
        this.visH = visH;

        //initUnipolar();
//        initBipolarRelative();
        initPushButton();
        //initPWM();


        if (visW > 0 && visH > 0) {

            cc = senseCamera(
                    //(x, y) -> inh(p(x, y), id),
                    (x, y) -> inh(id, p(x, y)),
                    new ScaledBitmap2D(
                            new SwingBitmap2D(noid)
                            , visW, visH
                    )/*.blur()*/);
            cc.resolution(0.25f);
        }


        if (numeric) {
            int digits = 2;

            float resX = 0.1f;
            float resY = 0.1f;

            Variable v = varDep(1);
            List<Sensor> l = List.of(
                    senseNumberN(inh(p(id, p("p", "x")), v), (() -> noid.paddle.x / noid.getWidth()), digits).resolution(resX),
                    senseNumberN(inh(p(id, p("b", "dx")), v), (() -> 0.5f + 0.5f * (noid.ball.x - noid.paddle.x) / noid.getWidth()), digits).resolution(resX),
                    senseNumberN(inh(p(id, p("b", "x")), v), (() -> (noid.ball.x / noid.getWidth())), digits).resolution(resX),
                    senseNumberN(inh(p(id, p("b", "y")), v), (() -> 1f - (noid.ball.y / noid.getHeight())), digits).resolution(resY)
            );

            nar.runLater(()->
                    window(NARui.beliefCharts(
                            l.stream().flatMap(x -> Streams.stream(x.components()))
                                    .collect(toList()), nar), 500, 500)
            );

        }

        /*action(new ActionConcept( $.func("dx", "paddleNext", "noid"), nar, (b, d) -> {
            if (d!=null) {
                paddleSpeed = Util.round(d.freq(), 0.2f);
            }
            return $.t(paddleSpeed, nar.confidenceDefault('.'));
        }));*/

        onFrame(noid::next);

        reward("alive",
                //() -> 1 - Math.min(1, noid.die - noid.prevDie)
                () -> alive() ? 1 : 0
//            EternalDefaultTable.ifNot(1, () -> 1 - Math.min(1, noid.die - noid.prevDie))
        );
//        nar.runLater(()->{
//            alive.addGuard(true,false);
//        });

        var score = reward("score",
                () -> noid.score > noid.prevScore ? 1 : 0);


        if (train) {
            float sharp = 2;
//            reward("align",
//            () -> (float)Math.pow(1 - Math.abs(noid.ball.x - noid.paddle.x) / noid.getWidth(), sharp)
//            ).resolution(0.1f);
            reward("(enough,L)",
                    () -> alive() ? (float)Math.pow(1 - Math.max(noid.ball.x - noid.paddle.x, 0) / noid.getWidth(), sharp) : 0
            ).resolution(0.1f);
            reward("(enough,R)",
                    () -> alive() ? (float)Math.pow(1 - Math.max(noid.paddle.x - noid.ball.x, 0) / noid.getWidth(), sharp) : 0
            ).resolution(0.1f);
        }

//        nar.onTask(t->{
//            if (t.isGoal()) {
//                if (!t.isInput()) {
//                    if (t.isEternal())
//                        System.err.println(t);
//                    else
//                        System.out.println(t);
//                }
//            }
////           if (t.isQuest()) {
////               nar.concepts.stream().filter(x -> x.op() == IMPL && x.sub(1).equals(t.target())).forEach(i -> {
////                   //System.out.println(i);
////                   //nar.que(i.sub(0), QUEST, t.start(), t.end());
////                   nar.want(i.sub(0), Tense.Present,1f, 0.9f);
////               });
////           }
//        },GOAL);

    }

    public static void main(String[] args) {


        Player p = new Player().fps(fps);
        for (int i =0; i < GAMES; i++) {
            ArkaNAR a = new ArkaNAR(
                    GAMES > 1 ? p("n", "a" + i) : $$("n")
            );
//            a.focus().log();
            p.add(a);
        }

//        p.meta = false;
        p.start();

        p.nar.runLater(()->{
            window(new Gridding(p.games().map(z -> ((ArkaNAR)z).view())), 800, 600);
        });

//        p.nar.freqResolution.set(0.1f);  //lo-res
//            n.confResolution.set(0.05f);

//        p.nar.runLater(()->{
//            Util.sleepMS(2000);
//            p.nar.focus.stream().flatMap(f -> f.get().metas(BagClustering.class)).map(z -> z.getValue()).forEach(c -> {
//                window(NARui.clusterView(c, p.nar), 400, 400);
//            });
//        });


    }

    public final ArkaNAR ballSpeed(float v) {
        ballSpeed.set(v);
        return this;
    }


    private boolean alive() {
        return noid.die <= 0;
    }

    public Surface view() {
        return new VectorSensorChart(cc, this).withControls();
    }

    private void initBipolarRelative() {
        actionBipolar(id,
                dx -> noid.paddle.move(dx * paddleSpeed()) ? dx : 0);
    }

    private void initPWM() {
        BiPolarAction lr = actionBipolar(inh(id, $.varDep(1)),
                new BiPolarAction.Analog(),
                (x) -> noid.paddle.move(x * paddleSpeed()) ? x : 0
        );
    }
    private void initPushButton() {
        BiPolarAction lr = actionToggle(
                inh(id, "L"),
                inh(id, "R"),
            () -> alive() && noid.paddle.move(-paddleSpeed()),
            () -> alive() && noid.paddle.move(+paddleSpeed())
        );
//        lr.pos.goalDefault($.t(0f, 0.0001f), nar);
//        lr.neg.goalDefault($.t(0f, 0.0001f), nar);
    }

    private void initUnipolar() {
        float res = 0.05f;
        final float thresh = 0.25f;
        action(inh(id, "L"),
                //u -> u > 0.5f && noid.paddle.move(-paddleSpeed * 2 * Util.sqr(2 * (u - 0.5f))) ? u : 0);
                u -> u > thresh && noid.paddle.move(-paddleSpeed() * Math.max(0,(u - thresh)) / (1-thresh)) ? u : 0).resolution(res);
        action(inh(id, "R"),
                //u -> u > 0.5f && noid.paddle.move(+paddleSpeed * 2 * Util.sqr(2 * (u - 0.5f))) ? u : 0);
                u -> u > thresh && noid.paddle.move(+paddleSpeed() * Math.max(0,(u - thresh)) / (1-thresh)) ? u : 0).resolution(res);
    }


    public float paddleSpeed() {
        return paddleSpeed.asFloat();
    }
    
    /**
     * https:
     */
    class Arkanoid extends Canvas implements KeyListener {

        public static final int SCREEN_HEIGHT = 250;
        static final int SCREEN_WIDTH = 250;
        static final int BLOCK_LEFT_MARGIN = 4;
        static final int BLOCK_TOP_MARGIN = 15;
        static final float BALL_RADIUS = 15.0f;
        static final float PADDLE_WIDTH = 45.0f;
        static final float PADDLE_HEIGHT = 20.0f;
        static final float BLOCK_WIDTH = 40.0f;
        static final float BLOCK_HEIGHT = 15.0f;
        static final int COUNT_BLOCKS_X = 5;
        static final int COUNT_BLOCKS_Y = 2; /* 3 */
        static final float FT_STEP = 4.0f;
        public final Paddle paddle = new Paddle(SCREEN_WIDTH / 2f, SCREEN_HEIGHT - PADDLE_HEIGHT);
        public final Ball ball = new Ball(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
        public final Collection<Brick> bricks = Collections.newSetFromMap(new ConcurrentHashMap());
        final AtomicInteger brickSerial = new AtomicInteger(0);
        int score = 0;


        /* GAME VARIABLES */
        int die = 0;
        float BALL_VELOCITY = 0.5f;
        private int prevScore = 0;
        private static final int HELL_DURATION = 10;

        Arkanoid() {

            setSize(SCREEN_WIDTH, SCREEN_HEIGHT);

//            this.setUndecorated(false);
//            this.setResizable(false);
//
//
//            if (visible)
//                this.setVisible(true);

            paddle.x = SCREEN_WIDTH / 2f;


//            setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
//
//            new Timer(1000 / FPS, (e) -> {
//                repaint();
//            }).start();

            reset();
        }

        @Override
        public void paint(Graphics g) {
//            super.paint(g);

            g.setColor(Color.black);
            g.fillRect(0, 0, getWidth(), getHeight());

            if (die > 0) return;

            ball.draw(g);
            paddle.draw(g);
            for (Brick brick : bricks)
                brick.draw(g);
        }

        void increaseScore() {
            score++;
            if (score == (COUNT_BLOCKS_X * COUNT_BLOCKS_Y))
                win();
        }

        protected void win() {
            reset();
        }

        protected void die() {
            die = HELL_DURATION;
        }

        boolean intersects(GameObject mA, GameObject mB) {
            return mA.right() >= mB.left() && mA.left() <= mB.right()
                    && mA.bottom() >= mB.top() && mA.top() <= mB.bottom();
        }

        void testCollision(Paddle mPaddle, Ball mBall) {
            if (!intersects(mPaddle, mBall))
                return;
            mBall.velocityY = -BALL_VELOCITY;
            mBall.velocityX = mBall.x < mPaddle.x ? -BALL_VELOCITY : BALL_VELOCITY;
        }

        void testCollision(Brick mBrick, Ball mBall) {
            if (!intersects(mBrick, mBall))
                return;

            mBrick.destroyed = true;

            increaseScore();

            float overlapLeft = mBall.right() - mBrick.left();
            float overlapRight = mBrick.right() - mBall.left();
            float overlapTop = mBall.bottom() - mBrick.top();
            float overlapBottom = mBrick.bottom() - mBall.top();

            boolean ballFromLeft = overlapLeft < overlapRight;
            boolean ballFromTop = overlapTop < overlapBottom;

            float minOverlapX = ballFromLeft ? overlapLeft : overlapRight;
            float minOverlapY = ballFromTop ? overlapTop : overlapBottom;

            if (minOverlapX < minOverlapY) {
                mBall.velocityX = ballFromLeft ? -BALL_VELOCITY : BALL_VELOCITY;
            } else {
                mBall.velocityY = ballFromTop ? -BALL_VELOCITY : BALL_VELOCITY;
            }
        }

        void initializeBricks(Collection<Brick> bricks) {


            bricks.clear();

            for (int iX = 0; iX < COUNT_BLOCKS_X; ++iX) {
                for (int iY = 0; iY < COUNT_BLOCKS_Y; ++iY) {
                    bricks.add(new Brick((iX + 1) * (BLOCK_WIDTH + 3) + BLOCK_LEFT_MARGIN,
                            (iY + 2) * (BLOCK_HEIGHT + 3) + BLOCK_TOP_MARGIN));
                }
            }

        }

        public void reset() {
            initializeBricks(bricks);
            ball.x = SCREEN_WIDTH / 2f;
            ball.y = SCREEN_HEIGHT / 2f;
            ball.setVelocityRandom();
        }

        public float next() {
            if (die > 0) {
                die--;
                if (die == 0)
                    reset();
                else
                    return Float.NaN;
            }

            prevScore = score;

            BALL_VELOCITY = ballSpeed.floatValue();


            ball.update(paddle);
            testCollision(paddle, ball);


            Iterator<Brick> it = bricks.iterator();
            while (it.hasNext()) {
                Brick brick = it.next();
                testCollision(brick, ball);
                if (brick.destroyed)
                    it.remove();
            }


            return score;
        }

        @Override
        public void keyPressed(KeyEvent event) {


            switch (event.getKeyCode()) {
                case KeyEvent.VK_LEFT:
                    paddle.move(-paddleSpeed());
                    break;
                case KeyEvent.VK_RIGHT:
                    paddle.move(+paddleSpeed());
                    break;
                default:
                    break;
            }
        }


        @Override
        public void keyReleased(KeyEvent event) {
//            switch (event.getKeyCode()) {
//                case KeyEvent.VK_LEFT:
//                case KeyEvent.VK_RIGHT:
//                    break;
//                default:
//                    break;
//            }
        }

        @Override
        public void keyTyped(KeyEvent arg0) {

        }

        abstract class GameObject {
            abstract float left();

            abstract float right();

            abstract float top();

            abstract float bottom();
        }

        class Rectangle extends GameObject {

            public float x;
            public float y;
            public float sizeX;
            float sizeY;

            @Override
            float left() {
                return x - sizeX / 2.0f;
            }

            @Override
            float right() {
                return x + sizeX / 2.0f;
            }

            @Override
            float top() {
                return y - sizeY / 2.0f;
            }

            @Override
            float bottom() {
                return y + sizeY / 2.0f;
            }

        }

        class Paddle extends Rectangle {


            Paddle(float x, float y) {
                this.x = x;
                this.y = y;
                this.sizeX = PADDLE_WIDTH;
                this.sizeY = PADDLE_HEIGHT;
            }

            /**
             * returns percent of movement accomplished
             */
            public synchronized boolean move(float dx) {
                float px = x;
                x = Util.clamp(x + dx, sizeX, SCREEN_WIDTH - sizeX);
                return !Util.equals(px, x, 1f);
            }


            void draw(Graphics g) {
                g.setColor(Color.WHITE);
                g.fillRect((int) (left()), (int) (top()), (int) sizeX, (int) sizeY);
            }

            public void set(float freq) {
                x = freq * SCREEN_WIDTH;
            }

            public float moveTo(float target, float paddleSpeed) {
                target *= SCREEN_WIDTH;

                if (Math.abs(target - x) <= paddleSpeed) {
                    x = target;
                } else if (target < x) {
                    x -= paddleSpeed;
                } else {
                    x += paddleSpeed;
                }

                x = Math.min(x, SCREEN_WIDTH - 1);
                x = Math.max(x, 0);

                return x / SCREEN_WIDTH;
            }
        }

        class Brick extends Rectangle implements Comparable<Brick> {

            int id;
            boolean destroyed;

            Brick(float x, float y) {
                this.x = x;
                this.y = y;
                this.sizeX = BLOCK_WIDTH;
                this.sizeY = BLOCK_HEIGHT;
                this.id = brickSerial.incrementAndGet();
            }

            void draw(Graphics g) {
                g.setColor(Color.WHITE);
                g.fillRect((int) left(), (int) top(), (int) sizeX, (int) sizeY);
            }

            @Override
            public int compareTo(Brick o) {
                return Integer.compare(id, o.id);
            }
        }

        class Ball extends GameObject {

            public float x;
            public float y;
            public float velocityX;
            public float velocityY;
            float radius = BALL_RADIUS;

            Ball(int x, int y) {
                this.x = x;
                this.y = y;
                setVelocityRandom();
            }

            void setVelocityRandom() {
                this.setVelocity(BALL_VELOCITY, (float) (Math.random() * -Math.PI * (2 / 3f) + -Math.PI - Math.PI / 6));
            }

            public void setVelocity(float speed, float angle) {
                this.velocityX = (float) Math.cos(angle) * speed;
                this.velocityY = (float) Math.sin(angle) * speed;
            }

            void draw(Graphics g) {
                g.setColor(Color.WHITE);
                g.fillOval((int) left(), (int) top(), (int) radius * 2,
                        (int) radius * 2);
            }

            void update(Paddle paddle) {
                x += velocityX * FT_STEP;
                y += velocityY * FT_STEP;

                if (left() < 0)
                    velocityX = BALL_VELOCITY;
                else if (right() > SCREEN_WIDTH)
                    velocityX = -BALL_VELOCITY;
                if (top() < 0) {
                    velocityY = BALL_VELOCITY;
                } else if (bottom() > SCREEN_HEIGHT) {
                    velocityY = -BALL_VELOCITY;
                    x = paddle.x;
                    y = paddle.y - 50;
                    die();
                }

            }

            @Override
            float left() {
                return x - radius;
            }

            @Override
            float right() {
                return x + radius;
            }

            @Override
            float top() {
                return y - radius;
            }

            @Override
            float bottom() {
                return y + radius;
            }

        }


    }
}