package nars;

import org.codehaus.janino.Parser;
import org.codehaus.janino.Scanner;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.function.Predicate;

public class JaninoPredicateInline {

    static final Predicate a = x -> {
        System.out.println("a: " + x);
        return true;
    };
    static final Predicate b = (x) -> { System.out.println("b: " + x); return true; };

    public static void main(String[] args) throws Exception {
        //CompilerFactoryFactory.getDefaultCompilerFactory().newCompiler().


        System.out.println(parse(JaninoPredicateInline.class));

    }

    static Parser parse(Class<?> clazz) throws IOException {
        String path = clazz.getName();
        path = path.replaceFirst("\\$.*", "");
        path = path.replace(".", "/"/*DrillFileUtils.SEPARATOR*/);
        path = "/" + path + ".java";

        //logger.trace("Loading function code from the {}", path);
        try (InputStream is = clazz.getResourceAsStream(path)) {
            if (is == null) {
                throw new IOException(String.format(
                        "Failure trying to locate source code for class %s, tried to read on classpath location %s", clazz.getName(),
                        path));
            }
            String body = new String(is.readAllBytes());

            // TODO: Hack to remove annotations so Janino doesn't choke. Need to reconsider this problem...
            body = body.replaceAll("@\\w+(?:\\([^\\\\]*?\\))?", "");
//            try {
                return new Parser(new Scanner(null, new StringReader(body)));
//            } catch (CompileException e) {
//                throw new IOException(String.format("Failure while loading class %s.", clazz.getName()), e);
//            }

        }

    }
}
