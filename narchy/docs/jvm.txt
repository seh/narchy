java -XX:+AggressiveOpts -XX:+UnlockDiagnosticVMOptions -XX:+UnlockExperimentalVMOptions -XX:+PrintFlagsFinal -version


-XX:+UseLargePages

-XX:+UseNUMA -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCI -XX:+UseJVMCICompiler

~/graalvm-ce-java16-21.1.0-dev/bin/native-image --verbose -dsa -da -jar ~/n/narchy/lab/target/n.jar