#Print VM Flags
```
java -XX:+UnlockExperimentalVMOptions -XX:+EagerJVMCI -Dgraal.ShowConfiguration=info -XX:+EnableJVMCI -XX:+JVMCIPrintProperties
```
```
-Dgraal.AlwaysInlineIntrinsics=true
-Dgraal.EscapeAnalysisIterations=10
-Dgraal.InliningDepthError=2000
-Dgraal.MaximumDesiredSize=80000
-Dgraal.MaximumInliningSize=1000
-Dgraal.MaximumRecursiveInlining=10
-Dgraal.SimpleMethodCalls=2
-Dgraal.SimpleMethodGraphSize=1024
-Dgraal.SmallCompiledLowLevelGraphSize=1024
```