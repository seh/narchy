package nars.gui;

import jcog.event.Off;
import jcog.pri.PriReference;
import jcog.pri.bag.util.Bagregate;
import jcog.pri.op.PriMerge;
import nars.Task;
import nars.focus.BagFocus;
import nars.focus.Focus;
import spacegraph.space2d.container.ContainerSurface;
import spacegraph.space2d.container.ScrollXY;
import spacegraph.space2d.container.grid.GridRenderer;
import spacegraph.space2d.container.grid.ListModel;
import spacegraph.space2d.container.unit.MutableUnitContainer;

import java.util.function.Consumer;

public abstract class AbstractItemList<X> extends MutableUnitContainer implements GridRenderer<PriReference<X>>, Consumer<Task> {
	final Bagregate<X> bag;
	private final Focus focus;
	private final ScrollXY<PriReference<X>> scroll;
	private final ListModel.AsyncListModel model;
//	private final Lst<PriReference<X>> list = new Lst<>(0);

	float rate;
	private Off onTask;
//	private boolean refresh = true;

	protected AbstractItemList(Focus w, int capacity) {
		this.focus = w;

		rate =
			0.01f;
			//1;
			// 1f/capacity;

		int internalCapacity = ((BagFocus)focus).bag.capacity()*4; //TODO adaptive

		Bagregate<X> _bag = new Bagregate<>(internalCapacity,
				PriMerge.plus);
		this.bag = _bag;
		//bag = new SimpleBufferedBag<>(_bag);

		scroll = new ScrollXY(model = new ListModel.AsyncListModel(bag, capacity) {
//			@Override
//			public void update(ScrollXY s) {
//				if (refresh && visible()) {
//					bag.commit();
//					super.update(s);
//					refresh = false;
//				}
//			}
		} /*ListModel.of(list)*/, this);

		set(DurSurface.get(scroll, this::commit, w.nar) );
	}

	private void commit() {
//		refresh = true;
		//scroll.layout();
		if (/*refresh &&*/ visible()) {
			bag.commit();
			//scroll.layout();
			model.update(scroll);
			((ContainerSurface)scroll.center()).layout(); //HACK force re-render
			//refresh = false;
		}
	}

	@Override
	protected void starting() {
		super.starting();
		onTask = focus.onTask(this);
	}

	@Override
	public final void accept(Task x) {
		//TODO option for only if visible
		if (filter(x))
            bag.put(transform(x), value(x) * rate);
	}

	public float value(Task x) {
		//TODO temporal relevance
		return x.priElseZero();
	}

	protected abstract X transform(Task x);

	protected static boolean filter(Task x) {
		return true;
	}

	@Override
	protected void stopping() {
		onTask.close();
		onTask = null;
		super.stopping();
	}

}