package nars.gui;

import jcog.event.Off;
import jcog.exe.Loop;
import jcog.pri.UnitPri;
import jcog.pri.bag.Bag;
import jcog.thing.Part;
import nars.NAR;
import nars.Narsese;
import nars.Task;
import nars.Term;
import nars.focus.BagFocus;
import nars.focus.Focus;
import nars.focus.PriNode;
import nars.game.Game;
import nars.gui.graph.run.TaskLinkGraph2D;
import nars.premise.Premise;
import org.eclipse.collections.api.tuple.primitive.ObjectBooleanPair;
import spacegraph.SpaceGraph;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.Bordering;
import spacegraph.space2d.container.grid.Gridding;
import spacegraph.space2d.meta.LoopPanel;
import spacegraph.space2d.meta.ObjectSurface;
import spacegraph.space2d.widget.button.PushButton;
import spacegraph.space2d.widget.button.Submitter;
import spacegraph.space2d.widget.chip.ReplChip;
import spacegraph.space2d.widget.menu.TabMenu;
import spacegraph.space2d.widget.slider.FloatSlider;
import spacegraph.space2d.widget.text.BitmapLabel;
import spacegraph.space2d.widget.textedit.TextEdit;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;
import static jcog.Str.n2;
import static jcog.Str.n4;
import static nars.$.$$;
import static nars.gui.GameUI.gameUI;

public enum FocusUI {
    ;

    public static Surface focusUI(Focus f) {
        var m = new Bordering();
        var n = f.nar;

        Map<String, Supplier<Surface>> xx = Map.of(
                "Spect", () -> NARui.tasklinkSpectrogram(f, 300),
                "Stat", () -> FocusUI.focusStats(f),
                "Histo", () -> new BagView(((BagFocus) f).bag, n),
                "Graph", () -> TaskLinkGraph2D.tasklinks((BagFocus) f, n),
                "Tasks", () -> new TaskListView(f, 32),
                "List", () -> new ConceptListView(f, 32),
                "Input", () -> narseseInput(f),
                "Meta", () -> new ObjectSurface(f, 3)
        );
        TreeMap<String,Supplier<Surface>> x = new TreeMap<>(xx);

        Game g = f.focusLocal(Game.class);
        if (g != null) x.put("Game", ()->gameUI(g));

        PriNode p = f.pri;
        //NARPart p = f.meta(NARPart.class);
        if (p != null) x.put("Att", ()->AttentionUI.objectGraphs(p, n));



        m.center(new TabMenu(x));
        m.west(new MemUI(n));
        m.west(new Gridding(
                new PushButton("Clear", f::clear), //TODO n::clear "Clear All"

                Submitter.text("Load", u -> {
                    System.err.println("Load: TODO");
                    //throw new TODO();
                }),
                Submitter.text("Save", u -> {
                    System.err.println("Save: TODO");
                    //new TaskSummary().addConceptTasks(w.concepts()).reindex();
                    //throw new TODO(); //tagging
                }),
                new PushButton("List", ((BagFocus) f).bag::print) //TODO better
//			new PushButton("Impile", () -> SpaceGraph.window(impilePanel(w), 500, 500)) //TODO better

        ));
//        m.east(new Gridding(
//                //TODO interactive filter widgets
//        ));

        return m;
    }

    private static Surface focusStats(Focus f) {
        TextEdit t = new TextEdit(30, 6);
        StringBuilder tt = new StringBuilder();
        Bag<Premise, Premise> b = ((BagFocus) f).bag;
        return new LoopPanel(Loop.of(()->{
            focusStats(tt, b);
            t.text(tt.toString());  tt.setLength(0);
        }).fps(1)).set(t);
    }

    private static void focusStats(StringBuilder s, Bag<Premise, Premise> b) {
        //TODO other stats

        DoubleSummaryStatistics vol = new DoubleSummaryStatistics();
        b.forEach(x -> {
            vol.accept(x.volume());
        });

        s.append("pri: ").append(n4(b.priMin())).append("..").append(n4(b.priMax())).append('\n');
        s.append("vol: ").append(vol.getMin()).append("..").append(n2(vol.getAverage())).append("..").append(vol.getMax()).append('\n');
    }

    public static ReplChip narseseInput(Focus f) {
        NAR n = f.nar;
        return new ReplChip((cmd, receive) -> {
            cmd = cmd.trim();

            //try to parse task
            try {

                Task h = Narsese.task(cmd, n);
                if (h.COMMAND() && !cmd.endsWith(";"))
                    throw new RuntimeException("FocusUI HACK"); //HACK

                f.accept(h);


            } catch (Throwable e) {

                //try to parse term
                try {
                    Term t = $$(cmd);
                    NARui.conceptWindow(t, n);
                } catch (Throwable t) {
                    t.printStackTrace();
                    receive.accept(t.toString());
                }

                receive.accept(e.toString());
            }
        });
    }

//    private static Surface impilePanel(Focus w) {
//
//
//        Was h = new Was();
//        h.add(w);
//
//
//        float dur = w.dur();
//
//        Graph2D<Object> gg = new Graph2D<>();
//        Surface g = gg
//                .update(new Force2D())
//                .render(new NodeGraphRenderer<Term, Task>() {
//
//
//                    //@Override
//                    protected void updateEdge(Term x, NALTask task, Term y, EdgeVis<Term> ee) {
//                        //super.updateEdge(x, task, y, ee);
//
//                        boolean notQuestion = task.BELIEF_OR_GOAL();
//                        float freq = notQuestion ? task.freq() : 1f /* assume CONJ seq (question) */;
//                        float polarization = Math.abs(freq - 0.5f) * 2;
//                        int dt = task.term().dt(); //assuming IMPL
//                        if (dt == DTERNAL) dt = 0; //HACK
//
//                        float immediacy = 1f / (1 + Util.sqrt(Math.abs(dt) / dur));
//
//                        float R = polarization;
//                        float G = polarization;
//                        float B = Fuzzy.and(polarization, immediacy);
//                        float a = 0.25f;
//
//                        ee.color(R, G, B, a);
//
//                        ee.weight(0.01f + (notQuestion ? (float) task.conf() : 0));
//                    }
//
//
//                    //@Override
//                    protected void style(NodeVis<Term> node) {
//                        Term t = node.id;
//                        if (t instanceof Neg)
//                            node.color(0.75f, 0.25f, 0.25f, 0.75f);
//                        else
//                            node.color(0.25f, 0.75f, 0.25f, 0.75f);
//
//                        float s = 1f / (1 + Util.sqrt(t.volume()));
//                        node.pri = s * 2;
//                    }
//                })
//                .set(h).widget();
//
//        return new Bordering(g).south(
//                new Gridding(
//                        new PushButton("Reload", () -> {
//                            h.add(w);
//                            gg.set(h);
//                        }),
//                        new PushButton("Research", () -> {
//                            h.grow(w.nar);
//                            gg.set(h);
//                        }),
//                        new PushButton("Clear", () -> {
//                            h.clear();
//                            gg.set(h);
//                        }),
//                        new PushButton("Load", () -> {
//                            //
//                        }),
//                        new PushButton("Save", () -> {
//                            //
//                        })
//                )
//        );
//
//    }

    public static Surface focusMixer(NAR n) {
        WhatMixer w = new WhatMixer(n);
        return DurSurface.get(w, w::commit, n);
    }

    public static Surface focusUI(NAR n) {

        return new TabMenu() {
            Off updater;

            final Consumer<ObjectBooleanPair<Part<NAR>>> change = change -> {
                if (change.getOne() instanceof Focus) {
                    if (parent == null)
                        return;
                    update();
                }
            };

            {
                updater = n.eventOnOff.onWeak(change);

                update();
            }

            private void update() {
                n.runLater(this::_update);
            }

            private synchronized void _update() {
                Map<String, Supplier<Surface>> attentions = new TreeMap<>();

                //TODO live update:
                n.focus.stream().map(focusPLink -> focusPLink.id).forEach(
                    v  -> attentions.put(v.id.toString(), () -> focusUI(v)));

                set(attentions);
            }
        };
    }

    static class WhatMixer extends Gridding {

        private final NAR n;
        private Off off;

        WhatMixer(NAR n) {
            this.n = n;
        }

        private static Surface focusIcon(Focus p) {
            //TODO enable/disable checkbox
            return new Bordering()
                    .set(new Gridding(
                            new UnitPriSlider(p.freq).text("freq"),
                            new UnitPriSlider(p.pri.pri).text("pri")
                    )).north(
                            new PushButton(p.term().toString(), () -> {
                                //TODO refine
                                SpaceGraph.window(p, 400, 400);
                            })
                    );
        }

        @Override
        protected void starting() {
            super.starting();
            off = n.eventOnOff.on(this::update);
            update();
        }

        private void update() {
            List<Surface> channel = n.parts(Focus.class).map(WhatMixer::focusIcon).collect(toList());
            if (channel.isEmpty())
                set(new BitmapLabel("Empty"));
            else
                set(channel);
        }

        void commit() {
            forEachRecursively(x -> {
                if (x instanceof UnitPriSlider) {
                    ((UnitPriSlider) x).commit();
                }
            });
        }

        @Override
        protected void stopping() {
            off.close();
            off = null;
            super.stopping();
        }

        private static class UnitPriSlider extends FloatSlider {

            private final UnitPri p;
            float next = Float.NaN;

            UnitPriSlider(UnitPri p) {
                super(p);
                this.p = p;
            }

            public void commit() {
                float nextPri;
                if (next == next) {
                    p.pri(nextPri = next);
                    next = Float.NaN;
                } else {
                    nextPri = p.pri();
                }
                //System.out.println(p + " "+ nextPri);
                set(nextPri);
            }
        }
    }
}