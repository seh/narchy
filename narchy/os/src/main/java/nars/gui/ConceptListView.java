package nars.gui;

import jcog.pri.PriReference;
import nars.Task;
import nars.Term;
import nars.focus.Focus;
import spacegraph.space2d.Surface;
import spacegraph.space2d.widget.button.PushButton;
import spacegraph.space2d.widget.text.VectorLabel;

public class ConceptListView extends AbstractItemList<Term> {

	public ConceptListView(Focus w, int capacity) {
		super(w, capacity);
	}

	@Override
	protected Term transform(Task x) {
		return x.term().root();
	}

	@Override
	public Surface apply(int x, int y, PriReference<Term> value) {
		return new PushButton(new VectorLabel(value.get().toString()));
	}
}