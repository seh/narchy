package nars.gui;

import jcog.Util;
import jcog.data.list.Lst;
import jcog.pri.Prioritized;
import jcog.signal.FloatRange;
import jcog.signal.IntRange;
import nars.NAR;
import nars.control.Cause;
import nars.exe.NARLoop;
import nars.exe.impl.ThreadedExec;
import nars.time.clock.RealTime;
import spacegraph.SpaceGraph;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.Bordering;
import spacegraph.space2d.container.Splitting;
import spacegraph.space2d.container.grid.Gridding;
import spacegraph.space2d.meta.LoopPanel;
import spacegraph.space2d.widget.Widget;
import spacegraph.space2d.widget.button.PushButton;
import spacegraph.space2d.widget.meter.BitmapMatrixView;
import spacegraph.space2d.widget.meter.Plot2D;
import spacegraph.space2d.widget.slider.FloatSlider;
import spacegraph.space2d.widget.slider.IntSlider;
import spacegraph.space2d.widget.text.AbstractLabel;
import spacegraph.space2d.widget.text.BitmapLabel;
import spacegraph.space2d.widget.text.VectorLabel;
import spacegraph.video.Draw;

import java.util.List;
import java.util.function.Consumer;

import static jcog.Util.lerpSafe;
import static spacegraph.space2d.container.grid.Containers.col;
import static spacegraph.space2d.container.grid.Containers.grid;

public class ExeUI {
    private static Surface metaGoalPlot2(NAR nar) {

        int s = nar.control.why.size();

        List<WhyIconButton> controls = new Lst(s);
        for (Cause w : nar.control.why)
			controls.add(new WhyIconButton(w));

        Surface g = new Gridding(controls);
//        BagChart g =
//            new BagChart<>(controls);
//        BitmapMatrixView bmp = new BitmapMatrixView(i ->
//            //Util.tanhFast(
//            gain.floatValue() * nar.control.why.get(i).pri()
//            //)
//            , s, Draw::colorBipolar);

        return DurSurface.get(g, ()->{
            for (WhyIconButton control : controls)
                control.update();
//            g.update();
        }, nar);
    }

    static class WhyIconButton extends PushButton implements Prioritized {
    	final Cause w;
    	float pri;
    	WhyIconButton(Cause w) {
    		super(new BitmapLabel(w.toString(), 16),
                    () -> SpaceGraph.window(w, 500, 300));
    		this.w = w;
    		update();
            //set(new Bordering(l)
//                .south(new FloatSlider(0, -2, +2).on(x->{
//                float p = (float) Math.pow(x, 10);
//                w.setPri(p);
//            })));
		}

        @Override
        public float pri() {
            return pri;
        }


        public void update() {
            this.pri = //unitizeSafe((1 + w.priElseZero())/2);
                    w.pri;
            Draw.hsl(
                    lerpSafe(pri, 0f, 1f),
                    0.9f, 0.5f,
                    this.color);
//            float v = w.value();
//            color.y = v!=v ? 0 : Util.unitizeSafe(v);
		}
	}

	private static Surface metaGoalPlot(NAR nar) {

        int s = nar.control.why.size();

        FloatRange gain = new FloatRange(1f, 0f, 100f);

        BitmapMatrixView bmp = new BitmapMatrixView(i ->
                //Util.tanhFast(
                    gain.floatValue() * nar.control.why.get(i).pri()
                //)
                , s, Draw::colorBipolar);

        return col(DurSurface.get(bmp, nar), 0.05f, new FloatSlider(gain, "Display Gain"));
    }

//    private static Surface metaGoalControls(CreditControlModel model, NAR n) {
//        CheckBox auto = new CheckBox("Auto");
//        auto.on(true);
//
//        float min = -1f;
//        float max = +1f;
//
//        double[] want = model.want;
//        //return DurSurface.get(
//         return grid( IntStream.range(0, want.length).mapToObj(
//            w -> new FloatSlider((float) want[w], min, max) {
//                @Override
//                protected void paintWidget(RectF bounds, GL2 gl) {
//                    if (auto.on()) {
//                        set((float) want[w]);
//                    }
//                }
//            }
//            .text(MetaGoal.values()[w].name())
//            .type(SliderModel.KnobHoriz)
//            .on((s, v) -> {
//                if (!auto.on())
//                    want[w] = v;
//            })
//        ));
//    }

    static Surface exePanel(NAR n) {

        if (!(n.exe instanceof ThreadedExec)) return new VectorLabel("TODO"); //TODO

        int plotHistory = 500;
//        MetalConcurrentQueue busyBuffer = new MetalConcurrentQueue(plotHistory);
//        MetalConcurrentQueue queueSize = new MetalConcurrentQueue(plotHistory);

        Plot2D exeQueue = new Plot2D(plotHistory)
                .add("queueSize", ((ThreadedExec) n.exe)::queueSize);
        Plot2D busy = new Plot2D(plotHistory)
                .add("Busy", n.emotion.busyVol);


        Gridding g = grid(exeQueue, busy);
        //            final Off c = n.onCycle((nn) -> {
//                busyBuffer.offer();
//                Exec nexe = n.exe;
//                if (nexe instanceof ThreadedExec)
//                    queueSize.offer();
//            });
        DurSurface d = DurSurface.get(g, n, (Consumer) nn -> {
//                if (g.parent!=null) {
                exeQueue.update();
                busy.update();
//                } else{
////                    c.close();
//                }
        });
        return d;

    }

    static Surface valuePanel(NAR n) {
        Bordering b = new Bordering(
            //metaGoalPlot(n),
            metaGoalPlot2(n)
        );
//        if (n.control.model instanceof CreditControlModel) {
//            b.east(
//                    metaGoalControls(((CreditControlModel)n.control.model), n)
//            );
//        }
        /*.south(
            //TODO menu selector
            new ObjectSurface(n.exe.governor)
        )*/
        //        //TODO
//        if (n.exe.governor instanceof Should.MLPGovernor) {
////            Should.MLPGovernor.Predictor[] p = ((Should.MLPGovernor) n.exe.governor).predictor;
////            if (p.length > 0) {
////                p[0].
////            }
//
//        }
        return b;

    }

    static class CausableWidget<X> extends Widget {

        CausableWidget(X c, String s) {
            set(new VectorLabel(s));
        }
    }

//    enum CauseProfileMode implements FloatFunction<How> {
//        Pri() {
//            @Override
//            public float floatValueOf(How w) {
//                return w.pri();
//            }
//        },
//        Value() {
//            @Override
//            public float floatValueOf(How w) {
//                return w.value;
//            }
//        },
//        ValueRateNormalized() {
//            @Override
//            public float floatValueOf(How w) {
//                return w.valueRateNormalized;
//            }
//        },
////        Time() {
////            @Override
////            public float floatValueOf(TimedLink w) {
////                return Math.max(0,w.time.get());
////            }
////        }
//        ;
//        /* TODO
//                                //c.accumTimeNS.get()/1_000_000.0 //ms
//                                //(c.iterations.getMean() * c.iterTimeNS.getMean())/1_000_000.0 //ms
//                                //c.valuePerSecondNormalized
//                                //c.valueNext
//                                //c.iterations.getN()
//                                //c...
//
//                        //,0,1
//         */
//    }
//
//    static Surface causeProfiler(AntistaticBag<How> cc, NAR nar) {
//
//        int history = 128;
//        Plot2D pp = new Plot2D(history,
//                //Plot2D.BarLanes
//                Plot2D.LineLanes
//                //Plot2D.Line
//        );
//
//        final MutableEnum<CauseProfileMode> mode = new MutableEnum<>(CauseProfileMode.Pri);
//
//        for (How c : cc) {
//            String label = c.toString();
//            //pp[i] = new Plot2D(history, Plot2D.Line).addAt(label,
//            pp.add(label, ()-> mode.get().floatValueOf(c));
//        }
//
//        Surface controls = new Gridding(
//                EnumSwitch.the(mode, "Mode"),
////                new PushButton("Print", ()-> {
////                    Appendable t = TextEdit.out();
////                    nar.exe.print(t);
////                    window(t, 400, 400);
////                }),
//                new PushButton("Clear", ()->pp.series.forEach(Plot2D.Series::clear))
//        );
//        return DurSurface.get(Splitting.column(pp, 0.1f, controls), nar, pp::commit);
//    }
//
//    public static Surface howChart(NAR n) {
//        return NARui.<How>focusPanel(n.how, h->h.pri(), h -> h.id.toString(), n);
//    }

    //    private static void causeSummary(NAR nar, int top) {
//        TopN[] tops = Stream.of(MetaGoal.values()).map(v -> new TopN<>(new Cause[top], (c) ->
//                (float) c.credit[v.ordinal()].total())).toArray(TopN[]::new);
//        nar.causes.forEach((Cause c) -> {
//            for (TopN t : tops)
//                t.add(c);
//        });
//
//        for (int i = 0, topsLength = tops.length; i < topsLength; i++) {
//            TopN t = tops[i];
//            System.out.println(MetaGoal.values()[i]);
//            t.forEach(tt->{
//                System.out.println("\t" + tt);
//            });
//        }
//    }


    /**
     * adds duration control
     */
    static class NARLoopPanel extends LoopPanel {

        final IntRange durMS = new IntRange(1, 1, 1000);
        private final FloatSlider durSlider;

        NARLoopPanel(NARLoop loop) {
            super(loop);
            NAR nar = loop.nar;
            durMS.set(nar.dur());
            durSlider = new IntSlider("Dur(ms)", durMS);
            //RealTime time;
            if (nar.time instanceof RealTime) {
                //time = ((RealTime) nar.time);
                add(
                        durSlider.on(durMS->nar.time.dur(Math.max(Math.round(durMS), 1))),
                        new FloatSlider(loop.throttle, "Throttle")
                );
            } else {
                //TODO
                //time = null;
            }
        }

        @Override
        public void update() {

            super.update();

            if (loop.isRunning()) {

                NAR n = ((NARLoop) loop).nar;
                if (n.time instanceof RealTime) {
                    double actualMS = ((RealTime) n.time).durSeconds() * 1000.0;
                    if (!Util.equals(durMS.doubleValue(), actualMS, 0.1)) {
                        durMS.set(actualMS); //external change singificant
                    }
                }
            }

        }
    }

    static Surface runPanel(NAR n) {
        AbstractLabel nameLabel;
        LoopPanel control = new NARLoopPanel(n.loop);
        Surface p = new Splitting(
                nameLabel = new BitmapLabel(n.self().toString()),
                0.25f, false, control
        );
        return DurSurface.get(p, control::update, n);
    }



}