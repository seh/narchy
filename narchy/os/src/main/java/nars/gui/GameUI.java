package nars.gui;

import com.google.common.collect.Streams;
import jcog.signal.anomaly.ewma.Ewma;
import nars.NAR;
import nars.focus.Focus;
import nars.game.Game;
import nars.game.reward.Reward;
import nars.game.reward.ScalarReward;
import nars.game.sensor.AbstractSensor;
import nars.game.sensor.VectorSensor;
import nars.gui.sensor.VectorSensorChart;
import nars.time.Moment;
import nars.time.When;
import org.jetbrains.annotations.NotNull;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.grid.Containers;
import spacegraph.space2d.container.grid.Gridding;
import spacegraph.space2d.meta.ObjectSurface;
import spacegraph.space2d.meta.Triggering;
import spacegraph.space2d.widget.menu.Menu;
import spacegraph.space2d.widget.menu.TabMenu;
import spacegraph.space2d.widget.meter.ChernoffFace;
import spacegraph.space2d.widget.meter.Plot2D;
import spacegraph.space2d.widget.text.Labelling;

import java.util.List;
import java.util.Map;

import static nars.gui.NARui.beliefIcons;

public class GameUI {

    public static Surface gameUI(Game g) {

        Iterable rewards = () -> Streams.stream(g.rewards)
            .flatMap(r -> Streams.stream(r.components()))
            .map(x -> ((ScalarReward)x).sensor.concept).iterator();

        Iterable sensors = ()->g.sensors.sensorComponents().iterator();

        Menu aa = new TabMenu(Map.of(
                g.toString(), () -> new ObjectSurface(g, 3),

                "stat", () -> new Triggering<>(g::onFrame, Containers.col(
                        statPlot(happyLongPlot(g), g),
                        statPlot(happyPlot(g), g),
                        statPlot(dexPlot(g), g)
                        //					new TriggeredSurface<>(
//						new Plot2D(512, Plot2D.Line).add("Frust", g::frustration, 0, 1),
//						Plot2D::commit),
//					new TriggeredSurface<>(
//						new Plot2D(512, Plot2D.Line).add("Coh", g::coherency, 0, 1),
//						Plot2D::commit)
                )),
//                        .addAt("Dex+2", () -> a.dexterity(a.now() + 2 * a.nar().dur()))
//                        .addAt("Dex+4", () -> a.dexterity(a.now() + 4 * a.nar().dur())), a),
                //"reward", () -> NARui.beliefCharts(rewards, g.nar()),
                "reward", () -> new Gridding(Streams.stream(g.rewards).map(r -> rewardUI(r, g.nar()))),

                "sense", () -> NARui.beliefCharts(sensors, g.nar()),
                "act", () -> NARui.beliefCharts((Iterable)()->g.actions.concepts().iterator(), g.nar()),
//                "curi", () -> new ObjectSurface(g.actions.curiosity),
                "q", () -> NARui.implMatrix(g.actions.actions, g.rewards, +Math.round(g.nar.dur()), g.nar),
                "face", () -> GameUI.face(g)
        ));
        return aa;
    }

    @NotNull
    private static Triggering<Plot2D> statPlot(Plot2D p, Game g) {
        return new Triggering<>(p, Plot2D::update).invisibleUpdate(true);
    }

    private static Surface face(Game g) {
        final ChernoffFace.FaceSurface f = new ChernoffFace.FaceSurface();
        return new Triggering<>(f, g::onFrame, (F)->{
            final When<Focus> t = g.time;
            float dur = t.dur;
            long s = t.s, e = t.e;
            double happyNow = g.happiness();
            int longHappinessDurs = 16;
            long r = e - s;
            double happyLong = g.happiness(s - r * longHappinessDurs, e, dur * longHappinessDurs);
            F.set(6, (float) happyNow);
            F.set(4, 1 - (float) happyLong);
            double dex = g.dexterity();

            F.set(8, dex==dex ? 0.75f + 0.25f * (float) Math.pow(dex, -5) : 0);

            float focusPri =
                    //g.focus().pri();
                    g.focus().focusPri();

            F.set(9, focusPri);
            F.set(10, focusPri);

        });
        //return f;
    }

    private static Surface rewardUI(Reward r, NAR n) {
        Iterable v = () -> Streams.stream(r.components()).iterator();
        return new Gridding(
            NARui.beliefCharts(v, n),
            new ObjectSurface(r)
        );
    }

    private static Plot2D happyLongPlot(Game g) {
        Plot2D p = new Plot2D(256);
        var mean = Plot2D.mean(g::happiness, 64);
        var ewma = new Ewma(0.1);
        p.add("happy", mean, 0, 1);
        p.add("HAPPY", ()-> {
            double m = mean.last();
            return m==m ? ewma.acceptAndGetMean(m) : Float.NaN;
        }, 0, 1);
        return p;
    }

    private static Plot2D happyPlot(Game g) {
        Plot2D p = new Plot2D(512);
        Moment gw = g.time;
        g.rewards.forEach(r -> p.add(r.term() + " happy",
            ()-> r.happy(gw.s, gw.e, gw.dur), 0, 1));
        return p;
    }

    private static Plot2D dexPlot(Game g) {
        Plot2D p = new Plot2D(512);
        g.actions.forEach(a ->
            p.add(a.term() + " dex", a::dexterity));
        return p;
    }

    public static Surface row(AbstractSensor v, Game g) {
        return rowcol(v, true, g);
    }

    public static Surface col(AbstractSensor v, Game g) {
        return rowcol(v, false, g);
    }

    private static Surface rowcol(AbstractSensor v, boolean rowOrCol, Game g) {
        if (v instanceof VectorSensor V)
            return Labelling.the(v.term().toString(),
                    new VectorSensorChart(V, rowOrCol ? V.size() : 1, rowOrCol ? 1 : V.size(), g));
        else
            return beliefIcons(List.of(v.term()), g.nar);
    }
}