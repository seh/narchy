package nars.gui.graph.run;

import jcog.data.map.CellMap;
import jcog.pri.Prioritized;
import nars.NAR;
import nars.Term;
import nars.focus.BagFocus;
import nars.gui.DurSurface;
import nars.link.TaskLink;
import nars.premise.Premise;
import nars.subterm.Subterms;
import nars.task.NALTask;
import nars.term.Compound;
import nars.term.Termed;
import nars.term.Variable;
import org.jetbrains.annotations.Nullable;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.graph.EdgeVis;
import spacegraph.space2d.container.graph.Graph2D;
import spacegraph.space2d.container.graph.NodeVis;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import static nars.term.atom.Bool.Null;

public class TaskLinkGraph2D extends TermGraph2D<Premise> {

    private final TasklinkVis vis;

    private TaskLinkGraph2D(NAR n) {
        super(n);
        render(
                Graph2D.InvalidateEdges,
                //                      new TermlinkVis(n),
//                        new SubtermVis(n),
                this.vis = new TasklinkVis()
                //new StatementVis(n)
        );
    }

    @Override
    protected void _set(Iterable<Premise> links, int n) {
        super._set(links, n);

        if (nodes()>0) {
            for (Premise t : buffer)
                vis.update(t, edit);
        }
    }

    @Override
    protected Stream<Term> terms(Premise z) {
        return z.self() ? Stream.of(z.from()) : Stream.of(z.from(), z.to());
    }

    public static Surface tasklinks(NAR n) {
        return tasklinks(((BagFocus) n.main()), n);
    }

    public static Surface tasklinks(BagFocus links, NAR n) {

        TaskLinkGraph2D g = new TaskLinkGraph2D(n);

        return DurSurface.get(g.widget(), () -> g.setAll(links.bag), n).every();

    }


//    @Override
//    protected Termed key(T termed) {
//        //collapse self termlinks to the their term
//        return termed instanceof Premise ? ((Premise) termed).from()/*.concept()*/ : termed;
//    }

//	@Override
//	protected void addControls(Gridding cfg) {
//        cfg.add(new ObjectSurface(maxNodes));
//	}

    private static class TasklinkVis implements Graph2D.Graph2DRenderer<Termed> {

        public final AtomicBoolean belief = new AtomicBoolean(true);
        public final AtomicBoolean goal = new AtomicBoolean(true);
        public final AtomicBoolean question = new AtomicBoolean(true);
        public final AtomicBoolean quest = new AtomicBoolean(true);
        public final AtomicBoolean subtermLinks = new AtomicBoolean(false);
        private final float[] tPri = new float[4];
        /**
         * a non-volatile cache; is this helpful?
         */
        private transient boolean _belief;
        private transient boolean _goal;
        private transient boolean _question;
        private transient boolean _quest;


        private TasklinkVis() {
        }

        @Override
        public void nodes(CellMap<Termed, ? extends NodeVis<Termed>> cells, GraphEditor<Termed> edit) {

            _belief = this.belief.getOpaque();
            _goal = this.goal.getOpaque();
            _question = this.question.getOpaque();
            _quest = this.quest.getOpaque();
            if (!_belief && !_goal && !_question && !_quest)
                return;

            cells.forEachValue(nv -> {
                nv.pri = 0; //reset
            });

            Graph2DRenderer.super.nodes(cells, edit);

        }

        @Override
        public void node(NodeVis<Termed> n, GraphEditor<Termed> graph) {
            Termed x = n.id;
            if (x instanceof Premise)
                update((Premise) x, graph);


        }

        public void update(Premise l, GraphEditor<Termed> graph) {

            Term from = l.from();//.concept();
            if (from == Null)
                return; //??
            @Nullable NodeVis<Termed> fromNode = graph.node(from);
            if (fromNode == null)
                return;

            Term to = l.to();//.concept();
            boolean self = from.equals(to);

            float pri = l.priElseZero();
            if (self) {
                fromNode.pri += pri;
            } else {
//				if (to == Null)
//					return; //?
                @Nullable NodeVis<Termed> toNode = from != to ? graph.node(to) : fromNode;
                if (toNode == null)
                    return;


                float ph = pri / 2;
                fromNode.pri += ph;
                toNode.pri += ph;
            }

            if (to instanceof Variable || self)
                return; //done if variables or self links


            EdgeVis<Termed> e = graph.edge(fromNode, to);
            if (e != null) {
                float pSum;
                e.colorLerp(0.1f, 0.1f, 0.1f, COLOR_FADE_RATE); //still visible a bit
                if (l instanceof TaskLink) {
                    pSum = colorTaskLink((TaskLink) l, e);
                } else {
                    pSum = pri;
                    colorLink(e, NALTask.i(l.task().punc()), pSum);
                }
                e.weightLerp(0.1f + 0.6f * pSum, WEIGHT_UPDATE_RATE);
            }

            //subterm links
            if (subtermLinks.get()) {
                Term xx = l.from().term();
                if (xx instanceof Compound) {
                    Subterms ss = xx.subtermsDirect();
                    float a = 0.01f / ss.subs() * COLOR_UPDATE_RATE;
                    for (Term s : ss) {
                        if (s.CONCEPTUALIZABLE()) {
                            EdgeVis<Termed> gg = graph.edge(xx, graph.nodeOrAdd(s));
                            if (gg != null)
                                gg.colorAddLerp(1, 1, 1, a).weightAddLerp(a, WEIGHT_UPDATE_RATE);
                        }
                    }
                }
            }
        }

        public float colorTaskLink(TaskLink l, EdgeVis<Termed> e) {
            float[] tPri = this.tPri;
            l.priGet(tPri);
            if (!_belief) tPri[0] = 0;
            if (!_question) tPri[1] = 0;
            if (!_goal) tPri[2] = 0;
            if (!_quest) tPri[3] = 0;

            float pSum = 0;
            for (int i = 0; i < 4; i++) {
                float ppi = tPri[i];
                if (ppi >= Prioritized.EPSILON) {
                    pSum += ppi;
                    colorLink(e, i, ppi);
                }
            }
            return pSum / 4;
        }

        public void colorLink(EdgeVis<Termed> e, int priID, float ppi) {
			      /*
                https://www.colourlovers.com/palette/848743/(_%E2%80%9D_)
                BELIEF   Red     189,21,80
                QUESTION Orange  233,127,2
                GOAL     Green   138,155,15
                QUEST    Yellow  248,202,0
                */
            int b;
            int g;
            int r;
            switch (priID) {
                case 0 -> {
                    r = 189;
                    g = 21;
                    b = 80;
                }
                case 1 -> {
                    r = 2;
                    g = 127;
                    b = 233;
                }
                case 2 -> {
                    r = 138;
                    g = 155;
                    b = 15;
                }
                case 3 -> {
                    r = 2;
                    g = 233;
                    b = 127;
                }
                default -> throw new UnsupportedOperationException();
            }

            e.colorLerp(r / 256f, g / 256f, b / 256f, COLOR_UPDATE_RATE * ppi);
        }
    }
}