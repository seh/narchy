package nars;

import jcog.Config;
import jcog.Util;
import jcog.data.list.Lst;
import jcog.exe.Loop;
import jcog.pri.PLink;
import jcog.pri.bag.impl.ArrayBag;
import jcog.pri.op.PriMerge;
import jcog.ql.dqn.ValuePredictAgent;
import jcog.signal.FloatRange;
import nars.action.answer.AnswerQuestionsFromConcepts;
import nars.action.link.LinkFlow;
import nars.action.link.STMLinker;
import nars.action.link.TermLinking;
import nars.action.link.index.CachedAdjacenctTerms;
import nars.action.link.index.EqualTangent;
import nars.action.transform.*;
import nars.derive.Deriver;
import nars.derive.impl.BagDeriver;
import nars.derive.impl.MixDeriver;
import nars.derive.pri.DefaultBudget;
import nars.derive.reaction.ReactionModel;
import nars.derive.reaction.Reactions;
import nars.exe.Exec;
import nars.exe.impl.UniExec;
import nars.exe.impl.WorkerExec;
import nars.focus.BagFocus;
import nars.focus.BasicTimeFocus;
import nars.focus.Focus;
import nars.focus.time.ActionTiming;
import nars.focus.time.PresentTiming;
import nars.func.Commentary;
import nars.func.Factorize;
import nars.func.stm.CompoundClustering;
import nars.game.Game;
import nars.game.action.util.Reflex;
import nars.game.meta.MetaGame;
import nars.game.meta.SelfMetaGame;
import nars.game.reward.LambdaScalarReward;
import nars.game.reward.Reward;
import nars.game.reward.ScalarReward;
import nars.game.sensor.FocusLoop;
import nars.game.sensor.VectorSensor;
import nars.gui.NARui;
import nars.link.flow.LinkFlows;
import nars.memory.CaffeineMemory;
import nars.memory.HijackMemory;
import nars.memory.Memory;
import nars.memory.TierMemory;
import nars.task.NALTask;
import nars.task.util.Eternalization;
import nars.task.util.PuncBag;
import nars.term.Termed;
import nars.term.atom.Atomic;
import nars.time.Time;
import nars.time.clock.RealTime;
import org.eclipse.collections.api.block.procedure.primitive.FloatProcedure;
import spacegraph.space2d.container.grid.Gridding;
import spacegraph.space2d.widget.button.PushButton;
import spacegraph.space2d.widget.text.BitmapLabel;

import java.util.Collection;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static nars.Op.*;
import static spacegraph.SpaceGraph.window;

/**
 * builder for NAR's that play games
 */
public class Player {

    public static final int REPROBES = 13;
    public double ramGB =
        Runtime.getRuntime().maxMemory() / (1024 * 1024 * 1024.0);

    public int threads =
        Config.INT("THREADS", Util.concurrencyExcept(1));

    /**
     * fundamental system framerate
     * TODO auto-determine by estimating from maximum required FPS of the game durations'
     */
    public float fps = 30;

    boolean eternalizationControl = false;

    boolean beliefConfControl = false;
    boolean goalConfControl = false;


    /** thread affinity, can improve latency
     *  TODO WARNING unsafe: threads may not respawn correctly TODO test
     */
    public boolean exeAffinity = false;

    public Consumer<NAR> ready = n -> { };
    public boolean nal = true;
    public boolean meta = true;

    public boolean inperience = true;

    /** global volmax control
     *  warning: may interfere with other simplicity controls */
    private final boolean volMaxControl = false;

    private final boolean durMeta = false;

    private final boolean selfFreq = false;

    public boolean gamePri = true;

    public boolean autoReward = true;

//    public boolean rewardConf = false;

//    /** @see DefaultDerivePri */
//    public boolean puncProb = false;
//    public boolean subMetaPuncPri = false;
//    public boolean subMetaOpPri = false;

    public boolean motivation = false;
    public boolean xor = false;

    public boolean answerQuestionsFromTaskLinks = false;

    public boolean goalSpread = false;

    public boolean actionRewardQuestion = false;


    public boolean arith = true;

    public boolean explode = true;
    public boolean factorize = false;

    public boolean abbreviation = false;

    public boolean globalAdjacency = false;
    public boolean ifThenElse = false;
    public boolean goalInduction = false;


    /** TODO split into separate 'dur' and 'shift' flags */
    boolean timeFocus =
        true;
        //false;

    /** perceptual bandwidth
     *  in 2^octaves durs */
    float timeOctaves =
        6;
        //7;
        //3;
        //4;
        //2;
        //5;
        //8;
        //10;
        //16;

    /** scannable horizon duration */
    float shiftEpochs =
        //0;
        //1;
        2;
        //4;
        //8;
        //16;
        //64;

    public boolean deltaIntro = true;
    public boolean deltaGoal = true;

//    static final boolean answerLogging = true;

    /** TODO maybe broken */
    @Deprecated public boolean gameReflex = false;

    public boolean selfMetaReflex = true;

    public boolean subMetaReflex = true;
//    public boolean subMetaReflexMerged = false;

    public boolean commentary = false;

    public boolean encourager = false;

    public boolean conjClusterBelief = true;
    public boolean conjClusterGoal = false;

    @SuppressWarnings("ConstantConditions")
    public boolean stmLinker =
            false;
            //!(conjClusterBelief && conjClusterGoal);
            //!conjClusterBelief;

    /** ability for a submeta to pause its game */
    public boolean pausing = false;

    public boolean uiTop = true;
    public boolean uiStats = false;

    public float focusSamplingExponent =
            ArrayBag.SHARP_DEFAULT
            //4
            //1
            //2
            //3
    ;

    public boolean cpuThrottle = true;

//    public ControlModel control = null;

    /** -1 for auto-size to heap (-Xmx) */
    public double conceptsMax = -1;

//    /** default focus capacity TODO */
//    public int focusCapacity =
//            //128
//            //256
//            512
//            //1024
//            //2048
//    ;

    public float beliefConf = 0.75f, goalConf   = beliefConf;
    //public float beliefConf = 0.5f, goalConf   = beliefConf;
    //public float beliefConf = 0.9f, goalConf   = beliefConf;
    //public float beliefConf = 0.85f, goalConf   = 0.9f;

    //public float beliefConf = 0.3f, goalConf   = beliefConf;
//    public float beliefConf =
//            (float) (Math.sqrt(2)/2)
//            //PHI_min_1f
//            , goalConf   = beliefConf;

    //public float beliefConf = Util.PHI_min_1f, goalConf   = 0.75f;
    //public float beliefConf = Util.PHI_min_1f, goalConf   = beliefConf * 1.1f;
//    public float beliefConf = Util.PHI_min_1f, goalConf   = beliefConf;
    //public float beliefConf = 0.85f, goalConf   = 0.9f;
//    public float beliefConf = 0.9f, goalConf   = beliefConf;
    //public float beliefConf = Util.PHI_min_1f, goalConf   = 0.9f;
//    public float beliefConf = 0.75f, goalConf   = beliefConf;
//      public float beliefConf = 0.5f, goalConf   = beliefConf;
    //public float beliefConf = 0.5f, goalConf   = 0.6f;


//    public float beliefConf = 0.5f, goalConf   = 0.9f;
//    public float beliefConf = 0.75f, goalConf   = 0.5f;
//    public float beliefConf = 0.9f, goalConf   = 0.5f;
//public float beliefConf = 0.25f, goalConf   = 0.25f;
//    public float beliefConf = 0.75f, goalConf   = 0.75f;
    //public float beliefConf = 0.5f, goalConf   = 0.15f;

    /** TODO */
    public float eternalization   =
        //0;
        1/32f;
        //1/16f;
        //1/8f;
        //1/24f;
        //0.1f/NAL.answer.REVISION_CAPACITY;
        //1 - Util.PHI_min_1f;
        //0;
        //1/1024f;
        //1/256f;
        //1/100f;
        //1/64f;
        //1/32f;
        //1/10f;
        //1/8f;
        //1/4f;
        //1/2f;

    public int volMax =
            //16
            //20
            //24
            //28
            32
            //48
            //64
            //96
            //128
            //256
    ;
    public int dtDither =
            //4
            //5  //200hz (100hz nyquist)
            //8
            //10   //100hz (50hz nyquist)
            20   //50hz  (25hz nyquist)
            //40   //25hz  (12.5hz nyquist)
    ;

    public float freqRes =
            0.01f;
            //0.02f;
            //NAL.truth.TRUTH_EPSILON; //minimum

    public float confRes =
            NAL.truth.TRUTH_EPSILON; //minimum
            //freqRes;
            //0.01f;


    /** constructed by calling start() */
    public volatile NAR nar;
    private Loop loop;

    /** initial min confidence */
    public double confMin =
        NAL.truth.CONF_MIN;
        //NAL.truth.CONF_MIN/2;
        //NAL.truth.CONF_MIN/4;
        //NAL.truth.CONF_MIN/16;
        //NAL.truth.TRUTH_EPSILON / NAL.answer.REVISION_CAPACITY; //coarse
        //0.0000001f;
        //0.0001;

    public boolean nalProcedural = true;
    public boolean nalStructural = true;
    public boolean nalSim = true;
    public boolean nalSets = false;

    @Deprecated public boolean iterTune = false;

    /** warning; can easily confuse other priority controls */
    public boolean simplicityControl = true;

    /** link prioritization */
    public boolean simplicityLinkControl = true;

    public boolean certaintyControl = true;

    public boolean opPri = false;

    public boolean puncPriLink = true;
    public boolean puncPriDerive = false;

    public float
            puncPriMin = 0.01f, puncPriMax = 1;
        //0.05f, 1,
        //0.1f, 1,
        //0.25f, 0.5f,
        //0.05f, 1,
        //0.03f, 0.5f,
        //0.02f, 0.1f,

    public boolean puncPriAmp = true;

    enum FocusForgetting {
        None, Balanced, Clear
    }
    public FocusForgetting focusForgetting = FocusForgetting.Clear;

    public boolean focusSharp = false;

    @Deprecated private final boolean implBeliefify = false, implGoalify = implBeliefify;

    private boolean curiosityControl = true;

    private boolean volMetaReduced = true;

    /** how many system durs in self-meta dur */
    float selfMetaDurs =
        2; //nyquist-like
        //1; //1:1

    /** NOTE this specifies the 'game frame divisor', not durs.
     *  probably > 1 is best */
    int subMetaDurs =
        2; //nyquist-like
        //4;
        //16;
        //1;

    int metaFocusCapacity = 512;
    int subMetaFocusCapacity = metaFocusCapacity;


    public Player() {
    }

    public Player(NARPart... parts) {
        this(Stream.of(parts));
    }

    public Player(Collection<? extends NARPart> parts) {
        this();
        add(parts);
    }

    public Player(Stream<? extends NARPart> parts) {
        this();
        add(parts);
    }

    @Deprecated public Player(float fps, Consumer<NAR> ready) {
        this(ready);
        fps(fps);
    }

    public Player(Consumer<NAR> ready) {
        this();
        ready(ready);
    }

    public synchronized Player ready(Consumer<NAR> r) {
        if (this.nar!=null)
            r.accept(nar);
        else
            this.ready = this.ready.andThen(r);
        return this;
    }

    public Player add(NARPart... p) {
        return add(Stream.of(p));
    }

    public Player add(Stream<? extends NARPart> p) {
        return ready((nn) -> p.forEach(nn::add));
    }

    public Player add(Collection<? extends NARPart> p) {
        return add(p.stream().parallel());
    }


//    public void control(ControlModel m) {
//
//        this.control = m;
//
////        if (!meta) {
////            n.emotion.want(MetaGoal.Question, -0.005f);
////            n.emotion.want(MetaGoal.Believe, 0.01f);
////            n.emotion.want(MetaGoal.Goal, 0.05f);
////        }
//    }

//    public void controlCreditAnalysis() {
//        control(new CreditAnalysisModel());
//    }
//
//    public void control(CreditControlModel.Governor g) {
//        control(new CreditControlModel(g));
//    }
//
//    public void controlMLP() {
//        control(new MLPGovernor());
//    }
//
//    public void controlLerp() {
//        control(new LERPGovernor());
//    }

    public synchronized Player start() {
        init();

        loop = nar.startFPS(fps);

        return this;
    }

    @Deprecated public synchronized Player runCycles(int cycles) {
        init();

        nar.run(cycles);

        return this;
    }

    public synchronized void init() {
        ensureStopped();
        if (nar == null) __init();
    }

    private void ensureStopped() {
        if (loop!=null) throw new RuntimeException("already running");
    }

    public synchronized Player stop() {
        //nar.synch();
//        nar.runLater(()->{
            nar.parts(Game.class).forEach(g -> nar.remove(g)); //HACK
//            nar.stop();
//        });
        return this;
    }


    private void __init() {
        NAR n = this.nar = nar();

        n.dtDither.set(dtDither);

        n.volMax.set(volMax);

        n.freqResolution.set(freqRes);
        n.confResolution.set(confRes);
        
        n.beliefConfDefault.set(beliefConf);
        n.goalConfDefault.set(goalConf);

        initEternalization(n);

        n.confMin.set(confMin);

        ready.accept(n);

        n.runLater(() -> n.parts(Game.class).forEach(g -> {
            if (volMetaReduced) {
                if (g instanceof MetaGame)
                    g.focus().volMax = Math.max(volMax / 2,
                            14); /* TODO */
            }

            ((ArrayBag) ((BagFocus) g.focus()).bag).sharp = focusSamplingExponent;
        }));

        if (meta) meta();

        if (commentary) {
            /*n.runLater(() -> */n.add(new Commentary(n));
        }

//        if (impiler) {
//            playing().forEach(g -> {
//                    Impiler.impile(g);
//            });
//        }
        if (gameReflex) {
            /*n.runLater(() -> */games().forEach(g -> {
                        Reflex bb = new Reflex(
                                ValuePredictAgent::DQN,
                                //ValuePredictAgent::DQN_NTM,
                                //ValuePredictAgent::DQN_LSTM,
                                //ValuePredictAgent::DQN1,
                                g,
                                1, 2, 4
                        );
//                n.runLater(()->
//                        window(NARui.reflexUI(bb), 900, 900));
                        //HACK
                        //g.actions.curiosity.enable.set(false);
//                    g.actions.curiosityReviseOrReplace = true;
            });
        }


        if (uiStats) {
            n.runLater(() -> {
                BitmapLabel stats = new BitmapLabel(String.valueOf((char) 0x00D8));
                window(stats, 400, 600);
                StringBuilder s = new StringBuilder(16 * 1024);
                Loop.of(() -> {
                    //stats.viewMin(stats.view().w, stats.view().h);
                    s.setLength(0);

                    n.stats(false, true, s);

                    stats.text(s.toString()); //TODO avoid toString() and read from CharSequence directly
                }).fps(0.25f);
            });
        }

        if (uiTop) {
            //n.synch();
            n.runLater(() -> {
                window(NARui.top(n), 1024, 800);


//                n.runLater(() -> {
//                    List<SubPart> rr = n.subPartStream().filter(pp -> pp instanceof Reflex).toList();
//                    if (!rr.isEmpty())
//                        window(new Surfaces(Iterables.transform(rr,
//                                pp -> NARui.reflexUI((Reflex) pp))), 800, 600);
//                });

                //        if (exeChart) {
                //            Exe.runLater(() -> n.runLater(() -> {
                //                window(NARui.exeChart((WorkerExec) n.exe), 800, 800);
                //            }));
                //        }

//                List<Reflex> l = n.parts(Reflex.class)
//                        .collect(toList());
//                if (!l.isEmpty()) {
//                    window(new Gridding(
//                        l.stream().map(NARui::reflexUI).collect(toList())
//                    ), 500, 500);
//                }

            });

        }

//        if (uiGames) {
//            nar.runLater(() -> {
//                SpaceGraph.window(new Gridding(nar.parts(Game.class).map(NARui::gameUI).collect(toList())), 1024, 768);
//            });
//        }


    }

    private void initEternalization(NAR n) {
        var ete =
            //new Eternalization.TemporalsAndVariables()
            new Eternalization.Flat()
        ;

        n.eternalization = ete.set(eternalization);
    }

    public Stream<Game> games() {
        return nar.parts(Game.class).filter(g -> !(g instanceof MetaGame));
    }

    public void meta() {


        SelfMetaGame meta = new SelfMetaGame(nar, selfMetaDurs, subMetaDurs) {

//            public Reflex rlBoostUI(Reflex r) {
//                nar.runLater(()-> window(NARui.reflexUI(r), 800, 600));
//                return r;
//            }
            @Override
            protected void initMeta() {
                heapSense();
                emotionSense();

                if (cpuThrottle) {
                    autoThrottle();
                    //cpuThrottle(0.05f, 1);
                }

                if (eternalizationControl)
                    eternalization(0.01f, 0.5f);



                if (beliefConfControl)
                    conf(Op.punc(BELIEF), nar.beliefConfDefault, 0.1f, 0.9f);

                if (goalConfControl)
                    conf(Op.punc(GOAL), nar.goalConfDefault, 0.1f, 0.9f);

                //confMin(8);

                //truthPrecision(true, false);

                //conf(nar.goalConfDefault, 0.25f, 0.75f);

                if (volMaxControl && volMax > 12) {
                    volMax(12, volMax);
                }

                if (durMeta)
                    durMeta(3, 2);

                if (selfFreq) {
                    action($.inh(id, "freq"), (float x) ->
                        focus().freq.setLerp(x, 0.01f, 0.1f)
                    );
                }
//                if (nar.control.model instanceof CreditControlModel) {
//                    metaGoals((CreditControlModel) nar.control.model);
////            CreditControlModel.Governor governor = ((CreditControlModel) nar.control.model).governor;
//                    //if (governor instanceof MLPGovernor) governor((MLPGovernor)governor); //probably isnt always necessary
//                }
        //        if (nar.memory instanceof HijackMemory) {
        //            memoryControl((HijackMemory)nar.memory);
        //        }

                    //overderive(DERIVE, derivePri, 1, 1.5f);
                    //opPri(SELF, derivePri.opPri);

            }

            private void autoThrottle() {
                //computes throttle from total demand freq of the subgames
                onFrame(()->{
                    double max = game.stream()
                        .flatMap(z -> Stream.of(z, z.game)) //all subgames and their meta's
                        .mapToDouble(g -> g.focus().freq())
                        //.average()
                        .max()
                        .getAsDouble();
                    //TODO + meta freq?

                    double demand = max;

                    //System.out.println(demand);

                    double minThrottle = 0.01;
                    nar.loop.throttle.set(Math.max(minThrottle, demand));
                });
            }

            @Override
            protected void initMetaEnd() {
                if (selfMetaReflex) {
                    var r = rlBoost(); //rlBoostUI(rlBoost());
//                    if (subMetaReflexMerged) {
//                        for (SubMetaGame sm : game)
//                            r.add(sm, r);
//                    }
                }
            }

            private void eternalization(float min, float max) {
                floatAction($.inh(id, "eternalize"), (FloatProcedure) e ->
                    ((Eternalization.Flat)nar.eternalization).ete.setLerp(e, min, max)).resolution(0.02f);
            }

            @Override
            protected void initMeta(SubMetaGame metaGame) {
                Game G = metaGame.game;

                if (actionRewardQuestion)
                    actionRewardQuestion(metaGame, G);

                if (curiosityControl)
                    metaGame.curiosity(G,
                    NAL.CURIOSITY_DURS * 4,
                    NAL.CURIOSITY_DURS / 4
                    );


                //senseGameRewards();

//                onFrame(()->{
////                    float subMetaDurs = 1;
////                    focus().dur(subMetaDurs * G.dur());
//
//                    metaGame.focus().dur(G.nar.dur());
//                });

                BagFocus f = (BagFocus) G.focus();
                DefaultBudget B = (DefaultBudget) f.budget;

                if (puncPriLink)
                    metaGame.puncPriSimple(puncPriDerive ? $.p(f.id, "link") : f.id,
                            puncPriMin, puncPriMax , puncPriAmp,
                        B.linkPuncPri
                        //B.puncNALPremise
                        //B.puncTaskifyPremise/*, B.puncDerived*/ /*, B.puncNALPremise,  */
                    );
                if (puncPriDerive)
                    metaGame.puncPriSimple(puncPriLink ? $.p(f.id, "derive") : f.id,
                            puncPriMin, puncPriMax , puncPriAmp,
                            B.puncDerived
                    );

                if (opPri)
                    metaGame.opPri(f.id, B.linkOpPri,
                            0.5f, 1);

//                if (subMetaPuncPri) {
//                    if (!(f.budget instanceof OpPuncActivator)) f.budget = new OpPuncActivator();
//                    g.puncPri(G.id, ((OpPuncActivator)f.budget).punc, 0.1f, 1f);
//                    //g.puncPriSimple(G.id, ((OpPuncActivator)f.activator).punc, 0.1f, 1f);
//                }
//                if (subMetaOpPri) {
//                    if (!(f.budget instanceof OpPuncActivator)) f.budget = new OpPuncActivator();
//                    g.opPri(G.id, ((OpPuncActivator)f.budget).opProb, 0.001f, 1);
//                }


                if (simplicityControl)
                    metaGame.simplicity(B, /*1*/ 0, 1);

                if (simplicityLinkControl)
                    metaGame.simplicityLink(B, 0.01f, 1 /*Util.PHI_min_1f*/);

                if (certaintyControl)
                    metaGame.certainty(B,
                        0, 1
                        //0.25f, 4
                        //0.5f, 1.5f
                    );
//                if (ampControl)
//                    g.amp(B, 0.1f, 0.4f);


                switch (focusForgetting) {
                    case None ->
                        metaGame.focusSustainBasic(f, 0, 1);
                    case Balanced ->
                        metaGame.focusSustainBasic(f, -1, 1);
                    case Clear -> {
                        //separate action
                        metaGame.focusClear(f);
                        metaGame.focusSustainBasic(f, 0, 1);
                    }
                }

                metaGame.focusSimpleSensor(f);
                metaGame.focusPuncSensor(f);

                if (focusSharp)
                    metaGame.focusSharp(
                            2,5
                            //0.5f, 4
                    );

                //memoryControlSimple(w, -0.9f, 0.9f);

                //memoryControlPrecise(w, false, true, false);
                //memoryControlPrecise(w, true, false, true);


                //memoryPreAmp(w, true, false);

                //curiosityShift();
//                if (curiosityControl)
//                    g.curiosityRate();
                //curiosityStrength();


//                {
//
//                    OpPri op = new OpPri();
//                    PuncPri punc = new PuncPri();
//                    w.taskLinkPri = t -> op.floatValueOf(t) * punc.floatValueOf(t) * t.pri();
//                    Term root = w.id; /* $.p(w.id, pri)*/
//                    opPri(root, op);
//                    puncPri(root, punc);
//
//                }




//				if (w.inBuffer instanceof PriBuffer.BagTaskBuffer)
//					floatAction($.inh(gid, input), ((PriBuffer.BagTaskBuffer) (w.inBuffer)).valve);


                if (timeFocus) {
                    metaGame.timeFocus(f.id, (BasicTimeFocus) f.time,
                            G::dur, //nar.dur(),
                            timeOctaves, shiftEpochs
                    );
                }

                final float amp =
                        1f;
                        //0.5f;
                G.actions.pri.amp(amp);
                G.sensors.pri.amp(amp);
                G.rewards.pri.amp(amp);

                if (gamePri) {
                    if (autoReward) {
                        autoReward(metaGame);

                    } else {
                        /* global reward priority control */
                        metaGame.priRewards();
                    }
                    metaGame.priActions();

                    //metaGame.priSensors();

                    metaGame.priVectorSensors(false, true, true);
                }
//                if (rewardConf) {
//                    final float confBase = g.nar.confDefault(GOAL);
//                    g.rewardConf(confBase / 4, confBase);
//                }


                //TODO make this a reward proxy, to avoid redundant concept
                Reward gameReward = metaGame.gameReward();

                Reward dexReward = metaGame.dexReward();//.strength(0.5f);

                if (subMetaReflex) {
                    Reflex rr = metaGame.rlBoost();
//                    nar.runLater(()-> rlBoostUI(rr));
                }

                if (pausing)
                    metaGame.pausing();

            }

            private void autoReward(SubMetaGame metaGame) {
                float min =
                    //0.01f;
                    //0.001f;
                    Math.max(0.01f, 0.1f/(metaGame.game.rewards.size()));
                //0.5f/metaGame.game.rewards.size();

                for (Reward r : metaGame.game.rewards) {
                    float initialStrength = r.strength();
                    if (initialStrength > min*2) {
                        metaGame.action($.inh(r.id, conf), s -> {
                            /* controls reward goal conf */
                            r.strength(s);

                            /* controls reward sensor priority */
                            if (r instanceof ScalarReward)
                                ((ScalarReward)r).amp(s);
                            else {
                                /*throw new TODO();*/
                            }

                        }, min, initialStrength /* use initial strength as max */,Float.NaN);
                    }
                }
            }

            private void actionRewardQuestion(SubMetaGame g, Game G) {
                G.onFrame(()->{
                    Random rng = G.nar().random();
                    FocusLoop _s = G.sensors.sensors.get(rng);
                    Term s;
                    if (_s instanceof VectorSensor)
                        s = ((Termed)new Lst<>(_s.components()).get(rng)).term();
                    else
                        s = _s.term();

                    Reward _r = G.rewards.rewards.get(rng);


                    if (rng.nextBoolean())
                        s = s.neg();

                    Term r = _r.term(); //TODO goal term
                    Term q = CONJ.the(XTERNAL, s, r);

                    long[] startend = G.focus().when(); long start = startend[0], end = startend[1];
                    final byte punc =
                            //QUESTION;
                            QUEST;
                    Task p = NALTask.task(q, punc, null, start, end, g.nar.evidence());
                    p.pri(0.25f);
//                        System.out.println(p);
                    G.focus().accept(p);
                });
            }


        };

        //self.logActions();



        if (encourager) {
            FloatRange enc = new FloatRange(0.5f, 0, 1);
            LambdaScalarReward e = meta.addEncouragement(enc);
            nar.runLater(() -> { //HACK
                window(new Gridding(NARui.beliefChart(e.sensor, nar), new PushButton(":)", () -> enc.set(1)), new PushButton(":(", () -> enc.set(0))), 800, 800);
            });
        }


        ((BagFocus) meta.focus()).capacity.set(metaFocusCapacity);

        nar.runLater(() -> { //HACK
            meta.game.forEach(g ->
                ((BagFocus) g.focus()).capacity.set(subMetaFocusCapacity));
        });

        meta.onFrame(() -> {
            float metaFreq = 0.05f / meta.game.size();
            meta.focus().freq(metaFreq);
                        //.freqAndPri(p);
        });

        nar.add(meta);

        nar.synch();

//        nar.runLater(() -> {
//            List<RLBooster> b = nar.parts(RLBooster.class).toList();
//            if (!b.isEmpty()) {
//                SpaceGraph.window(
//                    new Gridding(b.stream().map(b1 -> NARui.rlbooster(b1)))
//                    , 800, 800);
//            }
//        });
        

    }

    public Time clock = new RealTime.MS();

    /** TODO automatically compute this based on the maximum framerate of any game, up to a system hard limit (ex: 100 fps) */
    public Player fps(float fps) {
        this.fps = fps;
        clock = new RealTime.MS().durFPS(
            fps
            //fps/2 //nyquist
        );
        return this;
    }

    private NAR nar() {

        Exec exe = threads == 1 ? new UniExec() : new WorkerExec(threads, exeAffinity) {

            @Override protected ReactionModel reaction(NAR n) {
                return Player.this.deriver().compile(n);
                //nn.runLater(()-> window(new ObjectSurface(m, 2, NAR_UI(nn)), 600, 600) );
            }
        };

        NAR n = new NARS()
                .exe(exe)
                .time(clock)
                .memory(memory())
                .get();


        if (threads == 1) {
            //Deriver d = new BagDeriver(deriver(), n);
            Deriver d = new MixDeriver(deriver(), n);
            //HACK TODO
            n.onDur((N)->{
                for (PLink<Focus> f : N.focus)
                    d.next(f.id);
            });
        }


        if (iterTune) {
            final double subCycles = 1; //TODO adjust dynamically to min # of focuses per threads
            BagDeriver.tune(()->n.loop.periodNS()/subCycles,  (_iter)->{
                int iter = Math.round(_iter);

                //HACK
                nar.parts(BagDeriver.class).forEach(deriver ->
                    deriver.iter = iter
                );
            });
        }

        n.main().pri(0, 0);
        n.focus.remove(n.main()); //HACK disables default context
        
        return n;
    }

    /** TODO parameters */
    private Memory memory() {

        return memoryHijackAtomCompoundSplit(1, 96);

        //return memoryHijackTier();

        //return memoryCaffeine();

        //return memoryCaffeineSoft();


//        int largeThresh = Math.max(1,Util.sqrtInt(volMax));
        //				ramGB >= 0.75 ?
        //new HijackMemory((int) Math.round(ramGB * 64 * 1024), 3)

//					CaffeineMemory.soft()

    }

    
    private CaffeineMemory memoryCaffeine() {
        int concepts = (int) Math.round(ramGB * 10 * 1024);
        //return new CaffeineMemory(concepts);
        int averageVolume = volMax/2;
        return new CaffeineMemory(CaffeineMemory.VolumeWeigher, concepts * averageVolume);
    }

    /** dangerous, can lose hardwired concepts briefly during temporary eviction */
    private CaffeineMemory memoryCaffeineSoft() {
        return CaffeineMemory.soft();
    }

    /** TODO support changing volume */
    private TierMemory memoryHijackTier() {
        int reprobes = 7;

        int chunkSize = 3;
        int chunks = (int)Math.ceil(((float)(volMax-1))/chunkSize);
        Memory[] m = new Memory[chunks+1];
        m[0] = new HijackMemory((int) Math.round(ramGB * 2 * 1024), Math.max(3,reprobes/2));
        for (int i = 0; i < chunks; i++) {
            //TODO optional complexity curve
            double c = (conceptsMax <= 0 ? ramGB * 32 * 1024 : conceptsMax)/((double)chunks);
            m[i+1] = new HijackMemory((int) Math.round(c), reprobes);
        }
        return new TierMemory(
            k -> {
                int kv = k.volume();
                return kv == 1 ? 0 : (kv - 1) / chunkSize;
            }, m
        );
    }


    private TierMemory memoryHijackAtomCompoundSplit(int atomCap, int compoundCap) {
        return new TierMemory(
            k -> k instanceof Atomic ? 0 : 1,
            new HijackMemory((int) Math.round(ramGB * atomCap * 1024), 5),
            new HijackMemory((int) Math.round(conceptsMax <= 0 ? ramGB * compoundCap * 1024 : conceptsMax),
                    REPROBES)
        );
    }

    private TierMemory memoryHybridTier(int thresh) {
        return new TierMemory(
                k -> k.volume() < thresh ? 0 : 1,
                new HijackMemory((int) Math.round(conceptsMax <= 0 ? ramGB * 32 * 1024 : conceptsMax), 4),
                CaffeineMemory.soft()
        );
    }

//	public void addClock(NAR n) {
//
//		Atom FRAME = Atomic.atom("frame");
//
//		n.parts(Game.class).forEach(g -> g.onFrame(() -> {
//			long now = n.time();
//			int X = g.iterationCount();
//			int radix = 16;
//			Term x = $.pRecurse(false, $.radixArray(X, radix, Integer.MAX_VALUE));
//
//			Term f = $.funcImg(FRAME, g.id, x);
//			Task t = new SerialTask(f, BELIEF, $.t(1f, n.confDefault(BELIEF)),
//				now, Math.round(now + g.durLoop()),
//				new long[]{n.time.nextStamp()});
//			t.pri(n.priDefault(BELIEF) * g.what().pri());
//
//			g.what().accept(t);
//		}));
//	}

//	public void addFuelInjection(NAR n) {
//		n.parts(What.class).filter(w -> w.inBuffer instanceof PriBuffer.BagTaskBuffer).map(w -> (PriBuffer.BagTaskBuffer) w.inBuffer).forEach(b -> {
//			MiniPID pid = new MiniPID(0.007f, 0.005, 0.0025, 0);
//			pid.outLimit(0, 1);
//			pid.setOutMomentum(0.1);
//			float ideal = 0.5f;
//			n.onDur(() -> b.valve.set(pid.out(ideal - b.load(), 0)));
//		});
//
//	}

    public Reactions deriver() {


        Derivers d = new Derivers();

        if (nal) {
            //d = Derivers.nal(nalMin, nalMax);
            if (nalStructural)
                d.structural();
            if (nalSim)
                d.sim();
            if (nalProcedural)
                d.procedural();
            if (nalSets)
                d.sets();

            d.core(true, false);
        }

        ActionTiming a = new ActionTiming();

        if (stmLinker) {
            d.add(new STMLinker(1, true, true, false, false)/*.taskVol(3, 9)*/
//                .isNotAny(PremiseTask, IMPL).isNotAny(PremiseBelief, IMPL)
            );
//            d.add(new STMLinker(1, false, false, true, true));
        }

        if (conjClusterBelief)
            d.add(compoundClustering(BELIEF));

        if (conjClusterGoal)
            d.add(compoundClustering(GOAL));

//        if (taskResolveAction)
//            d.add(new TaskResolve(a, TaskResolver.AnyTaskResolver));
//
//
//        if (beliefResolveAction)
//            d.add(new BeliefResolve(true, true, true, true, a,
//                    TaskResolver.AnyTaskResolver)); //extra belief resolver for present focus

        if (motivation)
            d.files("motivation.nal");

        if (xor)
            d.files("xor.nal");

        if (goalSpread) {
            PuncBag punc = new PuncBag(0, 0, 1/16f, 1/32f);
            final LinkFlow l = new LinkFlow(
                    //LinkFlows.fromEqualsFrom,
                    LinkFlows.fromEqualsFromOrTo,
                    x -> {
                        float[] f = x.priGet(punc);
                        //transition matrix
//                    final float f3 = f[3];
//                    f[2] += f3 * 1/3f; //Q -> g
//                    f[1] += f3 * 1/3f; //Q -> q
//                    f[3] *= 1/3f;
                        return f;
                    }
                    ,
                    //PriMerge.max
                    PriMerge.plus
            );
            l.spread.set(
                //1
                2
            );
            d.add(l);

            //d.add(new LinkFlow(0f, 0, 0.1f, 0f, LinkFlows.edgeToEdge).neq(TheTask,TheBelief));

            //TODO impl->impl link creation from results of adjacents

            //d.add(new xxTermLinking.PremiseTermLinking(new CachedAdjacenctTerms(EqualTangent.the, false)));

        }



        if (ifThenElse)
            d.files("if.nal");


        if (goalInduction)
            d.files("induction.goal.nal");

        if (inperience) {
            int tl =
                0;
                //1;

            d.addAll(
                //new Inperience.BeliefInperience(BELIEF, tl).timelessOnly().hasNot(PremiseTask, IMPL,CONJ),
                new Inperience.BeliefInperience(GOAL, tl)
                    //.timelessOnly().hasNot(PremiseTask, IMPL,CONJ)
                ,new Inperience.QuestionInperience(QUESTION)
                    //.timelessOnly().hasNot(PremiseTask, IMPL,CONJ)
                ,new Inperience.QuestionInperience(QUEST)
                    //.timelessOnly().hasNot(PremiseTask, IMPL,CONJ)
            );
        }

        if (globalAdjacency)
            d.add(new TermLinking(new CachedAdjacenctTerms(EqualTangent.the, true)));

        if (explode)
            d.addAll(new ExplodeAction());

//        if (eternalizeImpl)
//            d.addAll(new EternalizeAction()
//                    .isAny(TheTask, Op.or(IMPL))
//            );

        if (deltaIntro) {
            //Δ
            d.files("delta.nal");
            //d.files("delta.more.nal");

            if (deltaGoal)
                d.files("delta.goal.nal");
        }



        if (implBeliefify) {
            d.addAll(
               new ImplTaskifyAction(true, true),
               new ImplTaskifyAction(false, true));
        }
        if (implGoalify) {
            d.addAll(
               new ImplTaskifyAction(true, false),
               new ImplTaskifyAction(false, false));
        }

        if (arith)
            d.add(new Arithmeticize.ArithmeticIntroduction());

        if (factorize)
            d.add(new Factorize.FactorIntroduction());


//        if (answerLogging) {
//            //.log(true).apply(false)
//
//            //d.add(new AnswerQuestionsFromBeliefTable(a, true, true)); //extra belief resolver for present focus
//            d.add(new AnswerQuestionsFromConcepts.AnswerQuestionsFromTaskLinks(new PresentFocus())
//                    //.log(true).apply(true)
//            );
//
//        }

        if (answerQuestionsFromTaskLinks) {
            d.add(new AnswerQuestionsFromConcepts.AnswerQuestionsFromTaskLinks(new PresentTiming()).taskVol(2, 16));
            //d.add(new AnswerQuestionsFromBeliefTable(a, true, false)) //extra belief resolver for present focus
            //d.add(new TermLinking.PremiseTermLinking(new FirstOrderIndexer())) //<- slow
        }

        if (abbreviation) {
            d.addAll(
                    new Abbreviate.AbbreviateRecursive(9, 16),
                    new Abbreviate.UnabbreviateRecursive());
            //TODO re-abbreviate: abbreviate unabbreviated instances terms recursively which also appear in the term abbreviated
//                //.add(new Abbreviation.AbbreviateRoot(4, 9))
//                //.add(new Abbreviation.UnabbreviateRoot())
        }
        return d;
    }

    private static CompoundClustering compoundClustering(byte punc) {
        CompoundClustering c = new CompoundClustering(punc,
                170, 512
                //170/2, 256
                //40, 128
                //340, 1024
        );
        c.outputRatio.set(
            //1
            1.5f
            //2
        );
        //c.condMax.set(3);
        return c;
    }

    public final <C> Stream<C> the(Class<C> c) {
        return nar.parts(c);
    }

    public void delete() {
        nar.runLater(()-> {
            stop();
            nar.delete();
            nar = null;
        });
    }

    public double rewardMean() {
        return nar.parts(Game.class).filter(g -> !(g instanceof MetaGame))
                .mapToDouble(Game::rewardMean).average().getAsDouble();
    }


}