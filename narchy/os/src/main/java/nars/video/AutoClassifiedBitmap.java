package nars.video;

import jcog.Fuzzy;
import jcog.Research;
import jcog.Util;
import jcog.data.list.Lst;
import jcog.deep.AbstractAutoencoder;
import jcog.deep.Autoencoder;
import jcog.func.IntIntToFloatFunction;
import jcog.io.BinTxt;
import jcog.math.GilbertCurve;
import jcog.random.XoRoShiRo128PlusRandom;
import jcog.signal.FloatRange;
import jcog.signal.wave2d.Bitmap2D;
import nars.$;
import nars.Term;
import nars.game.Controlled;
import nars.game.Game;
import nars.game.sensor.SubSignalComponent;
import nars.game.sensor.VectorSensor;
import nars.gui.sensor.VectorSensorChart;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spacegraph.space2d.Surface;
import spacegraph.space2d.container.grid.Gridding;
import spacegraph.space2d.meta.ObjectSurface;
import spacegraph.space2d.meta.Surfaces;
import spacegraph.space2d.widget.meter.BitmapMatrixView;

import java.util.Iterator;
import java.util.Map;

import static jcog.signal.wave2d.ColorMode.Hue;
import static spacegraph.space2d.widget.meter.BitmapMatrixView.colorBipolar;
import static spacegraph.space2d.widget.meter.BitmapMatrixView.colorHue;

/**
 * similar to a convolutional autoencoder
 * TODO pixelConf should be used in Signal truther
 * TODO encode multiple source feature channels besides just brightness, ex: hue
 */
public class AutoClassifiedBitmap extends VectorSensor implements Controlled<Game> {


    private static final Logger logger = LoggerFactory.getLogger(AutoClassifiedBitmap.class);



    public final AbstractAutoencoder ae;
    public final FloatRange alpha = new FloatRange(0.01f, 0, 1);
    public final float[][] pixRecon;
//    public final FloatRange confResolution = new FloatRange(0, 0, 1);
    private final Lst<SubSignalComponent> signals;
//    @Deprecated private final MetaBits metabits;

    /** per-pixel feature inputs */
    private final IntIntToFloatFunction[] pixIn;

    /**
     * interpret as the frequency component of each encoded cell
     */
    private final float[][][] encoded;
    private final float[] ins;

    /** superpixel's dimensions in pixels */
    private final int sw, sh;

    /** output dimensions in superpixels */
    private final int nw, nh;

    /** input dimensions in pixels */
    private final int pw, ph;

    private final Game game;
    private final Term[] feature;
    private final Term idPattern;

    private Bitmap2D src;
    boolean reconstruct = true;


    /** pre-dithers the autoencoder's encodings to the
     *  specified (frequency) resolution prior to backprop.
     *  as if it were a clue to the autoencoder that the extra
     *  precision will be discarded so it should settle on a
     *  discretized result instead.
     */
    private final boolean preDither = true;
    public int learnRandomly;

    public AutoClassifiedBitmap(Term root, float[][] pixIn, int sw, int sh, int states, Game game) {
        this(root,
                new IntIntToFloatFunction[] { (x, y) -> pixIn[x][y] },
                pixIn.length, pixIn[0].length,
                sw, sh, states, game);
    }


    public AutoClassifiedBitmap(Term root, Bitmap2D src, int superPixelsWide, int superPixelsHigh, int states, Game game) {
        this(root, src, superPixelsWide, superPixelsHigh, states, game, src::value);
    }

    public AutoClassifiedBitmap(Term root, Bitmap2D src, int superPixelsWide, int superPixelsHigh, int states, Game game, IntIntToFloatFunction... pixIn) {
        this(root, pixIn,
                src.width(), src.height(),
                src.width()/superPixelsWide, src.height()/superPixelsHigh,
                states, game);

        this.src = src;
    }

    @Override
    public void controlStart(Game g) {
        if (src instanceof Controlled)
            ((Controlled)src).controlStart(g); //HACK proxy to the source bitmap
    }

    /**
     * @param pw (& ph) dimensions of input bitmap
     * @param sw (& sh) dimensions of each superpixel
     *
     * metabits must consistently return an array of the same size, since now the size of this autoencoder is locked to its dimension
     */
    private AutoClassifiedBitmap(@Nullable Term id, IntIntToFloatFunction[] pixIn, int pw, int ph, int sw, int sh, int features, Game game) {
        super(id);

        if (pw % sw != 0)
            throw new UnsupportedOperationException("input width must be divisible by superpixel width");
        if (ph % sh != 0)
            throw new UnsupportedOperationException("input height must be divisible by superpixel height");

        this.idPattern = id; //since it may differ from super(id)'s

//        this.metabits = metabits;
        this.game = game;

        assert(pixIn.length > 0);
        this.pixIn = pixIn;

        this.sw = sw;
        this.sh = sh;
        this.pw = pw;
        this.ph = ph;
        this.nw = pw / sw;
        this.nh = ph / sh;

        int ins = sw * sh * pixIn.length;// + metabits.get(0, 0).length;
        this.ins = new float[ins];

        this.pixRecon = new float[ph][pw];

        this.encoded = new float[nh][nw][features];
        //this.pixConf = new float[nw][nh];


        this.feature = new Term[features];
        for (int i = 0; i < features; i++)
            feature[i] = $.quote(BinTxt.uuid64()); //HACK TODO check for repeat though extremely rare

        this.signals = new Lst<>(nw * nh);

        ae = new MyAutoencoder(ins, features, sw, sh, pw, ph);
        //ae.normalize.set(true);

        game.addSensor(this);

    }

    @Override
    public void start(Game game) {
        super.start(game);


        int features = feature.length;
        GilbertCurve.gilbertCurve(nw, nh, (x, y)->{
            for (int f = 0; f < features; f++) {
                int ff = f;
                signals.add(component(term(idPattern, x, y, f), () -> encoded[y][x][ff], game.nar));
            }
        });
        logger.info("{} pixels {}x{} ({}) => encoded {}x{} x {} features ({})", this, pw, ph, (pw * ph), nw, nh, features, signals.size());

    }

    public Surface newChart() {

        BitmapMatrixView.ViewFunction2D reconstructColorMode;
        if (src.mode() == Hue) {
            reconstructColorMode = colorHue(pixRecon);
        } else
            reconstructColorMode = colorBipolar(pixRecon);


        return new Gridding(
                ae instanceof Autoencoder ?
                    new BitmapMatrixView(((Autoencoder)ae).W) : Surfaces.TODO.get(),

                new ObjectSurface(this),

                new BitmapMatrixView(pixRecon, reconstructColorMode),

                new VectorSensorChart(this, game).withControls()) {
            {
                game.onFrame(() -> forEach(x -> {
                    if (x instanceof BitmapMatrixView)
                        ((BitmapMatrixView) x).updateIfShowing();
                }));
            }
        }
                ;
    }

    @Override
    public int size() {
        return signals.size();
    }

    public AutoClassifiedBitmap alpha(float alpha) {
        this.alpha.set(alpha);
        return this;
    }

    @Override
    public final void accept(Game g) {
        this.updateAutoclassifier();
        super.accept(g);
    }

    @Override
    public Iterator<SubSignalComponent> iterator() {
        return signals.iterator();
    }

    /**
     * @param root
     * @param x    x coordinate
     * @param y    y coordinate
     * @param feature    feature
     */
    protected Term term(Term root, int x, int y, int feature) {
        Term f = this.feature[feature];
        if (root.varDep() == 3) {
            return root.replace(Map.of(
                    $.varDep(1), $.the(x),
                    $.varDep(2), $.the(y),
                    $.varDep(3), f
            ));
        } else {
            return $.inh($.p(root, f),$.p((short) x, (short) y));
            //return $.inh(root,$.p($.p((short) x, (short) y),f));
            //return $.inh(root, $.p($.p((short) x, (short) y), ff));
            //return $.p(ff, $.p((short) x, (short) y), root);
                    //: $.inh($.p((short) x, (short) y), ff);
        }

        //return root!=null ? $.inh($.p(root, $.p(x, y)),feature[f]) : $.inh($.p(x, y), feature[f]);
        //return $.inh(component, feature[f]);
        //return $.inh($.p($.p($.the(x), $.the(y)), root), feature[f]);
        //return $.inh($.p(feature[f], $.p($.the(x), $.the(y))), root);
        //return $.funcImageLast(root, $.p($.the(x), $.the(y)), feature[f]);
    }

    private void updateAutoclassifier() {
        if (src != null)
            src.updateBitmap();

        learn();

    }

    /** current superpixel being trained */
    int superPixelCurrent = -1;

    protected void learn() {
        float pri = (float) (this.alpha.floatValue() /
                //Fuzzy.meanGeo(nw,nh),
                Fuzzy.mean((float)nw,nh));
                //noise = this.noise.floatValue();

        if (learnRandomly>0) {
            learnRandomly(learnRandomly, pri);
        }

        learnEachTile(pri);


    }

    private void learnRandomly(int iterations, float alpha) {
        superPixelCurrent = -1;
        //assert(metabits.get(0,0).length==0);

        int tiles = nw * nh;
        var rng = ae.rng;
        if (rng == null)
            rng = game.random();

        for (int i = 0; i < iterations; i++) {
            int x1 = rng.nextInt(src.width() - sw);
            int y1 = rng.nextInt(src.height() - sh);
            int x2 = x1 + sw, y2 = y1 + sh;
            loadPixels(ins, x1, y1, x2, y2);
        }
    }

    private void learnEachTile(float alpha) {
        for (float[] ii : loadIterator)
            ae.put(ii, alpha);
    }


    final Iterable<float[]> loadIterator = (new LoadIterator())::reset;

    private final class LoadIterator implements Iterator<float[]> {
        int zMax;

        public LoadIterator reset() {
            superPixelCurrent = -1;
            zMax = nw * nh;
            return this;
        }

        @Override
        public boolean hasNext() {
            return superPixelCurrent < zMax-1;
        }

        @Override
        public float[] next() {
            int z = ++superPixelCurrent;
            return loadSuperPixel(z % nw, z / nw); //???
        }
    }

    private float[] loadSuperPixel(int x, int y) {
        int sw = this.sw, sh = this.sh;
        int is = x * sw;
        int ie = Math.min(pw, is + sw);
        int js = y * sh;
        int je = Math.min(ph, sh + js);

        return loadPixels(ins, is, js, ie, je);
    }

    private float[] loadPixels(float[] input, int xs, int ys, int xe, int ye) {
        assert(ye > ys && xe > xs);

        int p = 0;
        for (int j = ys; j < ye; j++)
            for (int i = xs; i < xe; i++) {
                for (var f : pixIn)
                    input[p++] = f.value(i, j);
        }

//        //TODO do this once for all inputs
//        float[] metabits = this.metabits.get(x, y);
//        for (float m : metabits) input[p++] = m; //TODO arraycopy

        return input;
    }

    @FunctionalInterface
    public interface MetaBits {
        float[] get(int subX, int subY);
    }


    private class MyAutoencoder extends /*StackedAutoencoder*/ Autoencoder {
        private final int sw;
        private final int sh;
        private final int pw;
        private final int ph;

        MyAutoencoder(int ins, int features, int sw, int sh, int pw, int ph) {
            super(ins,
                    //new int[] { features, features }, features, new XoRoShiRo128PlusRandom());
                    features, new XoRoShiRo128PlusRandom());
            this.sw = sw;
            this.sh = sh;
            this.pw = pw;
            this.ph = ph;
        }

        @Override
        protected void latent(float[] x, float[] y, float[] z) {
            int p = superPixelCurrent;
            if (p >= 0) {
                int xx = p % nw, yy = p / nw;
                storeEncoding(xx, yy, y);

                if (reconstruct)
                    AutoClassifiedBitmap.this.reconstructEncoding(xx, yy, z);
            }
        }


        @Override
        @Research
        public float encodePost(float x) {
            //dither autoencoder encoding to match signal range and resolution
            x = super.encodePost(x);
            if (preDither)
                x = Util.unitizeSafe(Util.round(x, resolution()));
            return x;
        }
    }

    /**
     * save encoding for current learning example
     * @param x superpixel x coord
     * @param y superpixel y coord
     * @param f autoencoder encoded output feature vector
     */
    private void storeEncoding(int x, int y, float[] f) {
        float[] features = encoded[y][x];
        System.arraycopy(f, 0, features, 0, features.length);
    }

    private void reconstructEncoding(int x, int y, float[] z) {
        int is = x * sw;
        int js = y * sh;
        int p = 0;
        int ie = Math.min(pw, is + sw);
        int ce = Math.min(ph, js + sh);
//                for (int r = rs; r < re; r++) {
//                    float[] col = pixRecon[r];
//                    for (int c = cs; c < ce; c++)
//                        col[c] = z[p++];
//                }
        for (int j = js; j < ce; j++) {
            float[] row = pixRecon[j];
            for (int i = is; i < ie; i++)
                row[i] = z[p++];
        }

    }

}